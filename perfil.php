<?php
    require_once("php/cabecera.php");
    require_once("php/sesion.php");
    require_once("php/functions.php");
    mostrarCabecera("perfil");
    $id = $_SESSION["socio"];
    $consulta = consulta("select * from socios where id=$id");
    $cuotas = consulta("select * from cuotas where idSocio=$id order by id desc limit 1");
    $dato=$consulta[0];
    $porcentaje = porcentajeCuota($id);
    $foto = "panel/img/socios/foto-personal/".$dato["foto"];
    if($dato["foto"]==""){
      $foto = "img/perfil.jpg";
    }
?>
  <body>


    <div class="container animated zoomIn zona-perfil">


      <div class="row" style="margin-top:20px;">
        <div class="col-sm-3">
           <img style="max-height: 240px;" src="<?php echo $foto; ?>" class="img-thumbnail">
        </div>
        <div class="col-sm-9">
          <h1 class="tituloCategoria"><?php echo $dato["nombre"]." ".$dato["apellidos"];?></h1>

            <div class="row">
              <div class="col-sm-4">
              <div class="alert alert-light" role="alert">
                <span class="font-weight-bold" id="tPerfil1">Fecha de Alta</span></br>
                <?php echo fecha($dato["alta"]);?>
              </div>
              </div>
              <div class="col-sm-4">
              <div class="alert alert-light" role="alert">
                <span class="font-weight-bold" id="tPerfil2">Último pago</span></br>
                <?php echo fecha($cuotas[0]["fechapago"]); ?>
              </div>
              </div>
              <div class="col-sm-4">
              <div class="alert alert-light" role="alert">
                <span class="font-weight-bold" id="tPerfil3">Renovación</span></br>
                <?php echo fecha($cuotas[0]["fechafin"]); ?>
                </div>
              </div>


            <!--ROW-->

          </div>
          <!--ALERT-->
          <h5 id="#tPerfil4">Fin de tu suscripción</h5>
          <div class="progress" style="height:30px;">

                  <div class="progress-bar bg-verde progress-bar-striped progress-bar-animated" role="progressbar" style="width: <?php echo $porcentaje; ?>%;font-size:15px;color:black;" aria-valuenow="<?php echo $porcentaje; ?>" aria-valuemin="0" aria-valuemax="100"><?php echo $porcentaje; ?>%</div>
                </div>
        </div>

      </div>



      <!--<div class="row" style="margin-top:30px;">
        <div class="col-sm-6">
          <h5>Últimas genéticas</h5>
          <?php
            ultimasGeneticas($id);
          ?>
        </div>
        <div class="col-sm-6">
           <h5>Últimos productos</h5>
           <?php
             ultimosProductos($id);
           ?>
        </div>
      </div>-->
      <h5 class="mt-2 text-center"><i class="fas fa-history"></i> <span id="tPerfil5">ÚLTIMA ACTIVIDAD</span></h5>
      <ul class="nav nav-pills nav-justified mt-1" id="pills-tab" role="tablist">
        
        <?php
            $consulta = consulta("select * from retiradas where idSocio = $id order by id desc limit 5");
            
            if(count($consulta)!=0){
              foreach($consulta as $indice=>$valor){
                $fechaYHora = explode(" ", $valor["fecha"]);
                $fecha = $fechaYHora[0];
                if($indice == 0){
                          echo '<li class="nav-item">
                    <a class="nav-link active" id="btn-'.$indice.'" data-toggle="pill" href="#'.$indice.'" role="tab" aria-controls="pills-profile" aria-selected="true">'.fecha($fecha).'</a>
                  </li>';
                } else {
                          echo '<li class="nav-item">
                    <a class="nav-link" id="btn-'.$indice.'" data-toggle="pill" href="#'.$indice.'" role="tab" aria-controls="pills-profile" aria-selected="false">'.fecha($fecha).'</a>
                  </li>';
                }
               
              }
            }

        ?>
        

      </ul>
      <div class="tab-content" id="pills-tabContent">

       
        <?php 

        if(count($consulta)!=0){
              foreach($consulta as $indice=>$valor){
                $fechaYHora = explode(" ", $valor["fecha"]);
                $fecha = $fechaYHora[0];
                if($indice == 0){
                          echo '<div class="tab-pane fade show active" id="'.$indice.'" role="tabpanel" aria-labelledby="pills-home-tab">';

                          echo '<div class="row mt-2">
                                  <div class="col-sm-6">
                                    <h5>Genéticas</h5>';

                        ultimasGeneticas($valor["id"]);
                         echo '</div>
                                  <div class="col-sm-6">
                                     <h5>Productos</h5>';
                        ultimosProductos($valor["id"]);
                         echo '</div>
                                </div>';


                          echo '</div>';
                } else {
                          echo ' <div class="tab-pane fade" id="'.$indice.'" role="tabpanel" aria-labelledby="pills-profile-tab">';


                          echo '<div class="row mt-2">
                                  <div class="col-sm-6">
                                    <h5>Genéticas</h5>';

                        ultimasGeneticas($valor["id"]);
                         echo '</div>
                                  <div class="col-sm-6">
                                <h5>Productos</h5>';
                        ultimosProductos($valor["id"]);
                         echo '</div></div></div>';
                }
               
              }
            }

         ?>



        </div>
      </div>


</div>
</div>

<div class="botonVolver animated fadeInUp">
  <i class="fas fa-undo"></i></br><span class="text-atras">ATRÁS</span>
</div>
   <?php
    include "php/footer.php";
   ?>

   <script src="js/geneticas.js"></script>
    <script src="js/sesion.js"></script>
    <script src="js/traduccion.js"></script>

  </body>
</html>
