
<?php
    require_once("php/cabecera.php");

	session_start();
    mostrarCabecera("index");
?>
  <body>
    <div class="container-fluid animated fadeIn" id="zona-alta">

       <div id="zona-bienvenida" class="animated jackInTheBox">



         <h1 class="text-white text-center mt-5"><span id="tR1">Bienvenido</span>, <?php echo $_SESSION["nombre"];?></h1>
         <h4 class="text-white text-center mt-5 mb-5" id="tR2">Este es el nuevo dispensario digital.</br>Como es la primera vez que accedes, necesitamos que registres tu firma.</br>
         Lo harás una única vez y se realiza para registrar las aportaciones que retires, sin que tengas que volver a firmar cada vez.
     </br></h4><p class="text-white text-center mb-5" id="tR3"> Puedes estar tranquilo, tus datos están protegidos y son igual de confidenciales que antes. </br> Si necesitas más información, preguntanos y te resolvemos cualquier tipo de duda.
     	</p>
        <button class="btn btn-dark btn-block btn-lg mt-5" id="btn-firmar">Firmar <i class="fas fa-chevron-circle-right"></i></button>
       </div>





      <div id="zona-firma" class="text-center" style="display:none;">
    <h4 class="text-white mt-2" id="tR4">Firma para continuar</h4>
  <div class="wrapper">
    <canvas id="signature-pad" class="signature-pad" width=800 height=400></canvas>
  </div>

  <div class="row mt-3" style="width:800px;margin:auto;">
    <div class="col-2">
      <button class="btn btn-danger btn-lg" id="clear"><i class="fas fa-trash"></i> Borrar</button>
    </div>
    <div class="col-10">
      <button class="btn btn-dark btn-block btn-lg" id="btn-continuar">Continuar <i class="fas fa-chevron-circle-right"></i></button>
    </div>
  </div>



   <input class="d-none" id="idSocio" value="<?php echo $_SESSION["socio"];?>">

</div>



   <?php
    include "php/footer.php";
   ?>

      <script type="text/javascript" src="js/signature_pad.umd.js"></script>
 <script>
 	$("#btn-firmar").on("click", function(){
 		$("#zona-bienvenida").fadeOut("500", function(){
 			$("#zona-firma").fadeIn("500");
 			resizeCanvas();
 		});
 	});


 	var canvas = document.getElementById('signature-pad');

 	// Adjust canvas coordinate space taking into account pixel ratio,
 	// to make it look crisp on mobile devices.
 	// This also causes canvas to be cleared.
 	function resizeCanvas() {
 	    // When zoomed out to less than 100%, for some very strange reason,
 	    // some browsers report devicePixelRatio as less than 1
 	    // and only part of the canvas is cleared then.
 	    var ratio =  Math.max(window.devicePixelRatio || 1, 1);
 	    canvas.width = canvas.offsetWidth * ratio;
 	    canvas.height = canvas.offsetHeight * ratio;
 	    canvas.getContext("2d").scale(ratio, ratio);
 	}

 	window.onresize = resizeCanvas;
 	resizeCanvas();

 	var signaturePad = new SignaturePad(canvas, {
 	  backgroundColor: 'rgb(255, 255, 255)' // necessary for saving image as JPEG; can be removed is only saving as PNG or SVG
 	});



	$("#btn-continuar").on("click", function(){
		if (signaturePad.isEmpty()) {

		} else {
			var data = signaturePad.toDataURL('image/png');

			var id = $("#idSocio").val();
			$.post("panel/js/ajax/cargarFirmaSocio.php", {img:data, id:id}, function(result){
				if(result=="success"){
					$("#zona-firma").fadeOut("500", function(){
						location.href ="inicio.php";
					});
				} else {
					console.log(result);
				}
			})
		}


	});



 	document.getElementById('clear').addEventListener('click', function () {
 	  signaturePad.clear();
 	});
 </script>


  </body>
</html>
