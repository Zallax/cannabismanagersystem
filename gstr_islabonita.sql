-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:8889
-- Tiempo de generación: 30-08-2019 a las 11:53:19
-- Versión del servidor: 5.6.38
-- Versión de PHP: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `gstr_islabonita`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulos`
--

CREATE TABLE `articulos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `descripcion` text NOT NULL,
  `precio` float NOT NULL,
  `stock` int(3) NOT NULL,
  `categoria` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `articulos`
--

INSERT INTO `articulos` (`id`, `nombre`, `descripcion`, `precio`, `stock`, `categoria`) VALUES
(1, 'Coca-Cola', 'Descripción Cocacola', 1, 1, 1),
(8, 'Rio Tropical', 'Refresco sabor tropical', 1, 0, 1),
(10, 'Patatas Fritas', 'adasdasd', 1, 38, 24),
(11, 'Maxibon', 'Helado', 2, 8, 24),
(12, 'Librillo OCB', 'Librillo', 1, 75, 23),
(13, 'Calippo', 'De fresa', 2, 0, 24),
(16, 'Rollo de papel RAW', 'Papel de rollo', 2, 0, 23),
(17, 'Frigopie', 'Helado', 2, 0, 24),
(18, 'Mechero CLIPPER', 'Mechero', 1, 0, 23),
(19, 'Cruzcampo', 'Cerveza', 1, 0, 1),
(20, 'Monster', 'Bebida energetica', 1.5, 5, 1),
(21, 'Kas Naranja', 'Refresco de naranja', 1, -2, 1),
(22, 'Aquarade', 'Bebida isotonica', 1, 0, 1),
(26, 'Agua', 'Agua', 0.5, 3, 1),
(27, 'Librillo OCB Slim', 'Papel Largo', 1, 4, 23);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cajas`
--

CREATE TABLE `cajas` (
  `id` int(7) NOT NULL,
  `geneticas` text NOT NULL,
  `productos` text NOT NULL,
  `fecha` date NOT NULL,
  `horaAp` time NOT NULL,
  `hora` time NOT NULL,
  `total_geneticas` float NOT NULL,
  `total_productos` float NOT NULL,
  `total_cuotas` int(5) NOT NULL,
  `total` float NOT NULL,
  `idUsuario` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cajas`
--

INSERT INTO `cajas` (`id`, `geneticas`, `productos`, `fecha`, `horaAp`, `hora`, `total_geneticas`, `total_productos`, `total_cuotas`, `total`, `idUsuario`) VALUES
(30, '[[\"Amnesia Haze\",\"1.5\",6,\"v\"],[\"L de Marihuana\",\"1\",2,\"v\"],[\"Mango\",1,6,\"v\"],[\"OG Kush\",1,6,\"m\"],[\"Purple Haze\",2,6,\"v\"]]', '[[\"Coca-Cola\",\"1\",1]]', '2019-01-31', '00:00:00', '21:58:56', 35, 1, 0, 36, 5),
(31, '[[\"Purple Haze\",5,6,\"v\"],[\"Amnesia Haze\",2.5,6,\"v\"],[\"Gordo Master\",2,6,\"m\"],[\"OG Kush\",2,6,\"m\"],[\"Especial Extraccion\",\"0.5\",8,\"m\"],[\"Jamaican Dream\",5,6,\"v\"],[\"Lemon\",1,6,\"m\"],[\"Grape Fruit\",0.5,6,\"v\"],[\"Crystal Somango\",1,6,\"v\"],[\"Space\",2,6,\"v\"]]', '[[\"Librillo OCB\",1,1],[\"Cruzcampo\",1,1],[\"Coca-Cola\",1,1],[\"OCB Largo\",1,1]]', '2019-02-01', '00:00:00', '23:30:50', 130, 4, 0, 134, 5),
(32, '[[\"L de Marihuana\",2,2,\"v\"],[\"Amnesia Haze\",2,6,\"v\"],[\"Gordo Master\",2,6,\"m\"],[\"Rubio\",0.5,4,\"m\"],[\"OG Kush\",1,6,\"m\"],[\"Space\",\"1\",6,\"v\"],[\"Mazar\",\"1\",6,\"v\"],[\"Purple Haze\",4.5,6,\"v\"],[\"Jamaican Dream\",2,6,\"v\"],[\"Mango\",1,6,\"v\"]]', '[[\"Librillo OCB\",1,1],[\"Agua\",3,0.5],[\"Kikos\",1,0.4],[\"Golosinas Haribo\",2,1],[\"Coca-Cola\",1,1]]', '2019-02-02', '00:00:00', '23:06:47', 93, 5.9, 0, 98.9, 6),
(33, '[[\"L de Marihuana\",2,2,\"v\"],[\"Space\",2,6,\"v\"],[\"Amnesia Haze\",3.5,6,\"v\"],[\"Purple Haze\",2,6,\"v\"],[\"Rubio\",2.5,4,\"m\"],[\"Gordo Master\",1,6,\"m\"],[\"Jamaican Dream\",3.5,6,\"v\"],[\"Mango\",1.5,6,\"v\"],[\"OG Kush\",1,6,\"m\"]]', '[[\"Agua\",1,0.5],[\"Librillo OCB\",1,1]]', '2019-02-03', '00:00:00', '23:01:49', 101, 1.5, 0, 102.5, 6),
(34, '[[\"Amnesia Haze\",4,6,\"v\"],[\"Gordo Master\",2,6,\"m\"],[\"Jamaican Dream\",3.5,6,\"v\"],[\"Purple Haze\",3.5,6,\"v\"],[\"Space\",3.5,6,\"v\"],[\"L de Marihuana\",12,2,\"v\"],[\"Moby Dick\",2,6,\"v\"],[\"Rubio\",2,4,\"m\"],[\"OG Kush\",2.5,6,\"m\"],[\"Mango\",3,6,\"v\"]]', '[[\"Librillo OCB\",3,1],[\"Coca-Cola\",2,1],[\"Agua\",2,0.5],[\"Mechero CLIPPER\",\"1\",1]]', '2019-02-05', '00:00:00', '23:37:56', 176, 7, 0, 183, 6),
(35, '[[\"Purple Haze\",5.5,6,\"v\"],[\"Amnesia Haze\",2,6,\"v\"],[\"Space\",2,6,\"v\"],[\"Jamaican Dream\",4,6,\"v\"],[\"Gordo Master\",2,6,\"m\"],[\"Azura Haze\",1,6,\"v\"],[\"Mango\",1,6,\"v\"],[\"OG Kush\",1.5,6,\"m\"]]', '[[\"Agua\",1,0.5]]', '2019-02-06', '00:00:00', '23:25:15', 114, 0.5, 0, 114.5, 6),
(36, '[[\"OG Kush\",2,6,\"m\"],[\"Purple Haze\",3.5,6,\"v\"],[\"Mango\",0.5,6,\"v\"]]', '[[\"Agua\",1,0.5]]', '2019-02-07', '00:00:00', '23:45:42', 36, 0.5, 0, 36.5, 6),
(37, '[[\"Gordo Master\",2,6,\"m\"],[\"Amnesia Haze\",4,6,\"v\"],[\"Jamaican Dream\",3,6,\"v\"],[\"Mango\",7.5,6,\"v\"],[\"OG Kush\",1,6,\"m\"],[\"Purple Haze\",4.5,6,\"v\"],[\"Crystal Somango\",1,6,\"v\"],[\"Rubio\",2,4,\"m\"],[\"Especial Extraccion\",0.5,8,\"m\"],[\"Damnesia\",0.5,6,\"v\"],[\"L de Marihuana\",25,2,\"v\"]]', '[[\"Kas Naranja\",1,1],[\"Mechero CLIPPER\",1,1],[\"Coca-Cola\",2,1],[\"Cruzcampo\",2,1]]', '2019-02-08', '00:00:00', '22:58:34', 203, 6, 0, 209, 5),
(38, '[[\"Jamaican Dream\",\"2\",6,\"v\"],[\"Amnesia Haze\",3.5,6,\"v\"],[\"Mango\",2,6,\"v\"],[\"OG Kush\",1.5,6,\"m\"],[\"Gordo Master\",3,6,\"m\"]]', '[[\"Agua\",\"1\",0.5],[\"Mechero CLIPPER\",\"1\",1],[\"Librillo OCB\",1,1],[\"Golosinas Haribo\",1,1],[\"Coca-Cola\",\"1\",1]]', '2019-02-09', '00:00:00', '22:58:57', 72, 4.5, 0, 76.5, 6),
(39, '[[\"L de Marihuana\",2,2,\"v\"],[\"Caramel Ice\",0.5,6,\"v\"],[\"Mazar\",0.5,6,\"v\"],[\"Mango\",4,6,\"v\"],[\"Amnesia Haze\",4,6,\"v\"],[\"OG Kush\",1.5,6,\"m\"],[\"Rubio\",\"1\",4,\"m\"],[\"Purple Haze\",6.5,6,\"v\"],[\"Azura Haze\",1.5,6,\"v\"],[\"Crystal Somango\",1.5,6,\"v\"],[\"Gordo Master\",1,6,\"m\"],[\"Fresa\",\"0.5\",6,\"v\"],[\"Lemon\",\"1\",6,\"m\"]]', '[[\"Coca-Cola\",3,1],[\"Librillo OCB\",2,1],[\"Golosinas Haribo\",\"1\",1],[\"Kas Naranja\",2,1],[\"Cruzcampo\",\"1\",1]]', '2019-02-10', '00:00:00', '22:00:52', 143, 9, 0, 152, 6),
(40, '[[\"L de Marihuana\",6,2,\"v\"],[\"Mango\",7.5,6,\"v\"],[\"OG Kush\",2.5,6,\"m\"],[\"Amnesia Haze\",3.5,6,\"v\"],[\"Purple Haze\",1.5,6,\"v\"],[\"Black Dog\",1,6,\"v\"],[\"Azura Haze\",1.5,6,\"v\"],[\"Moby Dick\",2,6,\"v\"],[\"Rubio\",3.5,4,\"m\"],[\"Jamaican Dream\",6,6,\"v\"],[\"Crystal Somango\",1,6,\"v\"],[\"Gordo Master\",2,6,\"m\"]]', '[[\"Agua\",2,0.5],[\"Coca-Cola\",2,1],[\"Kas Naranja\",1,1],[\"Cruzcampo\",1,1]]', '2019-02-12', '00:00:00', '22:20:44', 197, 5, 0, 202, 5),
(41, '[[\"Jamaican Dream\",4,6,\"v\"],[\"Amnesia Haze\",4,6,\"v\"],[\"Mango\",4,6,\"v\"],[\"Space\",2,6,\"v\"],[\"OG Kush\",1,6,\"m\"],[\"Purple Haze\",4.5,6,\"v\"]]', '[[\"Agua\",2,0.5]]', '2019-02-13', '00:00:00', '22:01:14', 117, 1, 0, 118, 5),
(42, '[[\"L de Marihuana\",2,2,\"v\"],[\"Mango\",4,6,\"v\"],[\"Jamaican Dream\",2,6,\"v\"],[\"Amnesia Haze\",1.5,6,\"v\"],[\"Gordo Master\",3,6,\"m\"],[\"Azura Haze\",1,6,\"v\"],[\"Space\",2,6,\"v\"],[\"Lemon\",1,6,\"m\"]]', '[[\"Coca-Cola\",3,1],[\"Librillo OCB\",2,1],[\"Cruzcampo\",1,1],[\"Agua\",1,0.5]]', '2019-02-14', '00:00:00', '13:23:01', 91, 6.5, 0, 97.5, 6),
(43, '[[\"Mango\",3.5,6,\"v\"],[\"OG Kush\",1.5,6,\"m\"],[\"Purple Haze\",4.5,6,\"v\"],[\"Sour Diesel\",0.5,6,\"v\"],[\"Azura Haze\",0.5,6,\"v\"],[\"Amnesia Haze\",4,6,\"v\"],[\"L de Marihuana\",6,2,\"v\"],[\"Fresa\",1,6,\"v\"]]', '[[\"Coca-Cola\",2,1],[\"Cruzcampo\",1,1]]', '2019-02-15', '00:00:00', '23:21:33', 105, 3, 0, 108, 5),
(44, '[[\"L de Marihuana\",\"1\",2,\"v\"],[\"OG Kush\",3.5,6,\"m\"],[\"Purple Haze\",4.5,6,\"v\"],[\"Lemon\",2,6,\"m\"],[\"Mango\",2,6,\"v\"],[\"Amnesia Haze\",4.5,6,\"v\"],[\"Gordo Master\",3,6,\"m\"],[\"Jamaican Dream\",2.5,6,\"v\"]]', '[[\"Mechero CLIPPER\",\"1\",1],[\"Kikos\",\"2\",0.4],[\"Agua\",\"2\",0.5],[\"Kas Naranja\",1,1],[\"Librillo OCB\",\"1\",1],[\"Coca-Cola\",\"2\",1]]', '2019-02-16', '00:00:00', '22:57:25', 134, 6.8, 0, 140.8, 6),
(45, '[[\"Gordo Master\",4.5,6,\"m\"],[\"L de Marihuana\",8,2,\"v\"],[\"Moby Dick\",8,6,\"v\"],[\"Mango\",0.5,6,\"v\"],[\"Jamaican Dream\",2,6,\"v\"]]', '[[\"Coca-Cola\",3,1],[\"Librillo OCB\",2,1],[\"OCB Largo\",1,1]]', '2019-02-17', '00:00:00', '23:16:05', 106, 6, 0, 112, 5),
(46, '[[\"Moby Dick\",8,6,\"v\"],[\"Amnesia Haze\",5.5,6,\"v\"],[\"Lemon\",4,6,\"m\"],[\"Especial Extraccion\",1,8,\"m\"],[\"Mango\",3,6,\"v\"],[\"Purple Haze\",3.5,6,\"v\"],[\"Gordo Master\",0.5,6,\"m\"],[\"Rubio\",0.5,4,\"m\"],[\"Jamaican Dream\",1,6,\"v\"]]', '[[\"Cruzcampo\",1,1],[\"Librillo OCB\",1,1],[\"Agua\",3,0.5],[\"Kikos\",1,0.4],[\"Coca-Cola\",1,1]]', '2019-02-19', '00:00:00', '22:32:08', 163, 4.9, 0, 167.9, 5),
(47, '[[\"Purple Haze\",6,6,\"v\"],[\"Amnesia Haze\",4.5,6,\"v\"],[\"AFGHANHASH  GOLD SEAL\",2.5,7,\"m\"],[\"Mango\",2,6,\"v\"],[\"Moby Dick\",5.5,6,\"v\"]]', '[[\"Agua\",1,0.5]]', '2019-02-20', '00:00:00', '22:00:41', 125.5, 0.5, 0, 126, 5),
(48, '[[\"AFGHANHASH  GOLD SEAL\",3.5,7,\"m\"],[\"Amnesia Haze\",2,6,\"v\"],[\"Mango\",2,6,\"v\"],[\"Sour Diesel\",1.5,6,\"v\"],[\"Lemon\",1,6,\"m\"],[\"Purple Haze\",1.5,6,\"v\"]]', '[[\"Coca-Cola\",1,1]]', '2019-02-21', '00:00:00', '22:19:23', 72.5, 1, 0, 73.5, 5),
(49, '[[\"Mango\",1.5,6,\"v\"],[\"Critical CBD\",1,6,\"v\"],[\"Amnesia Haze\",3.5,6,\"v\"],[\"Moby Dick\",5,6,\"v\"],[\"Purple Haze\",5,6,\"v\"],[\"Lemon\",1,6,\"m\"],[\"AFGHANHASH  GOLD SEAL\",\"3\",7,\"m\"],[\"Jamaican Dream\",\"2\",6,\"v\"],[\"Mazar\",\"0.5\",6,\"v\"]]', '[[\"Librillo OCB\",\"6\",1],[\"Golosinas Haribo\",\"1\",1],[\"Coca-Cola\",\"1\",1]]', '2019-02-22', '00:00:00', '22:43:38', 138, 8, 0, 146, 6),
(50, '[[\"Purple Haze\",3.5,6,\"v\"],[\"Especial Extraccion\",\"3\",8,\"m\"],[\"Mango\",3,6,\"v\"],[\"AFGHANHASH  GOLD SEAL\",1,7,\"m\"],[\"Amnesia Haze\",7,6,\"v\"],[\"AMNESIA\",2,8,\"m\"],[\"Jamaican Dream\",5,6,\"v\"],[\"Lemon\",2,6,\"m\"],[\"Moby Dick\",1,6,\"v\"]]', '[[\"Coca-Cola\",\"1\",1],[\"OCB Largo\",\"1\",1],[\"Cruzcampo\",1,1],[\"Golosinas Haribo\",\"1\",1]]', '2019-02-23', '00:00:00', '22:59:16', 176, 4, 0, 180, 6),
(51, '[[\"Lemon\",3,6,\"m\"],[\"Mango\",2,6,\"v\"],[\"Fresa\",4.5,6,\"v\"],[\"Amnesia Haze\",4,6,\"v\"],[\"Azura Haze\",\"1\",6,\"v\"],[\"AMNESIA\",\"1\",8,\"m\"],[\"Purple Haze\",5.5,6,\"v\"]]', '[[\"Kikos\",\"2\",0.4],[\"Kas Naranja\",2,1],[\"Cruzcampo\",4,1],[\"OCB Largo\",1,1],[\"Coca-Cola\",\"2\",1],[\"Aquarade\",\"1\",1]]', '2019-02-24', '00:00:00', '21:58:41', 128, 10.8, 0, 138.8, 6),
(52, '[[\"Moby Dick\",8.5,6,\"v\"],[\"Amnesia Haze\",2,6,\"v\"],[\"Infusion de Cannabis\",2,2,\"v\"],[\"AFGHANHASH  GOLD SEAL\",4,7,\"m\"],[\"Jamaican Dream\",1,6,\"v\"],[\"Lemon\",0.5,6,\"m\"],[\"Rubio\",2,4,\"m\"],[\"Fresa\",0.5,6,\"v\"],[\"OG Kush\",1.5,6,\"m\"],[\"Lemon Kush\",1,6,\"v\"],[\"Gordo Master\",3,6,\"m\"],[\"Black Widow CBD\",6,6,\"v\"],[\"Sour Diesel\",1,6,\"v\"],[\"Grape Fruit\",1,6,\"v\"],[\"Mango\",\"1\",6,\"v\"],[\"AMNESIA\",4,8,\"m\"],[\"Space\",1.5,6,\"v\"]]', '[[\"Coca-Cola\",1,1],[\"Papel billete para liar\",4,2],[\"Grinder Hamburguesa\",1,5]]', '2019-02-26', '00:00:00', '19:49:18', 243, 14, 0, 257, 5),
(53, '[[\"OG Kush\",2.5,6,\"m\"],[\"Babylonia fruit\",4,6,\"v\"],[\"Amnesia Haze\",2,6,\"v\"],[\"Crystal Somango\",1,6,\"v\"],[\"Azura Haze\",3.5,6,\"v\"],[\"Jamaican Dream\",1,6,\"v\"],[\"Moby Dick\",6,6,\"v\"],[\"Gordo Master\",3,6,\"m\"],[\"Purple Haze\",4,6,\"v\"],[\"Caramel Ice\",1,6,\"v\"],[\"Sour Diesel\",\"1\",6,\"v\"],[\"Fresa\",1,6,\"v\"]]', '[[\"Cruzcampo\",1,1],[\"Coca-Cola\",\"1\",1],[\"Papel billete para liar\",\"1\",2]]', '2019-02-27', '00:00:00', '23:12:00', 180, 4, 0, 184, 5),
(54, '[[\"Amnesia Haze\",6.5,6,\"v\"],[\"Moby Dick\",13.5,6,\"v\"],[\"Purple Haze\",\"2.5\",6,\"v\"],[\"L de Marihuana\",\"1\",2,\"v\"],[\"OG Kush\",1.5,6,\"m\"]]', '[[\"Cruzcampo\",5,1],[\"Kas Naranja\",\"2\",1],[\"Librillo RAW\",\"1\",1],[\"Agua\",3,0.5],[\"Kikos\",\"1\",0.4],[\"Coca-Cola\",\"1\",1],[\"Mechero CLIPPER\",\"1\",1],[\"Grinder Hamburguesa\",\"1\",5],[\"Golosinas Haribo\",\"1\",1]]', '2019-02-28', '00:00:00', '22:12:54', 146, 17.9, 0, 163.9, 6),
(55, '[[\"L de Marihuana\",2,2,\"v\"],[\"Fresa\",4,6,\"v\"],[\"Jamaican Dream\",1,6,\"v\"],[\"Moby Dick\",8,6,\"v\"],[\"Gordo Master\",2,6,\"m\"],[\"OG Kush\",\"0.5\",6,\"m\"]]', '[[\"Golosinas Haribo\",1,1],[\"Coca-Cola\",1,1],[\"Agua\",\"2\",0.5],[\"Kikos\",\"1\",0.4]]', '2019-03-01', '00:00:00', '22:59:57', 97, 3.4, 0, 100.4, 6),
(56, '[[\"Jamaican Dream\",7,6,\"v\"],[\"Moby Dick\",3,6,\"v\"],[\"Sour Diesel\",1.5,6,\"v\"],[\"OG Kush\",0.5,6,\"m\"],[\"Lemon\",1,6,\"m\"]]', '[[\"Coca-Cola\",2,1],[\"Agua\",1,0.5]]', '2019-03-02', '00:00:00', '22:57:17', 78, 2.5, 0, 80.5, 6),
(57, '[[\"Gordo Master\",9,6,\"m\"],[\"Jamaican Dream\",3.5,6,\"v\"],[\"Sour Diesel\",2,6,\"v\"],[\"OG Kush\",4.5,6,\"m\"],[\"Moby Dick\",6.5,6,\"v\"],[\"Lemon\",1,6,\"m\"],[\"Fresa\",1,6,\"v\"],[\"L de Marihuana\",27,2,\"v\"]]', '[[\"Golosinas Haribo\",\"1\",1],[\"Coca-Cola\",2,1],[\"Librillo RAW\",\"1\",1]]', '2019-03-03', '00:00:00', '22:07:06', 219, 4, 0, 223, 6),
(58, '[[\"Jamaican Dream\",6.5,6,\"v\"],[\"OG Kush\",4.5,6,\"m\"],[\"Moby Dick\",5,6,\"v\"],[\"Gordo Master\",1,6,\"m\"],[\"Lemon\",1,6,\"m\"]]', '[[\"Papel billete para liar\",1,2],[\"Cruzcampo\",2,1],[\"Agua\",1,0.5]]', '2019-03-05', '00:00:00', '23:15:17', 108, 4.5, 0, 112.5, 5),
(59, '[[\"L de Marihuana\",1,2,\"v\"],[\"Black Dog\",0.5,6,\"v\"],[\"Space\",1,6,\"v\"],[\"Jamaican Dream\",1,6,\"v\"],[\"Moby Dick\",9,6,\"v\"],[\"Caramel Ice\",1,6,\"v\"],[\"Fresa\",1,6,\"v\"],[\"Rubio\",2,4,\"m\"],[\"AFGHANHASH  GOLD SEAL\",2,7,\"m\"],[\"OG Kush\",3,6,\"m\"],[\"AMNESIA\",2,8,\"m\"],[\"Gordo Master\",3,6,\"m\"]]', '[]', '2019-03-06', '00:00:00', '22:16:33', 157, 0, 0, 157, 5),
(60, '[[\"Moby Dick\",14.5,6,\"v\"],[\"Fresa\",1,6,\"v\"],[\"OG Kush\",\"1\",6,\"m\"],[\"Space\",1,6,\"v\"],[\"Gordo Master\",1.5,6,\"m\"]]', '[[\"Agua\",1,0.5]]', '2019-03-07', '00:00:00', '21:59:44', 114, 0.5, 0, 114.5, 5),
(61, '[[\"Gordo Master\",2,6,\"m\"],[\"Azura Haze\",1,6,\"v\"],[\"Jamaican Dream\",4.5,6,\"v\"],[\"L de Marihuana\",\"1\",2,\"v\"],[\"Lemon\",0.5,6,\"m\"],[\"Moby Dick\",7.5,6,\"v\"]]', '[[\"Kas Naranja\",1,1],[\"Cruzcampo\",1,1]]', '2019-03-08', '00:00:00', '22:57:32', 95, 2, 0, 97, 6),
(62, '[[\"OG Kush\",6,6,\"m\"],[\"Space\",2,6,\"v\"],[\"Jamaican Dream\",7,6,\"v\"],[\"Moby Dick\",9.5,6,\"v\"],[\"Cheese\",\"1\",6,\"v\"],[\"Gordo Master\",\"3\",6,\"m\"],[\"AMNESIA\",\"2\",8,\"m\"],[\"L de Marihuana\",\"1\",2,\"v\"],[\"Purple Haze\",0.5,6,\"v\"],[\"Sour Diesel\",2,6,\"v\"],[\"Lemon\",1,6,\"m\"]]', '[[\"OCB Largo\",\"1\",1],[\"Boquillas\",\"1\",1],[\"Coca-Cola\",2,1],[\"Golosinas Haribo\",1,1],[\"Cruzcampo\",3,1]]', '2019-03-09', '00:00:00', '22:58:53', 210, 8, 0, 218, 6),
(63, '[[\"L de Marihuana\",28,2,\"v\"],[\"Moby Dick\",15,6,\"v\"],[\"OG Kush\",4.5,6,\"m\"],[\"Babylonia fruit\",1,6,\"v\"],[\"Caramel Ice\",2.5,6,\"v\"],[\"Gordo Master\",3.5,6,\"m\"],[\"Jamaican Dream\",2.5,6,\"v\"],[\"Lemon\",2,6,\"m\"],[\"Rubio\",1,4,\"m\"],[\"Space\",2.5,6,\"v\"],[\"Azura Haze\",\"1\",6,\"v\"]]', '[[\"Agua\",4,0.5],[\"Kikos\",1,0.4],[\"OCB Largo\",\"1\",1],[\"Coca-Cola\",3,1],[\"Cruzcampo\",\"1\",1],[\"Librillo RAW\",\"1\",1],[\"Mechero CLIPPER\",\"1\",1]]', '2019-03-10', '00:00:00', '21:57:13', 267, 9.4, 0, 276.4, 6),
(64, '[[\"OG Kush\",3,6,\"m\"],[\"Especial Extraccion\",1,8,\"m\"],[\"Lemon\",1,6,\"m\"],[\"Moby Dick\",4,6,\"v\"],[\"Caramel Ice\",3,6,\"v\"],[\"Gordo Master\",2,6,\"m\"],[\"Black Dog\",1,6,\"v\"],[\"Mango\",14,6,\"v\"],[\"Space\",5,6,\"v\"],[\"Jamaican Dream\",2,6,\"v\"],[\"L de Marihuana\",1,2,\"v\"]]', '[[\"Coca-Cola\",1,1]]', '2019-03-12', '00:00:00', '22:23:40', 220, 1, 0, 221, 5),
(65, '[[\"Moby Dick\",\"5\",6,\"v\"],[\"Mango\",3.5,6,\"v\"],[\"Jamaican Dream\",\"0.5\",6,\"v\"],[\"OG Kush\",4.5,6,\"m\"],[\"AFGHANHASH  GOLD SEAL\",3,7,\"m\"],[\"Gordo Master\",3,6,\"m\"]]', '[[\"Cruzcampo\",\"1\",1]]', '2019-03-13', '00:00:00', '23:14:33', 120, 1, 0, 121, 5),
(66, '[[\"OG Kush\",3,6,\"m\"],[\"Mango\",1.5,6,\"v\"],[\"Black Dog\",1.5,6,\"v\"],[\"Gordo Master\",2,6,\"m\"],[\"Moby Dick\",3,6,\"v\"],[\"Space\",1,6,\"v\"],[\"Pineapple Kush\",1.5,6,\"v\"]]', '[[\"Cruzcampo\",1,1],[\"Papel billete para liar\",1,2]]', '2019-03-14', '00:00:00', '11:21:21', 81, 3, 0, 84, 6),
(67, '[[\"Space\",8,6,\"v\"],[\"Mango\",14.5,6,\"v\"],[\"Pineapple Kush\",16,6,\"v\"],[\"Sour Diesel\",8,6,\"v\"],[\"OG Kush\",4.5,6,\"m\"],[\"Moby Dick\",9,6,\"v\"],[\"L de Marihuana\",29,2,\"v\"],[\"AMNESIA\",1,8,\"m\"]]', '[[\"OCB Largo\",3,1],[\"Cruzcampo\",5,1],[\"Coca-Cola\",1,1],[\"Boquillas\",1,1],[\"Librillo RAW\",1,1]]', '2019-03-15', '00:00:00', '23:05:26', 426, 11, 0, 437, 6),
(68, '[[\"Pineapple Kush\",12,6,\"v\"],[\"Mango\",1,6,\"v\"],[\"OG Kush\",1.5,6,\"m\"],[\"Gordo Master\",2.5,6,\"m\"],[\"Moby Dick\",5,6,\"v\"],[\"L de Marihuana\",2,2,\"v\"]]', '[[\"Agua\",3,0.5],[\"Cruzcampo\",5,1]]', '2019-03-16', '00:00:00', '22:59:04', 136, 6.5, 0, 142.5, 6),
(69, '[[\"Pineapple Kush\",6,6,\"v\"],[\"Mango\",3,6,\"v\"],[\"Moby Dick\",2,6,\"v\"],[\"Critical CBD\",0.5,6,\"v\"],[\"OG Kush\",2,6,\"m\"],[\"Gordo Master\",2,6,\"m\"],[\"Azura Haze\",2,6,\"v\"]]', '[[\"Librillo RAW\",1,1]]', '2019-03-17', '00:00:00', '22:03:25', 105, 1, 0, 106, 6),
(70, '[[\"OG Kush\",3,6,\"m\"],[\"Gordo Master\",7,6,\"m\"],[\"Pineapple Kush\",4.5,6,\"v\"],[\"Moby Dick\",8.5,6,\"v\"],[\"Space\",1.5,6,\"v\"],[\"Black Dog\",1,6,\"v\"],[\"Mango\",1.5,6,\"v\"],[\"Lemon\",3,6,\"m\"],[\"Grape Fruit\",3,6,\"v\"]]', '[[\"Coca-Cola\",2,1],[\"Papel billete para liar\",1,2],[\"Mechero CLIPPER\",1,1],[\"Agua\",1,0.5]]', '2019-03-19', '00:00:00', '17:36:07', 198, 5.5, 0, 203.5, 5),
(71, '[[\"Pineapple Kush\",2,6,\"v\"],[\"Jamaican Dream\",2,6,\"v\"]]', '[[\"Boquillas\",\"1\",1],[\"Agua\",1,0.5]]', '2019-03-20', '00:00:00', '18:04:30', 24, 1.5, 0, 25.5, 5),
(72, '[[\"Jamaican Dream\",6,6,\"v\"],[\"Moby Dick\",9,6,\"v\"],[\"Caramel Ice\",2,6,\"v\"],[\"Gordo Master\",6,6,\"m\"],[\"Mango\",1.5,6,\"v\"],[\"Space\",1,6,\"v\"],[\"Lemon Kush\",2,6,\"v\"],[\"OG Kush\",4,6,\"m\"],[\"AFGHANHASH  GOLD SEAL\",3,7,\"m\"]]', '[]', '2019-03-21', '00:00:00', '19:23:17', 210, 0, 0, 210, 6),
(73, '[[\"Mango\",5,6,\"v\"],[\"Azura Haze\",\"3\",6,\"v\"],[\"Lemon Kush\",\"4\",6,\"v\"],[\"Fresa\",\"2\",6,\"v\"],[\"AFGHANHASH  GOLD SEAL\",\"2\",7,\"m\"],[\"Caramel Ice\",1,6,\"v\"],[\"Pineapple Kush\",1,6,\"v\"],[\"Gordo Master\",1.5,6,\"m\"],[\"Sour Diesel\",\"5\",6,\"v\"],[\"Space\",\"2\",6,\"v\"],[\"Moby Dick\",\"3\",6,\"v\"],[\"Jamaican Dream\",3,6,\"v\"],[\"OG Kush\",\"0.5\",6,\"m\"]]', '[[\"OCB Largo\",\"1\",1],[\"Bote de olor 5€\",\"2\",5],[\"Librillo RAW\",\"1\",1],[\"Cruzcampo\",3,1]]', '2019-03-22', '00:00:00', '22:59:38', 200, 15, 0, 215, 6),
(74, '[[\"Gordo Master\",4,6,\"m\"],[\"Pineapple Kush\",2,6,\"v\"],[\"Jamaican Dream\",5.5,6,\"v\"],[\"Lemon\",2.5,6,\"m\"],[\"OG Kush\",1.5,6,\"m\"]]', '[[\"Agua\",4,0.5],[\"OCB Largo\",1,1],[\"Coca-Cola\",1,1],[\"Cruzcampo\",\"2\",1],[\"Kikos\",1,0.4]]', '2019-03-23', '00:00:00', '23:02:55', 93, 6.4, 0, 99.4, 6),
(76, '[[\"Jamaican Dream\",4.5,6,\"v\"],[\"OG Kush\",2.5,6,\"m\"],[\"Gordo Master\",2,6,\"m\"],[\"Moby Dick\",6.5,6,\"v\"],[\"Space\",1,6,\"v\"],[\"Pineapple Kush\",3,6,\"v\"],[\"L de Marihuana\",2,2,\"v\"]]', '[[\"Librillo RAW\",1,1],[\"Cruzcampo\",2,1]]', '2019-03-24', '00:00:00', '09:32:42', 121, 3, 0, 124, 6),
(77, '[[\"Pineapple Kush\",4,6,\"v\"],[\"Moby Dick\",3.5,6,\"v\"],[\"OG Kush\",3.5,6,\"m\"],[\"Space\",1.5,6,\"v\"],[\"Jamaican Dream\",6,6,\"v\"],[\"L de Marihuana\",5,2,\"v\"]]', '[[\"Coca-Cola\",1,1],[\"Boquillas\",1,1],[\"Papel billete para liar\",1,2],[\"Kas Naranja\",1,1]]', '2019-03-26', '00:00:00', '20:08:27', 121, 5, 0, 126, 6),
(78, '[[\"Pineapple Kush\",4,6,\"v\"],[\"OG Kush\",6,6,\"m\"],[\"Jamaican Dream\",2.5,6,\"v\"],[\"Caramel Ice\",\"1\",6,\"v\"],[\"Lemon Kush\",\"1\",6,\"v\"],[\"AFGHANHASH  GOLD SEAL\",\"3\",7,\"m\"],[\"Gordo Master\",\"2\",6,\"m\"],[\"L de Marihuana\",2,2,\"v\"],[\"Moby Dick\",\"2\",6,\"v\"]]', '[[\"Coca-Cola\",1,1],[\"Mechero CLIPPER\",\"1\",1]]', '2019-03-27', '00:00:00', '22:01:29', 136, 2, 0, 138, 6),
(79, '[[\"Moby Dick\",4,6,\"v\"],[\"Ak 47\",8.5,6,\"v\"],[\"Jamaican Dream\",1,6,\"v\"],[\"Critical CBD\",1,6,\"v\"],[\"Gordo Master\",2,6,\"m\"],[\"Critical \",6,6,\"v\"]]', '[]', '2019-03-28', '00:00:00', '22:02:40', 135, 0, 0, 135, 5),
(80, '[[\"Critical \",9.5,6,\"v\"],[\"L de Marihuana\",\"1\",2,\"v\"],[\"Critical CBD\",\"2\",6,\"v\"],[\"Ak 47\",10.5,6,\"v\"],[\"Moby Dick\",4,6,\"v\"],[\"OG Kush\",1,6,\"m\"],[\"Lemon\",1,6,\"m\"]]', '[[\"Coca-Cola\",4,1],[\"Boquillas\",1,1]]', '2019-03-29', '00:00:00', '22:58:22', 170, 5, 0, 175, 6),
(81, '[[\"Critical \",8,6,\"v\"],[\"Ak 47\",10.5,6,\"v\"],[\"Gordo Master\",2,6,\"m\"],[\"Moby Dick\",6.5,6,\"v\"],[\"Lemon Kush\",\"2\",6,\"v\"],[\"Pineapple Kush\",\"2\",6,\"v\"],[\"Space\",4.5,6,\"v\"],[\"Jamaican Dream\",\"3\",6,\"v\"],[\"Black Dog\",\"1\",6,\"v\"],[\"Lemon\",2.5,6,\"m\"],[\"Critical CBD\",0.5,6,\"v\"],[\"OG Kush\",0.5,6,\"m\"]]', '[[\"Kas Naranja\",1,1],[\"Bote de olor 5€\",\"1\",5],[\"Librillo RAW\",\"1\",1],[\"Agua\",2,0.5],[\"Coca-Cola\",\"1\",1],[\"Golosinas Haribo\",2,1]]', '2019-03-30', '00:00:00', '22:59:43', 258, 11, 0, 269, 6),
(82, '[[\"Critical \",6.5,6,\"v\"],[\"Jamaican Dream\",3,6,\"v\"],[\"OG Kush\",2.5,6,\"m\"],[\"Ak 47\",7,6,\"v\"]]', '[[\"Agua\",\"1\",0.5],[\"Cruzcampo\",1,1],[\"Librillo RAW\",1,1],[\"Kas Naranja\",\"1\",1],[\"Coca-Cola\",1,1],[\"Golosinas Haribo\",1,1]]', '2019-03-31', '00:00:00', '22:07:51', 114, 5.5, 0, 119.5, 6),
(83, '[[\"Pineapple Kush\",2,6,\"v\"],[\"Ak 47\",4,6,\"v\"],[\"OG Kush\",3,6,\"m\"],[\"Lemon\",1,6,\"m\"],[\"Critical \",4,6,\"v\"],[\"Black Dog\",1,6,\"v\"],[\"Moby Dick\",4,6,\"v\"]]', '[[\"Coca-Cola\",1,1],[\"Librillo RAW\",1,1]]', '2019-04-02', '00:00:00', '22:03:30', 114, 2, 0, 116, 5),
(84, '[[\"Babylonia fruit\",1,6,\"v\"],[\"Ak 47\",7,6,\"v\"],[\"Moby Dick\",4,6,\"v\"],[\"Critical \",4,6,\"v\"]]', '[[\"Papel billete para liar\",1,2]]', '2019-04-03', '00:00:00', '22:00:25', 96, 2, 0, 98, 5),
(85, '[[\"Ak 47\",5,6,\"v\"],[\"Jamaican Dream\",2,6,\"v\"],[\"OG Kush\",1,6,\"m\"],[\"Lemon\",3,6,\"m\"],[\"Critical \",3.5,6,\"v\"]]', '[[\"Cruzcampo\",3,1],[\"Librillo RAW\",1,1]]', '2019-04-04', '00:00:00', '23:23:25', 87, 4, 0, 91, 5),
(86, '[[\"Ak 47\",8.5,6,\"v\"],[\"Moby Dick\",5.5,6,\"v\"],[\"OG Kush\",4.5,6,\"m\"],[\"L de Marihuana\",\"5\",2,\"v\"],[\"Critical \",0.5,6,\"v\"]]', '[[\"Coca-Cola\",1,1],[\"Agua\",1,0.5],[\"Cruzcampo\",3,1],[\"Kikos\",3,0.4]]', '2019-04-05', '00:00:00', '22:27:24', 124, 5.7, 0, 129.7, 5),
(87, '[[\"Ak 47\",10.5,6,\"v\"],[\"Jamaican Dream\",1.5,6,\"v\"],[\"Moby Dick\",4,6,\"v\"],[\"Critical \",6,6,\"v\"],[\"AFGHANHASH  GOLD SEAL\",4,7,\"m\"],[\"Gordo Master\",5,6,\"m\"],[\"Lemon\",4,6,\"m\"],[\"OG Kush\",2,6,\"m\"],[\"Pineapple Kush\",1,6,\"v\"],[\"Black Dog\",0.5,6,\"v\"],[\"Azura Haze\",1,6,\"v\"]]', '[[\"Golosinas Haribo\",4,1],[\"Librillo RAW\",1,1],[\"Agua\",2,0.5]]', '2019-04-06', '00:00:00', '21:58:04', 241, 6, 0, 247, 5),
(88, '[[\"Moby Dick\",\"2.5\",6,\"v\"],[\"OG Kush\",2,6,\"m\"],[\"Jamaican Dream\",1,6,\"v\"],[\"Lemon\",5,6,\"m\"]]', '[[\"Librillo RAW\",1,1],[\"Mechero CLIPPER\",1,1]]', '2019-04-07', '00:00:00', '21:14:13', 63, 2, 0, 65, 5),
(89, '[[\"Ak 47\",6.5,6,\"v\"],[\"Moby Dick\",10,6,\"v\"],[\"AMNESIA\",\"1\",8,\"m\"],[\"Space\",4,6,\"v\"],[\"OG Kush\",4,6,\"m\"],[\"L de Marihuana\",1,2,\"v\"],[\"Critical \",\"2\",6,\"v\"],[\"Black Dog\",\"1\",6,\"v\"],[\"Jamaican Dream\",1,6,\"v\"]]', '[[\"Coca-Cola\",\"1\",1],[\"Kas Naranja\",3,1],[\"Boquillas\",\"1\",1],[\"Mechero CLIPPER\",\"2\",1],[\"Agua\",1,0.5]]', '2019-04-09', '00:00:00', '22:09:32', 181, 7.5, 0, 188.5, 6),
(90, '[[\"Moby Dick\",13.5,6,\"v\"],[\"Critical \",3,6,\"v\"],[\"Lemon\",4,6,\"m\"],[\"Pineapple Kush\",3,6,\"v\"],[\"Amnesia Haze\",12,6,\"v\"],[\"Ak 47\",3.5,6,\"v\"],[\"Critical CBD\",3,6,\"v\"],[\"L de Marihuana\",1,2,\"v\"],[\"OG Kush\",1,6,\"m\"],[\"Gordo Master\",1,6,\"m\"],[\"Jamaican Dream\",1,6,\"v\"]]', '[[\"Papel billete para liar\",1,2],[\"Agua\",2,0.5],[\"Cruzcampo\",3,1],[\"Kas Naranja\",\"1\",1]]', '2019-04-10', '00:00:00', '22:12:59', 272, 7, 0, 279, 5),
(91, '[[\"Lemon\",5,6,\"m\"],[\"Amnesia Haze\",3,6,\"v\"],[\"L de Marihuana\",2,2,\"v\"],[\"Moby Dick\",2.5,6,\"v\"],[\"OG Kush\",4,6,\"m\"],[\"Gordo Master\",3,6,\"m\"],[\"AFGHANHASH  GOLD SEAL\",2,7,\"m\"]]', '[[\"Kas Naranja\",2,1],[\"Aquarade\",\"2\",1],[\"Golosinas Haribo\",\"1\",1]]', '2019-04-11', '00:00:00', '22:01:55', 123, 5, 0, 128, 6),
(92, '[[\"AMNESIA\",1,8,\"m\"],[\"OG Kush\",2,6,\"m\"],[\"Pineapple Kush\",7,6,\"v\"],[\"Ak 47\",4,6,\"v\"],[\"Lemon\",3,6,\"m\"],[\"L de Marihuana\",\"12\",2,\"v\"]]', '[[\"Coca-Cola\",1,1]]', '2019-04-12', '00:00:00', '22:58:15', 128, 1, 0, 129, 6),
(93, '[[\"Lemon\",8,6,\"m\"],[\"Amnesia Haze\",3,6,\"v\"],[\"L de Marihuana\",1,2,\"v\"],[\"Critical \",3.5,6,\"v\"],[\"Moby Dick\",1,6,\"v\"],[\"Pineapple Kush\",1,6,\"v\"],[\"Ak 47\",3,6,\"v\"]]', '[[\"Aquarade\",1,1],[\"Kas Naranja\",1,1]]', '2019-04-13', '00:00:00', '22:55:31', 119, 2, 0, 121, 6),
(95, '[[\"Pineapple Kush\",3.5,6,\"v\"],[\"Amnesia Haze\",8,6,\"v\"],[\"Lemon\",\"0.5\",6,\"m\"],[\"OG Kush\",3,6,\"m\"],[\"Moby Dick\",2,6,\"v\"],[\"Ak 47\",4,6,\"v\"]]', '[[\"Agua\",2,0.5],[\"Coca-Cola\",\"1\",1],[\"Kas Naranja\",\"1\",1]]', '2019-04-14', '00:00:00', '22:18:00', 126, 3, 0, 129, 6),
(96, '[[\"Pineapple Kush\",5,6,\"v\"],[\"Ak 47\",3,6,\"v\"],[\"OG Kush\",2,6,\"m\"],[\"Lemon\",7,6,\"m\"],[\"Amnesia Haze\",6.5,6,\"v\"]]', '[[\"Coca-Cola\",1,1],[\"Boquillas\",1,1],[\"Agua\",1,0.5]]', '2019-04-16', '00:00:00', '18:37:55', 141, 2.5, 0, 143.5, 6),
(97, '[[\"Pineapple Kush\",4,6,\"v\"],[\"Ak 47\",2.5,6,\"v\"],[\"Gordo Master\",5,6,\"m\"],[\"Amnesia Haze\",2,6,\"v\"],[\"L de Marihuana\",\"1\",2,\"v\"],[\"AFGHANHASH  GOLD SEAL\",\"3\",7,\"m\"],[\"Lemon\",\"3\",6,\"m\"],[\"Jamaican Dream\",1,6,\"v\"]]', '[[\"Cruzcampo\",\"2\",1],[\"Agua\",1,0.5],[\"Mechero CLIPPER\",\"1\",1]]', '2019-04-17', '00:00:00', '22:10:59', 128, 3.5, 0, 131.5, 6),
(98, '[[\"Moby Dick\",6,6,\"v\"],[\"Lemon\",5.5,6,\"m\"],[\"Critical \",3,6,\"v\"],[\"Black Dog\",1,6,\"v\"],[\"Amnesia Haze\",2,6,\"v\"],[\"Jamaican Dream\",\"0.5\",6,\"v\"],[\"AFGHANHASH  GOLD SEAL\",\"3\",7,\"m\"],[\"Ak 47\",5,6,\"v\"],[\"Pineapple Kush\",\"0.5\",6,\"v\"],[\"Especial Extraccion\",\"1\",8,\"m\"]]', '[[\"Coca-Cola\",3,1],[\"Agua\",5,0.5],[\"Cruzcampo\",\"2\",1],[\"Kikos\",\"1\",0.4],[\"OCB Largo\",2,1]]', '2019-04-18', '00:00:00', '22:15:11', 170, 9.9, 0, 179.9, 6),
(99, '[[\"Lemon\",5.5,6,\"m\"],[\"Critical \",3,6,\"v\"],[\"Moby Dick\",6,6,\"v\"],[\"Ak 47\",5,6,\"v\"],[\"AMNESIA\",\"2\",8,\"m\"],[\"Especial Extraccion\",\"2\",8,\"m\"],[\"Pineapple Kush\",3,6,\"v\"],[\"Lemon Kush\",1,6,\"v\"],[\"Amnesia Haze\",3,6,\"v\"]]', '[[\"Coca-Cola\",4,1],[\"Kas Naranja\",\"1\",1],[\"Golosinas Haribo\",1,1],[\"OCB Largo\",1,1]]', '2019-04-19', '00:00:00', '22:56:20', 191, 7, 0, 198, 6),
(100, '[[\"Amnesia Haze\",2,6,\"v\"],[\"Lemon\",0.5,6,\"m\"]]', '[[\"Cruzcampo\",1,1]]', '2019-04-20', '00:00:00', '23:19:42', 15, 1, 0, 16, 6),
(101, '[[\"Lemon\",4.5,6,\"m\"],[\"AMNESIA\",\"2.5\",8,\"m\"],[\"Pineapple Kush\",2,6,\"v\"],[\"Ak 47\",4,6,\"v\"],[\"Amnesia Haze\",3.5,6,\"v\"],[\"Black Dog\",0.5,6,\"v\"],[\"Critical \",2,6,\"v\"],[\"L de Marihuana\",\"15\",2,\"v\"]]', '[[\"Coca-Cola\",3,1],[\"Kikos\",1,0.4]]', '2019-04-21', '00:00:00', '22:09:40', 149, 3.4, 0, 152.4, 6),
(102, '[[\"Lemon\",3,6,\"m\"],[\"Ak 47\",4,6,\"v\"],[\"Moby Dick\",3,6,\"v\"],[\"Sour Diesel\",2,6,\"v\"],[\"Grape Fruit\",2,6,\"v\"],[\"Black Widow CBD\",4,6,\"v\"],[\"Pineapple Kush\",1,6,\"v\"],[\"OG Kush\",3.5,6,\"m\"],[\"Gordo Master\",4,6,\"m\"]]', '[[\"Coca-Cola\",2,1],[\"Boquillas\",1,1],[\"Agua\",1,0.5]]', '2019-04-23', '00:00:00', '21:53:07', 159, 3.5, 0, 162.5, 5),
(103, '[[\"Amnesia Haze\",4.5,6,\"v\"],[\"Black Dog\",0.5,6,\"v\"]]', '[[\"Coca-Cola\",1,1]]', '2019-04-24', '00:00:00', '22:02:39', 30, 1, 0, 31, 6),
(104, '[[\"Mataro CBD\",3.5,6,\"v\"],[\"Moby Dick\",3,6,\"v\"],[\"Especial Extraccion\",2,8,\"m\"],[\"OG Kush\",2,6,\"m\"],[\"Ak 47\",3,6,\"v\"],[\"Cheese\",1,6,\"v\"],[\"Critical \",0.5,6,\"v\"]]', '[[\"Papel billete para liar\",1,2]]', '2019-04-25', '00:00:00', '22:03:44', 94, 2, 0, 96, 5),
(105, '[[\"OG Kush\",4.5,6,\"m\"],[\"Gordo Master\",6,6,\"m\"],[\"Ak 47\",13,6,\"v\"],[\"Critical \",1,6,\"v\"],[\"L de Marihuana\",1,2,\"v\"],[\"Cheese\",1,6,\"v\"],[\"Space\",0.5,6,\"v\"],[\"Amnesia Haze\",\"1\",6,\"v\"],[\"Critical CBD\",\"1\",6,\"v\"]]', '[[\"Coca-Cola\",3,1],[\"Cruzcampo\",2,1],[\"Agua\",\"1\",0.5],[\"Aquarade\",\"1\",1],[\"Kikos\",\"1\",0.4],[\"Librillo RAW\",\"1\",1]]', '2019-04-26', '00:00:00', '22:55:46', 170, 7.9, 0, 177.9, 6),
(106, '[[\"Cheese\",6.5,6,\"v\"],[\"Critical CBD\",0.5,6,\"v\"],[\"Mataro CBD\",1.5,6,\"v\"],[\"Pineapple Kush\",1,6,\"v\"],[\"L de Marihuana\",1,2,\"v\"],[\"Caramel Ice\",1,6,\"v\"],[\"OG Kush\",6,6,\"m\"],[\"Gordo Master\",4,6,\"m\"],[\"Ak 47\",6,6,\"v\"],[\"Critical \",2,6,\"v\"],[\"Black Dog\",1,6,\"v\"],[\"Moby Dick\",3,6,\"v\"],[\"Amnesia Haze\",\"2\",6,\"v\"]]', '[[\"Cruzcampo\",2,1],[\"Agua\",2,0.5],[\"Mechero CLIPPER\",\"1\",1],[\"Librillo RAW\",2,1]]', '2019-04-27', '00:00:00', '23:23:01', 209, 6, 0, 215, 6),
(107, '[[\"OG Kush\",4,6,\"m\"],[\"Gordo Master\",3.5,6,\"m\"],[\"AFGHANHASH  GOLD SEAL\",\"1\",0,\"m\"],[\"L de Marihuana\",2,2,\"v\"],[\"Ak 47\",2.5,6,\"v\"],[\"Amnesia Haze\",3,6,\"v\"],[\"Cheese\",2,6,\"v\"],[\"Critical CBD\",3,6,\"v\"],[\"Mataro CBD\",3,6,\"v\"],[\"Moby Dick\",2,6,\"v\"]]', '[[\"Coca-Cola\",8,1],[\"Mechero CLIPPER\",1,1],[\"Librillo RAW\",4,1],[\"Cruzcampo\",4,1],[\"Papel billete para liar\",\"1\",2],[\"Agua\",1,0.5],[\"Kikos\",\"1\",0.4]]', '2019-04-28', '00:00:00', '22:24:36', 142, 19.9, 0, 161.9, 6),
(108, '[[\"Cheese\",5,6,\"v\"],[\"Mataro CBD\",9.5,6,\"v\"],[\"Lemon\",3,6,\"m\"],[\"Moby Dick\",2,6,\"v\"],[\"Ak 47\",2.5,6,\"v\"],[\"OG Kush\",3,6,\"m\"],[\"Critical CBD\",1,6,\"v\"],[\"Amnesia Haze\",4,6,\"v\"],[\"Critical \",2,6,\"v\"]]', '[[\"Agua\",1,0.5],[\"Boquillas\",1,1],[\"Coca-Cola\",3,1],[\"Cruzcampo\",1,1],[\"Cocteleo\",1,1]]', '2019-04-30', '00:00:00', '22:54:03', 192, 6.5, 0, 198.5, 5),
(109, '[[\"Critical \",\"2\",6,\"v\"],[\"Black Dog\",\"1\",6,\"v\"],[\"Cheese\",5,6,\"v\"],[\"Pineapple Kush\",1.5,6,\"v\"],[\"Ak 47\",2.5,6,\"v\"],[\"Gordo Master\",1.5,6,\"m\"],[\"OG Kush\",2,6,\"m\"],[\"Amnesia Haze\",\"0.5\",6,\"v\"]]', '[[\"Librillo RAW\",\"1\",1],[\"Mechero CLIPPER\",\"1\",1]]', '2019-05-01', '00:00:00', '21:54:48', 96, 2, 0, 98, 6),
(110, '[[\"Moby Dick\",2,6,\"v\"],[\"Mataro CBD\",3,6,\"v\"],[\"OG Kush\",6,6,\"m\"],[\"Gordo Master\",1,6,\"m\"],[\"Ak 47\",3.5,6,\"v\"],[\"Pineapple Kush\",2,6,\"v\"],[\"Amnesia Haze\",1,6,\"v\"],[\"Critical CBD\",0.5,6,\"v\"],[\"AFGHANHASH  GOLD SEAL\",3,7,\"m\"],[\"Lemon\",2,6,\"m\"]]', '[[\"Coca-Cola\",2,1],[\"Golosinas Haribo\",1,1]]', '2019-05-02', '00:00:00', '22:43:07', 147, 3, 0, 150, 5),
(111, '[[\"Ak 47\",8.5,6,\"v\"],[\"Mataro CBD\",1,6,\"v\"],[\"OG Kush\",4.5,6,\"m\"],[\"Gordo Master\",2.5,6,\"m\"],[\"Amnesia Haze\",3,6,\"v\"],[\"Cheese\",3,6,\"v\"],[\"Pineapple Kush\",1,6,\"v\"]]', '[[\"Coca-Cola\",1,1],[\"Cruzcampo\",3,1],[\"Agua\",1,0.5],[\"Librillo RAW\",\"1\",1],[\"Kikos\",\"2\",0.4],[\"Grinder Hamburguesa\",\"1\",5]]', '2019-05-03', '00:00:00', '22:58:43', 141, 11.3, 0, 152.3, 6),
(112, '[[\"Cheese\",6,6,\"v\"],[\"Ak 47\",8,6,\"v\"],[\"Gordo Master\",\"1\",6,\"m\"],[\"Pineapple Kush\",2.5,6,\"v\"],[\"Critical CBD\",8,6,\"v\"],[\"OG Kush\",3.5,6,\"m\"],[\"Moby Dick\",3,6,\"v\"],[\"Rubio\",5,4,\"m\"],[\"L de Marihuana\",1,2,\"v\"],[\"Amnesia Haze\",\"0.5\",6,\"v\"]]', '[[\"Agua\",2,0.5],[\"Golosinas Haribo\",\"1\",1],[\"Coca-Cola\",3,1]]', '2019-05-04', '00:00:00', '23:01:28', 217, 5, 0, 222, 6),
(113, '[[\"OG Kush\",4,6,\"m\"],[\"Critical CBD\",3,6,\"v\"],[\"Ak 47\",3,6,\"v\"],[\"Pineapple Kush\",\"1\",6,\"v\"],[\"Cheese\",\"5\",6,\"v\"],[\"Mataro CBD\",\"1\",6,\"v\"]]', '[[\"Coca-Cola\",1,1],[\"Librillo RAW\",\"1\",1],[\"Cruzcampo\",\"1\",1],[\"Papel billete para liar\",\"2\",2],[\"Kas Naranja\",\"2\",1],[\"Aquarade\",\"1\",1],[\"Rollo de papel RAW\",\"1\",2.5]]', '2019-05-05', '00:00:00', '22:40:15', 102, 12.5, 0, 114.5, 6),
(114, '[[\"Critical CBD\",7,6,\"v\"],[\"Mataro CBD\",5.5,6,\"v\"],[\"Moby Dick\",2,6,\"v\"],[\"Ak 47\",2,6,\"v\"],[\"OG Kush\",1.5,6,\"m\"],[\"Critical \",3,6,\"v\"],[\"Cheese\",8,6,\"v\"],[\"Pineapple Kush\",4.5,6,\"v\"],[\"Jamaican Dream\",1,6,\"v\"],[\"Black Dog\",1,6,\"v\"],[\"L de Marihuana\",4,2,\"v\"],[\"Amnesia Haze\",2,6,\"v\"],[\"AFGHANHASH  GOLD SEAL\",1,7,\"m\"]]', '[[\"Coca-Cola\",1,1],[\"Boquillas\",1,1],[\"Agua\",2,0.5],[\"Kas Naranja\",\"1\",1],[\"Cruzcampo\",\"2\",1],[\"Librillo RAW\",2,1]]', '2019-05-07', '00:00:00', '22:49:42', 240, 8, 0, 248, 5),
(115, '[[\"OG Kush\",1,6,\"m\"],[\"Gordo Master\",1,6,\"m\"],[\"Critical CBD\",1,6,\"v\"],[\"Moby Dick\",3,6,\"v\"],[\"L de Marihuana\",1,2,\"v\"],[\"Ak 47\",2,6,\"v\"]]', '[[\"Kikos\",1,0.4]]', '2019-05-08', '00:00:00', '22:02:07', 50, 0.4, 0, 50.4, 6),
(116, '[[\"Moby Dick\",1,6,\"v\"],[\"Rubio Plus\",3,4.5,\"m\"],[\"Cheese\",\"5\",6,\"v\"],[\"Ak 47\",3.5,6,\"v\"],[\"Gordo Master\",\"5\",6,\"m\"],[\"OG Kush\",\"4.5\",6,\"m\"],[\"L de Marihuana\",\"1\",2,\"v\"]]', '[[\"Coca-Cola\",\"1\",1],[\"Agua\",2,0.5]]', '2019-05-09', '00:00:00', '22:02:20', 129.5, 2, 0, 131.5, 6),
(117, '[[\"Rubio Plus\",2.5,4.5,\"m\"],[\"Gordo Master\",3.5,6,\"m\"],[\"Mataro CBD\",\"1\",6,\"v\"],[\"Moby Dick\",3,6,\"v\"],[\"Ak 47\",10,6,\"v\"],[\"Critical \",2,6,\"v\"],[\"Black Dog\",1,6,\"v\"],[\"Critical CBD\",5,6,\"v\"],[\"Pineapple Kush\",3,6,\"v\"],[\"Cheese\",\"2\",6,\"v\"]]', '[[\"Coca-Cola\",2,1],[\"Rollo de papel RAW\",\"1\",2.5],[\"Golosinas Haribo\",1,1],[\"Agua\",2,0.5],[\"Librillo RAW\",\"1\",1]]', '2019-05-10', '00:00:00', '23:21:15', 194.25, 7.5, 0, 201.75, 6),
(118, '[[\"Mataro CBD\",6,6,\"v\"],[\"Cheese\",9,6,\"v\"],[\"Rubio de 4.5\",9.5,4.5,\"m\"],[\"Gordo Master\",2,6,\"m\"],[\"Critical CBD\",4,6,\"v\"],[\"Amnesia Haze\",1,6,\"v\"],[\"Ak 47\",11,6,\"v\"],[\"Moby Dick\",2,6,\"v\"]]', '[[\"Coca-Cola\",1,1],[\"Papel billete para liar\",1,2],[\"Agua\",5,0.5],[\"Librillo RAW\",3,1],[\"OCB Largo\",1,1]]', '2019-05-12', '00:00:00', '22:13:25', 252.75, 9.5, 0, 262.25, 6),
(119, '[[\"Amnesia Haze\",7.5,6,\"v\"],[\"Black Domina\",2,6,\"v\"],[\"Gordo Master\",1,6,\"m\"],[\"Red Mandarine\",5,6,\"v\"],[\"L de Marihuana\",1,2,\"v\"]]', '[[\"Coca-Cola\",1,1]]', '2019-05-14', '00:00:00', '22:15:55', 95, 1, 0, 96, 5),
(120, '[[\"Moby Dick\",3,6,\"v\"],[\"Amnesia Haze\",12,6,\"v\"],[\"Rubio de 2\",5,4,\"m\"],[\"Mataro CBD\",3,6,\"v\"],[\"Red Mandarine\",15.5,6,\"v\"],[\"Rubio de 1\",7,4.5,\"m\"],[\"Gordo Master\",1,6,\"m\"],[\"AFGHANHASH  GOLD SEAL\",6,7,\"m\"]]', '[[\"Papel billete para liar\",1,2],[\"Coca-Cola\",1,1],[\"Cruzcampo\",\"2\",1]]', '2019-05-15', '00:00:00', '22:28:24', 300.5, 5, 0, 305.5, 6),
(121, '[[\"Critical \",3,6,\"v\"],[\"Black Domina\",1,6,\"v\"],[\"Red Mandarine\",3.5,6,\"v\"],[\"AFGHANHASH  GOLD SEAL\",2,7,\"m\"],[\"Critical CBD\",2,6,\"v\"],[\"Amnesia Haze\",1,6,\"v\"]]', '[[\"OCB Largo\",1,1],[\"Kikos\",1,0.4],[\"Coca-Cola\",2,1]]', '2019-05-16', '00:00:00', '20:13:31', 77, 3.4, 0, 80.4, 6),
(122, '[[\"Amnesia Haze\",4,6,\"v\"],[\"Red Mandarine\",14,6,\"v\"],[\"Gordo Master\",1,6,\"m\"],[\"Rubio de 1\",3.5,4.5,\"m\"],[\"Black Domina\",1,6,\"v\"]]', '[[\"Coca-Cola\",2,1],[\"Cruzcampo\",\"1\",1],[\"Librillo RAW\",\"1\",1]]', '2019-05-17', '00:00:00', '23:00:04', 135.75, 4, 0, 139.75, 6),
(123, '[[\"Red Mandarine\",17,6,\"v\"],[\"L de Marihuana\",1,2,\"v\"],[\"Black Domina\",3,6,\"v\"],[\"AFGHANHASH  GOLD SEAL\",4,7,\"m\"],[\"Rubio de 1\",3.5,4.5,\"m\"],[\"Mataro CBD\",2,6,\"v\"],[\"Amnesia Haze\",2.5,6,\"v\"],[\"Especial Extraccion\",\"1\",8,\"m\"],[\"Moby Dick\",1,6,\"v\"]]', '[[\"Coca-Cola\",3,1],[\"Librillo RAW\",\"1\",1]]', '2019-05-19', '00:00:00', '21:03:01', 206.75, 4, 0, 210.75, 6),
(124, '[[\"Red Mandarine\",11,6,\"v\"],[\"Moby Dick\",1,6,\"v\"],[\"Amnesia Haze\",3,6,\"v\"],[\"Lemon\",3,6,\"m\"],[\"Critical CBD\",3,6,\"v\"]]', '[[\"Coca-Cola\",1,1],[\"Agua\",1,0.5]]', '2019-05-21', '00:00:00', '22:07:06', 126, 1.5, 0, 127.5, 5),
(125, '[[\"Red Mandarine\",5.5,6,\"v\"],[\"Black Domina\",3,6,\"v\"],[\"Moby Dick\",4,6,\"v\"],[\"Rubio de 2\",3,4,\"m\"],[\"Lemon Kush\",2.5,6,\"v\"],[\"Amnesia Haze\",1,6,\"v\"]]', '[[\"Aquarade\",1,1],[\"Rollo de papel RAW\",1,2.5]]', '2019-05-22', '00:00:00', '22:06:16', 108, 3.5, 0, 111.5, 5),
(126, '[[\"Red Mandarine\",5.5,6,\"v\"],[\"Black Domina\",3,6,\"v\"],[\"Moby Dick\",4,6,\"v\"],[\"Rubio de 2\",3,4,\"m\"],[\"Lemon Kush\",2.5,6,\"v\"],[\"Amnesia Haze\",1,6,\"v\"]]', '[[\"Aquarade\",1,1],[\"Rollo de papel RAW\",1,2.5]]', '2019-05-23', '00:00:00', '22:50:09', 108, 3.5, 0, 111.5, 5),
(127, '[[\"Red Mandarine\",14,6,\"v\"],[\"Amnesia Haze\",\"1\",6,\"v\"],[\"Ak 47\",\"2\",6,\"v\"],[\"Lemon\",1.5,6,\"m\"],[\"Mataro CBD\",2,6,\"v\"]]', '[[\"Coca-Cola\",1,1],[\"Librillo RAW\",\"1\",1],[\"OCB Largo\",\"1\",1],[\"Agua\",2,0.5],[\"Golosinas Haribo\",1,1]]', '2019-05-24', '00:00:00', '22:56:46', 123, 5, 0, 128, 6),
(130, '[[\"Red Mandarine\",7,6,\"v\"],[\"Lemon\",10,6,\"m\"],[\"Critical CBD\",2.5,6,\"v\"],[\"Critical \",\"2\",6,\"v\"],[\"AFGHANHASH  GOLD SEAL\",2,7,\"m\"],[\"Cheese\",1,6,\"v\"],[\"Jamaican Dream\",0.5,6,\"v\"]]', '[[\"Mechero CLIPPER\",\"1\",1],[\"Kas Naranja\",1,1],[\"Kikos\",\"1\",0.4],[\"Golosinas Haribo\",\"1\",1]]', '2019-05-26', '00:00:00', '22:05:32', 152, 3.4, 0, 155.4, 6),
(131, '[[\"Mataro CBD\",1,6,\"v\"],[\"Cheese\",3,6,\"v\"],[\"Ak 47\",1.5,6,\"v\"],[\"Amnesia Haze\",1,6,\"v\"],[\"Red Mandarine\",1,6,\"v\"]]', '[[\"Kikos\",3,0.4],[\"Golosinas Haribo\",\"1\",1],[\"Coca-Cola\",3,1],[\"Agua\",1,0.5],[\"OCB Largo\",1,1],[\"Kas Naranja\",2,1],[\"Librillo RAW\",1,1]]', '2019-05-25', '00:00:00', '21:04:18', 45, 9.7, 0, 54.7, 6),
(132, '[[\"Red Mandarine\",10,6,\"v\"],[\"Amnesia Haze\",2.5,6,\"v\"],[\"Rubio de 1\",1,4.5,\"m\"],[\"Critical CBD\",1,6,\"v\"],[\"Lemon\",2,6,\"m\"],[\"Caramel Ice\",2,6,\"v\"]]', '[[\"Coca-Cola\",1,1]]', '2019-05-28', '00:00:00', '23:33:37', 109.5, 1, 0, 110.5, 6),
(133, '[[\"Moby Dick\",\"5\",6,\"v\"],[\"AMNESIA\",\"5\",8,\"m\"],[\"Red Mandarine\",1,6,\"v\"]]', '[]', '2019-05-29', '00:00:00', '17:46:21', 76, 0, 0, 76, 5),
(135, '[[\"Red Mandarine\",12.5,6,\"v\"],[\"Lemon\",8,6,\"m\"],[\"AMNESIA\",2,8,\"m\"],[\"Amnesia Haze\",2,6,\"v\"],[\"Mataro CBD\",1,6,\"v\"]]', '[[\"Coca-Cola\",2,1],[\"Agua\",1,0.5],[\"Cruzcampo\",2,1],[\"Librillo RAW\",1,1],[\"Golosinas Haribo\",1,1]]', '2019-05-30', '00:00:00', '00:18:43', 157, 6.5, 0, 163.5, 6),
(137, '[[\"Lemon\",3,6,\"m\"],[\"Amnesia Haze\",16,6,\"v\"],[\"AMNESIA\",6,8,\"m\"],[\"Critical CBD\",2,6,\"v\"],[\"Ak 47\",2,6,\"v\"],[\"Black Dog\",\"1\",6,\"v\"],[\"Babylonia fruit\",\"1\",6,\"v\"]]', '[[\"Coca-Cola\",\"2\",1],[\"Agua\",\"1\",0.5],[\"Librillo RAW\",\"1\",1]]', '2019-05-31', '00:00:00', '23:05:32', 198, 3.5, 0, 201.5, 6),
(138, '[[\"Lemon\",3,6,\"m\"],[\"Moby Dick\",2.5,6,\"v\"],[\"Pineapple Kush\",\"1\",6,\"v\"],[\"Amnesia Haze\",2,6,\"v\"],[\"AMNESIA\",0.5,8,\"m\"],[\"Mataro CBD\",1.5,6,\"v\"]]', '[[\"Bote de olor 1.50€\",\"1\",1.5],[\"Golosinas Haribo\",2,1],[\"Agua\",1,0.5]]', '2019-06-01', '00:00:00', '23:02:01', 64, 4, 0, 68, 6),
(141, '[[\"L de Marihuana\",\"23\",2,\"v\"],[\"AFGHANHASH  GOLD SEAL\",5,7,\"m\"],[\"Pineapple Kush\",\"1\",6,\"v\"],[\"Mataro CBD\",3,6,\"v\"],[\"Ak 47\",\"2\",6,\"v\"],[\"Critical CBD\",\"2.5\",6,\"v\"]]', '[[\"Fanta Naranja\",\"1\",1],[\"Coca-Cola\",1,1],[\"Librillo RAW\",\"1\",1]]', '2019-06-02', '00:00:00', '22:09:03', 132, 3, 0, 135, 6),
(142, '[[\"Amnesia Haze\",6,6,\"v\"],[\"Moby Dick\",7,6,\"v\"],[\"Lemon\",1.5,6,\"m\"],[\"AFGHANHASH  GOLD SEAL\",5,7,\"m\"],[\"Jamaican Dream\",4,6,\"v\"],[\"Ak 47\",7,6,\"v\"],[\"Damnesia\",\"3\",6,\"v\"],[\"Cheese\",3,6,\"v\"],[\"Mataro CBD\",3,6,\"v\"]]', '[[\"Coca-Cola\",1,1],[\"Papel billete para liar\",1,2],[\"OCB Largo\",\"1\",1],[\"Mechero CLIPPER\",1,1]]', '2019-06-04', '00:00:00', '22:18:24', 242, 5, 0, 247, 5),
(143, '[[\"Lemon\",3,6,\"m\"],[\"AFGHANHASH  GOLD SEAL\",2,7,\"m\"],[\"Ak 47\",5,6,\"v\"],[\"Jamaican Dream\",0.5,6,\"v\"]]', '[[\"Rollo de papel RAW\",1,2.5],[\"Agua\",\"1\",0.5]]', '2019-06-05', '00:00:00', '22:05:29', 65, 3, 0, 68, 6),
(144, '[[\"Ak 47\",5,6,\"v\"],[\"AMNESIA\",6.5,8,\"m\"],[\"Lemon\",4,6,\"m\"],[\"Critical \",1.5,6,\"v\"]]', '[[\"Kikos\",\"1\",0.4]]', '2019-06-06', '00:00:00', '21:57:55', 115, 0.4, 0, 115.4, 6),
(146, '[[\"Lemon\",7.5,6,\"m\"],[\"Amnesia Haze\",4,6,\"v\"],[\"Cheese\",4.5,6,\"v\"],[\"Ak 47\",9,6,\"v\"],[\"Especial Extraccion\",\"1\",8,\"m\"],[\"Black Widow CBD\",3,6,\"v\"],[\"Sour Diesel\",1,6,\"v\"],[\"Critical \",2.5,6,\"v\"]]', '[[\"Coca-Cola\",2,1],[\"Cruzcampo\",\"1\",1],[\"Agua\",2,0.5]]', '2019-06-07', '00:00:00', '23:00:15', 197, 4, 0, 201, 6),
(148, '[[\"Mataro CBD\",2.5,6,\"v\"],[\"Lemon\",3.5,6,\"m\"],[\"Moby Dick\",0.5,6,\"v\"],[\"Pineapple Kush\",\"0.5\",6,\"v\"],[\"Ak 47\",4,6,\"v\"],[\"Especial Extraccion\",\"1\",8,\"m\"]]', '[[\"Agua\",2,0.5],[\"Fanta Naranja\",1,1],[\"Golosinas Haribo\",\"1\",1],[\"Coca-Cola\",2,1],[\"Cocteleo\",1,1]]', '2019-06-08', '00:00:00', '23:42:03', 74, 6, 0, 80, 6),
(149, '[[\"Ak 47\",7.5,6,\"v\"],[\"Cheese\",5.5,6,\"v\"],[\"Lemon\",5,6,\"m\"],[\"Mataro CBD\",2,6,\"v\"],[\"Black Dog\",1,6,\"v\"]]', '[[\"Librillo RAW\",2,1],[\"Coca-Cola\",1,1],[\"Cruzcampo\",1,1]]', '2019-06-09', '00:00:00', '22:01:43', 126, 4, 0, 130, 6),
(150, '[[\"Lemon\",5,6,\"m\"],[\"Amnesia Haze\",3,6,\"v\"],[\"Critical \",3,6,\"v\"],[\"Ak 47\",3.5,6,\"v\"],[\"Moby Dick\",5.5,6,\"v\"],[\"L de Marihuana\",4,2,\"v\"],[\"AMNESIA\",\"3\",8,\"m\"],[\"Rubio de 2\",\"4.5\",4,\"m\"],[\"Jamaican Dream\",\"2.5\",6,\"v\"]]', '[[\"Coca-Cola\",1,1]]', '2019-06-11', '00:00:00', '22:42:39', 185, 1, 0, 186, 5),
(151, '[[\"Especial Extraccion\",2,8,\"m\"],[\"Ak 47\",4,6,\"v\"],[\"Amnesia Haze\",2,6,\"v\"],[\"Lemon\",3,6,\"m\"],[\"L de Marihuana\",3,2,\"v\"],[\"AMNESIA\",\"6\",8,\"m\"],[\"AFGHANHASH  GOLD SEAL\",\"2\",7,\"m\"],[\"Cheese\",2,6,\"v\"],[\"Azura Haze\",1,6,\"v\"],[\"Space\",1,6,\"v\"],[\"Moby Dick\",3,6,\"v\"]]', '[[\"Cruzcampo\",2,1],[\"Agua\",2,0.5],[\"Fanta Naranja\",1,1],[\"Coca-Cola\",1,1]]', '2019-06-12', '00:00:00', '22:21:55', 180, 5, 0, 185, 5),
(152, '[[\"Rubio de 2\",1,4,\"m\"],[\"Ak 47\",1,6,\"v\"],[\"Amnesia Haze\",2,6,\"v\"],[\"Lemon\",3,6,\"m\"],[\"Rosenthal\",4,6,\"v\"],[\"L de Marihuana\",5,2,\"v\"],[\"Cheese\",\"0.5\",6,\"v\"]]', '[[\"Coca-Cola\",1,1],[\"Cruzcampo\",1,1],[\"Agua\",2,0.5]]', '2019-06-13', '00:00:00', '22:44:22', 77, 3, 0, 80, 6),
(153, '[[\"Amnesia Haze\",11.99,30,\"v\"],[\"Rosenthal\",2.5,12,\"v\"],[\"Rubio de 1\",5.11,13.5,\"m\"],[\"Ak 47\",6,12,\"v\"],[\"L de Marihuana\",4,8,\"v\"]]', '[[\"Coca-Cola\",\"1\",1],[\"Agua\",2,0.5],[\"Fanta Naranja\",\"1\",1],[\"Tinto con limu00f3n\",\"1\",1],[\"Cocteleo\",1,1]]', '2019-06-14', '17:21:08', '22:48:58', 153.94, 5, 20, 178.94, 6),
(154, '[[\"Rosenthal\",8.95,18,\"v\"],[\"Cheese\",\"2.71\",6,\"v\"],[\"Rubio de 1\",4.71,13.5,\"m\"],[\"Lemon\",4,6,\"m\"],[\"AFGHANHASH  GOLD SEAL\",3,7,\"m\"],[\"L de Marihuana\",1,2,\"v\"],[\"Ak 47\",2,6,\"v\"],[\"Amnesia Haze\",3.5,12,\"v\"]]', '[[\"Agua\",2,0.5],[\"Coca-Cola\",1,1],[\"Librillo RAW\",\"1\",1]]', '2019-06-16', '16:21:46', '22:28:44', 171.16, 3, 0, 174.16, 6),
(155, '[[\"Rosenthal\",11,30,\"v\"],[\"Lemon\",2,6,\"m\"],[\"Moby Dick\",6,6,\"v\"],[\"Amnesia Haze\",8.5,24,\"v\"],[\"Rubio de 2\",4,4,\"m\"],[\"Critical \",4.5,12,\"v\"],[\"Azura Haze\",\"1.5\",6,\"v\"],[\"Jamaican Dream\",1.5,6,\"v\"],[\"L de Marihuana\",1,2,\"v\"],[\"Critical CBD\",2,6,\"v\"],[\"AFGHANHASH  GOLD SEAL\",0.5,7,\"m\"],[\"Ak 47\",1,6,\"v\"]]', '[[\"Coca-Cola\",1,1],[\"Agua\",1,0.5],[\"Fanta Naranja\",1,1]]', '2019-06-18', '17:26:49', '22:33:15', 249.5, 2.5, 0, 252, 5),
(156, '[[\"Amnesia Haze\",5,12,\"v\"],[\"Lemon\",5,12,\"m\"],[\"Rosenthal\",2.5,6,\"v\"],[\"Rubio de 1\",0.5,4.5,\"m\"],[\"Cheese\",3,6,\"v\"],[\"AMNESIA\",2,8,\"m\"]]', '[[\"Coca-Cola\",2,1],[\"Cruzcampo\",2,1],[\"Golosinas Haribo\",1,1],[\"Librillo RAW\",1,1]]', '2019-06-19', '17:18:08', '17:19:10', 111.25, 6, 0, 117.25, 5),
(157, '[[\"Rosenthal\",1,6,\"v\"],[\"Amnesia Haze\",1,6,\"v\"],[\"L de Marihuana\",\"1\",2,\"v\"],[\"Ak 47\",\"2\",6,\"v\"]]', '[[\"Maxibon\",\"1\",2],[\"Coca-Cola\",\"1\",1]]', '2019-06-20', '17:19:35', '21:03:14', 26, 3, 40, 69, 6),
(158, '[[\"AMNESIA\",4,8,\"m\"],[\"Lemon\",4,6,\"m\"],[\"Rubio de 1\",2,4.5,\"m\"],[\"L de Marihuana\",2,2,\"v\"]]', '[]', '2019-06-20', '21:15:11', '21:24:03', 69, 0, 0, 69, 6),
(159, '[[\"Critical CBD\",2,6,\"v\"],[\"Cheese\",2,6,\"v\"],[\"L de Marihuana\",1,2,\"v\"],[\"Ak 47\",2,6,\"v\"],[\"AMNESIA\",\"3\",8,\"m\"],[\"Amnesia Haze\",4.83,18,\"v\"],[\"Rubio de 1\",\"1.11\",4.5,\"m\"],[\"Especial Extraccion\",1,8,\"m\"]]', '[[\"Fanta Naranja\",\"1\",1],[\"Cruzcampo\",7,1],[\"Maxibon\",2,2]]', '2019-06-21', '16:33:36', '22:57:25', 103.98, 12, 0, 115.98, 6),
(160, '[[\"Critical CBD\",1,6,\"v\"],[\"Cheese\",4,12,\"v\"],[\"Mataro CBD\",3,12,\"v\"],[\"L de Marihuana\",\"1\",2,\"v\"],[\"Amnesia Haze\",7.17,24,\"v\"],[\"Rubio de 1\",4.94,18,\"m\"],[\"AFGHANHASH  GOLD SEAL\",2,7,\"m\"],[\"Rosenthal\",2.5,6,\"v\"]]', '[[\"Librillo RAW\",3,1],[\"Maxibon\",1,2],[\"Conos de Vainilla y Chocolate\",2,1],[\"Burn\",1,1],[\"Fanta Naranja\",\"1\",1],[\"Agua\",\"2\",0.5],[\"Coca-Cola\",1,1],[\"Golosinas Haribo\",1,1],[\"Tinto con limu00f3n\",1,1],[\"Flash de sabores\",1,0.3]]', '2019-06-22', '16:34:16', '23:02:09', 144.25, 13.3, 0, 157.55, 6),
(161, '[[\"Cheese\",\"1.66\",6,\"v\"],[\"AMNESIA\",\"3\",8,\"m\"],[\"Rubio de 1\",5.29,9,\"m\"],[\"Lemon\",\"0.28\",6,\"m\"],[\"Critical CBD\",\"1.5\",6,\"v\"]]', '[[\"Coca-Cola\",4,1],[\"Librillo RAW\",\"1\",1],[\"Golosinas Haribo\",2,1],[\"Agua\",\"1\",0.5]]', '2019-06-23', '16:20:18', '22:05:45', 68.44, 7.5, 0, 75.94, 6),
(162, '[[\"Rosenthal\",\"4\",6,\"v\"],[\"Lemon\",\"1\",6,\"m\"],[\"Ak 47\",3,12,\"v\"],[\"Cheese\",\"2\",6,\"v\"],[\"AMNESIA\",\"2\",8,\"m\"],[\"AFGHANHASH  GOLD SEAL\",\"0.5\",7,\"m\"],[\"L de Marihuana\",2,4,\"v\"],[\"Fresa\",10.5,36,\"v\"],[\"Critical \",2,6,\"v\"],[\"Especial Extraccion\",1,8,\"m\"],[\"Amnesia Haze\",1,6,\"v\"],[\"Critical CBD\",1,6,\"v\"]]', '[[\"Coca-Cola\",2,1],[\"Librillo RAW\",1,1],[\"Fanta Naranja\",1,1],[\"Cruzcampo\",3,1]]', '2019-06-25', '17:26:45', '22:03:07', 178.5, 7, 20, 205.5, 6),
(163, '[[\"L de Marihuana\",1,2,\"v\"],[\"Fresa\",4.5,12,\"v\"],[\"AFGHANHASH  GOLD SEAL\",3,7,\"m\"]]', '[[\"Agua\",3,0.5],[\"Cruzcampo\",3,1]]', '2019-06-26', '17:33:36', '17:15:09', 50, 4.5, 0, 54.5, 5),
(164, '[[\"Lemon\",6,18,\"m\"],[\"AMNESIA\",\"5\",8,\"m\"],[\"AFGHANHASH  GOLD SEAL\",\"2\",7,\"m\"],[\"L de Marihuana\",3,4,\"v\"],[\"Amnesia Haze\",2,6,\"v\"]]', '[[\"Cruzcampo\",1,1],[\"Librillo RAW\",\"1\",1],[\"Agua\",\"1\",0.5],[\"Coca-Cola\",1,1],[\"Conos de Vainilla y Chocolate\",1,1]]', '2019-06-27', '17:15:16', '22:01:40', 108, 4.5, 0, 112.5, 6),
(165, '[[\"Fresa\",5,12,\"v\"],[\"Lemon\",3.5,12,\"m\"],[\"Mataro CBD\",\"1\",6,\"v\"],[\"L de Marihuana\",4,4,\"v\"],[\"Rosenthal\",2,6,\"v\"],[\"Especial Extraccion\",1,8,\"m\"]]', '[[\"Cruzcampo\",1,1],[\"Coca-Cola\",\"2\",1],[\"Fanta Naranja\",1,1],[\"Burn\",1,1],[\"Conos de Vainilla y Chocolate\",1,1]]', '2019-06-28', '16:15:11', '23:00:17', 85, 6, 0, 91, 6),
(166, '[[\"Lemon\",4.5,18,\"m\"],[\"Cheese\",\"2\",6,\"v\"],[\"Fresa\",4,18,\"v\"],[\"Especial Extraccion\",\"3\",8,\"m\"],[\"L de Marihuana\",21,4,\"v\"],[\"Amnesia Haze\",1,12,\"v\"],[\"Ak 47\",1.5,6,\"v\"],[\"Rosenthal\",2,6,\"v\"]]', '[[\"Coca-Cola\",2,1],[\"Agua\",3,0.5],[\"Conos de Vainilla y Chocolate\",1,1],[\"Fanta Naranja\",1,1],[\"Librillo RAW\",1,1]]', '2019-06-29', '16:06:04', '22:57:30', 156, 6.5, 0, 162.5, 6),
(167, '[[\"Lemon\",\"0.5\",6,\"m\"],[\"L de Marihuana\",\"1.5\",2,\"v\"]]', '[[\"Conos de Vainilla y Chocolate\",\"1\",1]]', '2019-06-29', '22:57:42', '22:58:38', 6, 1, 0, 7, 6),
(168, '[[\"Amnesia Haze\",4.5,12,\"v\"],[\"Cheese\",5,18,\"v\"],[\"Ak 47\",\"0.5\",6,\"v\"],[\"Lemon\",3,6,\"m\"],[\"Fresa\",5,12,\"v\"],[\"L de Marihuana\",\"1\",2,\"v\"]]', '[[\"Coca-Cola\",1,1],[\"Kikos\",\"0.5\",0.4],[\"Burn\",\"1\",1],[\"Cocteleo\",\"1\",1],[\"Fanta Naranja\",1,1],[\"OCB Largo\",\"1\",1],[\"Agua\",\"1\",0.5],[\"Su00e1ndwich de nata\",\"1\",1]]', '2019-06-30', '16:14:41', '22:06:15', 110, 6.7, 0, 116.7, 6),
(169, '[[\"Rosenthal\",6,12,\"v\"],[\"Lemon\",4,12,\"m\"],[\"Critical CBD\",2,12,\"v\"],[\"Mataro CBD\",1,6,\"v\"],[\"L de Marihuana\",1,2,\"v\"],[\"Amnesia Haze\",3,18,\"v\"],[\"Especial Extraccion\",1,8,\"m\"]]', '[[\"Coca-Cola\",1,1]]', '2019-07-02', '17:47:11', '22:08:33', 106, 1, 40, 147, 5),
(170, '[[\"Fresa\",2,6,\"v\"],[\"Amnesia Haze\",2,6,\"v\"],[\"Especial Extraccion\",1,8,\"m\"]]', '[[\"Burn\",1,1]]', '2019-07-03', '18:00:42', '22:33:24', 32, 1, 0, 33, 5),
(171, '[[\"Lemon\",19,24,\"m\"],[\"Rosenthal\",4,12,\"v\"],[\"Ak 47\",1,6,\"v\"],[\"Critical \",3,6,\"v\"],[\"Fresa\",1.5,6,\"v\"],[\"Critical CBD\",\"1\",6,\"v\"],[\"Mataro CBD\",\"1\",6,\"v\"],[\"Amnesia Haze\",\"1\",6,\"v\"],[\"L de Marihuana\",\"1\",2,\"v\"]]', '[[\"Cruzcampo\",1,1],[\"Su00e1ndwich de nata\",1,1],[\"Coca-Cola\",2,1],[\"Cocteleo\",1,1],[\"Kikos\",1,0.4]]', '2019-07-04', '17:12:58', '22:00:08', 191, 5.4, 40, 236.4, 6),
(172, '[[\"L de Marihuana\",3,4,\"v\"],[\"Moby Dick\",\"2\",6,\"v\"],[\"Lemon\",\"3\",6,\"m\"],[\"Fresa\",6,12,\"v\"],[\"Amnesia Haze\",2,6,\"v\"],[\"Critical CBD\",1,6,\"v\"],[\"Ak 47\",1,6,\"v\"],[\"Rosenthal\",1,6,\"v\"]]', '[[\"Golosinas Haribo\",\"1\",1],[\"OCB Largo\",1,1],[\"Coca-Cola\",1,1]]', '2019-07-05', '16:27:48', '22:58:33', 102, 3, 0, 105, 6),
(173, '[[\"Lemon\",5.5,12,\"m\"],[\"Amnesia Haze\",2,6,\"v\"],[\"Fresa\",4,12,\"v\"],[\"Especial Extraccion\",3,16,\"m\"],[\"Rosenthal\",\"1\",6,\"v\"],[\"Cheese\",3,12,\"v\"],[\"Mataro CBD\",\"2\",6,\"v\"]]', '[[\"Cruzcampo\",1,1],[\"Coca-Cola\",1,1],[\"Golosinas Haribo\",\"1\",1]]', '2019-07-07', '16:19:44', '22:58:39', 129, 3, 0, 132, 6),
(174, '[[\"Fresa\",6,12,\"v\"],[\"Rosenthal\",2,6,\"v\"],[\"Cheese\",\"1.16\",6,\"v\"],[\"Especial Extraccion\",2,8,\"m\"],[\"Critical CBD\",1,6,\"v\"]]', '[[\"Coca-Cola\",1,1]]', '2019-07-09', '17:20:20', '22:56:24', 76.96, 1, 0, 77.96, 6),
(175, '[[\"Fresa\",12.5,36,\"v\"],[\"Pineapple Kush\",1,6,\"v\"],[\"Lemon\",9.5,24,\"m\"],[\"Especial Extraccion\",3.5,16,\"m\"],[\"Cheese\",1,6,\"v\"],[\"Critical CBD\",6,18,\"v\"],[\"Ak 47\",4,12,\"v\"],[\"Amnesia Haze\",0.5,6,\"v\"]]', '[[\"Coca-Cola\",1,1],[\"Cruzcampo\",1,1],[\"Mechero CLIPPER\",1,1],[\"Okolife\",1,80],[\"Kikos\",1,0.4]]', '2019-07-10', '17:03:07', '23:03:33', 235, 83.4, 120, 438.4, 5),
(176, '[[\"Mataro CBD\",1,6,\"v\"],[\"L de Marihuana\",4,8,\"v\"],[\"Fresa\",\"2\",6,\"v\"],[\"Lemon\",13.5,18,\"m\"],[\"Especial Extraccion\",\"2\",8,\"m\"]]', '[[\"Mechero CLIPPER\",\"1\",1],[\"Librillo RAW\",\"1\",1],[\"Agua\",\"1\",0.5]]', '2019-07-11', '16:53:41', '22:57:17', 123, 2.5, 0, 125.5, 6),
(177, '[[\"Ak 47\",3.5,12,\"v\"],[\"L de Marihuana\",6,4,\"v\"],[\"Fresa\",4,12,\"v\"],[\"Lemon\",7.33,24,\"m\"]]', '[[\"Burn\",\"1\",1],[\"Cruzcampo\",1,1]]', '2019-07-12', '16:34:14', '23:02:32', 100.98, 2, 0, 102.98, 6),
(178, '[[\"Especial Extraccion\",6,16,\"m\"],[\"Mataro CBD\",10,18,\"v\"],[\"Ak 47\",8,18,\"v\"],[\"Cheese\",4,18,\"v\"],[\"Fresa\",12,24,\"v\"],[\"Lemon\",5.5,12,\"m\"],[\"L de Marihuana\",2.5,2,\"v\"],[\"Babylonia fruit\",1,6,\"v\"]]', '[[\"OCB Largo\",1,1],[\"Agua\",2,0.5],[\"Grinder Green machine\",1,20],[\"Coca-Cola\",2,1],[\"Fanta Naranja\",\"2\",1]]', '2019-07-14', '18:24:54', '23:02:20', 296, 26, 40, 362, 6),
(179, '[[\"Lemon\",6.5,18,\"m\"],[\"Fresa\",4,12,\"v\"],[\"Rosenthal\",6,18,\"v\"],[\"Critical CBD\",1,6,\"v\"],[\"Ak 47\",1,6,\"v\"],[\"Amnesia Haze\",7.5,24,\"v\"]]', '[[\"Fanta Naranja\",1,1],[\"Coca-Cola\",1,1]]', '2019-07-16', '17:20:10', '22:40:12', 156, 2, 0, 158, 5),
(180, '[[\"Lemon\",5,12,\"m\"],[\"Fresa\",5,12,\"v\"],[\"Critical CBD\",5,18,\"v\"],[\"L de Marihuana\",2,4,\"v\"]]', '[[\"Cruzcampo\",1,1],[\"Agua\",2,0.5],[\"Coca-Cola\",1,1]]', '2019-07-17', '17:40:57', '22:56:26', 94, 3, 40, 137, 5),
(181, '[[\"L de Marihuana\",26.5,4,\"v\"],[\"Cheese\",5,12,\"v\"],[\"Amnesia Haze\",14,24,\"v\"],[\"Fresa\",5,18,\"v\"],[\"Mataro CBD\",1,6,\"v\"],[\"Moby Dick\",4,6,\"v\"],[\"Lemon\",21,24,\"m\"],[\"Ak 47\",3.5,12,\"v\"],[\"Critical CBD\",\"1\",6,\"v\"]]', '[[\"Cruzcampo\",5,1],[\"Fanta Naranja\",1,1]]', '2019-07-18', '16:28:53', '23:05:22', 380, 6, 20, 406, 6);
INSERT INTO `cajas` (`id`, `geneticas`, `productos`, `fecha`, `horaAp`, `hora`, `total_geneticas`, `total_productos`, `total_cuotas`, `total`, `idUsuario`) VALUES
(182, '[[\"Amnesia Haze\",7,18,\"v\"],[\"Lemon\",12.5,24,\"m\"],[\"Cheese\",4,12,\"v\"],[\"Ak 47\",6,12,\"v\"],[\"Especial Extraccion\",6,16,\"m\"],[\"Mataro CBD\",5,6,\"v\"],[\"Fresa\",2,6,\"v\"],[\"Rosenthal\",3.58,6,\"v\"]]', '[[\"Coca-Cola\",2,1],[\"Burn\",1,1],[\"Cruzcampo\",1,1],[\"OCB Largo\",1,1]]', '2019-07-19', '16:33:49', '23:02:00', 288.48, 5, 20, 313.48, 6),
(183, '[[\"Lemon\",8.5,24,\"m\"],[\"Cheese\",3,6,\"v\"],[\"Fresa\",3,6,\"v\"],[\"Especial Extraccion\",3.25,16,\"m\"],[\"Amnesia Haze\",2.5,18,\"v\"],[\"Mataro CBD\",1.5,12,\"v\"]]', '[[\"Papel billete para liar\",\"1\",2],[\"Agua\",2,0.5],[\"Coca-Cola\",3,1],[\"OCB Largo\",3,1]]', '2019-07-21', '16:21:12', '22:58:05', 137, 9, 0, 146, 6),
(185, '[[\"OG Kush\",\"1\",6,\"m\"],[\"Gordo Master\",2,14,\"m\"]]', '[]', '2019-07-23', '10:10:41', '10:11:25', 20, 0, 0, 20, 6),
(186, '[[\"Cheese\",5.58,17.58,\"v\"]]', '[]', '2019-07-23', '10:11:38', '10:31:09', 33.06, 0, 0, 33.06, 6),
(187, '[[\"Azura Haze\",6.58,39.48,\"v\"]]', '[]', '2019-07-23', '13:01:09', '13:33:08', 39.48, 0, 0, 39.48, 6),
(188, '', '', '2019-07-30', '14:13:18', '00:00:00', 0, 0, 0, 0, 6),
(189, '', '', '2019-08-27', '10:25:26', '00:00:00', 0, 0, 0, 0, 6),
(190, '', '', '2019-08-30', '09:53:45', '00:00:00', 0, 0, 0, 0, 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias_articulos`
--

CREATE TABLE `categorias_articulos` (
  `id` int(2) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `descripcion` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categorias_articulos`
--

INSERT INTO `categorias_articulos` (`id`, `nombre`, `descripcion`) VALUES
(23, 'Parafernalia', ''),
(24, 'Helados', ''),
(1, 'Bebidas', 'asds'),
(27, 'Prueba', 'asdasd');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `config`
--

CREATE TABLE `config` (
  `id` int(11) NOT NULL,
  `tipo` varchar(100) NOT NULL,
  `valor` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `config`
--

INSERT INTO `config` (`id`, `tipo`, `valor`) VALUES
(1, 'nombre', 'Asociación Cannábica Green Island'),
(2, 'nombre-corto', 'Green Island'),
(3, 'descuento', '7');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `correcciones`
--

CREATE TABLE `correcciones` (
  `id` int(7) NOT NULL,
  `fecha` date NOT NULL,
  `geneticas` varchar(3000) NOT NULL,
  `id_caja` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `correcciones`
--

INSERT INTO `correcciones` (`id`, `fecha`, `geneticas`, `id_caja`) VALUES
(2, '2019-05-06', '[[\"1\",\"3\",\"3\"],[\"2\",\"14.5\",\"3\"],[\"3\",\"11\",\"3\"],[\"4\",\"25\",\"3\"],[\"5\",\"2\",\"3\"],[\"6\",\"3\",\"3\"],[\"13\",\"8\",\"3\"],[\"14\",\"8\",\"3\"],[\"15\",\"8\",\"3\"],[\"16\",\"8\",\"3\"],[\"17\",\"8\",\"3\"],[\"18\",\"8\",\"3\"],[\"19\",\"8\",\"3\"],[\"20\",\"8\",\"3\"],[\"21\",\"8\",\"3\"],[\"22\",\"8\",\"3\"],[\"23\",\"8\",\"3\"],[\"24\",\"8\",\"3\"],[\"25\",\"8\",\"3\"],[\"26\",\"8\",\"3\"],[\"27\",\"8\",\"3\"],[\"28\",\"8\",\"3\"],[\"29\",\"8\",\"3\"],[\"32\",\"8\",\"3\"],[\"7\",\"8\",\"3\"],[\"30\",\"8\",\"3\"],[\"31\",\"8\",\"3\"]]', 6),
(3, '2019-05-06', '[[\"1\",\"3\",\"4\"],[\"2\",\"3\",\"4\"],[\"3\",\"3\",\"4\"],[\"4\",\"3\",\"4\"],[\"5\",\"3\",\"4\"],[\"6\",\"3\",\"4\"],[\"13\",\"3\",\"4\"],[\"14\",\"3\",\"4\"],[\"15\",\"3\",\"4\"],[\"16\",\"3\",\"4\"],[\"17\",\"3\",\"4\"],[\"18\",\"3\",\"4\"],[\"19\",\"3\",\"4\"],[\"20\",\"3\",\"4\"],[\"21\",\"3\",\"4\"],[\"22\",\"3\",\"4\"],[\"23\",\"3\",\"4\"],[\"24\",\"3\",\"4\"],[\"25\",\"3\",\"4\"],[\"26\",\"2\",\"4\"],[\"27\",\"3\",\"4\"],[\"28\",\"3\",\"4\"],[\"29\",\"3\",\"4\"],[\"32\",\"3\",\"4\"],[\"7\",\"3\",\"4\"],[\"30\",\"3\",\"4\"],[\"31\",\"3\",\"4\"]]', 6),
(4, '2019-05-06', '[[\"1\",\"4\",\"4\"],[\"2\",\"4\",\"4\"],[\"3\",\"4\",\"4\"],[\"4\",\"4\",\"4\"],[\"5\",\"4\",\"4\"],[\"6\",\"4\",\"4\"],[\"13\",\"4\",\"4\"],[\"14\",\"4\",\"4\"],[\"15\",\"4\",\"4\"],[\"16\",\"4\",\"4\"],[\"17\",\"4\",\"4\"],[\"18\",\"4\",\"4\"],[\"19\",\"4\",\"4\"],[\"20\",\"4\",\"4\"],[\"21\",\"4\",\"4\"],[\"22\",\"4\",\"4\"],[\"23\",\"4\",\"4\"],[\"24\",\"3\",\"4\"],[\"25\",\"4\",\"4\"],[\"26\",\"4\",\"4\"],[\"27\",\"4\",\"4\"],[\"28\",\"4\",\"4\"],[\"29\",\"4\",\"4\"],[\"32\",\"4\",\"4\"],[\"7\",\"4\",\"4\"],[\"30\",\"4\",\"4\"],[\"31\",\"4\",\"4\"]]', 23),
(5, '2019-05-06', '[[\"1\",\"4\",\"4\"],[\"2\",\"4\",\"4\"],[\"3\",\"4\",\"4\"],[\"4\",\"4\",\"4\"],[\"5\",\"4\",\"4\"],[\"6\",\"4\",\"4\"],[\"13\",\"3\",\"3\"],[\"14\",\"4\",\"4\"],[\"15\",\"4\",\"4\"],[\"16\",\"4\",\"4\"],[\"17\",\"4\",\"4\"],[\"18\",\"4\",\"4\"],[\"19\",\"4\",\"4\"],[\"20\",\"4\",\"4\"],[\"21\",\"3\",\"3\"],[\"22\",\"4\",\"4\"],[\"23\",\"4\",\"4\"],[\"24\",\"4\",\"4\"],[\"25\",\"3\",\"3\"],[\"26\",\"4\",\"4\"],[\"27\",\"4\",\"4\"],[\"28\",\"4\",\"4\"],[\"29\",\"4\",\"4\"],[\"32\",\"4\",\"4\"],[\"7\",\"4\",\"4\"],[\"30\",\"4\",\"4\"],[\"31\",\"3\",\"3\"]]', 30),
(6, '2019-05-07', '[[\"1\",\"4\",\"4\"],[\"2\",\"4\",\"4\"],[\"3\",\"4\",\"4\"],[\"4\",\"4\",\"4\"],[\"5\",\"4\",\"4\"],[\"6\",\"4\",\"4\"],[\"13\",\"3\",\"3\"],[\"14\",\"4\",\"4\"],[\"15\",\"4\",\"4\"],[\"16\",\"4\",\"4\"],[\"17\",\"4\",\"4\"],[\"18\",\"4\",\"4\"],[\"19\",\"4\",\"4\"],[\"20\",\"4\",\"4\"],[\"21\",\"3\",\"3\"],[\"22\",\"4\",\"4\"],[\"23\",\"4\",\"4\"],[\"24\",\"3\",\"3\"],[\"25\",\"3\",\"3\"],[\"26\",\"3\",\"15\"],[\"27\",\"4\",\"4\"],[\"28\",\"4\",\"4\"],[\"29\",\"4\",\"4\"],[\"32\",\"4\",\"4\"],[\"7\",\"4\",\"4\"],[\"30\",\"4\",\"4\"],[\"31\",\"3\",\"3\"]]', 33),
(7, '2019-05-29', '[[\"1\",\"4\",\"130\"],[\"2\",\"4\",\"120\"],[\"3\",\"2\",\"120\"],[\"4\",\"4\",\"120\"],[\"5\",\"4\",\"120\"],[\"6\",\"4\",\"120\"],[\"13\",\"1\",\"120\"],[\"14\",\"4\",\"120\"],[\"15\",\"4\",\"120\"],[\"16\",\"4\",\"120\"],[\"17\",\"4\",\"120\"],[\"18\",\"-352\",\"120\"],[\"19\",\"-0.5\",\"120\"],[\"20\",\"0.5\",\"120\"],[\"21\",\"1\",\"120\"],[\"22\",\"4\",\"120\"],[\"23\",\"4\",\"120\"],[\"24\",\"-10.2\",\"120\"],[\"25\",\"-52\",\"120\"],[\"26\",\"2\",\"120\"],[\"27\",\"53\",\"10\"],[\"28\",\"2\",\"120\"],[\"29\",\"4\",\"120\"],[\"32\",\"4\",\"120\"],[\"7\",\"-0.5\",\"10\"],[\"30\",\"2\",\"10\"],[\"31\",\"0\",\"10\"]]', 61),
(8, '2019-05-29', '[[\"1\",\"130\",\"130\"],[\"2\",\"120\",\"83\"],[\"3\",\"120\",\"10\"],[\"4\",\"120\",\"120\"],[\"5\",\"120\",\"120\"],[\"6\",\"120\",\"120\"],[\"13\",\"120\",\"120\"],[\"14\",\"120\",\"120\"],[\"15\",\"120\",\"120\"],[\"16\",\"120\",\"120\"],[\"17\",\"120\",\"120\"],[\"18\",\"120\",\"120\"],[\"19\",\"120\",\"120\"],[\"20\",\"119\",\"119\"],[\"21\",\"120\",\"120\"],[\"22\",\"120\",\"120\"],[\"23\",\"120\",\"120\"],[\"24\",\"119\",\"119\"],[\"25\",\"119\",\"119\"],[\"26\",\"120\",\"120\"],[\"27\",\"10\",\"10\"],[\"28\",\"120\",\"120\"],[\"29\",\"120\",\"120\"],[\"32\",\"120\",\"120\"],[\"7\",\"10\",\"10\"],[\"30\",\"10\",\"10\"],[\"31\",\"10\",\"10\"]]', 62),
(9, '2019-05-29', '[[\"1\",\"130\",\"130\"],[\"2\",\"120\",\"83\"],[\"3\",\"120\",\"10\"],[\"4\",\"120\",\"120\"],[\"5\",\"120\",\"120\"],[\"6\",\"120\",\"120\"],[\"13\",\"120\",\"120\"],[\"14\",\"120\",\"120\"],[\"15\",\"120\",\"120\"],[\"16\",\"120\",\"120\"],[\"17\",\"120\",\"120\"],[\"18\",\"120\",\"120\"],[\"19\",\"120\",\"120\"],[\"20\",\"119\",\"119\"],[\"21\",\"120\",\"120\"],[\"22\",\"120\",\"120\"],[\"23\",\"120\",\"120\"],[\"24\",\"119\",\"119\"],[\"25\",\"119\",\"119\"],[\"26\",\"120\",\"120\"],[\"27\",\"10\",\"10\"],[\"28\",\"120\",\"120\"],[\"29\",\"120\",\"120\"],[\"32\",\"120\",\"120\"],[\"7\",\"10\",\"10\"],[\"30\",\"10\",\"10\"],[\"31\",\"10\",\"10\"]]', 62),
(10, '2019-05-29', '[[\"1\",\"130\",\"120\"],[\"2\",\"83\",\"10\"],[\"3\",\"10\",\"50\"],[\"4\",\"120\",\"115\"],[\"5\",\"120\",\"120\"],[\"6\",\"120\",\"110\"],[\"13\",\"120\",\"100\"],[\"14\",\"120\",\"100\"],[\"15\",\"120\",\"130\"],[\"16\",\"120\",\"120\"],[\"17\",\"120\",\"120\"],[\"18\",\"120\",\"120\"],[\"19\",\"99\",\"99\"],[\"20\",\"119\",\"119\"],[\"21\",\"120\",\"120\"],[\"22\",\"120\",\"120\"],[\"23\",\"120\",\"120\"],[\"24\",\"119\",\"119\"],[\"25\",\"119\",\"119\"],[\"26\",\"120\",\"120\"],[\"27\",\"10\",\"10\"],[\"28\",\"120\",\"120\"],[\"29\",\"120\",\"120\"],[\"32\",\"120\",\"120\"],[\"7\",\"10\",\"10\"],[\"30\",\"10\",\"10\"],[\"31\",\"10\",\"10\"]]', 63),
(11, '2019-05-29', '[[\"1\",\"120\",\"130\"],[\"2\",\"10\",\"100\"],[\"3\",\"50\",\"123\"],[\"4\",\"115\",\"100\"],[\"5\",\"120\",\"115\"],[\"6\",\"110\",\"110\"],[\"13\",\"100\",\"14\"],[\"14\",\"99\",\"42\"],[\"15\",\"130\",\"14\"],[\"16\",\"120\",\"242\"],[\"17\",\"120\",\"123\"],[\"18\",\"120\",\"124\"],[\"19\",\"97\",\"112\"],[\"20\",\"119\",\"123\"],[\"21\",\"120\",\"124\"],[\"22\",\"120\",\"120\"],[\"23\",\"120\",\"120\"],[\"24\",\"115\",\"115\"],[\"25\",\"119\",\"119\"],[\"26\",\"120\",\"120\"],[\"27\",\"10\",\"10\"],[\"28\",\"120\",\"120\"],[\"29\",\"120\",\"120\"],[\"32\",\"120\",\"120\"],[\"7\",\"10\",\"10\"],[\"30\",\"10\",\"4\"],[\"31\",\"10\",\"10\"]]', 64),
(12, '2019-05-29', '[[\"1\",\"130\",\"130\"],[\"2\",\"100\",\"100\"],[\"3\",\"123\",\"123\"],[\"4\",\"100\",\"100\"],[\"5\",\"115\",\"115\"],[\"6\",\"110\",\"110\"],[\"13\",\"14\",\"14\"],[\"14\",\"42\",\"42\"],[\"15\",\"14\",\"14\"],[\"16\",\"242\",\"242\"],[\"17\",\"123\",\"123\"],[\"18\",\"124\",\"124\"],[\"19\",\"-11\",\"-11\"],[\"20\",\"123\",\"123\"],[\"21\",\"124\",\"124\"],[\"22\",\"120\",\"120\"],[\"23\",\"120\",\"120\"],[\"24\",\"115\",\"115\"],[\"25\",\"119\",\"119\"],[\"26\",\"120\",\"120\"],[\"27\",\"10\",\"10\"],[\"28\",\"120\",\"120\"],[\"29\",\"120\",\"120\"],[\"32\",\"120\",\"120\"],[\"7\",\"10\",\"10\"],[\"30\",\"4\",\"4\"],[\"31\",\"10\",\"10\"]]', 65),
(13, '2019-05-29', '[[\"1\",\"130\",\"110\"],[\"2\",\"100\",\"90\"],[\"3\",\"123\",\"123\"],[\"4\",\"100\",\"100\"],[\"5\",\"115\",\"115\"],[\"6\",\"110\",\"110\"],[\"13\",\"14\",\"14\"],[\"14\",\"42\",\"42\"],[\"15\",\"14\",\"14\"],[\"16\",\"242\",\"242\"],[\"17\",\"123\",\"123\"],[\"18\",\"124\",\"124\"],[\"19\",\"-11\",\"24\"],[\"20\",\"99\",\"99\"],[\"21\",\"124\",\"124\"],[\"22\",\"120\",\"120\"],[\"23\",\"120\",\"120\"],[\"24\",\"115\",\"115\"],[\"25\",\"119\",\"119\"],[\"26\",\"120\",\"120\"],[\"27\",\"10\",\"10\"],[\"28\",\"120\",\"120\"],[\"29\",\"120\",\"120\"],[\"32\",\"120\",\"120\"],[\"7\",\"10\",\"10\"],[\"30\",\"4\",\"4\"],[\"31\",\"10\",\"10\"]]', 66),
(14, '2019-07-30', '[[\"1\",\"70\",\"24\"],[\"2\",\"50\",\"24\"],[\"3\",\"100\",\"24\"],[\"4\",\"120\",\"24\"],[\"5\",\"110\",\"243\"],[\"6\",\"90\",\"52\"],[\"13\",\"14\",\"25\"],[\"14\",\"40.5\",\"25\"],[\"15\",\"14\",\"51\"],[\"16\",\"242\",\"24\"],[\"17\",\"123\",\"62\"],[\"18\",\"120.5\",\"44\"],[\"19\",\"13.84\",\"162\"],[\"20\",\"99\",\"22\"],[\"21\",\"124\",\"46\"],[\"22\",\"120\",\"35\"],[\"23\",\"120\",\"25\"],[\"24\",\"111.5\",\"235\"],[\"25\",\"111.45\",\"235\"],[\"26\",\"120\",\"53\"],[\"27\",\"4.42\",\"53\"],[\"28\",\"120\",\"25\"],[\"29\",\"120\",\"552\"],[\"32\",\"120\",\"5\"],[\"7\",\"9\",\"3\"],[\"30\",\"3\",\"52\"],[\"31\",\"8\",\"52\"]]', 188);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuotas`
--

CREATE TABLE `cuotas` (
  `id` int(3) NOT NULL,
  `idSocio` int(5) NOT NULL,
  `fechapago` date NOT NULL,
  `fechafin` date NOT NULL,
  `meses` int(2) NOT NULL,
  `cuota` int(3) NOT NULL,
  `idCaja` int(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cuotas`
--

INSERT INTO `cuotas` (`id`, `idSocio`, `fechapago`, `fechafin`, `meses`, `cuota`, `idCaja`) VALUES
(3, 0, '2018-02-12', '2019-02-12', 12, 0, 0),
(4, 0, '2018-02-12', '2019-02-12', 12, 0, 0),
(5, 0, '2018-02-12', '2019-02-12', 12, 0, 0),
(6, 0, '2018-02-12', '2019-02-12', 12, 0, 0),
(7, 0, '2017-02-12', '2017-08-12', 6, 0, 0),
(8, 0, '2018-02-11', '2019-02-11', 12, 0, 0),
(9, 0, '2018-02-12', '2019-02-12', 12, 0, 0),
(10, 0, '2018-01-24', '2019-01-24', 12, 0, 0),
(11, 0, '2018-10-05', '2019-04-05', 6, 0, 0),
(13, 0, '2017-06-13', '2017-12-13', 6, 0, 0),
(14, 0, '2018-03-22', '2019-03-22', 12, 0, 0),
(15, 0, '2018-11-09', '2019-11-09', 12, 0, 0),
(16, 0, '2018-04-06', '2019-04-06', 12, 0, 0),
(18, 0, '2018-08-12', '2019-02-12', 6, 0, 0),
(20, 0, '2018-04-08', '2018-10-08', 6, 0, 0),
(22, 0, '2017-06-14', '2018-06-14', 12, 0, 0),
(23, 0, '2017-09-22', '2018-03-22', 6, 0, 0),
(24, 0, '2018-08-10', '2019-08-10', 12, 0, 0),
(25, 0, '2017-05-17', '2018-05-17', 12, 0, 0),
(27, 0, '2017-10-28', '2018-04-28', 6, 0, 0),
(28, 0, '2017-06-13', '2018-06-13', 12, 0, 0),
(29, 0, '2018-08-24', '2019-08-24', 12, 0, 0),
(30, 0, '2018-08-24', '2019-08-24', 12, 0, 0),
(31, 0, '2017-06-28', '2017-12-28', 6, 0, 0),
(32, 0, '2017-10-27', '2018-04-27', 6, 0, 0),
(33, 0, '2017-06-09', '2017-12-09', 6, 0, 0),
(34, 0, '2017-11-05', '2018-05-05', 6, 0, 0),
(35, 0, '2019-01-31', '2020-01-31', 12, 0, 0),
(36, 0, '2018-08-24', '2019-02-24', 6, 0, 0),
(38, 0, '2018-08-01', '2019-02-01', 6, 0, 0),
(39, 0, '2018-06-21', '2018-12-21', 6, 0, 0),
(40, 0, '2018-03-06', '2018-09-06', 6, 0, 0),
(41, 0, '2018-03-06', '2018-09-06', 6, 0, 0),
(42, 0, '2018-04-30', '2018-10-30', 6, 0, 0),
(43, 0, '2018-04-06', '2018-10-06', 6, 0, 0),
(44, 0, '2017-10-12', '2018-04-12', 6, 0, 0),
(45, 0, '2017-10-26', '2018-04-26', 6, 0, 0),
(46, 0, '2018-02-09', '2018-08-09', 6, 0, 0),
(47, 0, '2017-05-24', '2017-11-24', 6, 0, 0),
(48, 0, '2017-10-10', '2018-04-10', 6, 0, 0),
(49, 0, '2017-10-06', '2018-04-06', 6, 0, 0),
(50, 0, '2016-02-23', '2016-08-23', 6, 0, 0),
(51, 0, '2016-11-15', '2017-11-15', 12, 0, 0),
(52, 0, '2017-11-11', '2018-05-11', 6, 0, 0),
(53, 0, '2018-07-17', '2019-01-17', 6, 0, 0),
(54, 0, '2017-11-05', '2018-11-05', 12, 0, 0),
(55, 0, '2017-11-05', '2018-11-05', 12, 0, 0),
(56, 0, '2017-11-18', '2018-05-18', 6, 0, 0),
(57, 0, '2017-12-30', '2018-12-30', 12, 0, 0),
(58, 0, '2016-09-25', '2017-03-25', 6, 0, 0),
(59, 0, '2018-06-24', '2018-12-24', 6, 0, 0),
(60, 0, '2017-12-01', '2018-06-01', 6, 0, 0),
(61, 0, '2017-12-30', '2018-06-30', 6, 0, 0),
(62, 0, '2016-05-26', '2016-11-26', 6, 0, 0),
(63, 0, '2018-05-10', '2018-11-10', 6, 0, 0),
(64, 0, '2018-05-10', '2018-11-10', 6, 0, 0),
(65, 0, '2018-07-17', '2019-01-17', 6, 0, 0),
(66, 0, '2017-12-26', '2018-12-26', 12, 0, 0),
(67, 0, '2018-01-06', '2018-07-06', 6, 0, 0),
(68, 0, '2018-02-10', '2019-02-10', 12, 0, 0),
(69, 0, '2018-02-15', '2018-08-15', 6, 0, 0),
(70, 0, '2018-02-28', '2018-08-28', 6, 0, 0),
(71, 0, '2018-02-28', '2018-08-28', 6, 0, 0),
(72, 0, '2018-03-03', '2019-03-03', 12, 0, 0),
(73, 0, '2018-03-20', '2019-03-20', 12, 0, 0),
(74, 0, '2018-03-20', '2018-09-20', 6, 0, 0),
(75, 0, '2018-04-20', '2018-10-20', 6, 0, 0),
(76, 0, '2018-05-31', '2019-05-31', 12, 0, 0),
(77, 0, '2018-05-31', '2019-05-31', 12, 0, 0),
(78, 0, '2018-07-22', '2019-01-22', 6, 0, 0),
(79, 0, '2018-07-17', '2019-01-17', 6, 0, 0),
(80, 0, '2018-08-10', '2019-08-10', 12, 0, 0),
(81, 0, '2018-08-24', '2019-08-24', 12, 0, 0),
(82, 0, '2018-08-29', '2019-08-29', 12, 0, 0),
(83, 0, '2018-09-04', '2019-09-04', 12, 0, 0),
(84, 0, '2018-09-08', '2019-03-08', 6, 0, 0),
(85, 0, '2018-10-19', '2019-04-19', 6, 0, 0),
(86, 0, '2018-10-02', '2019-04-02', 6, 0, 0),
(87, 0, '2018-11-12', '2019-05-12', 6, 0, 0),
(88, 0, '2018-11-12', '2019-05-12', 6, 0, 0),
(91, 0, '2019-03-13', '2020-03-13', 12, 0, 0),
(92, 0, '2019-03-13', '2020-03-13', 12, 0, 0),
(94, 0, '2019-03-18', '2021-03-18', 24, 0, 0),
(95, 0, '2019-03-18', '2021-03-18', 24, 0, 0),
(97, 0, '2019-03-18', '2023-05-18', 50, 0, 0),
(99, 1, '2017-05-07', '2018-05-07', 16, 0, 0),
(103, 1, '2018-05-07', '2019-05-07', 12, 40, 34),
(104, 1, '2019-05-07', '2020-05-07', 12, 40, 34),
(105, 7, '0000-00-00', '0000-00-00', 6, 20, 36),
(107, 91, '2019-05-15', '2021-05-15', 24, 50, 36),
(108, 89, '0000-00-00', '0000-00-00', 6, 20, 37),
(109, 89, '2019-05-15', '2019-11-15', 6, 20, 37),
(111, 4, '2019-05-17', '2020-05-17', 12, 20, 38),
(112, 7, '0000-00-00', '0000-00-00', 12, 40, 40),
(113, 7, '2019-05-16', '2020-05-16', 12, 30, 40),
(114, 90, '2019-05-16', '2020-05-16', 12, 40, 42),
(115, 99, '2019-05-20', '2020-05-20', 12, 40, 0),
(116, 99, '2019-05-20', '2020-05-20', 12, 40, 0),
(117, 99, '2019-05-20', '2020-05-20', 12, 40, 0),
(118, 99, '2019-05-20', '2020-05-20', 12, 40, 0),
(119, 99, '2019-05-20', '2020-05-20', 12, 40, 0),
(120, 99, '2019-05-20', '2020-05-20', 12, 40, 0),
(121, 99, '2019-05-20', '2020-05-20', 12, 40, 0),
(122, 99, '2019-05-20', '2020-05-20', 12, 40, 0),
(123, 99, '2019-05-20', '2020-05-20', 12, 40, 0),
(124, 99, '2019-05-20', '2020-05-20', 12, 40, 0),
(125, 107, '2019-05-20', '2019-07-20', 2, 3, 0),
(126, 107, '2019-05-20', '2019-07-20', 2, 3, 0),
(127, 107, '2019-05-20', '2019-07-20', 2, 3, 0),
(128, 107, '2019-05-20', '2019-07-20', 2, 3, 0),
(129, 107, '2019-05-20', '2019-07-20', 2, 3, 0),
(130, 107, '2019-05-20', '2019-07-20', 2, 3, 0),
(131, 107, '2019-05-20', '2019-07-20', 2, 3, 0),
(132, 107, '2019-05-20', '2019-07-20', 2, 3, 0),
(133, 107, '2019-05-20', '2019-07-20', 2, 3, 0),
(140, 116, '2017-11-10', '2018-11-10', 12, 30, 43),
(141, 116, '2018-11-10', '2019-05-10', 6, 20, 43),
(142, 116, '2019-05-10', '2019-11-10', 6, 20, 43),
(144, 121, '2019-05-28', '2020-05-28', 12, 24, 51),
(146, 96, '2019-05-29', '2020-05-29', 12, 10, 61),
(147, 77, '2019-05-29', '2020-05-29', 12, 30, 62),
(148, 35, '2016-06-11', '2017-06-11', 12, 2, 70),
(149, 35, '2017-06-11', '2018-06-11', 12, 40, 70),
(150, 35, '2018-06-11', '2019-06-11', 12, 40, 70),
(160, 122, '2019-06-10', '2021-06-10', 24, 2, 0),
(161, 35, '2019-06-11', '2019-12-11', 6, 20, 76);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `geneticas`
--

CREATE TABLE `geneticas` (
  `id` int(3) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `descripcion` text NOT NULL,
  `img` varchar(300) NOT NULL,
  `categoria` varchar(50) NOT NULL,
  `tipo` varchar(30) NOT NULL,
  `precio` float NOT NULL,
  `stock` float NOT NULL,
  `banco` varchar(30) NOT NULL,
  `pindica` varchar(15) NOT NULL,
  `psativa` varchar(15) NOT NULL,
  `pthc` varchar(15) NOT NULL,
  `pcbd` varchar(15) NOT NULL,
  `gusto` varchar(100) NOT NULL,
  `fecha` date NOT NULL,
  `destacada` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `geneticas`
--

INSERT INTO `geneticas` (`id`, `nombre`, `descripcion`, `img`, `categoria`, `tipo`, `precio`, `stock`, `banco`, `pindica`, `psativa`, `pthc`, `pcbd`, `gusto`, `fecha`, `destacada`) VALUES
(1, 'Space', 'Esta semilla de marihuana es de origen Afgano con grandísimos beneficios medicinales por su alto contenido                        ', '1.jpg', 'indica', 'verde', 6, 24, 'BANCO', '10', '20', '22', 'Bajo', 'Dulce, pino y madera', '2018-07-12', 1),
(2, 'Critical CBD', 'La índica perfecta para cultivadores y consumidores esporádicos\nCritical Mass CBD es una semilla de marihuana feminizada de dominancia índica de Dinafem Seeds diseñada para aquellos que no tienen mucho espacio ni demasiado tiempo para cultivar cannabis. También para quienes no buscan un efecto muy potente al consumir marihuana.', '2.jpg', 'indica', 'verde', 5, 24, 'Cannaweed', '22', '20', '20%', 'bajo', 'Limón, cítrico', '2018-07-12', 0),
(3, 'Pineapple Kush', 'Puede que Pineapple Kush sea un cruce entre dos cepas muy populares, pero la popularidad que ha acumulado es el resultado de la experta mezcla de esas dos cepas. Mientras que a veces se confunde con Pineapple Express porque ambos comparten un sabor tropical de piña, esta variedad destaca por la alta potencia que posee como resultado de ser cultivada a partir de la conocida cepa OG Kush', '3.jpg', 'sativa', 'verde', 6, 24, 'cannaweed', '21', '14', '60', 'alto', 'Pino, afrutado', '2018-07-12', 0),
(4, 'Mataro', 'El genotipo más difícil de todo el catálogo es un cruce a 3 vías que cuenta con lo mejor de varias genéticas predominantemente índicas. Entre ellas la Blue Monster que es muy productiva y que mantiene todas las propiedades por las que es conocida esta variedad (poca altura, floración rápida y tonos azulados).', '4.jpg', 'indica', 'verde', 6, 24, 'BancoSemillas', '25', '30', '52%', 'Alto', 'Pera menta', '2018-07-12', 1),
(5, 'Mango', 'Mango del banco de semillas KC Brains es el resultado del cruce entre tres variedades muy apreciadas en el mercado cannabico internacional como son:Kc Special Select x KC33 x Afghani.\nEl resultado tras el cruce de estas preciadas genéticas es una planta de marihuana capaz de combinar las mejores características de sus progenitoras. ', '5.png', 'indica', 'verde', 6, 243, 'cannaweed', '20', '10', '40', 'bajo', 'Mango, afrutada', '2018-07-12', 0),
(6, 'White Widow', 'Qué se puede decir de White Widow que no se haya dicho ya. Las semillas de cannabis de esta variedad mítica de los 90 son unas de las más demandadas del catálogo de Dinafem Seeds. ¿Por qué? Porque se trata de una de las mejores variedades de marihuana índicas que ha visto el mundo. Descendiente de un clon de élite del año 97, nuestra White Widow es una maravillosa y vigorosa planta de floración corta, con altos niveles de THC, una impresionante capacidad de producir resina y un delicioso aroma agridulce. Eso sin contar que, si existe una variedad con indiscutibles propiedades relajantes, esta es White Widow.', '6.jpg', 'indica', 'verde', 5, 52, 'cannaweed', '80', '24', '80', 'medio', 'Madera, pino', '2018-07-12', 0),
(7, 'Rubio', 'Descripcion Descripcion DescripcionDescripcionDescripcion Descripcion DescripcionDescripcionv Descripcionv Descripcion', '7.jpg', 'indica', 'marron', 4, 3, 'Isla Bonita', '', '', '', '', '', '2018-08-02', 1),
(13, 'Big Bud', 'Big Bud ha tenido por mucho tiempo fama de variedad de cannabis con rendimiento asombroso. Menos conocido es el hecho que su óptima herencia Indica ha sido cuidadosamente refinada y continuamente mejorada en el correr de los años. \nEl desarrollo primario de Big Bud ocurrió en los Estados Unidos, antes que en los años ochenta la variedad fuera traída a Holanda en busca de asilo político. Cuando se intensificó la Guerra a las Drogas, cultivadores pioneros norteamericanos temieron perder variedades de ganja irremplazables que significaban años de trabajo, estudio y dedicación. Muchos genotipos únicos y muy apreciados fueron llevados a los Países Bajos para asegurar que no se perdieran para siempre. \n\nLa Big Bud primaria era una Afgana robusta engendrada con genes de Skunk, un híbrido de vigor inusual e inesperado, con una producción que rompía todos los cánones. Distribuida en forma de clon, Big Bud rápidamente se convirtió en una variedad comercial popular. Cuando aquellos clones originales viajaron a Holanda, fueron la base para varias variedades estables, incluyendo cruzas con Skunk #1 y un híbrido muy popular de 75%BB-25%NL#1. Dos desarrollos importantes hicieron posible la variedad corriente y premiada de Big Bud. En primer lugar, investigaciones intensivas permitieron a Sensi identificar el genotipo y origen geográfico exacto del cultivo Afgani en Big Bud. En segundo lugar, la expansión del stock de genes a comienzo de los años noventa permitió a los cultivadores de Sensi acceder a un ejemplar primario de esta variedad. \n\nCon estas dos bases fuimos capaces de ver los orígenes de Big Bud. El híbrido final cruzado de Sensi recaptura el vigor del clon original y además se beneficia del sabor suave, afrutado, y de su enorme mejoramiento en potencia. \nLa enorme producción continúa siendo, naturalmente, la característica más destacada de la variedad de semillas Big Bud. Las plantas que tienen su tiempo de floración completo crecerán algo más altas y son por regla general los ejemplares de mayor rendimiento. Los tallos de Big Bud son gruesos y robustos, aunque las masas de cogollos gigantescos, dulces y brillantes pueden crecer tan pesadas que llegan a doblarlos y hasta romperlos, por lo cual las ramas más prometedoras deben ayudarse con lazos o cordeles tensados. ', '13.png', 'indica', 'verde', 5, 26, ' ', ' ', ' ', ' ', ' ', 'Pino, terroso y especias', '2018-10-24', 1),
(14, 'Black Dog', 'Black D.O.G. es una semilla de cannabis que produce un aroma y un sabor muy intensos, con\nnotas a frutos del bosque, a uva y a petróleo. Es una cepa de marihuana con un potente y\nduradero efecto, inicialmente cerebral, que en una segunda fase se transforma en una\nplacentera sensación de relajación corporal.', '14.png', 'indica', 'verde', 6, 25, ' Humboldt Seeds', ' 85', ' 15', ' 16-21', ' bajo', 'Uva, frutas del bosque y petróleo', '2018-10-24', 1),
(15, 'Black Domina', 'Semillas de marihuana Black Domina feminizada\nBlack Domina feminizada por fin disponible, una de las cepas de Cannabis más apreciadas ahora en versión femenina. Black Domina es casi Índica pura, obtenida crzando los mejores linajes Índica de Sensi Seeds.\n\nLa marihuana Black Domina feminizada ofrece una gran estabilidad al cultivador, plantas homogeneas y sin problemas, robustas y con buen vigor desde el primer momento. Esta maravilla se ha creado a partir de Northern Lights, Ortega Índica, Hash Plant y Afghani #1 ¿qué más pueden pedir los amantes de las Índicas? en nuestra opinión nada, ya que Black Domina feminizada te lo da todo.', '15.png', 'indica', 'verde', 6, 12, ' ', ' ', ' ', ' ', ' ', 'Picante y pimienta', '2018-10-24', 1),
(16, 'Black Widow CBD', 'Ahora en Alchimia la última creación de Positronics y CBD Crew, un híbrido entre una planta rica en Cannabidiol con el clon élite de Black Widow buscando aportarle a esta genética una carga elevada de este cannabinoide medicinal.\n\nHablamos de una planta fuerte, compacta y de baja estatura, con ramas secundarias bajas bastante pegadas al grueso tronco central.\n\nSus internodos son cortos, y su crecimiento columnar, recordándonos a las genéticas OGKush en cuanto a su aspecto, lo que la hace ideal para interior o cultivos en Mar Verde.', '16.png', 'indica', 'verde', 5, 24, ' ', '', '', '', '', ' Afrutado, dulce y torrado', '2018-10-24', 0),
(17, 'Mazar', 'Mazar de Dutch Passion es una de las variedades más robustas y fáciles de cultivar que tiene este banco holandés. La planta original viene de los años 80 y se ha retro-cruzado y seleccionado el clon más estable y productivo. Una planta que todo cultivador debe probar para saber de qué pasta está hecha esta genética. Una planta proveniente de un cruce de una afgana de la región Mazar-i-Shariff con una Skunk que le da algo más de porte y endulza el sabor.\n\nEn interior tenemos ejemplares de no más de un metro de altura pero muy ramificados; presentan una estructura muy clásica, recordando a un árbol de navidad. El aroma que produce cuando esta en floración es tremendamente fuerte, por lo que aconsejamos el uso de flitros anti-olor u otros sistemas de tratamiento de olores para paliar la fuerte fragancia que desprende. Grandes resultados de hasta 500g con tan solo unas 7 plantas por m2 podremos conseguir con Mazar de Dutch Passion. Una cepa que tiene un a floración bastante larga, pues estará lista en unas 9 semanas de floración.', '17.png', 'indica', 'verde', 6, 62, ' ', '', '', '', '', 'Afrutado Afghan y dulce Skunk', '2018-10-24', 0),
(18, 'Amnesia Haze', 'Después de la llegada de la genética haze en Holanda y de la obtención de varias híbridas, un expatriado americano combinó una de estas híbridas con un macho derivado de semillas haze de la vieja escuela para crear la Amnesia Haze.', '18.png', 'sativa', 'verde', 6, 44, '', '', '', '', '', 'Terroso, limón y cítrico', '2018-10-24', 0),
(19, 'Azure Haze', 'Bendecida con los genes de los dioses del ganja, la Azure Haze es el nuevo brote del bloque. Una Silver Haze madre cruzada con una Blueberry Indica macho, han dado lugar el mismo cruce que la Blue Dream. Dicho esto, la Azure Haze es en realidad una generación más cercana a su ascendencia autóctona P1. El cruce de la Blue Dream fue entre una F5 Blueberry macho y la F4 de Azure.\n\nEn cuanto a las características de la Azure Haze, los productores estarán encantados de saber que se trata de una planta muy fácil de cuidar - independientemente de su alta estatura. Este híbrido da muy poco trabajo tanto a los cultivadores novatos como a los expertos. Con un hermoso aspecto, la Azure Haze desarrolla unas alargadas flores en forma de lanza, que están densamente pobladas de pequeños (pero pesados) brotes. Su un poco lento periodo de floración, de un máximo de 10 semanas (70 días), es sólo un pequeño inconveniente teniendo en cuenta la cantidad de brotes que puedes cosechar Su alto rendimiento es solo la punta del iceberg.\n\nAl principio, los usuarios experimentarán sus efectos edificantes casi desde el primer momento, para pasar luego a un estado de ensueño - algo normal con una Indica. Los efectos son a la vez fuertes y duraderos, con una mínima acumulación de tolerancia. Una oferta afrutada, la Azure Haze es un combinado de melón, bayas, cítricos y notas verdes.\n\nLa Azura Haze es el tipo de cepa para disfrutar en compañía de amigos, día o noche.', '19.png', 'sativa', 'verde', 6, 162, '', '', '', '', '', 'Ahumados, de terrosos a dulces', '2018-10-24', 0),
(20, 'Bubble Gum', 'La variedad de marihuana Bubble Gum de Serious Seeds, como todas las genéticas de este banco de semillas, se volvió rápidamente muy famosa dentro de la comunidad de los cultivadores de marihuana, por sus aromas azucarados con sabor a chicle y su efecto high entusiasta.\n\nSe tratará de una planta de tamaño mediano que produce pocas ramas, pero cogollos compactos y cubiertos de tricomas.\n\nBubblegum es originaria de Indiana, en los Estados-Unidos, y viajó hasta Nueva Inglaterra, antes de llegar a los Países Bajos en donde el equipo de Serious Seeds la trabajó durante varias generaciones.', '20.png', 'sativa', 'verde', 6, 22, '', '', '', '', '', 'Chicle dulce, sándalo y especias', '2018-10-24', 0),
(21, 'Moby Dick', 'Moby Dick de Dinafem es la variedad más productiva que ofrece el catálogo de éste prestigioso banco nacional. Una apuesta segura si lo que buscamos es un monstruo gigantesco con dominancia sativa, el cual tiene unos olores y sabores muy buscados. Su genética proviene de dos cepas muy famosas en el mundo entero, como son Haze y White Widow. Su parte Haze le aporta un crecimiento desmesurado además de una resistencia frente a hongos muy alta, y su parte White Widow es la encargada de hacer brotar la resina de las apretadas flores que ofrece. Esta variedad lleva muchos años en el mercado, y cada vez más son los usuarios que la han probado y han quedado asombrados con los cogollos que regala. Moby Dick precisa grandes dosis de fertilizante en cualquier medio donde la plantemos, acabará rápidamente con nuestros abonos sin llegar a marcar ni un exceso. Excelente opción para cualquier cultivador que busque grandes cosechas en un híbrido versátil y muy estable.', '21.png', 'sativa', 'verde', 6, 46, '', '', '', '', '', 'Madera y dulce', '2018-10-24', 0),
(22, 'Purple Haze', 'Planta de enorme vigor, grandes tallos de grosor medio que adquieren tonalidades púrpuras. Gran distancia internodal, comparte patrón de crecimiento con Claustrum, repartiendo sus ramas en forma de abeto o candelabro, optimizando así la producción de racimos florales.\nLas flores adquirirán tonos púrpuras con matices que van desde el violetaz al granate, dandose una policromía única en esta variedad, todo un deleite visual.\nEfecto muy psicoactivo y duradero. Contágiate del espíritu hippie.', '22.png', 'sativa', 'verde', 6, 35, '', '', '', '22', '0.2', '', '2018-10-24', 0),
(23, 'Super Silver Haze', 'La Super Silver Haze de Green House Seeds es una de las Sativa las más famosas disponible en el mercado, encontraras esta maravillosa genética en el catalogo de semillas de marihuana de Alchimia.\n\nLa Super Silver Haze es una genética que ha recibido diferentes premios durante las Cannabis Cup. Se trata de un gran clásico de los coffeshops Holandeses de finales de los años 90. De hacho la Famosa Amnesia no es nada más que una selección de Super Silver Haze.\n\nEsta planta principalmente Sativa es muy vigorosa durante el crecimiento y desarrolla hojas largas y finas. El pasaje a floración es sinónimo de un estiramiento consecuente que se deberá controlar si la planta a recibido un crecimiento alargado. La planta produce flores largas y estiradas, que se cubrirán de una capa de resina blanca a mediado de floración.', '23.png', 'sativa', 'verde', 6, 25, '', '20', '80', '', '', 'Incienso y picante', '2018-10-24', 0),
(24, 'Babylonia fruit', 'Babylonia Fruit, de Vulkania Seeds, es una variedad de marihuana con genética índica procedente de Nepal.\n\nBabylonia Fruit presenta un crecimiento vigoroso con una distancia internodal media. Si la cultivamos de esqueje se puede poner directamente a floración. A tener en cuenta que se comporta muy bien utilizando la técnica de cultivo de Sea Of Green (SOG).', '24.png', 'hibrida', 'verde', 6, 235, '', '', '', '', '', 'Dulce, afrutado y tropical', '2018-10-24', 1),
(25, 'Candy Kush', ' 23    0  15 Dec 2014\nCandy Kush fue creada por la increíble demanda de una variedad de cannabis que pudiese producir el efecto eufórico sin tener el efecto noqueador que te deja tirado en el sofá. La cepa Candy Kush contiene un 25% de Sativa y un 75% de Indica. Esta combinación ofrece un efecto de colocón maravilloso.\n\nAl fumarla, nos quedamos con un sabor agradable, ligero y afrutado. Como Candy Kush es una cepa híbrida, contiene todo un abanico de sabores dulces a frutos rojos y un aroma floral muy agradable. Cuando fumo Candy Kush, siento un impulso rápido de un efecto que dura horas. Con un contenido de un 18% de THC, su fuerte efecto me permite centrarme con facilidad en ciertas tareas y no distraerme con todo lo que pasa a mi alrededor. Me hace sentirme calmado y me despeja.', '25.png', 'hibrida', 'verde', 6, 235, '', '', '', '', '', 'Cítrico, dulce y acre', '2018-10-24', 0),
(26, 'Caramel Ice', 'Caramelice de Positronics es una variedad muy potente con muy buen sabor, sin renunciar a altas cosechas de cogollos nevados en resina. Un fondo acaramelado aparecerá inundando la habitación, pero sólo si la dejamos madurar correctamente durante sus últimos días.\n\nEn interior la podremos cosechar en unos 60 días de floración, sacando fácilmente unos 400g si ponemos unas 10 plantas por m2. Las flores terminan con ese tono naranja radiactivo característico de las genéticas Skunk que le darán ese toque extremadamente llamativo nada más ver la flor. Unas ramas muy poco formadas y pegadas al tallo central, lo que le otorga una estructura perfecta para cultivos masivos, donde se busquen ejemplares productivos y no muy ramificados.', '26.png', 'hibrida', 'verde', 6, 53, '', '', '', '', '', 'naranja, lima y caramelo', '2018-10-24', 1),
(27, 'Cheese', 'Esta variedad, llamada de esta manera debido a su sabor dulzón, fuerte y de larga duración que nos recuerda al queso curado con toques de incienso, es muy valorada y comparada en importancia con otras variedad como la Critical + y la Skunk, con fama por todo el mundo.\n\nLa cheese es una planta larga, de hojas alargadas y tallos estrechos que nos dará un resultado visible a partir de las 8 semanas. Las semillas de marihuana cheese tienen un mantenimiento sencillo con muy buenos resultados para aquellos que quieren iniciarse en el mundo del cultivo de marihuana.', '27.png', 'hibrida', 'verde', 6, 53, '', '', '', '', '', 'Queso, terroso y picante', '2018-10-24', 0),
(28, 'Grape Fruit', 'La GRAPEFRUIT de Female Seeds es un Sativa new style, de maduración temprana y rápida floración. Sus yemas tienen un suave aroma a pomelo.\n\nSu sabor es dulce y ofrece un intenso punto tropical. Es un 75% C?99 x un 25% de potente sativa frutal, estabilizado en una maduración temprana desde hace ya algunas generaciones.\n\nLas plantas alcanzan los 50-60 cm de altura, si se colocan bajo 12/12 horas una semana después de la germinación.', '28.png', 'hibrida', 'verde', 6, 25, '', '', '', '', '', 'Uva, dulce y tropical', '2018-10-24', 1),
(29, 'Lemon Kush', 'La variedad Lemon Kush feminizada es un cruce de Critical con una Kush procedente de las montañas de mismo nombre que entre Pakistán y Afganistán.\n\nLemon Kush es muy fácil de cultivar y crece bastante para la genética que es, ya sea cultivada en interior o exterior.\n\nEsta cepa es principalmente Índica, con un pequeño toque de Sativa. Su aspecto es bastante compacto, desarrolla cogollos muy densos y al final de floración pega un pequeño estirón así que hay que asegurarse de que finaliza bien.\n\nLa marihuana Lemon Kush tiene un sabor a hierba cítrica, con un fuerte aroma a limón. El efecto es complejo, afectando de manera física y cerebral.\n\nEsta variedad de Cannabis finaliza su ciclo de floración en solamente 8 o 9 semanas, llegando a crecer en interior hasta una altura de unos 100 - 120 cm. En exterior se puede recoger desde finales de Septiembre hasta mediados de Octubre.', '29.png', 'hibrida', 'verde', 6, 552, '', '', '', '', '', 'Limón, cítrico y dulce', '2018-10-24', 0),
(30, 'OG Kush', 'Originaria del Norte de California, esta cepa de cannabis se ha dado a conocer rápidamente en todo el mundo por su distintivo aroma y sus fuertes efectos. La OG Kush es una variedad especialmente única que no se puede comparar con ninguna otra cepa de marihuana', '30.png', 'indica', 'marron', 6, 52, '', '', '', '', '', '', '2019-01-29', 0),
(31, 'Gordo Master', 'Gordo master', '31.png', 'hibrida', 'marron', 7, 52, '', '', '', '', '', '', '2019-01-29', 0),
(32, 'asdasd', 'asdasdasd', '32.png', 'indica', 'verde', 5, 12, '', '', '', '', '', '', '2019-01-31', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reg_gen`
--

CREATE TABLE `reg_gen` (
  `id` int(10) NOT NULL,
  `idGen` int(5) NOT NULL,
  `cant` int(5) NOT NULL,
  `cantAnterior` int(5) NOT NULL,
  `idUsuario` int(4) NOT NULL,
  `fechaHora` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `reg_gen`
--

INSERT INTO `reg_gen` (`id`, `idGen`, `cant`, `cantAnterior`, `idUsuario`, `fechaHora`) VALUES
(3, 15, 12, 41, 6, '2019-08-30 10:20:06'),
(4, 32, 24, 7, 6, '2019-08-30 10:29:28'),
(5, 32, 25, 24, 6, '2019-08-30 10:30:05'),
(6, 32, 12, 25, 6, '2019-08-30 10:32:27'),
(7, 32, 42, 12, 6, '2019-08-30 10:32:32'),
(8, 32, 41, 42, 6, '2019-08-30 10:32:35'),
(9, 32, 12, 41, 6, '2019-08-30 10:32:39');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `retiradas`
--

CREATE TABLE `retiradas` (
  `id` int(7) NOT NULL,
  `idSocio` int(3) NOT NULL,
  `articulos` text NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `idUsuario` int(2) NOT NULL,
  `idMod` int(11) NOT NULL,
  `manual` tinyint(1) NOT NULL,
  `precio_total` float NOT NULL,
  `idCaja` int(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `retiradas`
--

INSERT INTO `retiradas` (`id`, `idSocio`, `articulos`, `fecha`, `idUsuario`, `idMod`, `manual`, `precio_total`, `idCaja`) VALUES
(30, 35, '[[1,\"Candy Kush\",\"2\",6,\"v\"],[1,\"Caramel Ice\",\"1\",6,\"v\"]]', '2019-01-31 15:32:27', 6, 2, 0, 18, 0),
(31, 35, '[[1,\"Candy Kush\",10,6,\"v\"],[2,\"Mechero CLIPPER\",1,1],[2,\"Librillo OCB Slim\",1,1],[2,\"Librillo OCB\",1,1],[1,\"Caramel Ice\",3,6,\"v\"],[2,\"Monster\",1,1.5],[2,\"Kas Naranja\",1,1],[2,\"Aquarade\",1,1]]', '2019-02-01 08:26:53', 6, 0, 0, 84.5, 0),
(32, 35, '[[1,\"Babylonia fruit\",\"5\",6,\"v\"]]', '2019-02-15 14:16:01', 6, 6, 0, 30, 0),
(33, 3, '[[1,\"Pineapple Kush\",\"30\",6,\"v\"],[1,\"Gordo Master\",\"1\",7,\"m\"],[1,\"Grape Fruit\",\"10\",6,\"v\"],[1,\"Lemon Kush\",\"11\",6,\"v\"],[2,\"Mechero CLIPPER\",\"1\",1]]', '2019-02-07 13:14:12', 6, 6, 1, 314, 0),
(34, 2, '[[1,\"Grape Fruit\",\"1\",6,\"v\"]]', '2019-02-07 13:30:46', 6, 0, 1, 6, 0),
(35, 35, '[[1,\"Grape Fruit\",\"5\",6,\"v\"],[2,\"Mechero CLIPPER\",\"5\",1]]', '2019-02-06 14:44:26', 6, 6, 0, 35, 0),
(38, 35, '[[1,\"Grape Fruit\",\"1\",6,\"v\"],[1,\"Critical CBD\",\"2\",5,\"v\"],[1,\"Gordo Master\",\"2\",7,\"m\"],[1,\"Moby Dick\",\"3\",6,\"v\"],[1,\"Lemon Kush\",\"1\",6,\"v\"],[1,\"Rubio\",\"1\",4,\"m\"],[2,\"Librillo OCB Slim\",\"1\",1],[2,\"Mechero CLIPPER\",\"1\",1],[2,\"Kas Naranja\",\"4\",1],[2,\"Agua\",\"6\",0.5],[2,\"Maxibon\",\"1\",2]]', '2019-02-16 14:25:02', 6, 6, 1, 69, 0),
(39, 35, '[[1,\"Babylonia fruit\",1,6,\"v\"]]', '2019-02-20 16:10:19', 6, 0, 0, 6, 0),
(40, 3, '[[1,\"Babylonia fruit\",\"2.5\",6,\"v\"],[2,\"Mechero CLIPPER\",\"1\",1]]', '2019-04-15 15:25:34', 6, 0, 1, 16, 10),
(41, 35, '[[1,\"Babylonia fruit\",1,6,\"v\"]]', '2019-04-15 15:25:36', 6, 0, 0, 6, 10),
(42, 35, '[[1,\"Moby Dick\",\"1\",6,\"v\"],[2,\"Aquarade\",\"1\",1]]', '2019-04-15 13:44:32', 6, 0, 1, 7, 11),
(43, 1, '[[2,\"Aquarade\",\"1\",1]]', '2019-04-15 13:48:05', 6, 0, 1, 1, 12),
(44, 1, '[[2,\"Librillo OCB Slim\",\"1\",1]]', '2019-04-15 13:49:12', 6, 0, 1, 1, 12),
(45, 2, '[[1,\"Grape Fruit\",\"3\",6,\"v\"],[2,\"Aquarade\",\"2\",1]]', '2019-04-16 12:59:49', 6, 0, 1, 20, 13),
(46, 4, '[[1,\"Moby Dick\",\"2.5\",6,\"v\"]]', '2019-04-16 13:00:34', 6, 0, 1, 15, 14),
(47, 4, '[[1,\"Rubio\",\"2.5\",4,\"m\"],[2,\"Agua\",\"2\",0.5]]', '2019-04-16 13:03:36', 6, 0, 1, 11, 15),
(48, 2, '[[1,\"Babylonia fruit\",\"1\",6,\"v\"],[2,\"Aquarade\",\"1\",1]]', '2019-04-30 12:53:01', 6, 0, 1, 7, 16),
(49, 2, '[[1,\"Grape Fruit\",\"0.5\",6,\"v\"]]', '2019-04-30 13:02:15', 6, 0, 1, 3, 17),
(50, 2, '[[1,\"Bubble Gum\",\"1\",6,\"v\"]]', '2019-05-06 11:25:32', 6, 0, 1, 6, 19),
(51, 2, '[[1,\"Lemon Kush\",\"1\",6,\"v\"],[2,\"Librillo OCB Slim\",\"1\",1]]', '2019-05-06 11:30:15', 6, 0, 1, 7, 20),
(52, 2, '[[1,\"Caramel Ice\",\"1\",6,\"v\"]]', '2019-05-06 12:10:11', 6, 0, 1, 6, 21),
(53, 2, '[[1,\"Babylonia fruit\",\"1\",6,\"v\"]]', '2019-05-06 12:19:21', 6, 0, 1, 6, 22),
(54, 35, '[[2,\"Agua\",1,0.5]]', '2019-05-06 12:44:28', 6, 0, 1, 0.5, 23),
(55, 2, '[[1,\"Moby Dick\",\"1\",6,\"v\"]]', '2019-05-06 12:50:32', 6, 0, 1, 6, 24),
(56, 1, '[[1,\"Big Bud\",\"1\",5,\"v\"]]', '2019-05-06 12:50:56', 6, 0, 1, 5, 25),
(57, 3, '[[2,\"Maxibon\",\"1\",2]]', '2019-05-06 12:51:34', 6, 0, 1, 2, 26),
(58, 2, '[[1,\"Gordo Master\",\"1\",7,\"m\"]]', '2019-05-06 12:54:48', 6, 0, 1, 7, 27),
(59, 1, '[[2,\"Librillo OCB\",\"1\",1]]', '2019-05-06 12:57:35', 6, 0, 1, 1, 28),
(60, 2, '[[1,\"Candy Kush\",\"1\",6,\"v\"]]', '2019-05-06 12:58:05', 6, 0, 1, 6, 29),
(61, 2, '[[1,\"Caramel Ice\",\"1\",6,\"v\"]]', '2019-05-06 13:21:47', 6, 0, 1, 6, 30),
(62, 2, '[[1,\"Babylonia fruit\",\"1\",6,\"v\"]]', '2019-05-06 13:22:29', 6, 0, 1, 6, 31),
(63, 1, '[[1,\"Caramel Ice\",\"1\",6,\"v\"],[1,\"Rubio\",\"1\",4,\"m\"],[2,\"Agua\",\"1\",0.5]]', '2019-05-07 11:19:00', 6, 0, 1, 10.5, 33),
(64, 1, '[[1,\"Candy Kush\",\"1\",6,\"v\"]]', '2019-05-07 13:18:29', 6, 0, 1, 6, 34),
(65, 2, '[[1,\"Candy Kush\",\"1\",6,\"v\"]]', '2019-05-15 11:07:27', 6, 0, 1, 6, 36),
(66, 2, '[[1,\"Bubble Gum\",\"1.5\",6,\"v\"]]', '2019-05-16 11:36:20', 6, 0, 1, 9, 39),
(67, 2, '[[1,\"Babylonia fruit\",\"1.2\",6,\"v\"],[2,\"Librillo OCB Slim\",\"1\",1],[2,\"Agua\",\"1\",0.5]]', '2019-05-16 11:39:30', 6, 6, 1, 8.7, 40),
(68, 4, '[[1,\"Babylonia fruit\",\"12\",6,\"v\"]]', '2019-05-16 11:52:44', 6, 0, 1, 72, 40),
(69, 2, '[[1,\"Caramel Ice\",\"1\",6,\"v\"]]', '2019-05-16 12:00:43', 6, 0, 1, 6, 41),
(70, 3, '[[1,\"Candy Kush\",\"53\",6,\"v\"]]', '2019-05-16 12:09:22', 6, 0, 1, 318, 42),
(71, 2, '[[1,\"Rubio\",\"1.5\",4,\"m\"],[2,\"Agua\",\"1\",0.5]]', '2019-05-22 11:25:16', 6, 6, 1, 6.5, 45),
(72, 3, '[[1,\"Cheese\",\"80\",6,\"v\"]]', '2019-05-22 11:39:20', 6, 6, 1, 480, 46),
(73, 1, '[[1,\"Amnesia Haze\",\"356\",6,\"v\"]]', '2019-05-22 11:50:17', 6, 0, 1, 2136, 47),
(74, 2, '[[1,\"Caramel Ice\",\"1\",6,\"v\"]]', '2019-05-22 11:59:45', 6, 0, 1, 6, 48),
(75, 3, '[[1,\"Azura Haze\",\"1.5\",6,\"v\"],[1,\"Moby Dick\",\"1\",6,\"v\"],[1,\"Gordo Master\",\"1\",7,\"m\"],[2,\"Librillo OCB\",\"2\",1],[2,\"Calippo\",\"1\",2],[2,\"Aquarade\",\"1\",1]]', '2019-05-22 12:07:07', 6, 6, 1, 27, 48),
(76, 2, '[[1,\"Grape Fruit\",\"1\",6,\"v\"],[1,\"Gordo Master\",\"2\",7,\"m\"]]', '2019-05-23 11:37:17', 6, 0, 1, 20, 49),
(77, 1, '[[1,\"Moby Dick\",\"1\",6,\"v\"]]', '2019-05-23 13:09:25', 6, 6, 1, 6, 49),
(78, 1, '[[1,\"OG Kush\",\"1\",6,\"m\"]]', '2019-05-23 13:13:32', 6, 0, 1, 5.58, 49),
(79, 1, '[[1,\"Cheese\",\"1\",6,\"v\"],[1,\"Bubble Gum\",\"1\",6,\"v\"]]', '2019-05-27 11:26:41', 6, 0, 1, 11.16, 50),
(80, 1, '[[1,\"Bubble Gum\",\"1\",6,\"v\"]]', '2019-05-27 11:41:17', 6, 0, 1, 5.58, 50),
(81, 1, '[[1,\"Cheese\",\"1\",6,\"v\"]]', '2019-05-27 11:46:11', 6, 0, 1, 5.58, 50),
(82, 1, '[[1,\"Azura Haze\",\"1\",6,\"v\"]]', '2019-05-27 11:46:46', 6, 0, 1, 5.58, 50),
(84, 1, '[[1,\"Caramel Ice\",\"1\",\"5.58\",\"v\"]]', '2019-05-27 12:37:11', 6, 0, 1, 5.58, 50),
(85, 2, '[[1,\"Caramel Ice\",\"1\",6,\"v\"]]', '2019-05-27 12:37:50', 6, 0, 1, 6, 50),
(86, 1, '[[1,\"Rubio\",\"1\",\"3.72\",\"m\"]]', '2019-05-27 12:52:41', 6, 0, 1, 3.72, 50),
(87, 1, '[[1,\"Caramel Ice\",\"1\",\"5.58\",\"v\"]]', '2019-05-28 11:37:10', 6, 0, 1, 5.58, 51),
(88, 1, '[[2,\"Librillo OCB\",\"1\",1]]', '2019-05-28 11:38:22', 6, 0, 1, 1, 51),
(89, 2, '[[1,\"Cheese\",\"1\",6,\"v\"],[2,\"Agua\",\"1\",0.5]]', '2019-05-28 11:39:49', 6, 0, 1, 6.5, 52),
(90, 1, '[[1,\"Cheese\",\"1\",\"5.58\",\"v\"]]', '2019-05-28 11:40:08', 6, 0, 1, 5.58, 52),
(91, 3, '[[2,\"Maxibon\",\"1\",2]]', '2019-05-28 11:40:50', 6, 0, 1, 2, 52),
(92, 1, '[[1,\"Big Bud\",\"1\",5,\"v\"]]', '2019-05-28 12:47:33', 6, 6, 1, 5, 53),
(93, 5, '[[1,\"Rubio\",\"1\",4,\"m\"],[2,\"Librillo OCB Slim\",\"1\",1]]', '2019-05-28 13:05:33', 6, 0, 1, 5, 53),
(94, 2, '[[1,\"Azura Haze\",\"1\",6,\"v\"],[2,\"Agua\",\"1\",0.5]]', '2019-05-29 06:04:12', 6, 0, 1, 6.5, 54),
(95, 3, '[[1,\"Pineapple Kush\",\"1\",6,\"v\"]]', '2019-05-29 06:06:24', 6, 0, 1, 6, 55),
(96, 2, '[[1,\"Cheese\",\"1\",6,\"v\"]]', '2019-05-29 06:10:58', 6, 0, 1, 6, 56),
(97, 1, '[[1,\"Grape Fruit\",\"1\",\"5.58\",\"v\"],[1,\"Big Bud\",\"1\",\"4.65\",\"v\"],[2,\"Librillo OCB Slim\",\"1\",1],[2,\"Librillo OCB\",\"1\",1]]', '2019-05-29 06:21:08', 6, 0, 1, 12.23, 57),
(98, 1, '[[1,\"Caramel Ice\",\"1\",\"5.58\",\"v\"],[2,\"Librillo OCB Slim\",\"1\",1]]', '2019-05-29 06:27:52', 6, 0, 1, 6.58, 58),
(99, 2, '[[1,\"Pineapple Kush\",\"1\",6,\"v\"],[1,\"OG Kush\",\"1\",6,\"m\"],[2,\"Librillo OCB Slim\",\"1\",1]]', '2019-05-29 06:39:59', 6, 0, 1, 13, 58),
(100, 2, '[[1,\"Caramel Ice\",\"6\",6,\"v\"],[2,\"Librillo OCB Slim\",\"11\",1]]', '2019-05-29 06:41:33', 6, 0, 1, 47, 60),
(101, 2, '[[1,\"Candy Kush\",\"1\",6,\"v\"],[1,\"Babylonia fruit\",\"1\",6,\"v\"],[1,\"Bubble Gum\",\"1\",6,\"v\"],[2,\"Coca-Cola\",\"1\",1]]', '2019-05-29 07:58:35', 6, 0, 1, 19, 61),
(102, 1, '[[1,\"Azura Haze\",\"21\",\"5.58\",\"v\"],[2,\"Librillo OCB\",\"1\",1]]', '2019-05-29 09:02:04', 6, 0, 1, 118.18, 62),
(103, 1, '[[1,\"Babylonia fruit\",\"4\",5.58,\"v\"],[1,\"Azura Haze\",\"1\",5.58,\"v\"],[1,\"Black Dog\",\"1\",5.58,\"v\"],[1,\"Azura Haze\",\"1\",5.58,\"v\"],[2,\"Librillo OCB\",\"1\",1]]', '2019-05-29 09:10:04', 6, 6, 1, 40.06, 63),
(104, 3, '[[1,\"Azura Haze\",\"123\",6,\"v\"],[2,\"Librillo OCB Slim\",\"23\",1]]', '2019-05-29 09:17:21', 6, 0, 1, 761, 64),
(105, 9, '[[1,\"Bubble Gum\",\"24\",6,\"v\"],[2,\"Agua\",\"1\",0.5]]', '2019-05-29 09:22:05', 6, 6, 1, 144.5, 65),
(106, 1, '[[1,\"Azura Haze\",\"1\",\"5.58\",\"v\"]]', '2019-05-29 10:56:04', 6, 0, 1, 5.58, 66),
(107, 1, '[[1,\"Amnesia Haze\",\"3.5\",5.58,\"v\"],[2,\"Agua\",\"1\",0.5]]', '2019-05-29 11:56:41', 6, 6, 1, 20.03, 66),
(108, 3, '[[1,\"Candy Kush\",\"1\",6,\"v\"],[1,\"Rubio\",\"1\",4,\"m\"]]', '2019-05-29 13:11:59', 6, 6, 1, 10, 66),
(109, 1, '[[1,\"Babylonia fruit\",\"1\",\"5.58\",\"v\"]]', '2019-05-29 13:56:46', 6, 0, 1, 5.58, 67),
(110, 1, '[[1,\"Babylonia fruit\",\"3.5\",5.58,\"v\"],[2,\"Librillo OCB\",\"4\",1]]', '2019-05-30 12:11:00', 6, 6, 1, 23.53, 69),
(111, 35, '[[1,\"Babylonia fruit\",2,6,\"v\"]]', '2019-06-04 13:55:03', 6, 0, 1, 11.16, 70),
(112, 35, '[[1,\"Candy Kush\",3.55,6,\"v\"]]', '2019-06-05 11:43:07', 6, 0, 1, 19.81, 71),
(113, 35, '[[1,\"Candy Kush\",1,6,\"v\"]]', '2019-06-05 11:59:33', 6, 0, 1, 5.58, 71),
(114, 35, '[[1,\"Babylonia fruit\",1,5.58,\"v\"]]', '2019-06-05 12:25:44', 6, 0, 1, 5.58, 71),
(115, 35, '[[1,\"Black Dog\",\"1.5\",5.58,\"v\"]]', '2019-06-06 10:36:01', 6, 6, 1, 8.37, 72),
(116, 35, '[[1,\"Candy Kush\",2,6,\"v\"],[2,\"Agua\",1,0.5],[2,\"Coca-Cola\",1,1]]', '2019-06-14 05:33:07', 6, 0, 1, 13.5, 77),
(117, 122, '[[1,\"Azura Haze\",\"3.58\",6,\"v\"]]', '2019-07-23 06:06:59', 6, 0, 1, 21.48, 184),
(118, 2, '[[1,\"OG Kush\",\"1\",6,\"m\"]]', '2019-07-23 06:10:55', 6, 0, 1, 6, 185),
(119, 4, '[[1,\"Gordo Master\",\"1\",7,\"m\"]]', '2019-07-23 06:11:07', 6, 0, 1, 7, 185),
(120, 91, '[[1,\"Gordo Master\",\"1\",7,\"m\"]]', '2019-07-23 06:11:18', 6, 0, 1, 7, 185),
(121, 1, '[[1,\"Cheese\",\"1\",\"5.58\",\"v\"]]', '2019-07-23 06:11:49', 6, 0, 1, 5.58, 186),
(122, 4, '[[1,\"Cheese\",\"1\",6,\"v\"]]', '2019-07-23 06:12:12', 6, 0, 1, 6, 186),
(123, 13, '[[1,\"Cheese\",\"3.58\",6,\"v\"]]', '2019-07-23 06:29:32', 6, 0, 1, 21.48, 186),
(124, 4, '[[1,\"Azura Haze\",\"5.58\",6,\"v\"]]', '2019-07-23 09:01:54', 6, 0, 1, 33.48, 187),
(125, 5, '[[1,\"Azura Haze\",\"1\",6,\"v\"]]', '2019-07-23 09:02:07', 6, 6, 1, 6, 187);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sesiones`
--

CREATE TABLE `sesiones` (
  `id` int(5) NOT NULL,
  `id_usuario` int(2) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `sesiones`
--

INSERT INTO `sesiones` (`id`, `id_usuario`, `fecha`, `hora`) VALUES
(89, 6, '2019-01-29', '16:18:51'),
(90, 6, '2019-01-29', '18:58:46'),
(91, 6, '2019-01-29', '19:25:41'),
(92, 5, '2019-01-29', '20:22:19'),
(93, 5, '2019-01-29', '20:22:19'),
(94, 5, '2019-01-29', '21:28:24'),
(95, 6, '2019-01-29', '22:00:29'),
(96, 5, '2019-01-29', '22:01:29'),
(97, 6, '2019-01-29', '22:02:02'),
(98, 6, '2019-01-30', '15:09:32'),
(99, 6, '2019-01-30', '17:29:47'),
(100, 6, '2019-01-30', '17:31:10'),
(101, 5, '2019-01-30', '19:01:54'),
(102, 5, '2019-01-30', '19:01:55'),
(103, 5, '2019-01-30', '19:02:21'),
(104, 5, '2019-01-30', '19:05:51'),
(105, 6, '2019-02-01', '09:23:39'),
(106, 6, '2019-02-01', '09:25:58'),
(107, 6, '2019-02-01', '09:43:15'),
(108, 6, '2019-02-01', '09:52:54'),
(109, 6, '2019-02-04', '11:31:46'),
(110, 6, '2019-02-04', '14:47:23'),
(111, 6, '2019-02-04', '14:51:40'),
(112, 6, '2019-02-04', '17:31:41'),
(113, 6, '2019-02-05', '09:35:38'),
(114, 6, '2019-02-05', '10:32:31'),
(115, 6, '2019-02-05', '10:33:38'),
(116, 6, '2019-02-05', '15:59:01'),
(117, 6, '2019-02-06', '11:14:08'),
(118, 6, '2019-02-06', '11:33:16'),
(119, 6, '2019-02-06', '12:40:44'),
(120, 6, '2019-02-06', '15:19:03'),
(121, 6, '2019-02-07', '15:07:12'),
(122, 6, '2019-02-11', '16:01:27'),
(123, 6, '2019-02-12', '11:39:58'),
(124, 6, '2019-02-12', '15:10:22'),
(125, 6, '2019-02-12', '15:40:56'),
(126, 6, '2019-02-12', '15:41:07'),
(127, 6, '2019-02-13', '14:57:30'),
(128, 6, '2019-02-14', '16:49:31'),
(129, 6, '2019-02-14', '17:23:41'),
(130, 6, '2019-02-19', '15:22:33'),
(131, 6, '2019-02-19', '15:50:39'),
(132, 6, '2019-02-19', '15:50:45'),
(133, 6, '2019-02-20', '15:28:38'),
(134, 6, '2019-02-20', '15:41:13'),
(135, 6, '2019-02-20', '17:40:36'),
(136, 6, '2019-02-21', '13:21:38'),
(137, 6, '2019-02-21', '13:46:31'),
(138, 6, '2019-02-21', '15:18:33'),
(139, 6, '2019-02-21', '15:24:36'),
(140, 6, '2019-03-01', '11:16:03'),
(141, 6, '2019-03-04', '15:19:58'),
(142, 6, '2019-03-04', '15:47:19'),
(143, 6, '2019-03-05', '16:03:06'),
(144, 6, '2019-03-06', '15:17:23'),
(145, 6, '2019-03-06', '15:19:36'),
(146, 6, '2019-03-11', '15:05:45'),
(147, 6, '2019-03-11', '15:05:58'),
(148, 7, '2019-03-11', '15:10:13'),
(149, 6, '2019-03-11', '15:19:26'),
(150, 6, '2019-03-12', '11:39:52'),
(151, 6, '2019-03-12', '11:57:21'),
(152, 6, '2019-03-12', '16:02:58'),
(153, 6, '2019-03-13', '14:58:22'),
(154, 6, '2019-03-13', '15:19:26'),
(155, 6, '2019-03-13', '15:27:37'),
(156, 6, '2019-03-13', '15:34:49'),
(157, 6, '2019-03-13', '15:36:04'),
(158, 6, '2019-03-13', '16:53:36'),
(159, 6, '2019-03-13', '17:21:34'),
(160, 6, '2019-03-13', '17:27:55'),
(161, 6, '2019-03-14', '15:25:56'),
(162, 6, '2019-03-18', '09:56:05'),
(163, 6, '2019-03-18', '15:10:11'),
(164, 7, '2019-03-18', '15:15:49'),
(165, 6, '2019-03-18', '16:03:14'),
(166, 6, '2019-03-18', '16:05:55'),
(167, 6, '2019-03-26', '15:46:23'),
(168, 6, '2019-04-10', '16:30:16'),
(169, 6, '2019-04-10', '17:09:02'),
(170, 6, '2019-04-10', '17:09:55'),
(171, 6, '2019-04-10', '17:10:04'),
(172, 6, '2019-04-10', '17:13:37'),
(173, 6, '2019-04-10', '17:15:44'),
(174, 6, '2019-04-10', '17:16:28'),
(175, 6, '2019-04-10', '17:17:49'),
(176, 6, '2019-04-10', '17:18:04'),
(177, 6, '2019-04-10', '17:18:16'),
(178, 6, '2019-04-10', '17:18:37'),
(179, 6, '2019-04-10', '17:19:27'),
(180, 6, '2019-04-10', '17:20:22'),
(181, 6, '2019-04-10', '17:21:26'),
(182, 6, '2019-04-10', '17:23:05'),
(183, 6, '2019-04-10', '17:23:41'),
(184, 6, '2019-04-10', '17:43:45'),
(185, 6, '2019-04-10', '17:44:02'),
(186, 6, '2019-04-15', '16:02:20'),
(187, 6, '2019-04-15', '17:43:23'),
(188, 6, '2019-04-16', '16:58:39'),
(189, 6, '2019-04-30', '16:36:23'),
(190, 5, '2019-04-30', '16:37:05'),
(191, 6, '2019-04-30', '16:46:42'),
(192, 6, '2019-04-30', '16:47:01'),
(193, 5, '2019-04-30', '16:47:19'),
(194, 6, '2019-04-30', '17:00:19'),
(195, 6, '2019-05-06', '15:21:16'),
(196, 7, '2019-05-06', '15:22:01'),
(197, 6, '2019-05-06', '15:25:08'),
(198, 6, '2019-05-06', '15:29:21'),
(199, 6, '2019-05-06', '16:44:36'),
(200, 6, '2019-05-07', '14:59:24'),
(201, 6, '2019-05-07', '15:01:27'),
(202, 6, '2019-05-07', '15:01:43'),
(203, 6, '2019-05-10', '10:28:15'),
(204, 6, '2019-05-15', '15:06:50'),
(205, 5, '2019-05-15', '15:07:46'),
(206, 6, '2019-05-15', '15:09:55'),
(207, 5, '2019-05-15', '15:10:51'),
(208, 6, '2019-05-15', '15:12:12'),
(209, 7, '2019-05-15', '15:16:52'),
(210, 7, '2019-05-15', '15:19:54'),
(211, 5, '2019-05-15', '15:20:02'),
(212, 5, '2019-05-15', '16:08:55'),
(213, 6, '2019-05-16', '15:16:23'),
(214, 6, '2019-05-16', '16:08:21'),
(215, 6, '2019-05-20', '09:30:41'),
(216, 6, '2019-05-20', '15:55:25'),
(217, 6, '2019-05-21', '15:34:54'),
(218, 6, '2019-05-21', '16:55:34'),
(219, 6, '2019-05-22', '15:19:18'),
(220, 6, '2019-05-23', '15:13:58'),
(221, 6, '2019-05-27', '15:21:56'),
(222, 6, '2019-05-27', '15:37:15'),
(223, 6, '2019-05-28', '15:04:28'),
(224, 6, '2019-05-28', '17:33:04'),
(225, 6, '2019-05-29', '10:03:42'),
(226, 6, '2019-05-30', '14:40:43'),
(227, 6, '2019-05-30', '16:04:31'),
(228, 6, '2019-06-04', '15:14:29'),
(229, 6, '2019-06-04', '15:21:15'),
(230, 6, '2019-06-04', '17:34:35'),
(231, 6, '2019-06-04', '17:55:51'),
(232, 6, '2019-06-05', '15:41:42'),
(233, 6, '2019-06-05', '15:44:15'),
(234, 6, '2019-06-05', '15:59:36'),
(235, 6, '2019-06-05', '16:25:49'),
(236, 6, '2019-06-06', '14:35:01'),
(237, 6, '2019-06-06', '14:36:08'),
(238, 6, '2019-06-06', '14:39:39'),
(239, 6, '2019-06-06', '14:43:30'),
(240, 6, '2019-06-06', '17:44:14'),
(241, 6, '2019-06-10', '11:56:52'),
(242, 6, '2019-06-12', '14:56:51'),
(243, 6, '2019-06-12', '15:37:08'),
(244, 6, '2019-06-12', '16:18:41'),
(245, 6, '2019-06-12', '16:59:29'),
(246, 6, '2019-06-13', '15:25:56'),
(247, 6, '2019-06-14', '09:27:42'),
(248, 6, '2019-06-14', '09:33:49'),
(249, 6, '2019-06-14', '09:34:49'),
(250, 6, '2019-06-17', '13:17:15'),
(251, 6, '2019-07-23', '09:34:30'),
(252, 6, '2019-07-23', '10:30:47'),
(253, 6, '2019-07-30', '13:10:21'),
(254, 6, '2019-08-08', '09:20:39'),
(255, 6, '2019-08-08', '14:23:52'),
(256, 6, '2019-08-27', '10:25:24'),
(257, 6, '2019-08-30', '09:53:41');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `socios`
--

CREATE TABLE `socios` (
  `id` int(5) NOT NULL,
  `nsocio` int(5) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellidos` varchar(70) NOT NULL,
  `dni` varchar(15) NOT NULL,
  `avalador` varchar(80) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `direccion` varchar(250) NOT NULL,
  `alta` date NOT NULL,
  `comentarios` text NOT NULL,
  `documentacion` varchar(150) NOT NULL,
  `foto_dni` varchar(100) NOT NULL,
  `foto` varchar(40) NOT NULL,
  `email` varchar(40) NOT NULL,
  `requisitos` varchar(100) NOT NULL,
  `req_docu` varchar(50) NOT NULL,
  `consumo` int(3) NOT NULL,
  `activado` tinyint(1) NOT NULL,
  `firma` varchar(100) NOT NULL,
  `ultimo_acceso` datetime NOT NULL,
  `descuento` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `socios`
--

INSERT INTO `socios` (`id`, `nsocio`, `nombre`, `apellidos`, `dni`, `avalador`, `telefono`, `direccion`, `alta`, `comentarios`, `documentacion`, `foto_dni`, `foto`, `email`, `requisitos`, `req_docu`, `consumo`, `activado`, `firma`, `ultimo_acceso`, `descuento`) VALUES
(1, 1, 'Manuel Iván', 'Ortega Ibañez', '28802032Y', '1', '954910464', 'C/ Rafael Aguilar, 15', '2016-02-12', '', '', '', '', 'asociacionislabonita@gmail.com', '[\"req1:true\",\"req2:true\",\"req3:false\",\"req4:false\"]', '', 0, 1, '28802032Y.png', '0000-00-00 00:00:00', 1),
(2, 2, 'Francisco Javier', 'Ortiz Garrido', '53277347D', '1', '687317563', 'C/ Almeria, 7', '2016-02-12', '', '', '', '', 'asociacionislabonita@gmail.com', '[\"req1:true\",\"req2:true\",\"req3:true\",\"req4:true\"]', '', 0, 1, '53277347D.png', '2019-01-29 19:01:45', 0),
(4, 4, 'David', 'Triano Calado', '28807474C', '2', '663085839', 'C/ Pozo Bueno, 1', '2016-02-12', '', '', '', '', 'david_triano@hotmail.com', '[\"req1:true\",\"req2:false\",\"req3:true\",\"req4:true\"]', '', 0, 1, '', '0000-00-00 00:00:00', 0),
(5, 5, 'Jose Luis', 'Romero Jiménez', '53343576K', '2', '687470127', 'C/ Malaga, 3, 3º der.', '2016-02-12', '', '', '', '', 'asociacionislabonita@gmail.com', '[\"req1:true\",\"req2:true\",\"req3:true\",\"req4:true\"]', '', 0, 1, '', '0000-00-00 00:00:00', 0),
(6, 6, 'Mario', 'Márquez Donado', '77806459N', '2', '687859642', 'C/ Almería, 7, 4º der. ', '2016-02-12', '', '', '', '', 'greencity@gmail.com', '[\"req1:true\",\"req2:true\",\"req3:false\",\"req4:false\"]', '', 0, 1, '', '0000-00-00 00:00:00', 0),
(7, 7, 'Jose', 'Olaya Aguado', '53342727T', '2', '617783700', 'C/ Malaga, 2, 2º der.', '2016-02-12', '', '', '', '', 'jolayaguado@gmail.com', '[\"req1:true\",\"req2:true\",\"req3:true\",\"req4:false\"]', '', 0, 1, '', '0000-00-00 00:00:00', 0),
(9, 10, 'Oscar', 'Urbano Escudero', '28641566B', '2', '649867270', 'C/ Guadalete, 3', '2016-02-12', '', '', '', '', 'oscarurbano78@gmail.com', '[\"req1:true\",\"req2:true\",\"req3:true\",\"req4:true\"]', '', 0, 1, '', '0000-00-00 00:00:00', 0),
(10, 13, 'Jorge Luis', 'Risoto Davila', '52228965J', '2', '648542055', 'C/ Tesoro del Carambolo, 8', '2017-01-24', '', '', '', '', 'risotodavila1991@hotmail.com', '[\"req1:true\",\"req2:true\",\"req3:true\",\"req4:false\"]', '', 0, 1, '', '0000-00-00 00:00:00', 0),
(11, 27, 'Adrian', 'Rojas Dorado', '49028678S', '2', '677436049', 'C/ San Juan de Hornillo, 47', '2016-02-12', '', '', '', '', 'adrianrd-20@hotmail.com', '[\"req1:true\",\"req2:true\",\"req3:true\",\"req4:false\"]', '', 0, 1, '49028678S.png', '2019-01-29 19:50:42', 0),
(13, 30, 'Antonio Manuel', 'Rojas Dorado', '49028679Q', '11', '625804188', 'C/ San Juan de Hornillo, 47', '2016-04-22', '', '', '', '', 'lolailo_221@hotmail.com', '[\"req1:true\",\"req2:true\",\"req3:false\",\"req4:false\"]', '', 0, 1, '', '0000-00-00 00:00:00', 0),
(14, 31, 'Alvaro', 'Figueroa Guerrero', '49026432T', '2', '618882224', 'C/ Gabriel Garcia Marquez, 19', '2016-02-24', '', '', '', '', 'alvfiggue@hotail.com', '[\"req1:true\",\"req2:true\",\"req3:false\",\"req4:false\"]', '', 0, 1, '', '0000-00-00 00:00:00', 0),
(15, 33, 'Sergio', 'Mondaza Garcia', '49092871S', '2', '657611183', 'Pza. de la Zarzuela, 32', '2016-02-12', '', '', '', '', 'sergiodh91@gmail.com', '[\"req1:true\",\"req2:false\",\"req3:true\",\"req4:true\"]', '', 50, 1, '', '0000-00-00 00:00:00', 0),
(16, 34, 'Joaquin', 'Andujar Gutierrez', '49028175H', '15', '622006912', 'C/ Huerta Pilar Cantaelgallo, 8', '2016-02-19', '', '', '', '', '', '[\"req1:true\",\"req2:false\",\"req3:false\",\"req4:false\"]', '', 60, 1, '', '0000-00-00 00:00:00', 0),
(18, 39, 'Francisco Javier', 'Bonilla Fernandez', '49027301H', '1', '622253780', 'C/ Ronda de Altair, 2', '2016-02-12', '', '', '', '', '', '[\"req1:true\",\"req2:true\",\"req3:false\",\"req4:false\"]', '', 35, 1, '', '0000-00-00 00:00:00', 0),
(20, 41, 'Javier', 'Montes Gonzalez', '77585434Z', '1', '652857234', 'Urbn. Recreo San Jose, 9', '2016-03-02', '', '', '', '', 'javiermontesgonzalez@hotmail.com', '[\"req1:true\",\"req2:true\",\"req3:true\",\"req4:true\"]', '', 60, 1, '', '0000-00-00 00:00:00', 0),
(22, 44, 'Francisco Abraham', 'Gonzalez Moreno', '48963213P', '1', '615247018', 'C/ Gabriela Mistral, 27', '2016-03-09', '', '', '', '', 'inthesite@gmail.com', '[\"req1:true\",\"req2:true\",\"req3:true\",\"req4:false\"]', '', 60, 1, '', '0000-00-00 00:00:00', 0),
(23, 45, 'Francisco Javier', 'Alcacer Garrido', '49128674F', '2', '654023772', 'C/ Fuengirola, 21, 1º', '2016-03-10', '', '', '', '', '', '[\"req1:true\",\"req2:true\",\"req3:false\",\"req4:false\"]', '', 30, 1, '', '0000-00-00 00:00:00', 0),
(24, 46, 'Florencia ', 'De Miguel Dorado', '48884867T', '1', '618528080', 'C/ Arsenal, 19, 1º der.', '2016-03-17', '', '', '', '', 'floren_1982@hotmail.com', '[\"req1:true\",\"req2:true\",\"req3:false\",\"req4:false\"]', '', 60, 1, '', '0000-00-00 00:00:00', 0),
(25, 48, 'Ignacio', 'Picon Marin', '48964065D', '22', '615433345', 'C/ Ernest Hemingway, 29', '2016-03-19', 'Llamalaflo p\nPreguntarle por la cuota y decirle que le ocurría a su madre\n', '', '', '', 'chano.1986@hotmail.com', '[\"req1:true\",\"req2:false\",\"req3:true\",\"req4:true\"]', '', 60, 1, '', '0000-00-00 00:00:00', 0),
(27, 50, 'Alejandro', 'Ruiz Olivares', '49129436X', '18', '652534249', 'C/ Mijail Gorbachov, 6, 2º C4', '2016-04-20', '', '', '', '', '', '[\"req1:true\",\"req2:true\",\"req3:false\",\"req4:false\"]', '', 60, 1, '', '0000-00-00 00:00:00', 0),
(28, 52, 'Miguel Angel', 'Garrido Garcia', '28781342Q', '7', '665541341', 'C/ Angel Lopez Lopez, 13, Blq. 9, 6º A', '2016-02-12', '', '', '', '', 'magg1986@hotmail.com', '[\"req1:true\",\"req2:false\",\"req3:false\",\"req4:false\"]', '', 60, 1, '', '0000-00-00 00:00:00', 0),
(29, 53, 'Ezequiel ', 'Marquez Leon', '48964606K', '22', '660274636', 'Avda. Juan Pablo II, 22, Blq. 2, 1ºB', '2016-05-26', '', '', '', '', 'talleresmanuelmarquez@gmail.com', '[\"req1:true\",\"req2:true\",\"req3:true\",\"req4:true\"]', '', 60, 1, '', '0000-00-00 00:00:00', 0),
(30, 54, 'Laura', 'Corbacho Sanchez', '28930904D', '29', '637116991', 'Avda. Juan Pablo II, 22, Blq. 2, 1ºB', '2016-05-28', '', '', '', '', 'lauracorsan84@gmail.com', '[\"req1:true\",\"req2:true\",\"req3:false\",\"req4:false\"]', '', 60, 1, '', '0000-00-00 00:00:00', 0),
(31, 55, 'Alicia', 'Fortunez Muñoz', '48961580P', '30', '600061826', 'C/ Miguel Angel Asturias, 27', '2016-06-09', '', '', '', '', '', '[\"req1:true\",\"req2:true\",\"req3:true\",\"req4:true\"]', '', 60, 1, '', '0000-00-00 00:00:00', 0),
(32, 56, 'Daniel', 'Bermudez Romero', '49027781S', '15', '615127267', 'C/ Virgen del Valle, 34', '2016-08-09', '', '', '', '', 'daberro@gmail.com', '[\"req1:true\",\"req2:true\",\"req3:false\",\"req4:false\"]', '', 50, 1, '', '0000-00-00 00:00:00', 0),
(33, 57, 'Felipe', 'Gutierrez Noguero', '49130716W', '13', '617000418', 'C/ Bembezar, 16', '2017-06-09', '', '', '', '', 'nordic173@gmail.com', '[\"req1:true\",\"req2:true\",\"req3:false\",\"req4:false\"]', '', 60, 1, '', '0000-00-00 00:00:00', 0),
(34, 58, 'Maria Transito', 'Lopez Fraile', '80147714M', '0', '662197857', 'C/ Blanca, 5', '2017-03-16', '', '', '', '', 'transilf5@hotmail.com', '[\"req1:true\",\"req2:true\",\"req3:false\",\"req4:false\"]', '', 60, 1, '', '0000-00-00 00:00:00', 0),
(35, 59, 'Daniel', 'Cazalla Vazquez', '49130015Z', '2', '611433126', 'C/ Jucar, 25', '2017-04-04', '', '', '[\"35-1.png\",\"35-2.png\"]', '49130015Z.png', 'danielcazalla3@gmail.com', '[\"req1:true\",\"req2:true\",\"req3:true\",\"req4:true\"]', '', 40, 1, '49130015Z.png', '2019-08-27 13:21:46', 0),
(36, 60, 'Adan Jesus', 'Fernandez Losquiños', '30234363N', '35', '622692770', 'C/ Torremayor, 38', '2017-02-23', '', '', '', '30234363N.png', 'losky92@gmail.com', '[\"req1:true\",\"req2:false\",\"req3:false\",\"req4:false\"]', '', 30, 1, '', '0000-00-00 00:00:00', 0),
(38, 62, 'Ivan', 'de los Santos Garcia', '49129435D', '35', '671948073', 'C/ Ernest Hemingway, 48', '2017-07-31', '', '', '', '', 'ivandega22@gmail.com', '[\"req1:true\",\"req2:true\",\"req3:true\",\"req4:true\"]', '', 60, 1, '49129435D.jpg', '0000-00-00 00:00:00', 0),
(39, 63, 'Felix', 'Castilla Suarez', '49090766A', '39', '625758595', 'C/ Mijail Gorbachov, 8, 2º C4', '2017-08-25', '', '', '', '', 'v.felix.castilla@gmail.com', '[\"req1:true\",\"req2:true\",\"req3:true\",\"req4:false\"]', '', 50, 1, '', '0000-00-00 00:00:00', 0),
(40, 64, 'Juan Luis', 'Montoya Marchena', '48122420A', '35', '666170642', 'C/ Teresa de Jesus, 3', '2017-08-30', '', '', '', '', 'juanlu46@gmail.com', '[\"req1:true\",\"req2:true\",\"req3:true\",\"req4:false\"]', '', 40, 1, '', '0000-00-00 00:00:00', 0),
(41, 65, 'Celia', 'Martinez Ortiz', '47427858H', '35', '642773918', 'Avda. Andalucia, 2, 2º A', '2017-08-30', '', '', '', '', 'celia.martinez.94@gmail.com', '[\"req1:true\",\"req2:true\",\"req3:true\",\"req4:false\"]', '', 60, 1, '', '0000-00-00 00:00:00', 0),
(42, 66, 'Enrique', 'Ariza Torres', '49029330T', '39', '699073754', 'C/ Marconi, 2, Bj A', '2017-09-30', '', '', '', '', 'enrique_ariza@hotmail.com', '[\"req1:true\",\"req2:false\",\"req3:false\",\"req4:false\"]', '', 30, 1, '', '0000-00-00 00:00:00', 0),
(43, 67, 'Alejandro', 'Tortolero Martin', '49128882P', '42', '670537330', 'C/ Tajo, 105', '2017-10-06', '', '', '', '', 'alejandro_tortolero7@outlook.com', '[\"req1:true\",\"req2:false\",\"req3:false\",\"req4:false\"]', '', 30, 1, '', '0000-00-00 00:00:00', 0),
(44, 68, 'Raul', 'Cruz Garcia', '52661130P', '1', '647420287', 'C/ Liorna, 49', '2017-10-12', '', '', '', '', 'raulcruzgarci-@hotmail.com', '[\"req1:true\",\"req2:true\",\"req3:false\",\"req4:false\"]', '', 60, 1, '', '0000-00-00 00:00:00', 0),
(45, 69, 'Jesus', 'Carrion', '28620147M', '44', '655574633', 'Avda. Italia, 12', '2017-10-26', '', '', '', '', 'jcarrionj@hotmail.com', '[\"req1:true\",\"req2:true\",\"req3:false\",\"req4:false\"]', '', 50, 1, '', '0000-00-00 00:00:00', 0),
(46, 70, 'Marta', 'Hernandez-Palomo Peña', '28794096M', '38', '637638718', 'Avda. Juan Pablo II, 2, Bj. D', '2017-02-14', '', '', '', '', '', '[\"req1:true\",\"req2:true\",\"req3:false\",\"req4:true\"]', '', 50, 1, '', '0000-00-00 00:00:00', 0),
(47, 71, 'Gabriel', 'Garcia Cabello', '28637607P', '11', '615104008', 'C/ Alcalde Juan Fernandez, 31', '2016-05-06', '', '', '', '', 'gabrielito_sevilla@hotmail.com', '[\"req1:true\",\"req2:true\",\"req3:false\",\"req4:false\"]', '', 60, 1, '', '0000-00-00 00:00:00', 0),
(48, 72, 'Agustin', 'Dominguez Torres', '49129511Q', '43', '603576455', 'C/ Mijail Gorbachov, 2, 2º A4', '2017-10-12', '', '', '', '', 'agutonnes1994@gmail.com', '[\"req1:true\",\"req2:false\",\"req3:false\",\"req4:false\"]', '', 30, 1, '', '0000-00-00 00:00:00', 0),
(49, 73, 'Sergio', 'Lopez Castaño', '30245515D', '40', '685612273', 'C/ Fernando de Magallanes, 72', '2017-10-06', '', '', '', '', 'xorgiesnorris@gmail.com', '[\"req1:true\",\"req2:true\",\"req3:false\",\"req4:false\"]', '', 50, 1, '', '0000-00-00 00:00:00', 0),
(50, 75, 'Roberto', 'Perez Blanco', '45701234G', '0', '955678086', 'Urbn. El Eucaliptal, C/ Cordialidad, 23', '2016-02-23', '', '', '', '', 'ropeblan@gmail.com', '[\"req1:true\",\"req2:true\",\"req3:true\",\"req4:true\"]', '', 60, 1, '', '0000-00-00 00:00:00', 0),
(51, 76, 'Michael', 'Codling', '535264415', '15', '684044792', 'C/ Paseo de la Independencia, 65, 6º A', '2016-11-15', '', '', '', '', 'mikecodling@outlook.com', '[\"req1:true\",\"req2:false\",\"req3:true\",\"req4:true\"]', '', 40, 1, '', '0000-00-00 00:00:00', 0),
(52, 77, 'Francisco Manuel', 'Gomez Gonzalez', '14620752C', '2', '622822683', 'C/ Vilches, 2 ( Moron de la Fra.)', '2017-05-24', '', '', '', '', 'kukixan@gmail.com', '[\"req1:true\",\"req2:true\",\"req3:false\",\"req4:false\"]', '', 60, 1, '', '0000-00-00 00:00:00', 0),
(53, 78, 'Joaquin', 'Calderon Jaime', '49090693', '38', '658225177', 'C/ Parroco Ruiz Mantero, 10', '2017-09-15', '', '', '', '', 'joakincalderon@gmail.com', '[\"req1:true\",\"req2:false\",\"req3:true\",\"req4:true\"]', '', 60, 1, '', '0000-00-00 00:00:00', 0),
(54, 79, 'Marvin Bruno', 'Mancini', 'AA1359610', '10', '666773548', 'Avda. España, 34, 1º D', '2016-07-19', '', '', '', '', 'mancima23@hotmail.com', '[\"req1:true\",\"req2:false\",\"req3:false\",\"req4:true\"]', '', 15, 1, '', '0000-00-00 00:00:00', 0),
(55, 80, 'Maria Auxiliadora', 'Jimenez Alvarez', '52262546Z', '2', '620487917', 'C/ Gonzalo de Berceo, 124', '2016-06-18', '', '', '', '', 'mariaja67@gmail.com', '[\"req1:true\",\"req2:true\",\"req3:true\",\"req4:true\"]', '', 50, 1, '', '0000-00-00 00:00:00', 0),
(56, 81, 'Luis Alberto', 'Mariscal Castilla', '28782135G', '28', '665523895', 'C/ Reina Sofia, 36', '2017-11-18', '', '', '', '', 'luismariscal@gmail.com', '[\"req1:true\",\"req2:true\",\"req3:true\",\"req4:false\"]', '', 60, 1, '', '0000-00-00 00:00:00', 0),
(57, 82, 'Francisco Javier', 'Hermosa Garrido', '28822996V', '2', '665095012', 'C/ Perez Hervas, 11, 2º A', '2017-12-30', '- SOCIO CANTANTE', '', '', '', '', '[\"req1:true\",\"req2:false\",\"req3:false\",\"req4:false\"]', '', 60, 1, '', '0000-00-00 00:00:00', 0),
(58, 83, 'Juan Manuel', 'Sanchez Flores', '28828626N', '2', '625281130', 'C/ Antonio Gala, 20, Bj. C', '2016-09-25', '', '', '', '', 'axionmedia@hotmail.com', '[\"req1:true\",\"req2:true\",\"req3:false\",\"req4:false\"]', '', 50, 1, '', '0000-00-00 00:00:00', 0),
(59, 84, 'Juan Luis ', 'Rivas Marquez', '48958137S', '2', '652207733', 'C/ Tajo, 153', '2017-12-03', '', '', '', '', '', '[\"req1:true\",\"req2:true\",\"req3:true\",\"req4:false\"]', '', 60, 1, '', '0000-00-00 00:00:00', 0),
(60, 85, 'Rocio', 'Carrasco Burgos', '28619935T', '59', '666690322', 'Pza. Luceros, 13, 3º 14', '2017-12-01', '', '', '', '', 'chyo1988@hotmail.com', '[\"req1:true\",\"req2:true\",\"req3:true\",\"req4:true\"]', '', 40, 1, '', '0000-00-00 00:00:00', 0),
(61, 86, 'Jonas', 'Gonzalez Parra', '48958089J', '59', '633773224', 'Pza. Sortes, 4, 1º C', '2017-12-30', '', '', '', '', '', '[\"req1:true\",\"req2:true\",\"req3:true\",\"req4:false\"]', '', 60, 1, '', '0000-00-00 00:00:00', 0),
(62, 87, 'Juan', 'Cordoba Rodriguez', '28778586C', '59', '611100557', 'C/ Vergel, 26', '2016-05-26', '', '', '', '', 'jkvertiny@gmail.com', '[\"req1:true\",\"req2:true\",\"req3:true\",\"req4:true\"]', '', 60, 1, '', '0000-00-00 00:00:00', 0),
(63, 88, 'Maria del Rosario', 'Muñoz Camarena', '52234261L', '10', '605037467', 'C/ Calderon de la Barca, 11, 2º B', '2016-05-16', '', '', '', '', 'camarena.1967@hotmail.es', '[\"req1:true\",\"req2:true\",\"req3:true\",\"req4:false\"]', '', 30, 1, '', '0000-00-00 00:00:00', 0),
(64, 89, 'Marino', 'Rojo Romero', '05903853Y', '64', '625447346', 'C/ Calderon de la Barca, 11, 2º B', '2017-05-16', '', '', '', '', 'mrr.1566@hotmail.com', '[\"req1:true\",\"req2:true\",\"req3:true\",\"req4:false\"]', '', 30, 1, '', '0000-00-00 00:00:00', 0),
(65, 90, 'Juan Francisco', 'Aguilar Granados', '49032559D', '15', '625021742', 'Avda. Juan Pablo II, 4, 3º B', '2017-08-16', '', '', '', '', 'redline.24@gmail.com', '[\"req1:true\",\"req2:true\",\"req3:true\",\"req4:false\"]', '', 50, 1, '', '0000-00-00 00:00:00', 0),
(66, 91, 'Jesus', 'Copado Mejias', '49032378N', '65', '2147483647', 'C/ Clara Campoamor, 98', '2017-12-26', '', '', '', '', 'copadojesus@gmail.com', '[\"req1:true\",\"req2:false\",\"req3:false\",\"req4:false\"]', '', 50, 1, '', '0000-00-00 00:00:00', 0),
(67, 93, 'David', 'Lucas Baston', '49032634S', '38', '645723719', 'C/ Virgen de la Almudena, 34', '2018-01-06', '', '', '', '', 'basconlucas96@gmail.com', '[\"req1:true\",\"req2:true\",\"req3:false\",\"req4:false\"]', '', 60, 1, '', '0000-00-00 00:00:00', 0),
(68, 94, 'Laura', 'Gomez Martinez', '48964154Y', '2', '615837872', 'Avda. España, 45, 4ºB', '2018-02-10', '', '', '', '', 'quelaura@hotmail.com', '[\"req1:true\",\"req2:true\",\"req3:true\",\"req4:false\"]', '', 30, 1, '', '0000-00-00 00:00:00', 0),
(69, 95, 'Antonio Jose', 'Sevillano Rodriguez', '49090608Y', '32', '625581983', 'C/ Castaño, 10', '2018-02-15', '', '', '', '', 'koke_dh@hotmail.com', '[\"req1:true\",\"req2:false\",\"req3:false\",\"req4:false\"]', '', 60, 1, '', '0000-00-00 00:00:00', 0),
(70, 96, 'German Mauricio', 'Torre Rodriguez', '75881132S', '45', '617972256', 'C/ Jose Carlos Luna, 5, 3º C', '2018-02-28', '', '', '', '', 'gmdltr@gmail.com', '[\"req1:true\",\"req2:true\",\"req3:true\",\"req4:false\"]', '', 60, 1, '', '0000-00-00 00:00:00', 0),
(71, 97, 'Juan', 'Fernandez Jimenez', '28796486A', '44', '617972256', 'C/ Milan, 20', '2018-02-28', '', '', '', '', 'juanfernandezmail@gmail.com', '[\"req1:true\",\"req2:true\",\"req3:true\",\"req4:true\"]', '', 30, 1, '', '0000-00-00 00:00:00', 0),
(72, 98, 'Jose Manuel', 'Vazquez Velazquez', '52236830N', '59', '635670110', 'Urbn. Entre Pinares, 16', '2018-03-03', '', '', '', '', 'josevazquezelrubio@hotmail.com', '[\"req1:true\",\"req2:true\",\"req3:false\",\"req4:false\"]', '', 60, 1, '', '0000-00-00 00:00:00', 0),
(73, 99, 'Jeronimo', 'Blazquez Rojas', '28498595P', '20', '635565256', 'C/ Virgen de los Reyes, 2, 3º C', '2018-03-20', '', '', '', '', 'jero_cs_tunning@hotmail.com', '[\"req1:true\",\"req2:true\",\"req3:false\",\"req4:false\"]', '', 60, 1, '', '0000-00-00 00:00:00', 0),
(74, 100, 'Jesus', 'Ariza Torres', '48960614P', '39', '636972218', 'C/ Marconi, 2, Bj. A', '2018-03-20', '', '', '', '', 'jesusariza19@hotmail.com', '[\"req1:true\",\"req2:true\",\"req3:true\",\"req4:false\"]', '', 60, 1, '', '0000-00-00 00:00:00', 0),
(75, 101, 'Jose Antonio', 'Rincon Ramos', '49131034K', '2', '662326898', 'C/ Tajo, 37', '2018-04-20', '', '', '', '', 'josanbetis19@gmail.com', '[\"req1:true\",\"req2:true\",\"req3:true\",\"req4:false\"]', '', 60, 1, '', '0000-00-00 00:00:00', 0),
(76, 102, 'Joshua Valentin', 'Delveaux', '151006100', '15', '672234687', 'Rue de La Romanche, 2', '2018-05-31', '', '', '', '', 'delveaux_joshüa@orange.fr', '[\"req1:true\",\"req2:true\",\"req3:true\",\"req4:false\"]', '', 50, 1, '', '0000-00-00 00:00:00', 0),
(77, 103, 'Justine', 'Sadlej', '171262151', '76', '672239687', '2 Rue de la Romanche', '2018-05-31', '', '', '', '', 'sadlej-justine@hotmail.fr', '[\"req1:true\",\"req2:true\",\"req3:true\",\"req4:false\"]', '', 50, 1, '', '0000-00-00 00:00:00', 0),
(78, 105, 'Francisco Miguel', 'Caballero Triguerro', '47537596T', '16', '606543858', 'C/ Comunidad Autonoma Castilla La Mancha, 61', '2018-07-22', '', '', '', '', '', '[\"req1:true\",\"req2:false\",\"req3:false\",\"req4:false\"]', '', 60, 1, '', '0000-00-00 00:00:00', 0),
(79, 106, 'Sergio', 'Yubero Muriel', '27324908B', '45', '654590763', 'Avda. Enriquez de Ribera, 94', '2018-07-17', '', '', '', '', 'sergioyuberom@gmail.com', '[\"req1:true\",\"req2:false\",\"req3:true\",\"req4:true\"]', '', 20, 1, '', '0000-00-00 00:00:00', 0),
(80, 108, 'Pieter Roger C', 'Grouwels', 'B24885107', '16', '2147483647', 'Lanaken', '2018-08-10', '', '', '', '', 'xinline@icloud.com', '[\"req1:true\",\"req2:true\",\"req3:true\",\"req4:true\"]', '', 30, 1, '', '0000-00-00 00:00:00', 0),
(81, 109, 'Jose Antonio', 'Espada Redaño', '49029181N', '29', '639843219', 'C/ Galgo, 3, Bj. A', '2018-08-24', '', '', '', '', 'joseantonioespadanazalux@gmail.com', '[\"req1:true\",\"req2:true\",\"req3:true\",\"req4:false\"]', '', 60, 1, '', '0000-00-00 00:00:00', 0),
(82, 110, 'Jose Maria ', 'Romero Lera', '15414635N', '22', '625749756', 'C/ Avena, 4', '2018-08-29', '', '', '', '', 'jmromero523@gmail.com', '[\"req1:true\",\"req2:true\",\"req3:true\",\"req4:false\"]', '', 60, 1, '', '0000-00-00 00:00:00', 0),
(83, 111, 'Manuel Jesus', 'Coronado Delgado', '49136565D', '59', '658160441', 'Brda. Entrenaranjos, 8, 2º D', '2018-09-04', '', '', '', '', 'joukspider@gmail.com', '[\"req1:true\",\"req2:false\",\"req3:false\",\"req4:false\"]', '', 60, 1, '', '0000-00-00 00:00:00', 0),
(84, 112, 'Bianca Armstrong', 'Fernandez', '45877327J', '38', '602675327', 'C/ Ruiz Gijon, 17, 1º A', '2018-09-08', '', '', '', '', 'biancafernandezdance@gmail,com', '[\"req1:true\",\"req2:true\",\"req3:true\",\"req4:true\"]', '', 15, 1, '', '0000-00-00 00:00:00', 0),
(85, 113, 'David Nathaniel', 'Inglett', 'INGLE8081', '2', '2147483647', 'Nova Santi Petri', '2018-10-19', '', '', '', '', 'dinglett83@mail.com', '[\"req1:true\",\"req2:true\",\"req3:true\",\"req4:true\"]', '', 60, 1, '', '0000-00-00 00:00:00', 0),
(86, 114, 'Dylan Andre M', 'Warnier', '592451729', '15', '495915429', 'Rue Fontaine au Diez, 27', '2018-10-02', '', '', '', '', 'dylan.warnier@hotmail.com', '[\"req1:true\",\"req2:true\",\"req3:false\",\"req4:false\"]', '', 30, 1, '', '0000-00-00 00:00:00', 0),
(87, 116, 'Gregorio', 'Perez Romera', '49026896G', '16', '722232979', 'C/ Sierra Morena, 8', '2018-11-12', '', '', '', '', 'ashunshinshomein@gmail.com', '[\"req1:true\",\"req2:true\",\"req3:false\",\"req4:false\"]', '', 31, 1, '', '0000-00-00 00:00:00', 0),
(88, 117, 'Maria', 'Gavilan Fernandez', '20477161P', '16', '722252432', 'C/ Sierra Morena, 8', '2018-11-12', '', '', '', '', 'gaferma82@gmail.com', '[\"req1:true\",\"req2:true\",\"req3:false\",\"req4:false\"]', '', 30, 1, '', '0000-00-00 00:00:00', 0),
(89, 12, 'asdasdas', 'asdasdasd', 'asdasdasgsg', '35', '5345345345', 'asdasdasdas', '2019-03-12', '', '', '', '', 'asdasdas5@adfasf.com', '[\"req1:true\",\"req2:false\",\"req3:false\",\"req4:true\"]', '[\"req1:true\",\"req2:false\"]', 25, 1, 'asdasdasgsg.png', '0000-00-00 00:00:00', 0),
(90, 189, 'sdasd asdas', 'asfasfdasda sadsd', 'sad23434f', '35', '3434235235', 'aadfadfa343 df', '2019-03-13', '', '', '', '', 'asdasdasd@asdasd.com', '[\"req1:true\",\"req2:false\",\"req3:false\",\"req4:false\"]', '[\"req1:true\",\"req2:false\"]', 25, 1, 'sad23434f.png', '0000-00-00 00:00:00', 0),
(91, 8, 'asfasfasfas', 'asfasfas asfadfad', 'wer342534', '2', '5345345345324', 'adsfasfasfdasfasf', '2019-03-13', '', '', '', '', 'sdgfsdgsgsdgfsdgf', '[\"req1:false\",\"req2:true\",\"req3:true\",\"req4:true\"]', '', 35, 1, '', '0000-00-00 00:00:00', 0),
(92, 11, 'qewrfeafsd gsdg', 'sdgsdgsd gsdg', '4543gwrgsrt', '4', '34234234234', 'fasfadfsdf sdf sdf', '2019-03-13', '', '', '', '', 'ºdsgfsrgrwtwert', '[\"req1:false\",\"req2:true\",\"req3:true\",\"req4:true\"]', '[\"req1:true\",\"req2:false\"]', 25, 1, '', '0000-00-00 00:00:00', 0),
(96, 50, 'Daniel', 'Vázquez', 'asdfadfdaf', '0', '722712541', 'C/ Jucar 25', '2019-03-18', '', '', '', '', 'danielcazalla3@gmail.com', '[\"req1:false\",\"req2:true\",\"req3:true\",\"req4:false\"]', '[\"req1:true\",\"req2:false\"]', 24, 1, '', '0000-00-00 00:00:00', 0),
(97, 183, 'Daniel', 'Vázquez', '34234342', '18', '722712541', 'C/ Jucar 25', '2019-03-18', '', '', '', '', 'danielcazalla3@gmail.com', '[\"req1:true\",\"req2:false\",\"req3:true\",\"req4:true\"]', '[\"req1:true\",\"req2:false\"]', 25, 1, '', '0000-00-00 00:00:00', 0),
(116, 242, 'asdasdas', 'asdasdas', 'asdasdasd', '2', '32423423413', 'asdasdasdas', '2019-05-20', '', '', '', '', 'asfdasdasd@asdasd.com', '[\"req1:true\",\"req2:false\",\"req3:false\",\"req4:false\"]', '[\"req1:true\",\"req2:false\"]', 25, 1, '', '0000-00-00 00:00:00', 0),
(121, 223, 'asdasdas', 'asdasdasd', '23423432f', '0', '345345345', 'sdfgsdfsdf', '2019-05-20', '', '', '', '', 'sdfsdfsdfsd', '[\"req1:true\",\"req2:true\",\"req3:false\",\"req4:false\"]', '[\"req1:false\",\"req2:false\"]', 25, 1, '', '0000-00-00 00:00:00', 0),
(122, 3, 'Pruebass', 'CXD', '13423423a', '4', '2342342342', 'asdasdasdasd asdasd', '2019-06-10', '', '', '', '', 'asdfasdasd@wdasdasd.com', '[\"req1:true\",\"req2:true\",\"req3:false\",\"req4:false\"]', '[\"req1:true\",\"req2:false\"]', 5, 1, '', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tablet`
--

CREATE TABLE `tablet` (
  `id` int(10) NOT NULL,
  `estado` int(1) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `usuario` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tablet`
--

INSERT INTO `tablet` (`id`, `estado`, `fecha`, `hora`, `usuario`) VALUES
(86, 1, '2019-01-29', '16:20:17', 6),
(87, 0, '2019-01-29', '16:20:18', 6),
(88, 1, '2019-01-29', '18:58:48', 6),
(89, 0, '2019-01-29', '22:03:01', 5),
(90, 1, '2019-01-30', '17:31:11', 6),
(91, 0, '2019-01-30', '17:32:19', 6),
(92, 1, '2019-01-31', '15:15:57', 2),
(93, 0, '2019-01-31', '15:15:58', 2),
(94, 1, '2019-01-31', '15:38:53', 2),
(95, 1, '2019-01-31', '15:42:46', 2),
(96, 1, '2019-02-01', '09:24:13', 6),
(97, 1, '2019-02-04', '11:31:49', 6),
(98, 1, '2019-02-05', '09:35:39', 6),
(99, 1, '2019-02-06', '11:14:15', 6),
(100, 0, '2019-02-06', '12:46:12', 6),
(101, 1, '2019-02-06', '12:46:13', 6),
(102, 1, '2019-02-07', '15:14:20', 6),
(103, 1, '2019-02-07', '15:15:41', 6),
(104, 2, '2019-02-07', '15:30:53', 6),
(105, 1, '2019-02-12', '11:50:23', 6),
(106, 1, '2019-02-14', '16:49:18', 6),
(107, 0, '2019-02-14', '17:00:26', 6),
(108, 1, '2019-02-14', '17:23:42', 6),
(109, 1, '2019-02-19', '15:48:40', 6),
(110, 1, '2019-02-20', '15:28:40', 6),
(111, 1, '2019-02-21', '15:21:17', 6),
(112, 1, '2019-03-01', '11:16:04', 6),
(113, 1, '2019-03-04', '15:20:24', 6),
(114, 1, '2019-03-05', '16:03:13', 6),
(115, 1, '2019-03-06', '15:17:31', 6),
(116, 1, '2019-03-12', '11:40:04', 6),
(117, 0, '2019-03-12', '16:07:03', 6),
(118, 1, '2019-03-13', '15:19:27', 6),
(119, 1, '2019-04-10', '16:59:49', 6),
(120, 0, '2019-04-10', '16:59:50', 6),
(121, 1, '2019-04-10', '16:59:50', 6),
(122, 0, '2019-04-10', '16:59:51', 6),
(123, 1, '2019-04-10', '16:59:52', 6),
(124, 0, '2019-04-10', '17:00:01', 6),
(125, 1, '2019-04-10', '17:00:04', 6),
(126, 0, '2019-04-10', '17:00:05', 6),
(127, 1, '2019-04-10', '17:00:11', 6),
(128, 0, '2019-04-10', '17:01:40', 6),
(129, 1, '2019-04-10', '17:01:43', 6),
(130, 0, '2019-04-10', '17:01:44', 6),
(131, 0, '2019-04-10', '17:01:46', 6),
(132, 3, '2019-04-10', '17:09:03', 6),
(133, 3, '2019-04-10', '17:15:37', 6),
(134, 3, '2019-04-10', '17:16:18', 6),
(135, 3, '2019-04-10', '17:17:00', 6),
(136, 1, '2019-04-10', '17:17:54', 6),
(137, 0, '2019-04-10', '17:17:57', 6),
(138, 3, '2019-04-10', '17:17:58', 6),
(139, 3, '2019-04-10', '17:18:05', 6),
(140, 3, '2019-04-10', '17:18:39', 6),
(141, 3, '2019-04-10', '17:19:28', 6),
(142, 3, '2019-04-10', '17:20:24', 6),
(143, 1, '2019-04-10', '17:22:01', 6),
(144, 1, '2019-04-10', '17:23:02', 6),
(145, 3, '2019-04-10', '17:23:39', 6),
(146, 1, '2019-04-10', '17:23:43', 6),
(147, 3, '2019-04-10', '17:43:59', 6),
(148, 0, '2019-04-09', '17:44:05', 6),
(149, 1, '2019-04-10', '17:44:32', 6),
(156, 1, '2019-04-15', '17:25:43', 6),
(157, 2, '2019-04-15', '17:25:52', 6),
(159, 1, '2019-04-15', '17:39:05', 6),
(160, 0, '2019-04-15', '17:47:15', 6),
(161, 1, '2019-04-15', '17:47:32', 6),
(162, 0, '2019-04-15', '17:48:12', 6),
(163, 1, '2019-04-16', '16:59:34', 6),
(164, 0, '2019-04-16', '17:00:03', 6),
(165, 1, '2019-04-16', '17:00:13', 6),
(166, 0, '2019-04-16', '17:00:59', 6),
(167, 1, '2019-04-16', '17:02:58', 6),
(168, 0, '2019-04-16', '17:04:14', 6),
(169, 1, '2019-04-30', '16:36:28', 6),
(170, 0, '2019-04-30', '17:00:54', 6),
(171, 1, '2019-04-30', '17:01:03', 6),
(172, 0, '2019-04-30', '17:02:19', 6),
(173, 1, '2019-04-30', '17:02:24', 6),
(174, 1, '2019-05-06', '15:25:19', 6),
(175, 0, '2019-05-06', '15:25:36', 6),
(176, 1, '2019-05-06', '15:29:54', 6),
(177, 0, '2019-05-06', '15:34:44', 6),
(178, 1, '2019-05-06', '15:41:35', 6),
(179, 0, '2019-05-06', '16:10:17', 6),
(180, 1, '2019-05-06', '16:10:26', 6),
(181, 0, '2019-05-06', '16:19:26', 6),
(182, 1, '2019-05-06', '16:20:35', 6),
(183, 0, '2019-05-06', '16:50:00', 6),
(184, 1, '2019-05-06', '16:50:03', 6),
(185, 0, '2019-05-06', '16:50:36', 6),
(186, 1, '2019-05-06', '16:50:40', 6),
(187, 0, '2019-05-06', '16:51:01', 6),
(188, 1, '2019-05-06', '16:51:11', 6),
(189, 0, '2019-05-06', '16:51:38', 6),
(190, 1, '2019-05-06', '16:51:40', 6),
(191, 0, '2019-05-06', '16:56:01', 6),
(192, 1, '2019-05-06', '16:56:06', 6),
(193, 0, '2019-05-06', '16:57:44', 6),
(194, 1, '2019-05-06', '16:57:51', 6),
(195, 0, '2019-05-06', '16:58:12', 6),
(196, 1, '2019-05-06', '16:58:33', 6),
(197, 0, '2019-05-06', '17:22:08', 6),
(198, 1, '2019-05-06', '17:22:13', 6),
(199, 0, '2019-05-06', '17:22:39', 6),
(200, 1, '2019-05-06', '17:22:43', 6),
(201, 1, '2019-05-07', '15:06:58', 6),
(202, 0, '2019-05-07', '16:33:06', 6),
(203, 1, '2019-05-07', '16:40:53', 6),
(204, 1, '2019-05-10', '10:28:22', 6),
(205, 1, '2019-05-15', '15:06:57', 6),
(206, 0, '2019-05-15', '16:08:59', 5),
(207, 1, '2019-05-15', '16:09:18', 5),
(208, 1, '2019-05-16', '15:27:31', 6),
(209, 0, '2019-05-16', '15:36:06', 6),
(210, 1, '2019-05-16', '15:36:09', 6),
(211, 0, '2019-05-16', '15:38:35', 6),
(212, 1, '2019-05-16', '15:38:45', 6),
(213, 0, '2019-05-16', '15:53:14', 6),
(214, 1, '2019-05-16', '15:54:18', 6),
(215, 0, '2019-05-16', '16:00:59', 6),
(216, 1, '2019-05-16', '16:08:30', 6),
(217, 0, '2019-05-16', '16:19:06', 6),
(218, 0, '2019-05-16', '16:19:31', 6),
(219, 0, '2019-05-16', '16:20:15', 6),
(220, 0, '2019-05-16', '16:20:39', 6),
(221, 0, '2019-05-16', '16:20:42', 6),
(222, 0, '2019-05-16', '16:23:50', 6),
(223, 0, '2019-05-16', '16:24:10', 6),
(224, 0, '2019-05-16', '16:24:44', 6),
(225, 0, '2019-05-16', '16:25:09', 6),
(226, 0, '2019-05-16', '16:25:21', 6),
(227, 0, '2019-05-16', '16:25:42', 6),
(228, 0, '2019-05-16', '16:27:42', 6),
(229, 1, '2019-05-21', '15:44:01', 6),
(230, 0, '2019-05-21', '15:54:45', 6),
(231, 1, '2019-05-21', '16:58:19', 6),
(232, 1, '2019-05-22', '15:21:48', 6),
(233, 0, '2019-05-22', '15:28:03', 6),
(234, 1, '2019-05-22', '15:38:55', 6),
(235, 0, '2019-05-22', '15:40:03', 6),
(236, 1, '2019-05-22', '15:42:37', 6),
(237, 0, '2019-05-22', '15:50:25', 6),
(238, 1, '2019-05-22', '15:59:32', 6),
(239, 1, '2019-05-23', '15:37:03', 6),
(240, 1, '2019-05-27', '15:23:50', 6),
(241, 1, '2019-05-28', '15:11:05', 6),
(242, 0, '2019-05-28', '15:38:55', 6),
(243, 1, '2019-05-28', '15:39:21', 6),
(244, 0, '2019-05-28', '15:41:00', 6),
(245, 1, '2019-05-28', '16:47:15', 6),
(246, 1, '2019-05-29', '10:03:50', 6),
(247, 0, '2019-05-29', '10:05:26', 6),
(248, 1, '2019-05-29', '10:06:10', 6),
(249, 0, '2019-05-29', '10:06:31', 6),
(250, 1, '2019-05-29', '10:10:48', 6),
(251, 0, '2019-05-29', '10:11:02', 6),
(252, 1, '2019-05-29', '10:20:47', 6),
(253, 0, '2019-05-29', '10:21:12', 6),
(254, 1, '2019-05-29', '10:27:32', 6),
(255, 0, '2019-05-29', '10:27:57', 6),
(256, 1, '2019-05-29', '10:38:00', 6),
(257, 0, '2019-05-29', '10:40:31', 6),
(258, 1, '2019-05-29', '10:41:16', 6),
(259, 0, '2019-05-29', '10:41:43', 6),
(260, 1, '2019-05-29', '11:27:11', 6),
(261, 0, '2019-05-29', '11:59:26', 6),
(262, 1, '2019-05-29', '12:00:11', 6),
(263, 0, '2019-05-29', '13:02:33', 6),
(264, 1, '2019-05-29', '13:06:54', 6),
(265, 0, '2019-05-29', '13:10:16', 6),
(266, 1, '2019-05-29', '13:10:43', 6),
(267, 0, '2019-05-29', '13:17:52', 6),
(268, 1, '2019-05-29', '13:19:23', 6),
(269, 0, '2019-05-29', '13:22:14', 6),
(270, 1, '2019-05-29', '13:22:24', 6),
(271, 0, '2019-05-29', '17:13:40', 6),
(272, 1, '2019-05-29', '17:24:20', 6),
(273, 0, '2019-05-29', '17:56:49', 6),
(274, 1, '2019-05-29', '17:57:02', 6),
(275, 1, '2019-05-30', '14:40:59', 6),
(276, 0, '2019-05-30', '16:11:53', 6),
(277, 1, '2019-06-04', '15:21:09', 6),
(278, 1, '2019-06-05', '15:41:53', 6),
(279, 1, '2019-06-06', '14:35:03', 6),
(280, 3, '2019-06-06', '17:44:11', 6),
(281, 1, '2019-06-10', '12:09:40', 6),
(282, 0, '2019-06-10', '13:54:51', 6),
(283, 0, '2019-06-10', '13:55:22', 6),
(284, 0, '2019-06-10', '15:18:16', 6),
(285, 1, '2019-06-12', '15:36:10', 6),
(286, 0, '2019-06-13', '15:26:30', 6),
(287, 0, '2019-06-13', '15:37:16', 6),
(288, 1, '2019-06-14', '09:27:50', 6),
(289, 0, '2019-06-14', '09:34:54', 6),
(290, 1, '2019-06-17', '13:47:42', 6),
(291, 1, '2019-07-23', '10:06:45', 6),
(292, 0, '2019-07-23', '10:09:09', 6),
(293, 1, '2019-07-23', '10:10:41', 6),
(294, 0, '2019-07-23', '10:11:25', 6),
(295, 1, '2019-07-23', '10:11:38', 6),
(296, 0, '2019-07-23', '10:31:09', 6),
(297, 1, '2019-07-23', '13:01:09', 6),
(298, 0, '2019-07-23', '13:33:08', 6),
(299, 1, '2019-07-30', '14:13:18', 6),
(300, 1, '2019-08-27', '10:25:26', 6),
(301, 1, '2019-08-30', '09:53:45', 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(2) NOT NULL,
  `tipo` int(1) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `apellidos` varchar(50) NOT NULL,
  `clave` varchar(200) NOT NULL,
  `fecha_registro` date NOT NULL,
  `correo` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `tipo`, `nombre`, `apellidos`, `clave`, `fecha_registro`, `correo`) VALUES
(5, 1, 'Francisco Javier', 'Ortiz Garrido', '827ccb0eea8a706c4c34a16891f84e7b', '2019-01-17', 'fran@fran.com'),
(6, 1, 'Daniel', 'Cazalla Vázquez', '813aefa4f0d38be9bf47753ce2e27b18', '2019-01-18', 'danielcazalla3@gmail.com'),
(7, 2, 'Prueba', 'prueba', '827ccb0eea8a706c4c34a16891f84e7b', '2019-03-18', 'a@a.com');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `articulos`
--
ALTER TABLE `articulos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cajas`
--
ALTER TABLE `cajas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categorias_articulos`
--
ALTER TABLE `categorias_articulos`
  ADD KEY `id` (`id`);

--
-- Indices de la tabla `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `correcciones`
--
ALTER TABLE `correcciones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cuotas`
--
ALTER TABLE `cuotas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `geneticas`
--
ALTER TABLE `geneticas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `reg_gen`
--
ALTER TABLE `reg_gen`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `retiradas`
--
ALTER TABLE `retiradas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sesiones`
--
ALTER TABLE `sesiones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `socios`
--
ALTER TABLE `socios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tablet`
--
ALTER TABLE `tablet`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `articulos`
--
ALTER TABLE `articulos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT de la tabla `cajas`
--
ALTER TABLE `cajas`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=191;

--
-- AUTO_INCREMENT de la tabla `categorias_articulos`
--
ALTER TABLE `categorias_articulos`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT de la tabla `config`
--
ALTER TABLE `config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `correcciones`
--
ALTER TABLE `correcciones`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `cuotas`
--
ALTER TABLE `cuotas`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=162;

--
-- AUTO_INCREMENT de la tabla `geneticas`
--
ALTER TABLE `geneticas`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT de la tabla `reg_gen`
--
ALTER TABLE `reg_gen`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `retiradas`
--
ALTER TABLE `retiradas`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=126;

--
-- AUTO_INCREMENT de la tabla `sesiones`
--
ALTER TABLE `sesiones`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=258;

--
-- AUTO_INCREMENT de la tabla `socios`
--
ALTER TABLE `socios`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;

--
-- AUTO_INCREMENT de la tabla `tablet`
--
ALTER TABLE `tablet`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=302;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
