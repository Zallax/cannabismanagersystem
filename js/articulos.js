cargarContenido();
$(document).ready(function() {
    $.post("php/ajax/articulos.php",{funcion:"listaArt"}, function(result){
           $("#zonaContenedoresArticulos").html(result);
           cargarArticulos($("#zonaCategoriasArticulos a:first").attr("data-id-cat"));

           
       
       });
 

	
});

function cargarContenido(){

    $.post("php/ajax/articulos.php",{funcion:"catArticulos"}, function(result){

        $("#zonaCategoriasArticulos").html(result);
        $("#zonaCategoriasArticulos a").on("click", function(){
            cargarArticulos($(this).attr("data-id-cat"));

        })




    });




}

function cargarArticulos(categoria){

    $.post("php/ajax/articulos.php",{funcion:"contenidoArticulos", categoria:categoria},function(result){
        $("#seccionCat"+categoria).html(result);
         $(".btn-mas").on("click", sumarCantidad);
         $(".btn-menos").on("click", restarCantidad);
         $(".btn-anadir-articulo").on("click", anadirArticulo);
       
       
    });

  
}


function sumarCantidad(){
  var este = $(this);
  var cantidad = parseInt(este.siblings('.contador').html());
  cantidad+=1;

  este.siblings('.contador').html(cantidad);

}
function restarCantidad(){
  var este = $(this);
  var cantidad = parseInt(este.siblings('.contador').html());
  if(cantidad!=1){
    cantidad-=1;
    este.siblings('.contador').html(cantidad);
  }
  

  

}

function anadirArticulo(){
  var boton = $(this);
  boton.attr("disabled", true);
  var cantidad = parseInt(boton.parent().siblings(".col-sm-6").children(".btn-cantidad").children(".contador").html());
  var id = boton.attr("data-id");
  var precio = parseFloat(boton.attr("data-precio")).toFixed(2);
  editarCookieArticulos(id,cantidad, 2, precio);
  boton.parent().siblings(".col-sm-6").children(".btn-cantidad").children(".contador").html("1");
  var iconoAnterior = boton.html();

  boton.children('i').fadeOut(500, function(){
    boton.html('<i class="fas fa-check"></i>');
    boton.children('i').fadeIn(1000,);

    boton.children('i').delay(1000).fadeOut(500, function(){
      boton.html(iconoAnterior);
       boton.attr("disabled", false);
    });
  });
  rellenarCarrito();
}

function editarCookieArticulos(id, cantidad, tipo, precio){
  var existe = false;
  if (getCookie('art')!=""){
    var articulos=JSON.parse(getCookie('art'));
    var narticulos = articulos.length;
    for(var i=0;i<articulos.length;i++){ 

  
      if(articulos[i][1]==id){
        if(articulos[i][0]==2){
          existe = true;
          articulos[i][2] = parseInt(articulos[i][2]+cantidad);
        }
        
      
  
      
      }
    }

  } else {
    var narticulos = 0;
    var articulos = [];
  }
  
  if(!existe){
    var objeto = [tipo, id, cantidad, precio];
    articulos.push(objeto);
    
  } 

  document.cookie = "art="+JSON.stringify(articulos); 


  
  actualizarNArticulos();
 

  
}




