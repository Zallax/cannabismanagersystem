

let idioma = localStorage.getItem("lang");

if(idioma=="en"){
	traducirIngles();
	

}



/*$("#btn-esp").on("click", function(){
	localStorage.setItem("lang","es");
	location.href ="actualizar.php";
});

$("#btn-en").on("click", function(){
	localStorage.setItem("lang","en");
	location.href ="actualizar.php";
});	

*/

function traducirIngles(){
		//otros
		$(".text-salir").text("EXIT");
		$(".btn-anadir").text("ADD");
		$(".btn-anadir-prod").text("ADD");
		$(".text-atras").text("BACK");

		//Home
		$("#tHome1").text("CANNABIS");
		$("#tHome2").text("SHOP");
		$("#tHome3").text("PAY");
		$("#tHome4").text("Profile")
		$("#tHome5").text("Recomendations and news");


		//Perfil
		$("#tPerfil1").text("Up date");
		$("#tPerfil2").text("Last pay");
		$("#tPerfil3").text("Renewal date");
		$("#tPerfil4").text("End of your membership");
		$("#tPerfil5").text("LAST ACTIVITY");

		//Inicio
		$(".zona-login #dni").attr("placeholder","ID NUMBER");
		$(".zona-login #nsocio").attr("placeholder","MEMBERSHIP NUMBER");
		$("#btn-acceder span").text("Access");
		$("#tIndex1").text("NEW MEMBERSHIP");
		$(".zona-error").html("The information entered is not correct");

		//Zona Cannabis
		$("#btn-zona-hibridas").text("HYBRID");
		$("#btn-zona-indicas").text("INDICA");
		$("#btn-zona-sativas").text("SATIVA");
		$("#btn-zona-extraccion").text("EXTRACTION");
		$("#btn-zona-otros").text("OTHERS")
		$(".mostrar-infoGenetica").text("See");
		$(".zona-contador .col-sm-12").text("How much do you want?");
		$("#btn-seleccionar-genetica").html('<i class="fas fa-plus"></i> SELECT');
		$("#t-marihuana").text("MARIJUANA");



		//Alta
		$(".inicio").text("Home");
		$("#tAlta1").text("Welcome!");
		$("#tAlta2").html("We welcome you to our association.</br>"+
			"We want you to feel at home, so we ask you to respect all the real estate material, as well as the rest of the partners to achieve a pleasant coexistence together.</br>"+
			"Next we are going to request a series of personal data.</br></br>"+
			"This data is stored for the sole purpose of managing your membership internally.</br>Only the board of directors will have access to this data, which is duly protected."+
			"You can ask any questions with the association staff.");
		$("#btn-paso0").text("Start")

		$(".t-nuevosocio").text("NEW MEMBERSHIP");
		$("#z1").text("Step 1");
		$("#z2").text("Step 2");
		$("#z3").text("Step 3");
		$("#z4").text("Step 4");

		$("#tAlta3").text("Personal Information");

		$(".input-ampliado").text("Enter the information");

		$("#tAlta4").text("Name");
		$("#nombre-socio").attr("placeholder", "Enter your Name");

		$("#tAlta5").text("Last Name");
		$("#apellidos-socio").attr("placeholder", "Enter your Last Name");

		$("#tAlta6").text("ID / NIE / DNI");
		$("#dni-socio").attr("placeholder", "Enter your ID");

		$("#email-socio").attr("placeholder", "Enter your Email");

		$("#tAlta8").text("Address");
		$("#direccion-socio").attr("placeholder", "Enter your Address");

		$("#tAlta9").text("Phone number");
		$("#tel-socio").attr("placeholder", "Enter your Phone number");

		$(".mensaje-error-val").text("You must complete all fields correctly")

		$("#tAlta10").text("BE A CANNABIS SATIVA L. CONSUMER");

		$("#tAlta11").text("BE A TOBACCO CONSUMER");

		$("#tAlta12").text("BE A PLANTS WITH THERAPEUTIC PROPERTIES CONSUMER");

		$("#tAlta13").text("BE A CANNABIS HABITUAL CONSUMER FOR THERAPEUTIC REASONS");

		$("#tAlta14").text("and have diagnosed a disease for which cannabis has"+
			"therapeutic effects according to the IACM, or that the palliative use of cannabinoids has been scientifically proven and provide a"+
			"medical certificate accrediting the request of cannabis use by the patient without medical opposition, or have a license to use"+
			"cannabis from any country in the world,");

		$("#tAlta15").text("ATTACHMENTS");

		$("#tAlta16").text("COPY ID / NIE");

		$("#tAlta17").text("MEDICAL CERTIFICATE");

		$("#tAlta18").html("<b>AFFIRM</b> my desire to join as PARTNER of the CANNABIC ASSOCIATION ");

		$("#tAlta19").text("knowing the statutes and objectives of it and I am committed to comply with them, with the rules of internal operation and with Spanish legislation, with special attention to:");

		$("#tAlta20").text("Article 19 of the Organic Law 1/1992, on Protection of Citizen Security:");

		$("#tAlta21").text("The agents of the Security Forces and Corps may limit or"+
	"restrict, for the necessary time, the circulation or permanence in roads or public places in cases of disturbing order, citizen security or"+
	"peaceful coexistence, when necessary for its restoration. Likewise, they may occupy preventively the effects or instruments that may be"+
	"used for illegal actions, giving them the destination that legally applies.");

		$("#tAlta22").text("For the discovery and detention of participants in a criminal act"+
	"causing serious social alarm and for the collection of instruments, effects or tests thereof, controls may be established on the roads,"+
	"places or public facilities, as necessary for the purpose of this section, in order to proceed to the identification of the persons who transit"+
	"or are in them, to the registration of the vehicles and to the superficial control of the personal effects in order to verify that they do not"+
	"carry substances or instruments prohibited or dangerous. The result of the diligence will be immediately reported to the Public Prosecutor.");


		$("#tAlta23").text("Article 25.1 of the Organic Law 1/1992, on Protection of Citizen Security:");
		$("#tAlta24").text("Serious infringements to citizen security are the consumption in"+
		"places, roads, establishments or public transports, as well as illegal possession, even if it was not destined for trafficking toxic drugs,"+
		"narcotics or psychotropic substances, provided that it does not constitute a criminal offense, as well as the abandonment in the"+
		"aforementioned sites of tools or instruments used for their consumption.");

		$("#tAlta25").text("Article 368 of the Spanish Penal Code, Organic Law 5/2010:");

		$("#tAlta26").text("Those who execute acts of cultivation, processing or trafficking, or otherwise"+
	"promote, favor or facilitate the illegal consumption of toxic drugs, narcotics or psychotropic substances, or possess them with those"+
	"ends, will be punished with prison sentences of three to six years and fine of up to three times the value of the drug subject to the crime"+
	"if they were substances or products that cause serious damage to health, and from prison to one three years and fine up to double in"+
	"the other cases.");
		$("#tAlta27").text("Article 18 of the Spanish Constitution:");

		$("#tAlta28").text("The right to honor, to personal and family privacy and to one&#39;s own image is guaranteed.");
		$("#tAlta29").text("The address is inviolable. No entry or registration may be made in it without the consent of the owner or court order, except in case of flagrante crime.");
		$("#tAlta30").text("The secrecy of communications and, in particular, of postcards, telegraphs and telephones is guaranteed, except for judicial decisions.");
		$("#tAlta31").text("The Law will limit the use of information technology to guarantee the personal and family privacy of citizens and the full exercise of their rights.");

		$("#tAlta32").text("APPLICATION FOR PARTICIPATION IN THE COLLECTIVE CULTIVATION PROGRAM OF THE CANNABIC ASSOCIATION")

		$("#tAlta33").text("How much do you want to participate in the cultivation program?");

		$("#tAlta34").text("This amount should be an approximation and should not exceed 60 grams.");

		$("#cant-socio").attr("placeholder", "Grams");

		$("#tAlta35").text("I hereby manifest to be over 21 years old and my condition as a regular consumer of Cannabis Sativa L. and I request to participate in the"+
	"collective cultivation program with the members of the association assuming my co-responsibility in the crop with the rest of the people"+
	"participating in it.")

		$("#tAlta36").text("The product of my participation in the associative crop is ONLY AND EXCLUSIVELY FOR MY PERSONAL CONSUMPTION IN THE PRIVATE"+
	"SCOPE OF THE HEADQUARTERS, assuming any responsibility of my acts contrary to the Law that could be derived and exempting the"+
	"Association, Associates and Board of Directors.")

		$("#tAlta37").text("For this purpose, the sponsorship/endorsement of A PARTNER of the Association is attached to the present application that"+
	"states that it is aware of cannabis use by the person requesting its incorporation into the Association or documentation proving"+
	"that it is of a consumer person.");

		$("#tAlta38").html('I request to participate in the amount of <span class="font-weight-bold" id="cant-elegida">(NO SELECT)</span> gr. of cannabis per month, amount that will be reviewed or confirmed by'+
	'the partner each quarter. In case of not confirming or reviewing the amount already declared will be extended.');


		$("#tAlta39").text("The partner authorizes the active partners of the Association to cultivate, collect and distribute the Cannabis from associative"+
	"collective cultivation for consumption among the members.");

		$("#tAlta40").text("The partner is obliged to communicate immediately, at the time he/she decides to abandon his/her participation in the"+
	"Association, the request to leave the Association. The status of partner must be ratified annually.");

		$("#tAlta41").text("The partner will notify to the Association the notification of sanction based on article 25.1 of the Organic Law 1/1992, on Citizen"+
	"Protection, or the imputation of the offense established in article 3668 of the Penal Code, which may imply in the first case and"+
	"involving in the second the expulsion of the partner.");

		$("#tAlta42").text("The partner agrees not to assign his/her membership card, which is non-transferable; the transfer of the membership card will be"+
	"grounds for expulsion from the association. The partner will always show their identity document and the membership card to"+
	"withdraw their material.");

		$("#tAlta43").text("Any breach of the commitments acquired by signing this may imply or imply the expulsion of the Association through the"+
	"corresponding procedure established in the statutes.");

		$("#tAlta44").text("The applicant states that he/she has read the statutes and the present application to participate in the collective cultivation"+
	"program.");

		$("#tAlta45").text("Guarantor Member");

		$("#tAlta46").text("Guarantor ID");

		$("#tAlta47").text("Guarantor Membership number");

		$("#tAlta48").text("The data does not match any member");

		$("#tAlta49").text("Your sign");

		$("#clear").text("Clear");

		$("#boton-finalizar").text("Finish");

		$("#consultar-a").text("Check");

		$("#dni-a").attr("placeholder","Enter the ID of the Guarantor Member");

		$("#n-a").attr("placeholder","Enter the Membership number of your Guarantor");

		$("#tAlta50").text("Congratulation!");
		$("#tAlta51").html("It has been registered correctly. To finish the process, check with the staff</br>"+
	"of the association and you will be provided with a membership number so</br>you can start enjoying our services.");

		$("#tAlta52").html("Home");

		$(".text-pagar").html('PAY');

		$(".text-atras").html("BACK");



		//Barra lateral
		$(".tituloCategoriaArticulos").text("List of items");

		$("#sm1").text("No items");
		$("#sm2").text("No items");
		$("#vaciar-cesta").text("DELETE ALL");


		//FINALIZAR
		$(".btn-confirmar").text("CONFIRM");	


		//REVISION
		$("#tR1").html("Welcome");
		$("#tR2").html("This is the new digital dispensary. </br> As it is the first time you access, we need you to register your signature."+
			"</br>You will do it only once and it is done to register the contributions you withdraw, without having to re-sign each time.");
		$("#tR3").html("You can be calm, your data is protected and they are as confidential as before. </br> If you need more information, ask us and we will answer any questions.");

		$("#btn-firmar").html('Sign <i class="fas fa-chevron-circle-right"></i>');

		$("#tR4").html("Sign to continue");

		$("#btn-continuar").html("Continue");





}