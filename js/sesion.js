	$("#tablaCannabis").hide();
	$("#tablaProductos").hide();

	var eventoEliminarArticulo = false;

	var precio = 0;

	nArticulosCargados = 0;

if (getCookie('art')!=""){
	var articulos=JSON.parse(getCookie('art'));
	var narticulos = articulos.length;
	rellenarCarrito();


	
	


} else {
	var narticulos = 0;
	$("#tablaCannabis").hide();
	$("#tablaProductos").hide();
	//vaciarCarrito();
}

$(document).ready(function() {

	guardarUltimoToque();
	$(".botonSalir").on("click", cerrarSesion);
	$(".botonVolver").on("click", function(){
		location.href ="inicio.php";
	});
	$(".botonPagar").on("click", function(){
		location.href ="finalizar.php";
	});
	$("#n-articulos").html(narticulos);
	habilitarConfirmar();
	$(".btn-confirmar").on("click", registrarCompra);
	$("#otras-operaciones .btn-no").on("click", irInicio);
	$("#otras-operaciones .btn-si").on("click", animacionSalir);


});

$("body").on("click",guardarUltimoToque);
	var t;
	var k;
	var j;
	var totalTime;
function guardarUltimoToque(){

	clearTimeout(t);
	let capa = '<div id="salir" class="animated zoomIn text-center text-white"><h4>Saliendo en</h4><div class="contador">10</div><button id="btn-cancelar" class="btn btn-dark btn-block btn-lg">Cancelar</button></div>'
	//t = setTimeout(cerrarSesion, 120000);
	t = setTimeout(function(){
		clearTimeout(k);
		totalTime=11;
		updateClock();
		
		$("body").append(capa);
		$("*").on("click", function(){
			clearTimeout(k);
			$("#salir").removeClass("zoomIn").addClass("zoomOut");
			setTimeout(function(){
				$("#salir").remove();
			},1000);
			$(".container").removeClass("blur");
			totalTime=0;
			 //ARREGLAR ESTO JODER

		});
		$(".container").addClass("blur");
		k = setTimeout(cerrarSesion, 11000);
	}, 110000);
}

function updateClock() {

	
		$(".contador").html(totalTime);
		if(totalTime==0){
			$("#btn-cancelar").unbind();
		} else {
			totalTime-=1;
			
			j = setTimeout(updateClock,1000);
			
		}
	
	
	
}
function cerrarSesion(){
	$.post("php/cerrar-sesion.php",function(){
		document.cookie = "art=";
		location.href ="index.php";
	});

}
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function rellenarCarrito(){
	$("#vaciar-cesta").unbind().on("click", function(){
		document.cookie="art=[]";
		
			$(".tablaListaArticulos").fadeOut("500", function(){
				$("#sm1").fadeIn("500");
				$("#sm2").fadeIn("500");
			});

		
		rellenarCarrito();
		actualizarNArticulos();
	});
	$("#tablaCannabis tbody, #tablaProductos tbody").empty();
	var articulos=JSON.parse(getCookie('art'));
	if(articulos.length!=0){
		$("#vaciar-cesta").fadeIn();
	}
	for(i=0;articulos.length>i;i++){

		anadirArticuloIndividual(articulos[i]);
	}
}

function rellenarFinalizarCompra(){

	$(".btn-vaciar").unbind().on("click", function(){
		document.cookie="art=[]";
		window.location = "inicio.php";
	});
	var articulos=JSON.parse(getCookie('art'));
	
	if($("#descuento").val()==1){
		
		/*let precioArt = datosArticulo[0]["precio"];
		let pdescuento = parseFloat($("#pdescuento").val()/100);
		let cantDesc = (precioArt * pdescuento).toFixed(2);
		precioFinal = precioArt - cantDesc;*/
		$(".precio-finalizar").append('<span class="text-success font-weight-bold">Se aplicará un descuento del '+$("#pdescuento").val()+'% por socio terapéutico en las genéticas</span>')
	}
	for(i=0;i<articulos.length;i++){

		anadirArticuloIndividualEnFinalizar(articulos[i]);

	}


}

function anadirArticuloIndividual(articulo){
	
	articulo=articulo+'';
	var aArticulo = articulo.split(",");
	
	if(aArticulo[0]=="1"){
		$("#tablaCannabis").show();
		$("#sm1").hide();
		$.post("js/ajax/datoArticulo.php",{tabla:"geneticas", id:aArticulo[1]}, function(result){
			var datosArticulo = JSON.parse(result);
			$("#tablaCannabis tbody").append('<tr data-id="'+aArticulo[1]+'"><td>'+datosArticulo[0]["nombre"]+'</td><td class="text-center">'+aArticulo[2]+'</td><td><button class="btn btn-quitar-articulo1"><i class="fas fa-trash"></i></button></td></tr>');
			$(".btn-quitar-articulo1").unbind();
			$(".btn-quitar-articulo1").on("click", quitarArticuloCarro);

		});
		
	} else if (aArticulo[0]=="2"){
		$("#tablaProductos").show();
		$("#sm2").hide();
		$.post("js/ajax/datoArticulo.php",{tabla:"articulos", id:aArticulo[1]}, function(result){
			var datosArticulo = JSON.parse(result);
	
			$("#tablaProductos tbody").append('<tr data-id="'+aArticulo[1]+'"><td>'+datosArticulo[0]["nombre"]+'</td><td class="text-center">'+aArticulo[2]+'</td><td><button class="btn btn-quitar-articulo2"><i class="fas fa-trash"></i></button></td></tr>');
			$(".btn-quitar-articulo2").unbind();
			$(".btn-quitar-articulo2").on("click", quitarArticuloCarro);
		});

		
	}
	
}

function anadirArticuloIndividualEnFinalizar(articulo){
	
	articulo=articulo+'';
	var aArticulo = articulo.split(",");

	if(aArticulo[0]=="1"){
		console.log(aArticulo[3]);
		$("#listaCannabis").show();
		$("#zonaArticulos #sm1").hide();
		$.post("js/ajax/datoArticulo.php",{tabla:"geneticas", id:aArticulo[1]}, function(result){
			var datosArticulo = JSON.parse(result);
			let precioFinal;
			 /*else {
				
			}*/
			precioFinal = aArticulo[3];

			precioArticulo = parseFloat(precioFinal*aArticulo[2]);
			precio = precio+precioArticulo;
			var fondo = "background-image:url('panel/img/geneticas/"+datosArticulo[0]["img"]+"');";
			$("#listaCannabis").append(
				' <li class="list-group-item"><div class="row"><div class="col-2 col-sm-2 col-md-1"><div class="img-finalizar" style="'+fondo+' width:50px;height:50px;background-size:100%;border-radius:5px;"></div></div><div class="col-10 col-sm-10 col-md-11"><h3 class="n-articulo float-left" cantidad="'+aArticulo[2]+'">'+datosArticulo[0]["nombre"]+' ('+aArticulo[2]+' Gr.)</h3> <button class="btn btn-dark btn-lg btn-quitar-articulo-finalizar float-right" id="'+datosArticulo[0]["id"]+'" precio="'+precioFinal+'"><i class="fas fa-minus"></i></button></div></div></li>'
				);
			$(".btn-quitar-articulo-finalizar").unbind();
			$(".btn-quitar-articulo-finalizar").on("click", quitarArticuloFinalizar);
			nArticulosCargados+=1;
			if(nArticulosCargados==articulos.length){
				cargarPrecio();
			}
		});
		
	} else if (aArticulo[0]=="2"){

		$("#listaArticulos").show();
		$("#sm2").hide();
		$.post("js/ajax/datoArticulo.php",{tabla:"articulos", id:aArticulo[1]}, function(result){
			var datosArticulo = JSON.parse(result);
			precioArticulo = parseFloat(datosArticulo[0]["precio"]*aArticulo[2]);
			precio = precio+precioArticulo;
			
			$("#listaArticulos").append('<li class="list-group-item"><div class="row"><div class="col-12 col-sm-12 col-md-12"><h5 class="n-articulo float-left" cantidad="'+aArticulo[2]+'">'+datosArticulo[0]["nombre"]+' ('+aArticulo[2]+' Ud.)</h5><button class="btn btn-dark btn-lg float-right btn-quitar-articulo-finalizar2" id="'+datosArticulo[0]["id"]+'" precio="'+datosArticulo[0]["precio"]+'"><i class="fas fa-minus"></i></button></div></li>');
			$(".btn-quitar-articulo-finalizar2").unbind();
			$(".btn-quitar-articulo-finalizar2").on("click", quitarArticuloFinalizar);
			nArticulosCargados+=1;
			if(nArticulosCargados==articulos.length){
				cargarPrecio();
			}
		});

		
	}


	
}






function quitarArticuloCarro(){

	var id = $(this).parent("td").parent("tr").attr("data-id");;
	

	var articulos=JSON.parse(getCookie('art'));
	for(var i=0;i<articulos.length;i++){ 
		
		var aArticulo = parseInt(articulos[i][1]);
		console.log(aArticulo);
		if(aArticulo==id){
			
			articulos.splice(i, 1);
			narticulos = articulos.length;
			document.cookie = "art="+JSON.stringify(articulos)	; 
		
		}
	}


	
	$(this).parent("td").parent("tr").fadeOut(500,function(){
		var n = $(this).parent("tbody").children("tr").length;
		var tabla = $(this).parent("tbody").parent("table");
		$(this).remove();
	
		if(n==1){
			if(tabla.hasClass("1")){
				tabla.fadeOut("500", function(){
					$("#sm2").fadeIn("500");
				});

			}
			if(tabla.hasClass("2")){
				tabla.fadeOut("500", function(){
					$("#sm1").fadeIn("500");
				});
			}
		}
		
	});

	actualizarNArticulos();

	
}

function quitarArticuloFinalizar(){

	var id = $(this).attr("id");;
	var cantidad = $(this).parent("div").parent("div").find(".n-articulo").attr("cantidad");
	
	precio = parseFloat(precio)-(parseFloat($(this).attr("precio"))*cantidad);

	var articulos=JSON.parse(getCookie('art'));
	for(var i=0;i<articulos.length;i++){ 
		
		var aArticulo = parseInt(articulos[i][1]);

		if(aArticulo==id){
			
			articulos.splice(i, 1);
			narticulos = articulos.length;
			document.cookie = "art="+JSON.stringify(articulos)	; 
		
		}
	}


	
	$(this).parent("div").parent("div").parent("li").fadeOut(500,function(){
		var n = $(this).parent("ul").children("li").length;
		console.log(n);
		if(n==1){
			$(this).parent("ul").fadeOut("500").parent("div").children(".sin-geneticas").fadeIn("500");
		}

		$(this).remove();
	
		
		
	});

	actualizarNArticulos();
	cargarPrecio();
	
}

function actualizarNArticulos(){

  if (getCookie('art')!="" || getCookie('art')!="[]"){
    var articulos=JSON.parse(getCookie('art'));
    var narticulos = articulos.length;
   
  }

  if(narticulos==0){
  	$("#vaciar-cesta").fadeOut();
  }

  $("#n-articulos").html(narticulos);
  
  habilitarConfirmar();

}



function cargarPrecio(){

	$("#zonaArticulos .precio>span").html(precio.toFixed(2)+"€");
	var precioTotal = $("#zonaArticulos .precio>span").html();
	
	if(precioTotal=="0.00€"){
	  $(".btn-confirmar").fadeOut("500");
	  $(".btn-vaciar").fadeOut("500");
	}
}

function registrarCompra(){
	$(".btn-confirmar").unbind();
	$("#zonaFinalizar").fadeOut("500", ejecutarInsertCompra);

	
}


function ejecutarInsertCompra(){
	$(".procesando-compra").fadeIn("500");
	var articulos = getCookie('art');
	var total = parseFloat($(".precio-finalizar>.precio>span").html()).toFixed(2);
	let idCaja = $("#idCaja").val();
	
	if(idCaja==""){
		cerrarSesion();
		
	} else {
		$.post("js/ajax/procesarCompra.php", {articulos:articulos, total:total, idCaja, }, function(result){
			console.log(result);
			if(result=="success"){
				$(".procesando-compra").fadeOut("500", function(){
					$(".compra-procesada").fadeIn("500");
					document.cookie = "art=";
				});
				
				
			}
		});
	}
	
}


function irInicio(){
	
	location.href ="inicio.php";
}

function animacionSalir(){
	$(".compra-procesada").fadeOut("500", function(){
		cerrarSesion();
	})
}


function habilitarConfirmar(){
	if($("#n-articulos").html()!=0){
		$(".botonPagar").fadeIn("100");
		//$("#btn-finalizar").attr("href","#");
		$("#btn-finalizar").on("click", function(){
		  $(this).addClass("animated zoomOut");
		  $(".touchmenu.uno").fadeOut();
		  $(".touchmenu.dos").fadeOut();
		  esconderInterfaz();
		  setTimeout(function(){
		    window.location = "finalizar.php";
		  },500);


		});
	} else {
		$("#btn-finalizar").unbind();
		$(".botonPagar").fadeOut("100");
	}
}