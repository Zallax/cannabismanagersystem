
$(document).ready(function() {
	/*document.querySelector('input').addEventListener('keydown', function (e) {
	    if (e.which == 9) {
	        e.preventDefault();
	    }
	});*/

	$("#formAltaSocio").on("submit", function(e){
		e.preventDefault();
	});
	$(window).keydown(function(e) {  

 	  if (e.keyCode === 13) {//ENTER
 	    e.prevenDefault();
 	    alert("PARO");
 	  }   
 	});
	
	$("#zona-alta").fadeIn("500");

	//Formulario de ALTA
	$("#btn-paso0").on("click", function(){
		$("#alta-0").addClass("animated zoomOut");
		setTimeout(function(){
			$("#alta-0").hide();
			$(".card").fadeIn("500");
		},600);
	});
	$("#alta-1 #boton-siguiente").on("click", irAlta2);


	$("#alta-2 #boton-atras").on("click", irAlta1);
	$("#alta-2 #boton-siguiente").on("click", irAlta3);

	$("#alta-3 #boton-atras").on("click", irAlta2Atras);
	$("#alta-3 #boton-siguiente").on("click", irAlta4);

	$("#alta-4 #boton-atras").on("click", irAlta3Atras);

	$("#alta-4 #boton-finalizar").on("click", realizarAlta);

	$("#alta-1 input").on("focusin", ampliarInput);
	$("#alta-1 input").on("focusout", ocultarInput);

	$("#alta-1 input").on("focusout", comprobarDatoInput);

	$("#consultar-a").on("click", consultarAvalador);

});




function irAlta1(){
	
		$("#alta-2").fadeOut("500", function(){
			$("#alta-1").fadeIn("500");
			$("#z2").removeClass("activo");
			$("#z1").addClass("activo");

		});
	
	
}

function irAlta2(){

	if(validacion1()){
		$("#alta-1").fadeOut("500", function(){
			$("#alta-2").fadeIn("500");
			$("#z1").removeClass("activo");
			$("#z2").addClass("activo");
		});		
	}
}
function irAlta2Atras(){

	if(validacion1()){
		$("#alta-3").fadeOut("500", function(){
			$("#alta-2").fadeIn("500");
			$("#z3").removeClass("activo");
			$("#z2").addClass("activo");
		});		
	}
}
function irAlta3(){

	if(!$("#check5").prop('checked') && !$("#check6").prop('checked')){
		$("#check5").focus();
	} else {
		$("#alta-2").fadeOut("500", function(){
			$("#alta-3").fadeIn("500");
			$("#z2").removeClass("activo");
			$("#z3").addClass("activo");
	
			$("#cant-socio").on("keyup", function() {
			  $("#cant-elegida").html($(this).val());
			});
		});		
	}

	
}

function irAlta3Atras(){

	if(validacion1()){
		$("#alta-4").fadeOut("500", function(){
			$("#alta-3").fadeIn("500");
			$("#z4").removeClass("activo");
			$("#z3").addClass("activo");
		});		
	}
}
function irAlta4(){
	var valido = true;
	if($("#cant-socio").val()==""){
		$("#cant-socio").focus().addClass('is-invalid');
		valido = false;
	} 
	if(!$("#check-estatutos").prop('checked')){
		$("#check-estatutos").focus();
		valido = false; 
	} 
	
	if(valido){

		$("#alta-3").fadeOut("500", function(){
			$("#alta-4").fadeIn("500");
			$("#z3").removeClass("activo");
			$("#z4").addClass("activo");
			
	
	
		});		
	} 
	
	
}




function validacion1(){
	var validado = true;
	if($("#nombre-socio").val().length<2){
		$("#nombre-socio").addClass("is-invalid");
		validado = false;

	} else {
		$("#nombre-socio").removeClass("is-invalid");
	}


	if($("#apellidos-socio").val().length<2){
		$("#apellidos-socio").addClass("is-invalid");
		validado = false;

	} else {
		$("#apellidos-socio").removeClass("is-invalid");
	}


	if($("#dni-socio").val().length<4){
		$("#dni-socio").addClass("is-invalid");
		validado = false;

	} else {
		$("#dni-socio").removeClass("is-invalid");
	}


	if($("#email-socio").val().length<10){
		$("#email-socio").addClass("is-invalid");
		validado = false;

	} else {
		$("#email-socio").removeClass("is-invalid");
	}


	if($("#direccion-socio").val().length<9){
		$("#direccion-socio").addClass("is-invalid");
		validado = false;

	} else {
		$("#direccion-socio").removeClass("is-invalid");
	}


	if($("#tel-socio").val().length<9){
		$("#tel-socio").addClass("is-invalid");
		validado = false;

	} else {
		$("#tel-socio").removeClass("is-invalid");
	}

	if(!validado){
		$(".mensaje-error-val").fadeIn(500);
	} else {
		$(".mensaje-error-val").hide();
	}

	return validado;
}




function comprobarDatoInput(){
	var idActual = $(this).attr("id");
	console.log(idActual);
	var actual = $(this);
	if (idActual=="nombre-socio"){
		if($(actual).val()==""){
			$(actual).addClass("is-invalid");
		} else {
			$(actual).removeClass("is-invalid");
		}
		
	}

	if (idActual=="apellidos-socio"){
		if(actual.val()==""){
			$(actual).addClass("is-invalid");
		}else {
			$(actual).removeClass("is-invalid");
		}

		
	}

	if (idActual=="dni-socio"){
		if(actual.val()==""){
			$(actual).addClass("is-invalid");
		}else {
			$(actual).removeClass("is-invalid");
		}

		
	}

	if (idActual=="email-socio"){
		if(actual.val()==""){
			$(actual).addClass("is-invalid");
		}else {
			$(actual).removeClass("is-invalid");
		}

		
	}

	if (idActual=="direccion-socio"){
		if(actual.val()==""){
			$(actual).addClass("is-invalid");
		}else {
			$(actual).removeClass("is-invalid");
		}

		
	}

	if (idActual=="tel-socio"){
		if(actual.val()==""){
			$(actual).addClass("is-invalid");
		}else {
			$(actual).removeClass("is-invalid");
		}

		
	}

}


function consultarAvalador(){
	var dni = $("#dni-a").val();
	var nsocio = $("#n-a").val();
	if(dni!="" && nsocio!=""){
		$("#error-datos").fadeOut("500");
		$("#zona-busqueda").fadeOut("500", function(){
			$("#carga-a").fadeIn("500", function(){
				$.post("js/ajax/comprobarAvalador.php", {dni:dni, n:nsocio}, function(result){
					if(result!="error"){
						$("#datos-avalador").html(result);
						$("#carga-a").fadeOut("500", function(){
							$("#datos-avalador").fadeIn("500");
							$("#zona-firma").fadeIn("500", function(){
								resizeCanvas();
								$("#boton-finalizar").addClass("animated zoomInRight").show();
							});
						});
						
					} else {
						$("#carga-a").fadeOut("500", function(){
							$("#zona-busqueda").fadeIn("500");
							$("#error-datos").fadeIn("500");
						});
						
					}
				});
			});
		});
		
	}
}


















var canvas = document.getElementById('signature-pad');

// Adjust canvas coordinate space taking into account pixel ratio,
// to make it look crisp on mobile devices.
// This also causes canvas to be cleared.
function resizeCanvas() {
    // When zoomed out to less than 100%, for some very strange reason,
    // some browsers report devicePixelRatio as less than 1
    // and only part of the canvas is cleared then.
    var ratio =  Math.max(window.devicePixelRatio || 1, 1);
    canvas.width = canvas.offsetWidth * ratio;
    canvas.height = canvas.offsetHeight * ratio;
    canvas.getContext("2d").scale(ratio, ratio);
}

window.onresize = resizeCanvas;
resizeCanvas();

var signaturePad = new SignaturePad(canvas, {
  backgroundColor: 'rgb(255, 255, 255)' // necessary for saving image as JPEG; can be removed is only saving as PNG or SVG
});



/*document.getElementById('save-jpeg').addEventListener('click', function () {
  if (signaturePad.isEmpty()) {
    return alert("Please provide a signature first.");
  }

  var data = signaturePad.toDataURL('image/jpeg');
  console.log(data);
  alert(data);
});

*/

document.getElementById('clear').addEventListener('click', function () {
  signaturePad.clear();
});


function realizarAlta(){
	if(signaturePad.isEmpty()){

		$(".mensaje-firma").addClass("animated fadeInUp").show();
		setTimeout(function(){
			$(".mensaje-firma").removeClass("fadeInUp").addClass("fadeOutDown");
			setTimeout(function(){
				$(".mensaje-firma").removeClass("fadeOutDown animated").hide();
			}, 1000);
		},3000);
	} else {
		var nombre = $("#nombre-socio").val();
		var apellidos = $("#apellidos-socio").val();
		var dni = $("#dni-socio").val();
		var mail = $("#email-socio").val();
		var direccion = $("#direccion-socio").val();
		var telefono = $("#tel-socio").val();
		var consumo = $("#cant-socio").val();
		var requisitos = new Array();
		var req_docu = new Array();
		var avalador = $("#idAv").attr("idav");
		var firma = signaturePad.toDataURL('image/png');

		if($("#check1").prop('checked')){
		  requisitos[0] = 'req1:'+true;
		} else {
		  requisitos[0] = 'req1:'+false;
		}
		if($("#check2").prop('checked')){
		  requisitos[1] = 'req2:'+true;
		} else {
		  requisitos[1] = 'req2:'+false;
		}
		if($("#check3").prop('checked')){
		  requisitos[2] = 'req3:'+true;

		} else {
		  requisitos[2] = 'req3:'+false;
		}
		if($("#check4").prop('checked')){
		  requisitos[3] = 'req4:'+true;

		} else {
		  requisitos[3] = 'req4:'+false;

		 }

		 jsonRequisitos = JSON.stringify(requisitos);
		 

		 if($("#check5").prop('checked')){
		   req_docu[0] = 'req1:'+true;
		 } else {
		   req_docu[0] = 'req1:'+false;
		 }
		 if($("#check6").prop('checked')){
		   req_docu[1] = 'req2:'+true;
		 } else {
		   req_docu[1] = 'req2:'+false;
		 }

		 jsonRequisitosDocu = JSON.stringify(req_docu);
		 $(".card").fadeOut("500");
		 $.post("js/ajax/altaSocio.php", {nombre:nombre, apellidos:apellidos, dni:dni, mail:mail, direccion:direccion, telefono:telefono, consumo:consumo, requisitos:jsonRequisitos, req_docu:jsonRequisitosDocu, avalador:avalador, firma:firma}, function(result){
		 	if(result=="success"){
		 		$("#alta-5").show();
		 	} else {
		 		alert("ERROR");
		 	}
		 });
	}
}


function ampliarInput(){
	var texto = $(this).val();
	var input = $(this);
	
	$("#nombre-input").html(input.attr("placeholder"));
	$(".container-fluid").css("filter"," brightness(50%)");
	$(".container-fluid").css("transition","ease-in-out 0.3s");
	$(".zona-input-ampliado").fadeIn("100", function(){
		if(texto!=""){
			$(".input-ampliado").html(input.val());
		}
	});
	$(this).on("keyup", function(){	
		$(".input-ampliado").html(input.val());

	});
}

function ocultarInput(){
	$(".container-fluid").css("filter"," grayscale(0%)");
		$(".zona-input-ampliado").fadeOut("100", function(){
			let idioma = localStorage.getItem("lang");

			if(idioma=="en"){
				$(".input-ampliado").html("Enter the information");
			} else {
				$(".input-ampliado").html("Introduce la información");
			}
			
	});

}