

     <?php
        require_once("../../panel/php/conexion.php");
        extract($_POST);
        $consultaGenetica = consulta("select * from geneticas where id=$id ");
        session_start();
        $genetica=$consultaGenetica[0];
        if($_SESSION["medicinal"]==1){
          $precioArt = $genetica["precio"];
          $pdescuento = $_SESSION["pdescuento"]/100;
          $cantDesc = $precioArt * $pdescuento;
          $precioFinal = money_format('%.2n',$precioArt - $cantDesc);
        } else {
          $precioFinal = $genetica["precio"];
        }


        if($genetica["tipo"]=="verde"){
          $tipo="v";
        } else {
          $tipo = "m";
        }

     ?>       

              <input id="id" style="display:none;" value="<?php echo $genetica["id"];?>">
              <input id="precio" style="display:none;" value="<?php echo $precioFinal;?>">
              <input id="tipo" style="display:none;" value="<?php echo $tipo;?>">

              <div class="container-fluid">
                <div class="row">
                  <div class="col-sm-4">

                     <div class="fotoGenetica" <?php 
                          echo 'style="width:100%;height:300px;background-image:url(panel/img/geneticas/'.$genetica["img"].');background-size:100%;background-size: auto 100%;background-position: center;background-repeat:no-repeat"';
                      ?>></div>
                  </div>
                  <div class="col-sm-8">
                    <div style="background-color:#e3e3e3;padding:20px;height:300px" class="descripcion">
                      <input style="display:none;" id="stock" value="<?php echo $genetica["stock"];?>">
                      <h1 style="text-transform:uppercase; font-size:50px;"><?php echo $genetica["nombre"]?></h1>
                      <p style="font-size:20px;color:#676767;"><?php echo $genetica["descripcion"]?></p>
                    </div>
                </div>
              </div>
              <div class="row" style="margin-top:30px">
                <div class="col-sm-6">
                    <div class="tipoDato">Gusto</div>
                    <div class="dato"><?php echo $genetica["gusto"]?></div>
                </div>
               
                <div class="col-sm-6">
                    <div class="tipoDato">Banco</div>
                    <div class="dato"><?php echo $genetica["banco"]?></div>
                </div>
              </div>
              <div class="row" style="margin-top:30px">
                <div class="col-sm-3">
                    <div class="tipoDato">% Indica</div>
                    <div class="dato"><?php echo $genetica["pindica"]?></div>
                </div>
                <div class="col-sm-3">
                    <div class="tipoDato">% Sativa</div>
                    <div class="dato"><?php echo $genetica["psativa"]?></div>
                </div>
                <div class="col-sm-3">
                    <div class="tipoDato">% THC</div>
                    <div class="dato"><?php echo $genetica["pthc"]?></div>
                </div>
                <div class="col-sm-3">
                    <div class="tipoDato">% CBD</div>
                    <div class="dato"><?php echo $genetica["pcbd"]?></div>
                </div>
              </div>


              <div class="row zona-contador">
                <div class="col-sm-12" style="text-align:center;">
                  ¿Que cantidad deseas?
                </div>
                <div class="col-sm-5">
                  <div id="btn-menos" class="text-center btn-cont">
                    <i class="fas fa-minus"></i> 1.00 
                  </div>
                  <div id="btn-menos2" class="text-center btn-cont">
                    <i class="fas fa-minus"></i> 0.10
                  </div>
                  <div id="btn-menos3" class="text-center btn-cont">
                    <i class="fas fa-minus"></i> 0.01
                  </div>

                </div>
                <div class="col-sm-2">
                   <div id="cont-aportaciones">
                    0.00
                  </div>
                </div>
                <div class="col-sm-5">
                   <div id="btn-mas" class="text-center btn-cont">
                    <i class="fas fa-plus"></i> 1.00
                  </div>
                   <div id="btn-mas2" class="text-center btn-cont">
                    <i class="fas fa-plus"></i> 0.10
                  </div>
                   <div id="btn-mas3" class="text-center btn-cont">
                    <i class="fas fa-plus"></i> 0.01
                  </div>
                </div>
            </div>
            <div class="text-center"><button type="button" id="btn-seleccionar-genetica" style="height:70px;" class="btn btn-dark btn-lg mt-3"><i class="fas fa-plus"></i> SELECCIONAR</button></div>

      
            <script type="text/javascript">



              if(idioma=="en"){
                traducirIngles();
                

              }


              $("#btn-seleccionar-genetica").unbind().on("click", function(){
                anadirAlCarro(1);
              });
             var aportacion =  parseFloat(0.00);
             var maximo = parseFloat($("#stock").val());
              $("#btn-mas").on("click", sumarAportacion1);
              $("#btn-menos").on("click", restarAportacion1);

               $("#btn-mas2").on("click", sumarAportacion2);
              $("#btn-menos2").on("click", restarAportacion2);

               $("#btn-mas3").on("click", sumarAportacion3);
              $("#btn-menos3").on("click", restarAportacion3);

              function sumarAportacion1(){
              
                
                if(aportacion<=maximo-1){
                  aportacion=aportacion+1;
                  
                  $("#cont-aportaciones").html(aportacion.toFixed(2));
                  aportacion =  parseFloat($("#cont-aportaciones").html());
                  console.log(aportacion);
                 } 
              }
               function restarAportacion1(){
                 
                
                if(aportacion!=0.00 && (aportacion-1.00)>=0){
                  console.log(aportacion);
                  aportacion=aportacion-1;
                  
                  $("#cont-aportaciones").html(aportacion.toFixed(2));
                  aportacion =  parseFloat($("#cont-aportaciones").html());
                }
                
               
              }

              function sumarAportacion2(){
              
                
                if(aportacion<=maximo-0.10){
                  aportacion=aportacion+0.10;
                  
                  $("#cont-aportaciones").html(aportacion.toFixed(2));
                  aportacion =  parseFloat($("#cont-aportaciones").html());
                  console.log(aportacion);
                 } 
              }
               function restarAportacion2(){
                 
                console.log(aportacion);
                if(aportacion!=0.00 && (aportacion-0.10)>=0){
                  aportacion=aportacion-0.10;
                  
                  $("#cont-aportaciones").html(aportacion.toFixed(2));
                  aportacion =  parseFloat($("#cont-aportaciones").html());
                }
                
               
              }


              function sumarAportacion3(){
             
                
                if(aportacion<=maximo-0.01){
                  aportacion=aportacion+0.01;
                  
                  $("#cont-aportaciones").html(aportacion.toFixed(2));
                  aportacion =  parseFloat($("#cont-aportaciones").html());
                  console.log(aportacion);
                 } 
              }
               function restarAportacion3(){
                 
                
                if(aportacion!=0.00){
                  aportacion=aportacion-0.01;
                  
                  $("#cont-aportaciones").html(aportacion.toFixed(2));
                  aportacion =  parseFloat($("#cont-aportaciones").html());
                }
                
               
              }
            </script>
