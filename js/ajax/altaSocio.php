<?php
	require_once("../../panel/php/conexion.php");
	
	
	$datos = $_POST;
	
	if(altaSocio($datos)!=0){
		echo "success";
	} else {
		echo "error";
	}

	
	function altaSocio($datos)
		{	

			extract($datos);

			define('UPLOAD_DIR', '../../panel/img/socios/firma/');

			$fecha = date("y-m-d");
			
			$img = str_replace('data:image/png;base64,', '', $firma);
			$data = base64_decode($img);
			$file = UPLOAD_DIR . $dni . '.png';
			$success = file_put_contents($file, $data);
			$activacion = 0;
			$ruta_firma = $dni.".png";
			$sql = "insert into socios (nombre, apellidos, dni, avalador, telefono, direccion, alta, email, requisitos, req_docu, consumo, activado, firma) values (:nombre, :apellidos, :dni, :avalador, :telefono, :direccion, :alta, :email, :requisitos, :req_docu, :consumo, :activado, :firma);";
			


			$miconexion=connectDB();
			$statement = $miconexion->prepare($sql);
			$statement ->setFetchMode(PDO::FETCH_ASSOC);
			$statement->bindParam(':nombre', $nombre, PDO::PARAM_STR);
			$statement->bindParam(':apellidos', $apellidos, PDO::PARAM_STR);
			$statement->bindParam(':dni', $dni, PDO::PARAM_STR);
			$statement->bindParam(':avalador', $avalador, PDO::PARAM_INT);
			$statement->bindParam(':telefono', $telefono, PDO::PARAM_STR);
			$statement->bindParam(':direccion', $direccion, PDO::PARAM_STR);
			$statement->bindParam(':alta', $fecha, PDO::PARAM_STR);
			$statement->bindParam(':email', $mail, PDO::PARAM_STR);
			$statement->bindParam(':requisitos', $requisitos, PDO::PARAM_STR);
			$statement->bindParam(':req_docu', $req_docu, PDO::PARAM_STR);
			$statement->bindParam(':consumo', $consumo, PDO::PARAM_INT);
			$statement->bindParam(':activado', $activacion, PDO::PARAM_INT);
			$statement->bindParam(':firma', $ruta_firma, PDO::PARAM_STR);
			if(!$statement->execute()){
				echo "ERROR";
				$miconexion->close();
			} else {
				return $statement->rowCount();
			}
			

		}

?>