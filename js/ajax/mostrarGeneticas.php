<?php

		require_once("../../panel/php/conexion.php");

	extract($_POST);
	if($tipo=="verde"){
		mostrarVerdes($categoria);
	} else if ($tipo=="marron"){
		mostrarMarron($categoria);
	}

	else {
		mostrarOtros();
		
	}

	

	function mostrarVerdes($categoria){
		$geneticas = consulta("select * from geneticas where tipo='verde' and categoria='$categoria' order by nombre");

		if(count($geneticas)!=0){
					echo '<div class="row">';
					foreach($geneticas as $genetica){
						if($genetica["stock"]<=0){
							$claseAgotado = " agotado";
							$disabled = " disabled";
						} else {
							$claseAgotado = "";
							$disabled = "";
						}
						echo '<div class="col-sm-4 touchmenu">
			                 <div class="pCannabis'.$claseAgotado.' text-center"><div class="imagenCannabis" style="background-image:url(panel/img/geneticas/'.$genetica["img"].');"></div>
			                    <h1 class="nombreCannabis">'.$genetica["nombre"].'</h1>
			                    <button'.$disabled.' data-toggle="modal" data-id="'.$genetica["id"].'" data-target="#modal-genetica" class="btn btn-block btn-dark btn-lg mostrar-infoGenetica d-none">Ver</button>
			                 </div>
			                </div>';
					}

					echo '</div>';
					echo '<script type="text/javascript">$(".mostrar-infoGenetica").on("click", verGenetica);</script>';
				} else {
					echo '<div class="sin-geneticas">No hay genéticas disponibles</div>';
				}

		
	}

	function mostrarMarron($categoria){
		$geneticas = consulta("select * from geneticas where tipo='marron' and categoria='$categoria' order by nombre");

		if(count($geneticas)!=0){
					echo '<div class="row">';
					foreach($geneticas as $genetica){
						if($genetica["stock"]==0){
							$claseAgotado = " agotado";
							$disabled = " disabled";
						} else {
							$claseAgotado = "";
							$disabled = "";
						}
						echo '<div class="col-sm-3 touchmenu">
			                 <div class="pCannabis'.$claseAgotado.' text-center"><div class="imagenCannabis" style="background-image:url(panel/img/geneticas/'.$genetica["img"].');"></div>
			                    <h1 class="nombreCannabis">'.$genetica["nombre"].'</h1>
			                    <button'.$disabled.' data-toggle="modal" data-id="'.$genetica["id"].'" data-target="#modal-genetica" class="btn btn-block btn-dark btn-lg mostrar-infoGenetica">Ver</button>
			                 </div>
			                </div>';
					}

					echo '</div>';
					echo '<script type="text/javascript">$(".mostrar-infoGenetica").on("click", verGenetica);</script>';
				} else {
					echo '<div class="sin-geneticas">No hay genéticas disponibles</div>';
				}

		
	}

	function mostrarOtros(){
		$genetica=consulta("select * from geneticas where tipo='otros' and categoria='otros' order by nombre");
        count($genetica)>0 ? $datos=true : $datos=false;
        if($datos){

        		echo ' <ul class="list-group">';
                    for($i=0;$i<count($genetica);$i++){
                        if($genetica[$i]["stock"]<=0){
                          $claseAgotado = " pAgotado";
                          $disabled = " disabled";
                        } else {
                          $claseAgotado = "";
                          $disabled = "";
                        }

                        echo '
                       		<li class="list-group-item'.$claseAgotado.'"><div class="row">
                       		<div class="col-sm-4'.$claseAgotado.'">'.$genetica[$i]["nombre"].'

                       		</div>
                       		<div class="col-sm-6 text-center zonaCantidad">
                            <small style="font-size:10px;">CANTIDAD</small>
                       			<div class="btn-group'.$claseAgotado.' mr-2 btn-cantidad" style="border:1px solid #bababa;border-radius:5px;" role="group" aria-label="First group">
                       			    <button'.$disabled.' type="button" class="btn btn-secondary btn-menos">-</button>
                       			    <button'.$disabled.' type="button" class="btn contador">1</button>
                       			    <button'.$disabled.' type="button" class="btn btn-secondary btn-mas">+</button>
                       			  </div>
                       		</div>
                       		<div class="col-sm-2 text-right">
                       		 <button'.$disabled.' class="btn btn-lg btn-dark btn-anadir-articulo'.$claseAgotado.'" data-id="'.$genetica[$i]["id"].'" data-precio="'.$genetica[$i]["precio"].'">
                       		                       		<i class="fa fa-shopping-bag"></i>
                       		</button>
                       		</div>
                       		</div>
                       		</li>
                        ';
                    }
                echo '</ul>';

            } else {
                echo "<small class='text-muted'>No hay datos disponibles</small>";
            }
	}

	

	
?>

