
cargarHibridas();
cargarIndicasMarron();
$(document).ready(function() {

	$(".can").delay(400).fadeIn().on("click", function(){
		$(".zona-seleccion").fadeOut(500, function(){
			$(".zona-marihuana").fadeIn(500);
		});
	});
	$(".hac").delay(600).fadeIn().on("click", function(){
		$(".zona-seleccion").fadeOut(500, function(){
			$(".zona-hachis").fadeIn(500);
		});
	});
	$(".can2").delay(400).fadeIn().on("click", function(){
		$(".zona-hachis").fadeOut(500, function(){
			$(".zona-marihuana").fadeIn(500);
			
			$("#btn-zona-hibridas").tab('show');
			cargarHibridas();
		});
	});
	$(".hac2").delay(600).fadeIn().on("click", function(){
		$(".zona-marihuana").fadeOut(500, function(){
			$(".zona-hachis").fadeIn(500);
			$("#btn-zona-indicas-marron").tab('show');
			cargarIndicasMarron();
		});

	});


	var URLactual = window.location+'';
	var par = URLactual.split("?");

	if(par[1]!=undefined){
		verGenetica("auto", par[1]);
		$("#modal-genetica").modal("show");
		
	}
	//var modalId = localStorage.getItem("modal");
/*	if(modalId!="null"){
		verGenetica("auto", modalId);
		$("#modal-genetica").modal("show");

		localStorage.setItem("modal", null);
	}*/
	$("#btn-zona-hibridas").on("click", cargarHibridas);
	$("#btn-zona-indicas").on("click", cargarIndicas);
	$("#btn-zona-sativas").on("click", cargarSativas);
	$("#btn-zona-extraccion").on("click", cargarExtraccion);
	$("#btn-zona-otros").on("click", cargarOtros);


	$("#btn-zona-indicas-marron").on("click", cargarIndicasMarron);
	$("#btn-zona-sativas-marron").on("click", cargarSativasMarron);
	$("#btn-zona-hibridas-marron").on("click", cargarHibridasMarron);
	$("#btn-zona-extraccion-marron").on("click", cargarExtraccionMarron);


	

	$(".historial .btn-anadir").on("click", anadirAlCarroGPerfil);
	$(".historial .btn-anadir-prod").on("click", anadirAlCarroPPerfil);




});

function verGenetica(modo, bloque){
	if(modo=="bloque"){
		if(!(bloque.hasClass("agotado"))){
			$("#modal-genetica").modal("show");
			var id = bloque.find(".mostrar-infoGenetica").attr("data-id");
		}

	} else if(modo=="auto"){
			var id = bloque;
	} else {
		var boton=$(this);
		var id = boton.attr("data-id");
	}


	$.post("js/ajax/datosGenetica.php", {id:id}, function(result){
	    $("#modal-datosGenetica").html(result);

	});
}

function cargarHibridas(){
	limpiarTabs();
	$.post("js/ajax/mostrarGeneticas.php",{tipo:"verde",categoria:"hibrida"}, function(result){

        $("#zona-hibridas").empty().append(result);
        $(".pCannabis").unbind().on("click", function(){

        	verGenetica("bloque", $(this));
        });


    });
}



function cargarIndicas(){
	limpiarTabs();
	$.post("js/ajax/mostrarGeneticas.php",{tipo:"verde",categoria:"indica"}, function(result){

        $("#zona-indicas").empty().append(result);
        $(".pCannabis").unbind().on("click", function(){

        	verGenetica("bloque", $(this));
        });


    });
}

function cargarSativas(){
	limpiarTabs();
	$.post("js/ajax/mostrarGeneticas.php",{tipo:"verde",categoria:"sativa"}, function(result){

        $("#zona-sativas").empty().append(result);
        $(".pCannabis").unbind().on("click", function(){

        	verGenetica("bloque", $(this));
        });


    });
}

function cargarExtraccion(){
	limpiarTabs();
	$.post("js/ajax/mostrarGeneticas.php",{tipo:"verde",categoria:"extraccion"}, function(result){

        $("#zona-extraccion").empty().append(result);
        $(".pCannabis").unbind().on("click", function(){

        	verGenetica("bloque", $(this));
        });


    });
}
function cargarOtros(){
	limpiarTabs();
	postGeneticasOtros().done(function(data){

		var result = data;
		

        $("#zona-otros").empty().append(result);
        $(".btn-anadir-articulo").unbind().on("click", function(){
        	var boton = $(this);
        	boton.attr("disabled", true);
        	var cantidad = parseInt(boton.parent().siblings(".col-sm-6").children(".btn-cantidad").children(".contador").html());
        	var id = boton.attr("data-id");
        	var precio = parseFloat(boton.attr("data-precio")).toFixed(2);
        	var iconoAnterior = boton.html();
        	anadirAlCarroOtros(id, cantidad, precio, "otros");
        	boton.children('i').fadeOut(500, function(){
        	    boton.html('<i class="fas fa-check"></i>');
        	    boton.children('i').fadeIn(1000,);

        	    boton.children('i').delay(1000).fadeOut(500, function(){
        	      boton.html(iconoAnterior);
        	       boton.attr("disabled", false);
        	    });
        	  });
        });
        $(".btn-mas").unbind().on("click", sumarCantidad);
        $(".btn-menos").unbind().on("click", restarCantidad);

    });
}

function cargarIndicasMarron(){
	limpiarTabs();
	$.post("js/ajax/mostrarGeneticas.php",{tipo:"marron",categoria:"indica"}, function(result){

        $("#zona-indicas-marron").empty().append(result);
        $(".pCannabis").unbind().on("click", function(){

        	verGenetica("bloque", $(this));
        });

    });
}
function cargarSativasMarron(){
	limpiarTabs();
	$.post("js/ajax/mostrarGeneticas.php",{tipo:"marron",categoria:"sativa"}, function(result){

        $("#zona-sativas-marron").empty().append(result);
        $(".pCannabis").unbind().on("click", function(){

        	verGenetica("bloque", $(this));
        });


    });
}

function cargarHibridasMarron(){
	limpiarTabs();
	$.post("js/ajax/mostrarGeneticas.php",{tipo:"marron",categoria:"hibrida"}, function(result){

        $("#zona-hibridas-marron").empty().append(result);
        $(".pCannabis").unbind().on("click", function(){

        	verGenetica("bloque", $(this));
        });


    });
}

function cargarExtraccionMarron(){
	limpiarTabs();
	$.post("js/ajax/mostrarGeneticas.php",{tipo:"marron",categoria:"extraccion"}, function(result){

        $("#zona-extraccion-marron").empty().append(result);
        $(".pCannabis").unbind().on("click", function(){

        	verGenetica("bloque", $(this));
        });


    });
}

function anadirAlCarro(tipo){
	var cantidad = parseFloat($("#cont-aportaciones").html()).toFixed(2);
	var id = $("#id").val();
	var precio = parseFloat($("#precio").val()).toFixed(2);
	var stock = parseFloat($("#stock").val());
	var tipoGen = $("#tipo").val();

	if(cantidad==stock){
		anularCannabis(id);
	}
	if (cantidad!=0){
		editarCookieArticulosG(id,cantidad, tipo, precio, tipoGen);
	}
	rellenarCarrito();


}

function anadirAlCarroOtros(id, cantidad, precio, tipoGen){

	if (cantidad!=0){
		editarCookieArticulosG(id,cantidad, 1, precio, tipoGen);
	}
	rellenarCarrito();


}

function anadirAlCarroGPerfil(){

  var boton = $(this);

  boton.attr("disabled", true);
  var cantidad = (boton.parent().find(".cant").html()).toFixed(2);
  var id = boton.attr("data-id");
  var precio = boton.attr("data-precio");
  var tipoGen = boton.attr("data-tipo");
  editarCookieArticulosG(id,cantidad, 1, precio, tipoGen);
  
  var iconoAnterior = boton.html();

  boton.fadeOut(500, function(){
    boton.html('AÑADIDO');
    boton.fadeIn(1000,);
    boton.attr("disabled", true);
   
  });
  rellenarCarrito();
}

function anadirAlCarroPPerfil(){

  var boton = $(this);

  boton.attr("disabled", true);
  var cantidad = parseInt(boton.parent().find(".cant").html());
  var id = boton.attr("data-id");
  var precio = parseFloat(boton.attr("data-precio"));

  editarCookieArticulos(id,cantidad, 2, precio);
  
  var iconoAnterior = boton.html();

  boton.fadeOut(500, function(){
    boton.html('AÑADIDO');
    boton.fadeIn(1000,);
    boton.attr("disabled", true);
   
  });
  rellenarCarrito();
}

function editarCookieArticulosG(id, cantidad, tipo, precio, tipoGen){
	var existe = false;
	if (getCookie('art')!=""){
	  var articulos=JSON.parse(getCookie('art'));
	  var narticulos = articulos.length;
	  for(var i=0;i<articulos.length;i++){


		if(articulos[i][1]==id){
		  if(articulos[i][0]==1){
		  	existe = true;
		  	var aux = parseFloat(cantidad).toFixed(2);
		  	var aux2 = parseFloat(articulos[i][2]).toFixed(2);
		  	var suma = parseFloat(aux)+parseFloat(aux2);
		  	
		  	articulos[i][2] = suma.toString();
		  }
		  



		}
	  }

	} else {
	  var narticulos = 0;
	  var articulos = [];
	}

	if(!existe){
	  var objeto = [tipo, id, cantidad, precio, tipoGen];
	  articulos.push(objeto);

	}

	document.cookie = "art="+JSON.stringify(articulos);

	actualizarNArticulos();
	$("#btn-seleccionar-genetica").removeClass("btn-dark").addClass("btn-success").css("transform","scale(1.03)");

	setTimeout(function(){
		$("#modal-genetica").modal("hide");
		$("#btn-seleccionar-genetica").removeClass("btn-success").addClass("btn-dark").css("transform","scale(1.0)");
	},1000);


}

function editarCookieArticulos(id, cantidad, tipo, precio){
  var existe = false;
  if (getCookie('art')!=""){
    var articulos=JSON.parse(getCookie('art'));
    var narticulos = articulos.length;
    for(var i=0;i<articulos.length;i++){ 

  
      if(articulos[i][1]==id){
        if(articulos[i][0]==2){
        existe = true;
        articulos[i][2] = parseInt(articulos[i][2]+cantidad);
      	}
  
      
      }
    }

  } else {
    var narticulos = 0;
    var articulos = [];
  }
  
  if(!existe){
    var objeto = [tipo, id, cantidad, precio];
    articulos.push(objeto);
    
  } 

  document.cookie = "art="+JSON.stringify(articulos); 


  
  actualizarNArticulos();
 

  
}






function actualizarNArticulos(){


	if (getCookie('art')!=""){
		var articulos=JSON.parse(getCookie('art'));
		var narticulos = articulos.length;

	} else {
		var narticulos = 0;
	}

	$("#n-articulos").html(narticulos);


}


function anularCannabis(id){
	console.log($("button[data-id="+id+"]").html());
}


function sumarCantidad(){

  var este = $(this);
  var cantidad = parseInt(este.siblings('.contador').html());
  cantidad+=1;

  este.siblings('.contador').html(cantidad);

}
function restarCantidad(){
  var este = $(this);
  var cantidad = parseInt(este.siblings('.contador').html());
  if(cantidad!=1){
    cantidad-=1;
    este.siblings('.contador').html(cantidad);
  }
}



//POST
function postGeneticasOtros(){
        return $.post("js/ajax/mostrarGeneticas.php",{tipo:"otros",categoria:"otros"}, function(result){
            resultado = result;
        });
}

function limpiarTabs(){
	$(".tab-panel").html("");
}