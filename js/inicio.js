	$(".btn-nuevo-socio").on("click", abrirFormAlta);
	$("#btn-acceder").on("click", comprobarLogin);
function abrirFormAlta(){
	$("body").fadeOut("500", function(){
		location.href ="alta.php";
	});
	
}

function comprobarLogin(){
	$(".logo").css("height","220px");
	$(".logo").css("opacity","100");
	$(".logo img").fadeIn();
	$(".btn-nuevo-socio").fadeOut("500");
	$("#btn-esp").fadeOut();
	$("#btn-en").fadeOut();
	$(".zona-login").addClass("zoomOut").delay(300).fadeOut();
	$(".zona-cargando").delay(700).fadeIn("1000");

	var dni = $("#dni").val();
	var nsocio = $("#nsocio").val();
	$.post("js/ajax/iniciar-sesion.php",{dni:dni, socio:nsocio},function(result){
		
		if(result=="success"){

			location.href ="inicio.php";
			

			
		} else {
			var mensaje="";
			var anterior = $(".zona-error").text();
			if(result=="error-2"){
				mensaje="Tu cuenta no ha sido activada";
				if(idioma=="en"){
					mensaje="Your account has not been activated";
					

				}
				$(".zona-error").html(mensaje);
			} 
			if(result=="error-3"){
				mensaje="Tu suscripción ha caducado. Consulta con el personal para solucionarlo";
				if(idioma=="en"){
					mensaje="Your membership has expired";
					

				}
				$(".zona-error").html(mensaje);
			} 
			if(result=="error-4"){
				mensaje="Tu cuenta ha sido suspendida. Consulta con el personal para solucionarlo";
				if(idioma=="en"){
					mensaje="Your account has been suspended";
					

				}
				$(".zona-error").html(mensaje);
			} 
			console.log(result);
			$(".zona-cargando").fadeOut("500",function(){
				$(".zona-error").fadeIn(function(){
					$("#dni").val("");
					$("#nsocio").val("");
					$(".zona-error").delay(3000).removeClass("animated zoomIn").fadeOut("1000",function(){
						$(".zona-login").css("display","block").removeClass("zoomOut").addClass("zoomIn");
						$(".btn-nuevo-socio").fadeIn("500");
						$("#btn-esp").fadeIn();
						$("#btn-en").fadeIn();
						$(".zona-error").text(anterior);
					});
					
				});
				//
		});
	}
});
}


