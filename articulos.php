<?php
    require_once("php/cabecera.php");
    require_once("php/sesion.php");
    mostrarCabecera("index");
?>
  <body>

    <?php cargarListaArticulosCarrito();?>
    <div class="container">
 <?php   cargarHeader();  ?>


<h1 class="tituloCategoria animated fadeIn">ARTICULOS</h1>
<div class="container animated fadeInUp" id="zonaArticulos">
  <div class="row">
    <div class="col-sm-4">
      <div class="row">
        <div class="col-sm-12">
          <h2>Categorías</h2>

        </div>
      </div>
      <div class="nav flex-column nav-pills" id="zonaCategoriasArticulos" role="tablist" aria-orientation="vertical">

        <div class="text-center"><img src="img/cargando.gif" style="height:128px;"></div>

      </div>
    </div>
    <div class="col-sm-8">

        <h2>Artículos</h2>
      <div class="tab-content" id="zonaContenedoresArticulos">
        <div class="text-center"><img src="img/cargando.gif" style="height:128px;"></div>


      </div>
    </div>
  </div>
  </div>
</div>
<div class="botonPagar animated fadeInUp text-center">
 <i class="fas fa-comment-dollar"></i></br><span class="text-pagar">PAGAR</span>
</div>
<div class="botonVolver animated fadeInUp text-center">
 <i class="fas fa-undo"></i></br><span class="text-atras">ATRÁS</span>
</div>



   <?php
    include "php/footer.php";
   ?>
  <script src="js/sesion.js"></script>
  <script src="js/articulos.js"></script>
  </body>
</html>
