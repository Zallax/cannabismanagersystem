<?php
    require_once("php/cabecera.php");
    require_once("php/sesion.php");
    mostrarCabecera("index");
?>
  <body>

    <div class="container animated zoomIn" id="zonaFinalizar">
 <?php   cargarHeader();  ?>

<input id="idCaja" class="d-none" value="<?php echo ultimaCaja(); ?>">
<input id="descuento" class="d-none" value="<?php echo $_SESSION["medicinal"];?>">
<input id="pdescuento" class="d-none" value="<?php echo $_SESSION["pdescuento"];?>">
<h1 class="tituloCategoria">FINALIZAR</h1>
<div class="container" id="zonaArticulos">
    <div>

      <div class="card lista-finalizar-cannabis" ">
        <div class="card-header">
          <h1 style="margin-bottom:0px;">Cannabis</h1>
        </div>
        <div class="sin-geneticas" id="sm1">No se ha añadido nada</div>
        <ul class="list-group list-group-flush" id="listaCannabis" style="display:none;">


        </ul>


      </div>
    </div>

    <div>

      <div style="margin-top:10px;" class="card lista-finalizar-articulos">
        <div class="card-header">
          <h1 style="margin-bottom:0px;">Productos</h1>
        </div>
        <div class="sin-geneticas" id="sm2">No se ha añadido nada</div>
        <ul class="list-group list-group-flush" id="listaArticulos"  style="display:none;">


        </ul>
      </div>
    </div>

    <div>
      <div class="row">
        <div class="col-sm-3">
            <div class="btn-vaciar btn-info">
            <i class="fa fa-trash"></i><p>VACIAR</p>
          </div>
        </div>
        <div class="col-sm-6 precio-finalizar text-center">
          <div class="precio">
            <small>TOTAL</small></br>
            <span>0.00€</span>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="btn-confirmar btn-success">
          <i class="fa fa-check"></i><p>CONFIRMAR</p>
        </div>
        </div>
      </div>

</div>


  </div>
</div>
  <div class="procesando-compra text-center" style="display:none;">
    <img src="img/cargando.svg" style="margin-top:20%">
  </div>
  <div class="compra-procesada text-center animated zoomIn" style="display:none;" >
    <div class="row">
      <div class="col-sm-4">
      </div>
      <div class="col-sm-4">
        <div class="fondo-finalizar-compra">
          <h2>Compra registrada correctamente</h2>
          ¿Deseas hacer otra operación?
        </div>
        <div class="row" id="otras-operaciones">
          <div class="col-sm-6">
          <div class="btn-no btn-dark">
          SI
        </div>
          </div>
          <div class="col-sm-6">
          <div class="btn-si btn-light">
          NO
        </div>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
      </div>
    </div>
  </div>



  <div class="botonVolver animated fadeInUp">
    <i class="fas fa-undo"></i></br><span>ATRÁS</span>
  </div>
   <?php
    include "php/footer.php";
   ?>

  <script src="js/sesion.js"></script>

  <script type="text/javascript">
    rellenarFinalizarCompra();

  </script>
  </body>
</html>
