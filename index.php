<?php

    require_once("php/cabecera.php");

    mostrarCabecera("index");
    session_start();
    session_destroy();
?>
  <body>

    <?php


      if(comprobarUltimoAcceso()){



    ?>

 

    <div class="container animated fadeIn">


    <div class="row center-block">
      <div class="col-sm-4">

      </div>

      <div class="col-sm-4 touchmenu">
        <div class="logo"><img src="panel/img/logo.png"></div>
        <div class="cont-login">
        <div class="zona-login animated">
          <input autocomplete="off" type="text" id="dni" class="form-control-lg" placeholder="DNI">
          <input autocomplete="off" type="number" id="nsocio" class="form-control-lg" placeholder="Nº DE SOCIO">
          <button class="btn-dark btn btn-block btn-lg" id="btn-acceder"><span>Acceder</span> <i class="fas fa-sign-in-alt"></i></button>
        </div>
        <div class="zona-cargando" style="display:none;">
          <img src="img/cargando.svg">
        </div>
        <div class="zona-error">
            Los datos no son correctos
        </div>
  </div>
        <div class="btn-nuevo-socio">
            <h1><i class="fa fa-user-plus"></i> <span id="tIndex1">Nuevo socio</span></h1>

        </div>
        <div class="text-center mt-5">
          <a href="actualizar.php"><img src="img/spain.png" id="btn-esp"></a>
            <a href="actualizar.php?en"><img src="img/en.png" id="btn-en"></a>
         
       </div>

    </div>


      <div class="col-sm-4">

      </div>

  </div>

</div>
<div class="version">
  Ver. 3.0
</div>
<script type="text/javascript">
 setInterval(function(){

  $.post("js/ajax/comprobarUltimoAcceso.php", function(result){

    if(result=="false"){

      document.location.reload();
    }
  });

 },2000);
  </script>



   <?php
 }   else {

?>

<div class="container text-center" style="
    background-color: rgba(255, 255, 255, 0.5215686274509804);
    height: 100%;
    margin-top: 50px;
    padding: 30px;
    box-shadow: 0 0 4px #00000059;
    border-radius: 4px;
">
  <h1 style="color: #921e1e;font-size:50px;">ACCESO BLOQUEADO</h1>
  <p style="text-transform:uppercase;color: black;font-size:20px;">Contacta con el personal de la Asociación</p>
  <img src="img/logo.png">
</div>

<script type="text/javascript">
/*setTimeout(function(){
  $.post("php/functions.php",{funcion:"comprobarUltimoAccesoAjax"}, function(result){
    if(result=="false"){
      document.location.reload();
    }
  });

 },5);  */

 setInterval(function(){

  $.post("js/ajax/comprobarUltimoAcceso.php", function(result){

    if(result!="false"){

      document.location.reload();
    }
  });

 },1000);
  </script>

<?php

 }
    include "php/footer.php";
   ?>
     <script src="js/inicio.js"></script>
     <script src="js/traduccion.js"></script>
  </body>
</html>
