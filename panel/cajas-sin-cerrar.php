<?php
  require_once("php/config.php");
  require_once ("php/funciones.php");
  include "php/cabecera.php";
  extract($_GET);
  $consulta = consulta("select t.id as id, t.estado as estado, t.fecha as fecha, t.hora as hora, u.nombre as nombre, u.apellidos as apellidos from tablet t inner join usuarios u where t.usuario = u.id order by id desc;");
?>

 <?php cabecera("hist-tablet");?>
  <div class="content-wrapper animated fadeIn">
    <div class="container">

      <div class="alert alert-info" role="alert">
        <div class="titulo-seccion"><span>Cajas sin cerrar</span></div>
        
      </div>
    
        <div class="row zona-fechas">
          <div class="col-12 text-center">
            <span class="text-muted">No hay cajas sin cerrar.</span>
          </div>
        </div>
      
            
      </div>


        
       
         
        

        


       </div>
    <?php include "php/footer.php";?>
    <script type="text/javascript">
      
      $(document).ready(function(){
        comprobarCajas();
      });

      function comprobarCajas(){
        $.post("php/funciones.php",{funcion:"comprobarCajasAbiertas"} ,function(result){
          if(result!=""){
            let fechas = JSON.parse(result);
            if(fechas.length!=0){
              $(".zona-fechas").html("");
              let div = "";
              $(fechas).each(function(n){
                div = '<div class="col-sm-2 caja text-center" data-id="'+this["id"]+'">'+fecha(this["fecha"])+'</div>';
                $(".zona-fechas").append(div);
              });

              $(".caja").on("click", function(){
                location.href="caja-antigua.php?id="+$(this).attr('data-id');
                  
              });
             

            }
          }
        });
      }


      

    </script>

      
  
</div>
</body>

</html>
