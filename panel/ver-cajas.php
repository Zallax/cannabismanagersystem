<?php
  require_once("php/config.php");
  require_once ("php/funciones.php");
  include "php/cabecera.php";
  extract($_GET);
  $filtro ="";
  if(ISSET($_GET["f"])){
    if($_GET["f"]!=""){
      $filtro="and fecha='".$_GET["f"]."'";
      $fFiltro=$_GET["f"];
    }
  }
  $consulta = consulta("select * from cajas where hora != '00:00:00' $filtro order by id desc;");
?>

 <?php cabecera("hist-tablet");
  if($_SESSION["tipo"]==1){
    ?>
 <div class="modal animated zoomIn" style="display: none !important;" id="modalCaja" tabindex="-1" role="dialog" aria-labelledby="modalCajaLabel" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 95%;" >
     <div class="modal-content">
       <div class="modal-header">
         <h5 class="modal-title" id="modalCajaLabel">Mostrando caja</h5>
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
           <span aria-hidden="true">&times;</span>
         </button>
       </div>
       <div class="modal-body bg-gris" id="contenidoModalCaja">
         <div class="text-center cargando"><img src="img/cargando.svg"></div>
      
         <div id="resultados" style="display:none;">
           
          <div class="row">
            <div class="col-lg-3">

                    <div class="card mb-2">
                      <div class="card-header bg-white">
                        <h5 class="card-title">Aportaciones de cultivo</h5>

                    
                    
                  </div>
                  <div class="card-footer text-muted text-right">
                     <div style="font-size:24px; font-weight:100;"><small class="text-muted">TOTAL</small> <i class="fas fa-angle-right"> </i><span class="text-dark"><span id="total-geneticas">0</span> €</span></div>
                   </div>
                </div>

              <!--<div class="alert text-center" role="alert">
                 <small class="text-muted">TOTAL GENÉTICAS</small></br><span class="badge badge-pill badge-success" style="font-size:50px; font-weight:100;"><span id="total-geneticas"></span> €</span>
              </div>-->
            </div>
            <div class="col-lg-3">
                  <div class="card mb-2">
                    <div class="card-header bg-white">
                      <h5 class="card-title">Total productos</h5>

                  
                  
                </div>
                <div class="card-footer text-muted text-right">
                   <div style="font-size:24px; font-weight:100;"><small class="text-muted">TOTAL</small> <i class="fas fa-angle-right"> </i><span class="text-dark"><span id="total-productos">0</span> €</span></div>
                 </div>
              </div>


              <!--<div class="alert text-center" role="alert">
                <small class="text-muted">TOTAL PRODUCTOS</small></br><span class="badge badge-pill badge-success" style="font-size:50px; font-weight:100;"><span id="total-productos"></span> €</span>
              </div>-->


            </div>
            <div class="col-lg-3">

                <div class="card mb-2">
                  <div class="card-header bg-white">
                    <h5 class="card-title">Total Cuotas</h5>

                
                
              </div>
              <div class="card-footer text-muted text-right">
                 <div style="font-size:24px; font-weight:100;"><small class="text-muted">TOTAL</small> <i class="fas fa-angle-right"> </i><span class="text-dark"><span id="total-cuotas">0</span> €</span></div>
               </div>

             <!-- <div class="alert text-center" role="alert">
              <small class="text-muted">TOTAL PERÍODO</small></br><div class="cant-total badge badge-pill badge-info"><span id="total-hoy">0</span>€</span></div>
              </div>-->
            </div>
          </div>

            <div class="col-lg-3">

                  <div class="card mb-2">
                    <div class="card-header">
                      <h5 class="card-title">Total general</h5>

                  
                  
                </div>
                <div class="card-footer bg-success  text-muted text-right">
                   <div style="font-size:24px; font-weight:100;"><small class="text-muted">TOTAL</small> <i class="fas fa-angle-right"> </i> <span class="text-white"> <span id="total-hoy">0</span> €</span></div>
                 </div>



              <!--<div class="alert text-center" role="alert">
              <small class="text-muted">TOTAL PERÍODO</small></br><div class="cant-total badge badge-pill badge-info"><span id="total-hoy">0</span>€</span></div>-->

              </div>
            </div>
    

         </div>
         <div class="row mt-3">
           <div class="col-lg-6 col-12">

             <div class="card mb-2">
               <div class="card-header bg-white">
                 <h5 class="card-title">Genéticas dispensadas</h5>
               </div>
               <div class="class-body">
                 
                 <div id="zonaTablaGen"></div>
               </div>
             </div>
             <!--
             <h3>Genéticas dispensadas</h3>
            <div id="zonaTablaGen"></div>-->


           </div>
           <div class="col-lg-6 col-12">
            <div class="card">
                          <div class="card-header bg-white">
                            <h5 class="card-title">Productos vendidos</h5>
                          </div>
                          <div class="class-body">
                            
                            <div id="zonaTablaProd"></div>
                          </div>
                        </div>
             </div>

           </div>



</div>
         </div>
         <!-- FINAL BODY MODAL-->
       </div>
      
     </div>
   </div>
 </div>

  <div class="content-wrapper animated fadeIn" style="display:none;">
    <div class="container">

      <div class="card">
       <div class="card-header bg-white">
        <h5>Historial de Cajas</h5>
       </div>
       <div class="card-body">
        <div class="row">
        <div class="col-sm-4">
        </div>
        <div class="col-sm-4 text-center">
          <h4><i class="fas fa-search"></i> Filtrar por fecha</h4>
         
            <input type="date" class="form-control form-control-lg" placeholder="Elige la fecha" aria-label="Recipient's username" aria-describedby="button-addon2" id="filtro" value="<?php echo $fFiltro; ?>">
            
        </div>
        </div>
       </div>
        
      </div>
    
        <div class="row zona-fechas">
        <?php 
          if(!empty($consulta)){
              $delay=0;
              $f="";
              $espaciador = "";
              foreach($consulta as $indice=>$valor){
                $u = consulta("select nombre, apellidos from usuarios where id = ".$valor["idUsuario"]);
                if(!empty($u)){
                  $nombre = $u[0]["nombre"]." ".$u[0]["apellidos"];
                } else {
                  $nombre = "Usuario eliminado";
                }
                $delay+=0.1;

                if($f!=$valor["fecha"]){
                  $f=$valor["fecha"];
                  $espaciador = '<div class="col-12 mt-3"><h5>'.fecha($f).'</h5></div>';
                  echo $espaciador;
                }


                
              ?>
              <div class="col-sm-6 col-md-6 col-lg-6 animated zoomIn" style="animation-delay: <?php echo $delay;?>s" id-caja="<?php echo $valor["id"];?>" >
               
                <div class="card mb-2 mt-2 caja" id-caja="<?php echo $valor["id"];?>" >
                  <div class="card-header bg-white">
                    <?php 
                        echo '<i class="fas fa-user"></i> '.$nombre;
                    ?>  
                  </div>
                 <div class="card-body row">
                  <div class="col-7 col-sm-7 col-md-7">
                    <h3><i class="far fa-calendar-alt"></i> <?php echo fecha($valor["fecha"]);?></h3>
                    
                  </div>
                  <div class="col-5 col-sm-5  col-md-5 text-right">
                    <h2 class="text-success"><i class="far fa-chart-bar"></i> <?php echo $valor["total"];?>€ </h2>

                  </div>
                  <div class="col-12">
                    <i class="far fa-clock"></i> <span class="text-success"><?php echo $valor["horaAp"]; ?></span> - <span class="text-danger"><?php echo $valor["hora"]; ?></span>
                  </div>
                 </div>
                  
                </div>
           
              </div>
             

              <?php
            }
          } else {




          ?>
          <div class="col-12 text-center mt-3">
            <span class="text-muted">No hay cajas.</span>
          </div>

          <?php
          }
          ?>
        </div>
      
            
      </div>


        
       
         
        

        


       </div>
        <?php
      } else {
        errorDePermisos();
      }
   ?>
    <?php include "php/footer.php";?>
    <script type="text/javascript">
  
     
      $(document).ready(function(){
        $(".content-wrapper").show();
        $("#filtro").change(function(){

          $(location).attr('href',"ver-cajas.php?f="+$(this).val());
         // comprobarCajas($(this).val());
        })
      });

      

      

    </script>

      
  
</div>
<script type="text/javascript">
  
$(document).ready(function() {
$(".caja").on("click", function(){
   $(".cargando").show();
   $("#resultados").hide();
   $("#modalCaja").modal("show");
});


var totalGeneticas = 0;
var totalProductos = 0;
var tablas = false;




 $(".caja").on("click", function(){
    var idCaja = $(this).attr("id-caja");

 $("#modalCaja").modal("show");
    consultarVentas(idCaja);


 
  });

 



});


    var productos = new Array();
    var geneticas = new Array();

function consultarVentas(id){
  $("#zonaTablaGen").empty();
  $("#zonaTablaGen").html(' <table class="table table-hover table-light" id="tabla-caja-geneticas"><thead class="bg-white text-grey"><tr><th style="width:10%;">TIPO</th><th style="width:50%;">Nombre</th><th>Cantidad</th><th>Total</th></tr></thead><tbody></tbody></table>');
  $("#zonaTablaProd").empty();
  $("#zonaTablaProd").html('<table class="table table-hover table-light" id="tabla-caja-productos"><thead class="bg-white text-grey"><tr><th style="width:60%;">Nombre</th><th>Cantidad</th><th>Total</th></tr></thead><tbody></tbody></table>');

   totalGeneticas = 0;
    totalProductos = 0;
    productos = [];
    geneticas = [];
    var compras = new Array();
    postVentasFechas(id).done(function(data){
      compras = JSON.parse(data);

    }).done(function(){
      $(compras).each(function(n){
          guardarCompra(JSON.parse(compras[n]["geneticas"]), "genetica");
          guardarCompra(JSON.parse(compras[n]["productos"]), "producto");
      });

   
     
       $("#total-geneticas").html(compras[0]["total_geneticas"]);
      
      $("#total-productos").html(compras[0]["total_productos"]);

      $("#total-cuotas").html(compras[0]["total_cuotas"]);

      $("#total-hoy").html(compras[0]["total"]);
      
      $(geneticas).each(function(c){
        var tipo="";
    
        if(geneticas[c][3]=="v" || geneticas[c][3]=="o"){
          tipo = '<div class="tipo verde" tipo="v">v</div>';
        } else {
          tipo = '<div class="tipo marron" tipo="m">m</div>';
        }
        var td = '<tr>';
        td += '<td>';
        td += tipo;
        td += '</td>';
        td += '<td>';
        td += geneticas[c][0];
        td += '</td>';
        td += '<td class="text-right">';
        td += (parseFloat(geneticas[c][1])).toFixed(2);
        td += '</td><td class="text-right"><span class="precio-total-genetica">';
        
        let total = parseFloat(geneticas[c][2]);
        td +=total+"</span> €</tr>";


        $("#tabla-caja-geneticas tbody").append(td);

        
       
      });


     
      //$("#tabla-caja-productos tbody").empty();
      $(productos).each(function(c){
        var td = '<tr>';
        td += '<td>';
        td += productos[c][0];
        td += '</td>';
        td += '<td class="text-right">';
        td += productos[c][1];
        td += '</td>';
        td += '</td><td class="text-right"><span class="precio-total-genetica">';
        td +=(productos[c][2]*productos[c][1]).toFixed(2)+"</span> €</tr>";
        $("#tabla-caja-productos tbody").append(td);

       
      });

          
       

          
            
         


          /* $("#total-geneticas").html(totalGeneticas.toFixed(2));
          $("#total-hoy").html((totalGeneticas+totalProductos).toFixed(2));
          $("#total-productos").html(totalProductos.toFixed(2));*/
    });
    setTimeout(function(){
      $(".cargando").fadeOut("500", function(){
      $("#resultados").fadeIn("500");
         $("#tabla-caja-productos").DataTable( {
                    "language": espanol,
                    "paging":   false,
                    "info":     false,
                    

        });
         $("#tabla-caja-geneticas").DataTable( {
                     "order": [[ 1, "asc" ]],
                      "language": espanol,
                      "paging":   false,
                      "info":     false,


                                 
           }); 
      });

      }, 500);


}

function guardarCompra(compra, tipo){
  $(compra).each(function(e){
   
    if(tipo=="genetica"){
      console.log("cannabis-> "+this[0]+"x"+this[1]);
      var producto = new Array();

      //METE EN VARIABLE PRODUCTO NOMBRE Y CANTIDAD
      producto.push(this[0], this[1], this[2], this[3]);

      totalGeneticas+=parseFloat(this[2]*this[1]);
      var existe = false;
      if(geneticas.length!=0){

        $(geneticas).each(function(k){
          
          if(geneticas[k].includes(producto[0])){
            existe=true;
            this[1]=parseFloat(this[1])+parseFloat(producto[1]);
            this[2]=parseFloat(this[2])+parseFloat(producto[2]);
            producto = [];
          }
        });
        if(!existe){
           geneticas.push(producto);
            
            
            producto = [];
        }
      } else {
        geneticas.push(producto);
            producto = [];
            
      }

     



    } else {
      console.log("producto-> "+this[0]);
      var producto = new Array();

      //METE EN VARIABLE PRODUCTO NOMBRE Y CANTIDAD
      producto.push(this[0], this[1], this[2]);

      totalProductos+=parseFloat(this[2]*this[1]);

      var existe = false;
      if(productos.length!=0){

        $(productos).each(function(k){
          
          if(productos[k].includes(producto[0])){
            existe=true;
            this[1]=parseFloat(this[1])+parseFloat(producto[1]);
            
            producto = [];
          }
        });
        if(!existe){
           productos.push(producto);
            
            
            producto = [];
        }
      } else {
        productos.push(producto);
            producto = [];
            
      }
       
    }
  });
  
}

  

function postVentasFechas(id){

  return $.post("js/ajax/caja.php", {funcion:"consultarCaja", id:id}, function(result){
  
    resultado = result;

  });

}



//TRADUCCIÓN DATATABLE
 var espanol={
                     "lengthMenu": "Mostrar _MENU_ registros por página",
                     "zeroRecords": "No se han encontrado datos",
                     "info": "Mostrando página _PAGE_ de _PAGES_",
                     "infoEmpty": "No se han encontrado registros.",
                     "infoFiltered": "(filtered from _MAX_ total records)",
                     "paginate": {
                            "first":      "Primero",
                            "last":       "Último",
                            "next":       ">",
                            "previous":   "<"
                        },
                     "search":         "Buscar:",
                };




  if(opcionLateral!="si"){
    $("#d-cajas").addClass("menu-seleccionado");
    $("#expandir-mostrador").collapse('toggle');
  }


</script>
</body>

</html>
