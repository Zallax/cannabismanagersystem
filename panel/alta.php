<!DOCTYPE html>
<html lang="es">
<?php
  require_once("php/config.php");
  require_once("php/funciones.php");
  include "php/cabecera.php";
  require_once("js/ajax/comprobarNSocioLibre.php");
?>

 <?php cabecera("");?>
 <div class="modal fade" id="modalConfirmar" tabindex="-1" role="dialog" aria-labelledby="modalConfirmarLabel" aria-hidden="true">
            <div class="modal-dialog" style="max-width:90%" role="document">
              <div class="modal-content" >
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Revisión</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body" style="padding:50px;">

                  <div class="row" >
                     <div class="col-12">
                        <h3>¿Son correctos todos los datos?</h3>
                      </div>
                  <div class="col-sm-6">
                    <h5>Información personal</h5>
                      <div class="form-group input-group-lg">
                        <label for="rnsocio">Nº de Socio </label>
                        <input type="number" class="form-control" id="rnsocio" disabled>

                      </div>
                      <div class="form-group input-group-lg">
                        <label for="rnombre">Nombre</label>
                        <input type="text" class="form-control" id="rnombre" disabled>

                      </div>
                      <div class="form-group input-group-lg">
                        <label for="rapellidos">Apellidos</label>
                        <input type="text" class="form-control" disabled id="rapellidos">

                      </div>
                       <div class="form-group input-group-lg">
                        <label for="rc-dni">DNI</label>
                        <input type="text" class="form-control" disabled id="rc-dni">

                      </div>
                       <div class="form-group input-group-lg">
                        <label for="rdireccion">Dirección</label>
                        <input type="text" class="form-control" disabled id="rdireccion">

                      </div>
                       <div class="form-group input-group-lg">
                        <label for="rtelefono">Teléfono</label>
                        <input type="number" class="form-control" disabled id="rtelefono">

                      </div>
                      <div class="form-group input-group-lg">
                        <label for="rcorreo">Correo</label>
                        <input type="text" class="form-control" disabled id="rcorreo">

                      </div>

                  </div>
                  <div class="col-sm-6">
                    <h5>Otros datos</h5>
                  <div class="form-group input-group-lg">
                        <label for="ravalador">Socio Avalador</label>
                        <input type="text" class="form-control" disabled id="ravalador">

                      </div>
                      <div class="form-group input-group-lg">
                            <label for="rcuota">Meses</label>
                           <input type="text" class="form-control" disabled id="rcuota">


                      </div>
                      <div class="form-group input-group-lg">
                            <label for="rcant">Cantidad (€)</label>
                           <input type="text" class="form-control" disabled id="rcant">
                           

                      </div>
                     <div class="row">
                     <div class="form-group input-group-lg col-sm-6">
                      <label for="rfAlta">Fecha de Alta</label>
                      <input type="date" class="form-control" disabled id="rfAlta">

                    </div>
                     <div class="form-group input-group-lg col-sm-6">
                      <label for="rfPago">Fecha de Pago</label>
                      <input type="date" class="form-control" disabled id="rfPago">

                    </div>
                  </div>

                     <h5 class="mt-3">Requisitos</h5>
                     <div class="form-group input-group-lg">
                        <label for="rconsumo">Consumo mensual (gr)</label>
                        <input type="number" class="form-control" disabled id="rconsumo">

                      </div>
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" class="custom-control-input" disabled id="rcheck1">
                      <label class="custom-control-label" for="rcheck1">SER CONSUMIDOR/A DE CANNABIS SATIVA L.</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" class="custom-control-input" disabled id="rcheck2">
                      <label class="custom-control-label" for="rcheck2">SER CONSUMIDOR/A DE TABACO</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" class="custom-control-input" disabled id="rcheck3">
                      <label class="custom-control-label" for="rcheck3">SER CONSUMIDOR/ A DE PLANTAS CON PROPIEDADES TERAPÉUTICAS</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" class="custom-control-input" disabled id="rcheck4">
                      <label class="custom-control-label" for="rcheck4">SER CONSUMIDOR/A HABITUAL DE CANNABIS POR RAZONES TERAPÉUTICAS, <small>y tener diagnosticada una enfermedad para la que el cannabis tiene efectos terapéuticos según la IACM, o que el uso paliativo de los cannabinoides ha sido probado científicamente y aportar certificado médico acreditativo de la solicitud de uso de cannabis por parte del paciente sin oposición médica, o tener una licencia de uso de cannabis de cualquier país del Mundo.</small></label>
                    </div>

                      <h5 class="mt-3">Documentación aportada</h5>
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" class="custom-control-input" disabled id="rcheck5">
                      <label class="custom-control-label" for="check5">Copia DNI/NIE</label>
                    </div>
                    <div class="custom-control custom-checkbox mb-4">
                      <input type="checkbox" class="custom-control-input" disabled id="rcheck6">
                      <label class="custom-control-label" for="check6">Certificado médico</label>
                    </div>



                  </div>

                </div>
                <div class="modal-footer">
                 <button id="btnRegistrarSocio" class="btn btn-info btn-block btn-lg">Confirmar</button>
                </div>
              </div>
            </div>
          </div>
          </div>
  <div class="content-wrapper animated fadeIn">

    <div class="container">

    <h3 class="titulo-seccion">Añadir nuevo socio</h3>
      <div class="tab-content tab-registro">
         <form id="formAltaSocio">

         <div id="zonaAlta">
        <div class="row" >

          <div class="col-sm-6">
            <h5>Información personal</h5>
              <div class="form-group input-group-lg">
                <label for="nsocio">Nº de Socio <small style="display:inline-block;margin-left:20px;" class="form-text text-muted">Números disponibles: <?php ultimoNSocio(); ?></br>Último disponible: #<?php maximoSocio(); ?></small></label>
                <input type="number" class="form-control" id="nsocio" min="0" required name="nsocio" placeholder="Número de socio">

              </div>
              <div class="form-group input-group-lg">
                <label for="nombre">Nombre</label>
                <input type="text" class="form-control" required id="nombre" name="nombre">

              </div>
              <div class="form-group input-group-lg">
                <label for="apellidos">Apellidos</label>
                <input type="text" class="form-control" required name="apellidos" id="apellidos">

              </div>
               <div class="form-group input-group-lg">
                <label for="c-dni">DNI</label>
                <input type="text" class="form-control" required name="dni" id="c-dni">

              </div>
               <div class="form-group input-group-lg">
                <label for="direccion">Dirección</label>
                <input type="text" class="form-control" required name="direccion" id="direccion">

              </div>
               <div class="form-group input-group-lg">
                <label for="telefono">Teléfono</label>
                <input type="number" class="form-control" required name="telefono" id="telefono">

              </div>
              <div class="form-group input-group-lg">
                <label for="correo">Correo</label>
                <input type="mail" class="form-control" name="correo" id="correo">

              </div>





          </div>
          <div class="col-sm-6">
            <h5>Otros datos</h5>
          <div class="form-group input-group-lg">
                <label for="direccion">Socio Avalador</label>
                <select class="form-control" id="savalador" required name="savalador">
                      <?php
                          selectSocios();
                      ?>
                </select>

              </div>
              <div class="form-group input-group-lg">
                    <label for="cuota">Cuota</label>
                    <select class="form-control" id="cuota" required name="cuota">
                          <?php
                              $cuotas=getCuotas();
                              echo '<option value="">Selecciona</option>';
                              foreach($cuotas as $indice=>$valor){
                                echo '<option value="'.$indice.':'.$valor.'">'.$indice.' meses ('.$valor.' €)</option>';
                              }
                          ?>
                          <option value="manual">Elegir otra...</option>
                    </select>

                  </div>
                  <div id="zona-meses" class="mb-3" style="display:none;">
                    <div class="mb-2">
                   <label>
                     Meses
                   </label>
                   
                     <input class="form-control form-control-lg" id="meses" min="1" type="number"></input>
                   </div>
                    <div class="mb-2">
                   <label>
                     Cantidad (€)
                   </label>
                   
                     <input class="form-control form-control-lg" id="cant" min="1" type="number"></input>
                   </div>

                   
                  </div>
             <div class="row">
             <div class="form-group input-group-lg col-sm-6">
              <label for="fAlta">Fecha de Alta</label>
              <input type="date" class="form-control" name="fAlta" required id="fAlta">

            </div>
             <div class="form-group input-group-lg col-sm-6">
              <label for="fPago">Fecha de Pago</label>
              <input type="date" class="form-control" name="fPago" required id="fPago">

            </div>
          </div>

             <h5>Requisitos</h5>
             <div class="form-group input-group-lg">
                <label for="consumo">Consumo mensual (gr)</label>
                <input type="number" class="form-control" required name="consumo" min="1" id="consumo">

              </div>
            
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="check1">
              <label class="custom-control-label" for="check1">SER CONSUMIDOR/A DE CANNABIS SATIVA L.</label>
            </div>
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="check2">
              <label class="custom-control-label" for="check2">SER CONSUMIDOR/A DE TABACO</label>
            </div>
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="check3">
              <label class="custom-control-label" for="check3">SER CONSUMIDOR/ A DE PLANTAS CON PROPIEDADES TERAPÉUTICAS</label>
            </div>
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="check4">
              <label class="custom-control-label" for="check4">SER CONSUMIDOR/A HABITUAL DE CANNABIS POR RAZONES TERAPÉUTICAS, <small>y tener diagnosticada una enfermedad para la que el cannabis tiene efectos terapéuticos según la IACM, o que el uso paliativo de los cannabinoides ha sido probado científicamente y aportar certificado médico acreditativo de la solicitud de uso de cannabis por parte del paciente sin oposición médica, o tener una licencia de uso de cannabis de cualquier país del Mundo.</small></label>
            </div>
 
              <h5 class="mt-3">Documentación aportada</h5>
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="check5">
              <label class="custom-control-label" for="check5">Copia DNI/NIE</label>
            </div>
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="check6">
              <label class="custom-control-label" for="check6">Certificado médico</label>
            </div>



          </div>


        </div>
        <button id="btnConfirmarRegistrarSocio" class="btn btn-segundo btn-block btn-lg"><i class="fas fa-file-alt"></i> Revisar datos</button>

        </div>
     </form>

       <div id="zonaAltaImagenes" style="display:none;">
        <div class="row" id="confirmarAlta">
          <div class="col-sm-3">
          </div>
          <div class="col-sm-6">
            <div class="row" style="margin-bottom:30px;color:white;background-color:#81AE5F; padding:20px">
              <div class="col-sm-3">
                <i style="font-size:5em" class="fa fa-user-plus"></i>

              </div>
              <div class="col-sm-9">
                  <h3 style="font-weight: 500"><span id="nombreCSocio"></span>ha sido dado de alta</h3>
                  <p>Para completar la documentación del socio, accede a su perfil.</p>

              </div>

            </div>
            <center><a href="alta.php" class="btn btn-primary">Realizar otro alta</a></center>
          </div>
        </div>

       </div>

        </div>
      </div>

    <?php include "php/footer.php";?>




 <script src="js/socios.js"></script>
<script>

  $("#cuota").change(function(){
    if($(this).val()=="manual"){
      $("#zona-meses").fadeIn("500");
      $("#meses").attr("required",true);
      $("#cant").attr("required",true);
    } else {
      $("#zona-meses").fadeOut("500");
      $("#mesesRenov").val("");
      $("#meses").prop("required",false);
    $("#cant").prop("required",false);
    }
  });
</script>
  </div>
</body>

</html>
