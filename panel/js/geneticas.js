 $(document).ready(function() {

    

    cargarIndicas();
    $("#btnMostrarIndicas").on("click", cargarIndicas);
    $("#btnMostrarSativas").on("click", cargarSativas);
    $("#btnMostrarExtraccion").on("click", cargarExtraccion);
    $("#btnMostrarHibridas").on("click", cargarHibridas);
    $("#btnEditarGenetica").on("click", editarGenetica);



});

     function cargarFormEditarGenetica(){

        var boton=$(this);
        var id = boton.attr("data-id");
        
        $.post("php/ajax/formEditarGenetica.php", {id: id}, function(result){

            $("#contenidoModalGenetica").html(result);

        });
    }	


function cargarIndicas(){
    	
    	$.post("php/ajax/cargaTablas.php",{tipo:"verde",categoria:"indica"}, function(result){
           
        $(".tablaIndicas").empty().append(result);

             $('#tablageneticaindica').DataTable( {
                 "language": espanol,
                 "autoWidth": false,
                 scrollY:        200,
                 scrollCollapse: true,
                 paging:         false,
                 "columns": [
                     { "width": "100px" },
                     null,
                     { "width": "50%" },
                     null,
                     null,
                     null
                   ]

                            
            } 

             );

             $('a[data-toggle="tab"]').on( 'shown.bs.tab', function (e) {
                  $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
              } );
   
        $(".btnEditarGenetica").on("click",cargarFormEditarGenetica);


    });
}

function cargarSativas(){
    	
    	$.post("php/ajax/cargaTablas.php",{tipo:"verde",categoria:"sativa"}, function(result){
           
        $(".tablaSativas").empty().append(result);

             $('#tablageneticasativa').DataTable( {
                 "language": espanol,
                 "autoWidth": false,
                 scrollY:        200,
                 scrollCollapse: true,
                 paging:         false,
                 "columns": [
                     { "width": "100px" },
                     null,
                     { "width": "50%" },
                     null,
                     null,
                     null
                   ]

                            
            } 

             );



   	
        $(".btnEditarGenetica").on("click",cargarFormEditarGenetica);


    });
}

function cargarExtraccion(){
    	
    	$.post("php/ajax/cargaTablas.php",{tipo:"verde",categoria:"extraccion"}, function(result){
           
        $(".tablaExtraccion").empty().append(result);
         $('#tablageneticaextraccion').DataTable( {
                 "language": espanol,
                 "autoWidth": false,
                 scrollY:        200,
                 scrollCollapse: true,
                 paging:         false,
                 "columns": [
                     { "width": "100px" },
                     null,
                     { "width": "50%" },
                     null,
                     null,
                     null
                   ]

                            
            } 

             );

           
   
        $("#btnEditarGenetica").on("click",cargarFormEditarGenetica);


    });
}

function cargarHibridas(){
        
        $.post("php/ajax/cargaTablas.php",{tipo:"verde",categoria:"hibrida"}, function(result){
        $(".tablaHibridas").empty().append(result);
       /*  $('#tablageneticahibrida').DataTable( {
                 "language": espanol,
                 "autoWidth": false,
                 scrollY:        200,
                 scrollCollapse: true,
                 paging:         false,
                 "columns": [
                     { "width": "100px" },
                     null,
                     { "width": "50%" },
                     null,
                     null,
                     null
                   ]

                            
            } 

             );*/

   
        $("#btnEditarGenetica").on("click",cargarFormEditarGenetica);


    });
}

function editarGenetica(){
		$("#btnEditarGenetica").html('<img src="./img/cargandomini.gif" style="width:30px;">');

		var nombre = $("#nombreGenetica").val();
		var descripcion = $("#descripcionGenetica").val();
		var precio = $("#precioGenetica").val();
		var stock = $("#stockGenetica").val();
		var banco = $("#bancoGenetica").val();
		var pindica = $("#pindicaGenetica").val();
		var psativa = $("#psativaGenetica").val();
		var pthc = $("#pthcGenetica").val();
		var pcbd = $("#pcbdGenetica").val();
        var gusto = $("#gustoGenetica").val();
        var id = $("#idGenetica").val();



		$.post("js/ajax/actualizarGenetica.php",{nombre:nombre, descripcion:descripcion, precio:precio, stock:stock,banco:banco,pindica:pindica,psativa:psativa,pthc:pthc,pcbd:pcbd, id:id, gusto:gusto}, function(result){
        
           if(result=="ok"){
           	$('#modalGenetica').modal('hide');
            mostrarMensaje('<i class="fa fa-check-circle"></i> Se han guardado los cambios correctamente');
            $("#btnEditarGenetica").html('<i class="fa fa-save"></i> Guardar');

            cargarIndicas();
           } else {
           	alert("error");
           }
           

    });


}


//TRADUCCIÓN DATATABLE
 var espanol={
                     "lengthMenu": "Mostrar _MENU_ registros por página",
                     "zeroRecords": "No se han encontrado datos",
                     "info": "Mostrando página _PAGE_ de _PAGES_",
                     "infoEmpty": "No se han encontrado registros.",
                     "infoFiltered": "(filtered from _MAX_ total records)",
                     "paginate": {
                            "first":      "Primero",
                            "last":       "Último",
                            "next":       ">",
                            "previous":   "<"
                        },
                     "search":         "Buscar:",
                };

function mostrarMensaje(mensaje){
    $("#mensajes").empty().append(mensaje);
    
    $("#mensajes").fadeIn(500).delay(3000).fadeOut(500);


}