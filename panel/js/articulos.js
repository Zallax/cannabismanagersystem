//Cargar contenido






$(document).ready(function() {
   
    cargarContenido();
    $("#btnEliminarProducto").on("click", comprobarEliminarProducto);
    $("#zonaCategoriasArticulos a").each(function(){
        var este = $(this);
        console.log(este.attr("data-id-cat"));


        
    })
    $("#btnEditarCat").on("click", function(){
        actualizarCategoria();
    });
   

    $("#formNuevaCategoriaArticulo").on("submit", function(e){
        e.preventDefault();
        nuevaCategoria();
      })
      $("#formNuevoArticulo").on("submit", function(e){
        e.preventDefault();
        nuevoArticulo();
      })


      var categoria =  $("#zonaCategoriasArticulos a:first").attr("data-id-cat");
    //  cargarArticulos(categoria);


      $("#btnGuardarProducto").on("click", function(){
        var  id = $(".modal-body #id-articulo").val();
        var nombre = $(".modal-body #nombre-articulo").val();
        var descripcion = $(".modal-body #desc-articulo").val();
        var precio = $(".modal-body #precio-articulo").val();
        var stock = $(".modal-body #stock-articulo").val();
        var cat = $(".modal-body #categoria-articulo").val();
        $.post("js/ajax/articulos.php", {accion:"actualizarProducto", id:id, nombre:nombre, descripcion:descripcion, precio:precio, stock:stock, categoria:cat}, function(result){

            if(result=="success"){
                $("#modalProducto").modal("hide");
                mostrarMensaje("El producto "+nombre+" ha sido editado","success");
                cargarArticulos(cat);
           
            } else {
                mostrarMensaje("No ha realizado ningún cambio");

            }
        });
      });

      $("#btnSiEliminar").on("click", function(){
        var  id = $(".modal-body #id-articulo").val();
        var nombre = $(".modal-body #nombre-articulo").val();
        var cat = $(".modal-body #categoria-articulo").val();
        $.post("js/ajax/articulos.php", {accion:"eliminarProducto", id:id}, function(result){

            if(result=="success"){
                $("#modalProducto").modal("hide");
                mostrarMensaje("El producto "+nombre+" ha sido eliminado");
                cargarArticulos(cat);

            } else {
                mostrarMensaje("Ha ocurrido un error al eliminar el producto");
                
            }
        });
      });
      
    
      


});


function cargarArticulos(categoria){

    $.post("php/ajax/cargarTablasArticulos.php",{categoria:categoria},function(result){
       $("#zonaContenedoresArticulos #seccionCat"+categoria).html(result);
       $(".btnEditarProducto").unbind();
                    $(".btnEditarProducto").on("click", mostrarEditarArticulo);
                   $("#tablaCategoria"+categoria).each(function(n){

                       $(this).DataTable( {
                           "language": espanol,
                           scrollCollapse: true,
                           paging:         false,
                           "columns": [
                               { "width": "90%" },
                               null,
                               null,
                               null
                             ]

                                      
                      } 

                       );
                       $(this).css("display","table");
                       $(this).css("width","100%");
                   });
                   
    });

  
}

function nuevaCategoria(){
    var nombre = $("#nombre-cat").val();
    var descripcion = $("#desc-cat").val();

    $.post("js/ajax/articulos.php",{accion:"n-categoria", n:nombre, d:descripcion},function(result){

        if(result=="correcto"){
            cargarContenido();
            var categoria =  $("#zonaCategoriasArticulos a:first").attr("data-id-cat");
            cargarArticulos(categoria);
            mostrarMensaje("La categoría "+nombre+" ha sido añadida correctamente", "success");
            


        }

        if(result=="existe"){
            mostrarMensaje("Ya existe la categoría "+nombre, "error");
        }

        $("#nombre-cat").val("");
        $("#desc-cat").val("");
        

    });
        
}
/*
function nuevaCategoria(){
    var nombre = $("#nombre-cat").val();
    var descripcion = $("#desc-cat").val();

    $.post("js/ajax/articulos.php",{accion:"n-categoria", n:nombre, d:descripcion},function(result){

        if(result=="correcto"){
            cargarContenido();
            var categoria =  $("#zonaCategoriasArticulos a:first").attr("data-id-cat");
            cargarArticulos(categoria);
            mostrarMensaje("La categoría "+nombre+" ha sido añadida correctamente");
        }

        if(result=="existe"){
            mostrarMensaje("Ya existe la categoría "+nombre);
        }

        $("#nombre-cat").val("");
        $("#desc-cat").val("");
        

    });
        
}
*/
function nuevoArticulo(){

    var nombre = $("#nombre-articulo").val();
    var descripcion = $("#desc-articulo").val();
    var precio = $("#precio-articulo").val();
    var stock = $("#stock-articulo").val();
    var cat = $("#selectCategorias").val();
    $.post("js/ajax/articulos.php",{accion:"n-articulo", n:nombre, d:descripcion, p:precio, s:stock, c:cat},function(result){
        console.log(result);
        if(result=="correcto"){
            cargarContenido();
            var categoria =  $("#zonaCategoriasArticulos a:first").attr("data-id-cat");
            cargarArticulos(categoria);
            mostrarMensaje("El artículo "+nombre+" ha sido añadido correctamente");
            
        }

        if(result=="existe"){
            mostrarMensaje("Ya existe el artículo "+nombre);
        }

        $("#nombre-articulo").val("");
        $("#desc-articulo").val("");
        $("#precio-articulo").val("");
        $("#stock-articulo").val("");
        $("#selectCategorias").val("");
        

    });
        
}

//TRADUCCIÓN DATATABLE
var espanol={
    "lengthMenu": "Mostrar _MENU_ registros por página",
    "zeroRecords": "No se han encontrado datos",
    "info": "Mostrando página _PAGE_ de _PAGES_",
    "infoEmpty": "No se han encontrado registros.",
    "infoFiltered": "(filtered from _MAX_ total records)",
    "paginate": {
           "first":      "Primero",
           "last":       "Último",
           "next":       ">",
           "previous":   "<"
       },
    "search":         "Buscar:",
};

function afterModalTransition(e) {
    e.setAttribute("style", "display: none !important;");
  }

$('.fade2').on('hide.bs.modal', function (e) {
	setTimeout( () => afterModalTransition(this), 200);
})



function cargarContenido(){
    
    $.post("php/ajax/zonaArticulos.php",{accion:"categorias"}, function(result){
        
        $("#zonaCategoriasArticulos").html(result);
        $("#zonaCategoriasArticulos a").on("click", function(){
           // cargarArticulos($(this).attr("data-id-cat"));
        })

          $(".btn-eliminar-cat").unbind();
            
            $(".btn-eliminar-cat").on('click', function(){
                
                var este = $(this).parent();

                var id = este.attr("data-id-cat");
             
                eliminarCategoria(id);
            });

        $(".btn-editar-cat").unbind();
            
            $(".btn-editar-cat").on('click', function(){
                
                var este = $(this).parent();

                var id = este.attr("data-id-cat");
               
                editarCategoria(id);
            });

    });
     $.post("php/ajax/zonaArticulos.php",{accion:"contenedores"}, function(result){
            $("#zonaContenedoresArticulos").html(result);
            // $("#seccionCat"+categoria).html(result);
            // $('#tablaCategoria'+categoria).css('width', '100%');
             $(".btnEditarProducto").unbind();
             $(".btnEditarProducto").on("click", mostrarEditarArticulo);
            $(".table").each(function(n){

                $(this).DataTable( {
                    "language": espanol,
                    scrollCollapse: true,
                    paging:         false,
                    "columns": [
                        { "width": "90%" },
                        null,
                        null,
                        null
                      ]

                               
               } 

                );
                $(this).css("display","table");
                $(this).css("width","100%");
            });
            
            $.post("js/ajax/articulos.php",{accion:"selectCategorias"}, function(result){

                $("#selectCategorias").html('<option value="">Selecciona una categoría...</option>'+result);
            });
        
        });


    




}

function eliminarCategoria(id){

   // $('#modalEliminarCat').modal("show");
    $.post("php/ajax/confirmarEliminarCategoria.php",{id:id},function(result){

        $("#contenidoModalCategoria").html(result);
       
        $("#btnEliminarCat").unbind();
        $("#btnEliminarCat").on("click",function(){
            $.post("js/ajax/articulos.php",{accion:"eliminar-cat",id:id},function(result){

                   cargarContenido();
                   var categoria =  $("#zonaCategoriasArticulos a:first").attr("data-id-cat");
                   cargarArticulos(categoria);
                   mostrarMensaje("La categoría se ha eliminado correctamente");
                   console.log(result);

                   $('#modalEliminarCat').modal("hide");
               });
            
        });
    });



   
}

function primeraCarga(){

    $.post("php/ajax/zonaArticulos.php",{accion:"categorias"}, function(result){

        $("#zonaCategoriasArticulos").html(result);

        $("#zonaCategoriasArticulos a").on("click", function(){
            //cargarArticulos($(this).attr("data-id-cat"));

        })



    });
     $.post("php/ajax/zonaArticulos.php",{accion:"contenedores"}, function(result){
            $("#zonaContenedoresArticulos").html(result);

            $.post("js/ajax/articulos.php",{accion:"selectCategorias"}, function(result){

                $("#selectCategorias").html('<option value="">Selecciona una categoría...</option>'+result);
            });
        
        });
    


}


function mostrarEditarArticulo(){

    $.post("php/ajax/formEditarArticulo.php", {id:$(this).attr("data-id")}, function(result){
        $("#contenidoModalArticulo").html(result);
    })

}


function comprobarEliminarProducto(){
    $("#zonaEliminarProd").fadeOut('500',function(){
        $("#confirmarEliminar").fadeIn('500');
    });

    $("#btnNoEliminar").on("click",function(){
        $("#confirmarEliminar").fadeOut('500',function(){
            $("#zonaEliminarProd").fadeIn('500');
        });
        
    })
    $("#modalGenetica").on('hidden.bs.modal', function () {
        $("#confirmarEliminar").hide();
        $("#zonaEliminar").show();
});
}

function editarCategoria(id){
    $.post("js/ajax/articulos.php", {accion: "getCategoria", id:id}, function(result){
        let datos = JSON.parse(result);
        $("#i-mod-cat").val(datos[0]["nombre"]).attr("idCat", datos[0]["id"]);
        $("#t-mod-cat").html(datos[0]["descripcion"]);
        
    });

            /*$(boton).append('<div class="input-group mb-3" style="display:none">'+
      '<input type="text" class="form-control" value="'+boton.find(".n-cat").html()+'">'+
      '<div class="input-group-append">'+
        '<button class="btn btn-dark" type="button"><i class="fas fa-save"></i></button>'+
      '</div>'+
    '</div>');
     setTimeout(function(){
        $(".input-group").fadeIn(500);
    }, 550);
   // boton.find("span").fadeOut(500);*/
}


function actualizarCategoria(){
    let id =  $("#i-mod-cat").attr("idCat");
    let n =  $("#i-mod-cat").val();
    let d =  $("#t-mod-cat").val();

    $.post("js/ajax/articulos.php", {accion: "actCategoria", id:id, n:n, d:d}, function(result){
        if(result==""){
            mostrarMensaje("La categoría ha sido actualizada correctamente", "success");
            cargarContenido();
            $("#modalModCat").modal("hide");
        } else {
            mostrarMensaje("Ha ocurrido un error al editar la categoría", "error");
     
        }

    });
}


