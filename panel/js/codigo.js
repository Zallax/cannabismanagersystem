var opcionLateral = localStorage.getItem("barra-expandida");

if(opcionLateral=="si"){
	$("#page-top").addClass("sidenav-toggled");
} else {
	$("#page-top").removeClass("sidenav-toggled");
}

var estadoCaja = $(".btn-tablet").attr("abierta");
if(estadoCaja=="si"){
	$(".btn-tablet").prop("disabled", true);
}


$(document).ready(function() {

	$("#alertsDropdown").on("click", cargarNotificaciones);
	if(localStorage.getItem("mensaje")!=""){
		
		mostrarMensaje(localStorage.getItem("mensaje"),"success");
		localStorage.setItem("mensaje", "");	
	}
	$("#sidenavToggler").on("click", cambiarPosicionLateral);
	$("#btnPanico").on("click", panicoPulsado);

	if($(".btn-tablet").attr("abierta")=="no"){
		$(".btn-tablet").css("background-color", "#a71414");
	} 
	if ($(".btn-tablet").attr("abierta")=="cerrada") {
		$(".btn-tablet").css("background-color", "#525253").prop("disabled", true);
	}

	if ($(".btn-tablet").attr("abierta")=="si") {
		$(".btn-tablet").css("background-color", "#1aa95b");
	}

	$(".btn-tablet").on("click", estadoTablet);

});

function cambiarPosicionLateral(){
		if($("#page-top").hasClass("sidenav-toggled")){
			localStorage.setItem("barra-expandida", "si");
		} else {
			localStorage.setItem("barra-expandida", "no");
		}
}

function panicoPulsado(){
	var idU = $("#btnPanico").attr("data-id");
	$.post("php/funciones.php",{funcion:"botonPanico", idU:idU});
	location.href ="https://www.google.com";
}




function mostrarMensaje(mensaje, tipo){
	$("#mensajes").empty();
    if(tipo=="success"){

      $("#mensajes").css("background-color","rgba(205, 255, 152, 0.89)");
      $("#mensajes").append('<i class="material-icons">check</i>');
    }

    if(tipo=="error"){
      $("#mensajes").css("background-color","rgba(255, 152, 152, 0.89)");
      $("#mensajes").append('<i class="material-icons">error</i>');
    }
     

     $("#mensajes").append(mensaje);
     $("#mensajes").fadeIn().delay(3000).fadeOut(500);

}

function estadoTablet(){
	$(".btn-tablet").css("background-color", "#e06f0b").html('<img src="img/cargandomini.svg" width="15px">').prop("disabled",true);
	var u = $(this).attr("usuario");
	var estado = $(this).attr("abierta");
	if(estado == "si"){
		$.post("js/ajax/estado-tablet.php", {accion:"bloquear", u:u}, function(result){
			if(result==""){
				$(".btn-tablet").css("background-color", "#a71414").prop("disabled",false).html('<i class="fas fa-lock-open"></i> <small>ABRIR CAJA</small>').attr("abierta","no");

			}
		});
	} else if(estado =="bloqueada"){
		$.post("js/ajax/estado-tablet.php", {accion:"panico", u:u}, function(result){
			if(result==""){
				$(".btn-tablet").css("background-color", "#1aa95b").prop("disabled",true).html('<i class="fas fa-lock"></i> <small>CAJA ABIERTA</small>').attr("abierta","si");
			} else {
				console.log(result);
			}
		});
	} else {
		$.post("js/ajax/estado-tablet.php", {accion:"desbloquear", u:u}, function(result){
			if(result==""){
				//$(".btn-tablet").css("background-color", "#1aa95b").prop("disabled",true).html('<i class="fas fa-lock"></i> <small>CAJA ABIERTA</small>').attr("abierta","si");
				location.reload();
			} else {
				console.log(result);
			}
		});
	}
}


function cargarNotificaciones(){
	$.post("php/notificaciones.php",{funcion:"mostrar"},function(result){
		$("#zona-notificaciones").html(result);
	});
}

function fecha(fecha){
	var f = fecha.split("-");
	return f[2]+"/"+f[1]+"/"+f[0];
}