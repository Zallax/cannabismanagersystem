 $(document).ready(function() {
	var recordar = localStorage.getItem('email');

	if(recordar!=null){
		$("#recordar-mail").prop('checked', true);
		$("#inputMail").val(recordar);
	}

 	$("#btnAcceder").on("click", comprobarLogin);
 	$(window).bind('keydown', function(e) {            
 	  if (e.charCode == 13 || e.keyCode == 13) {//ENTER
 	   	comprobarLogin();
 	  }    
 	});

});


function comprobarLogin(){
	var mail = $("#inputMail").val();
	var pass = $("#inputPass").val();
	if(pass==""){
			$("#inputPass").focus();
			$("#inputPass").addClass("is-invalid");
	} else {
		
		$("#inputPass").removeClass("is-invalid");
		
	}
	if(mail==""){
		$("#inputMail").focus();
		$("#inputMail").addClass("is-invalid");
	} else {
		$("#inputMail").removeClass("is-invalid");
		if($("#recordar-mail").prop('checked')){
			localStorage.setItem('email', mail);
		} else {
			localStorage.setItem('email', '');
		}
		
	} 
	

	if(mail!="" && pass!=""){
		var txtAnterior = $("#btnAcceder").html();
		$("#btnAcceder").html("<img width='20px' src='img/cargandomini.svg'>");
		$(".alert").fadeOut("500");
		$.post("js/ajax/comprobarLogin.php",{m:mail, p:pass}, function(result){
			if(result=="success"){
				 location.href ="index.php";
			} else {
				//$("#inputMail").val("");
				$("#inputPass").val("");
				$("#btnAcceder").html(txtAnterior);
				console.log(result);
				$(".alert").fadeIn("500");
				$(".card").addClass("animated shake");
				setTimeout(function(){
					$(".card").removeClass("animated shake");
				}, 500);

			}
		})
	}
}