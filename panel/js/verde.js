 $(document).ready(function() {

    
  var tipo = $("#tipo").val();
    cargarIndicas(tipo);
    habilitarCargaImagenes();
    $("#btnMostrarIndicas").on("click", function(){
        cargarIndicas(tipo);
    });
    $("#btnMostrarSativas").on("click", function(){
        cargarSativas(tipo);
    });
    $("#btnMostrarExtraccion").on("click", function(){
        cargarExtraccion(tipo);
    });
    $("#btnMostrarHibridas").on("click", function(){
        cargarHibridas(tipo);
    });
    $("#btnMostrarOtros").on("click", function(){
        cargarOtros();
    });
    $("#btnEditarGenetica").on("click", function(){
        editarGenetica(tipo);
    });
  
    $("#btnEliminarGenetica").on("click", comprobarEliminarGenetica);

    $("#btnSiEliminar").on("click", function(){
        eliminarGenetica(tipo);
    });


    
    /* $("#formAnadirGenetica").on("submit", function(e){
      e.preventDefault();
      nuevaGenetica(tipo);
    })*/


});

     function cargarFormEditarGenetica(){

        var boton=$(this);
        var id = boton.attr("data-id");
        $(".modal-footer").hide();
        $("#contenidoModalGenetica .contenido").hide();
        $("#contenidoModalGenetica .cargando").fadeIn();
        $.post("php/ajax/formEditarGenetica.php", {id: id}, function(result){
            $("#contenidoModalGenetica .contenido").html(result);
            $("#contenidoModalGenetica .cargando").fadeOut("500", function(){
                $("#contenidoModalGenetica .contenido").fadeIn("500");
                $(".modal-footer").fadeIn();
            });
            

          
    

        });
    }	


function cargarIndicas(tipo){
    	
    	$.post("php/ajax/cargaTablas.php",{tipo:tipo,categoria:"indica"}, function(result){
           
        $(".tablaIndicas").empty().append(result);
             $('#tablageneticaindica').css('width', '100%');
             $('#tablageneticaindica').DataTable( {
                 "language": espanol,
                 scrollY:        '50vh',
                 scrollCollapse: true,
                 paging:         false,
                 "columns": [
                     { "width": "100px" },
                     { "width": "90%" },
                     { "width": "50px" },
                     { "width": "50px" },
                     null,
                     { "width": "50px" },
                   ]

                            
            } 

             );

             $('a[data-toggle="tab"]').on( 'shown.bs.tab', function (e) {
                  $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
              } );
   
        $(".btnEditarGenetica").on("click",cargarFormEditarGenetica);


    });
}

function cargarSativas(tipo){
    	
    	$.post("php/ajax/cargaTablas.php",{tipo:tipo,categoria:"sativa"}, function(result){
           
        $(".tablaSativas").empty().append(result);
        $('#tablageneticasativa').css('width', '100%');
             $('#tablageneticasativa').DataTable( {
                 "language": espanol,
                 scrollY:        '50vh',
                 scrollCollapse: true,
                 paging:         false,
                 "columns": [
                     { "width": "100px" },
                     { "width": "90%" },
                     { "width": "50px" },
                     { "width": "50px" },
                     null,
                     { "width": "50px" },
                   ]

                            
            } 

             );


   	
        $(".btnEditarGenetica").on("click",cargarFormEditarGenetica);


    });
}

function cargarExtraccion(tipo){
    	
    	$.post("php/ajax/cargaTablas.php",{tipo:tipo,categoria:"extraccion"}, function(result){
           
        $(".tablaExtraccion").empty().append(result);
        $('#tablageneticaextraccion').css('width', '100%');
         $('#tablageneticaextraccion').DataTable( {
                 "language": espanol,
                 scrollY:        '50vh',
                 scrollCollapse: true,
                 paging:         false,
                 "columns": [
                     { "width": "100px" },
                     { "width": "90%" },
                     { "width": "50px" },
                     { "width": "50px" },
                     null,
                     { "width": "50px" },
                   ]

                            
            } 

             );
           
   
        $(".btnEditarGenetica").on("click",cargarFormEditarGenetica);


    });
}

function cargarHibridas(tipo){
        
        $.post("php/ajax/cargaTablas.php",{tipo:tipo,categoria:"hibrida"}, function(result){
        $(".tablaHibridas").empty().append(result);
        $('#tablageneticahibrida').css('width', '100%');
         $('#tablageneticahibrida').DataTable( {
                 "language": espanol,
                 scrollY:        '50vh',
                 scrollCollapse: true,
                 paging:         false,
                 "columns": [
                     { "width": "100px" },
                     { "width": "90%" },
                     { "width": "50px" },
                     { "width": "50px" },
                     null,
                     { "width": "50px" },
                   ]

                            
            } 

             );
           
   
        $(".btnEditarGenetica").on("click",cargarFormEditarGenetica);


    });
}

function cargarOtros(){
        
        $.post("php/ajax/cargaTablas.php",{tipo:"otros",categoria:"otros"}, function(result){
        $(".tablaOtros").empty().append(result);
        $('#tablageneticaotros').css('width', '100%');
         $('#tablageneticaotros').DataTable( {
                 "language": espanol,
                 scrollY:        '50vh',
                 scrollCollapse: true,
                 paging:         false,
                 "columns": [
                     { "width": "100px" },
                     { "width": "90%" },
                     { "width": "50px" },
                     { "width": "50px" },
                     null,
                     { "width": "50px" },
                   ]

                            
            } 

             );
           
   
        $(".btnEditarGenetica").on("click",cargarFormEditarGenetica);


    });
}

function editarGenetica(tipo){
		$("#btnEditarGenetica").html('<img src="./img/cargandomini.gif" style="width:30px;">');

		var nombre = $("#nombreGenetica").val();
		var descripcion = $("#descripcionGenetica").val();
		var precio = $("#precioGenetica").val();
		var stock = $("#stockGenetica").val();
		var banco = $("#bancoGenetica").val();
		var pindica = $("#pindicaGenetica").val();
		var psativa = $("#psativaGenetica").val();
		var pthc = $("#pthcGenetica").val();
		var pcbd = $("#pcbdGenetica").val();
        var gusto = $("#gustoGenetica").val();
        var id = $("#idGenetica").val();
        var imagen = $(".fotoGenetica>.file-input>.file-preview>.file-drop-disabled>.file-preview-thumbnails>.file-preview-frame>.kv-file-content>.file-preview-image").attr("src");
        if($("#check1").prop("checked")==true){
            var destacada = 1;
        } else {
            var destacada = 0;
        }


		$.post("js/ajax/actualizarGenetica.php",{nombre:nombre, descripcion:descripcion, precio:precio, stock:stock,banco:banco,pindica:pindica,psativa:psativa,pthc:pthc,pcbd:pcbd, id:id, gusto:gusto, imagen:imagen, destacada:destacada}, function(result){
        
           if(result=="ok"){
           	$('#modalGenetica').modal('hide');
            mostrarMensaje('<i class="fa fa-check-circle"></i> Se han guardado los cambios correctamente',"success");
     
            $("#btnEditarGenetica").html('<i class="fa fa-save"></i> Guardar');
           
            cargarGeneticas(tipo);
           } else {
            //mostrarMensaje('Ha ocurrido un error');
            console.log(result);
           }
           

    });


}
function comprobarEliminarGenetica(){
    $("#zonaEliminar").fadeOut('500',function(){
        $("#confirmarEliminar").fadeIn('500');
    });

    $("#btnNoEliminar").on("click",function(){
        $("#confirmarEliminar").fadeOut('500',function(){
            $("#zonaEliminar").fadeIn('500');
        });
        
    })
    $("#modalGenetica").on('hidden.bs.modal', function () {
        $("#confirmarEliminar").hide();
        $("#zonaEliminar").show();
});
}
function eliminarGenetica(tipo){
    var id = $("#idGenetica").val();
    $.post("js/ajax/eliminarGenetica.php",{id:id},function(result){
        $('#modalGenetica').modal('hide');
        mostrarMensaje('<i class="fa fa-trash"></i> La genética se ha eliminado correctamente',"error");        
        cargarGeneticas(tipo);
        console.log(result);
    });
}


//TRADUCCIÓN DATATABLE
 var espanol={
                     "lengthMenu": "Mostrar _MENU_ registros por página",
                     "zeroRecords": "No se han encontrado datos",
                     "info": "Mostrando página _PAGE_ de _PAGES_",
                     "infoEmpty": "No se han encontrado registros.",
                     "infoFiltered": "(filtered from _MAX_ total records)",
                     "paginate": {
                            "first":      "Primero",
                            "last":       "Último",
                            "next":       ">",
                            "previous":   "<"
                        },
                     "search":         "Buscar:",
                };



function nuevaGenetica(tipo){
    


  
    var validado2=false;
    var mensaje1="";
    var mensaje2="";

   

    if($(':input:checked').attr("id")!= undefined){

        validado2 = true;
        mensaje="";
    } else {
        validado2 = false;

        mensaje2='<p><i class="fa fa-times-circle" ></i> No has elegido ninguna categoría</p>';
    }


    if(validado2){
        var imagen = $(".fotoGenetica>.file-input>.file-preview>.file-drop-disabled>.file-preview-thumbnails>.file-preview-frame>.kv-file-content>.file-preview-image").attr("src");
        
        var n = $("#ngenetica").val();
        var d = $("#dgenetica").val();
        var g = $("#ggenetica").val();
        var b = $("#bgenetica").val();
        var pi = $("#pigenetica").val();
        var ps = $("#psgenetica").val();
        var pt = $("#ptgenetica").val();
        var pc = $("#pcgenetica").val();
        var s = $("#sgenetica").val();
        var p = $("#pgenetica").val();
        var cat = $(':input:checked').attr("id");
        if(cat=="otros"){
            tipo=="otros";
        }
        alert(cat);
        alert(tipo);

          $.post("js/ajax/cargarNuevaGenetica.php",{tipo:tipo, imagen:imagen, nombre:n, descripcion:d, gusto:g, banco:b, pindica:pi, psativa:ps, pthc:pt, pcbd:pc, categoria:cat, stock:s, precio:p}, function(result){

            if (result=="success"){
              mostrarMensaje("Se ha añadido la genética "+n,"success");
              $(".fotoGenetica").html('<div class="file-loading"><input id="foto-a-genetica" name="avatar-1" type="file" accept="image/*" required> </div>');
              habilitarCargaImagenes();
              $("input").each(function(){
                $(this).val("");
              })
              $("textarea").val("");
              cargarGeneticas();
            } else {
              mostrarMensaje('<i class="fa fa-times-circle" ></i> '+result,"error"); 
            }



        });
    } else {
      mostrarMensaje(mensaje1+mensaje2, "error"); 

    }


 
       
       


    
}

function mostrarMensaje(mensaje, tipo){
    if(tipo=="success"){
      $("#mensajes").css("background-color","rgba(205, 255, 152, 0.89)");
    }

    if(tipo=="error"){
      $("#mensajes").css("background-color","rgba(255, 152, 152, 0.89)");
    }
     $("#mensajes").empty();

     $("#mensajes").append(mensaje);
     $("#mensajes").fadeIn().delay(3000).fadeOut(500);

}

function cargarGeneticas(tipo){
    cargarIndicas(tipo);
    cargarSativas(tipo);
    cargarHibridas(tipo);
    cargarExtraccion(tipo);
}


function habilitarCargaImagenes(){
    var btnCust = '<button id="btnSubirFotoPersonal" type="button" class="btn btn-info btn-block" title="Guardar imagen" >' +
        '<i class="fa fa-edit"></i> Guardar' +
        '</button>'; 
    $("#foto-a-genetica").fileinput({
        theme: "fa",
        overwriteInitial: true,
        maxFileSize: 5000,
        showClose: false,
        showCaption: false,
        showBrowse: false,
        browseOnZoneClick: true,

        elErrorContainer: '#kv-avatar-errors-2',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<div style="width:100%;height:250px;background-color:#618645;"><div style="background-image:url(img/maria.png);;width:100%;height:100%;background-size: 100px;background-repeat: no-repeat;background-position: center;"></div>',
        layoutTemplates: {main2: '{preview} ' + ' {browse}'},
        allowedFileExtensions: ["jpg", "png", "gif"]
    });
}

function afterModalTransition(e) {
    e.setAttribute("style", "display: none !important;");
  }

  
  $('.fade2').on('hide.bs.modal', function (e) {
    setTimeout( () => afterModalTransition(this), 200);
})
