

  var f = new Date();
  var fecha = f.getFullYear()+"-"+(f.getMonth()+1)+"-"+f.getDate();
  var totalGeneticas = 0;
  var totalProductos = 0;
$(document).ready(function() {

  $("#btn-guardar").on("click", guardarCaja);



});
let idCaja = $(".btn-tablet").attr("id-caja");
if(idCaja != ""){
  consultarVentas(idCaja);

  var productos = new Array();
  var geneticas = new Array();

}
    
function consultarVentas(idCaja){
    
    var compras = new Array();
    postVentasHoy(idCaja).done(function(data){
      compras = JSON.parse(data);

    }).done(function(){
      $(compras).each(function(n){
          guardarCompra(JSON.parse(compras[n]["articulos"]));
      });
      console.log(totalGeneticas);
      console.log("N Productos-> "+productos.length);
      console.log("N Cannabis-> "+geneticas.length);

      $(geneticas).each(function(c){
        var tipo="";
    
        if(geneticas[c][3]=="v" || geneticas[c][3]=="o"){
          tipo = '<div class="tipo verde" tipo="v">v</div>';
        } else {
          tipo = '<div class="tipo marron" tipo="m">m</div>';
        }
        var td = '<tr>';
        td += '<td>';
        td += tipo;
        td += '</td>';
        td += '<td>';
        td += geneticas[c][0];
        td += '</td>';
        td += '<td class="text-right">';
        td += geneticas[c][1];
        td += '</td><td class="text-right"><span class="precio-total-genetica">';
      //  alert(geneticas[c][2]+" x "+geneticas[c][1]+" = "+geneticas[c][2]*geneticas[c][1]);
     // alert(geneticas[c][2]);
        td +=(geneticas[c][2])+"</span> €</tr>";

        $("#tabla-caja-geneticas tbody").append(td);

        
       
      });

      $("#tabla-caja-geneticas").DataTable( {
                  "order": [[ 1, "asc" ]],
                   "language": espanol,
                   "paging":   false,
                   "info":     false,
                   responsive: true,
                   "autoWidth": false,
                   scrollY:        500,
                   scrollCollapse: true,


                              
        }); 
      $("#total-geneticas").html(totalGeneticas.toFixed(2));

      $(productos).each(function(c){
        var td = '<tr>';
        td += '<td>';
        td += productos[c][0];
        td += '</td>';
        td += '<td class="text-right">';
        td += productos[c][1];
        td += '</td>';
        td += '</td><td class="text-right"><span class="precio-total-genetica">';
        td +=productos[c][2]*productos[c][1]+"</span> €</tr>";
        $("#tabla-caja-productos tbody").append(td);

       
      });


        $("#total-productos").html(totalProductos.toFixed(2));

          $("#tabla-caja-productos").DataTable( {
                     "language": espanol,
                     "paging":   false,
                     "info":     false,
                     responsive: true,
                     "autoWidth": false,
                     scrollY:        500,
                     scrollCollapse: true,
                     

         });
          let total = parseFloat($("#total-hoy").html())+(totalGeneticas+totalProductos);
       
          $("#total-hoy").html(total.toFixed(2));
          consultarCuotas(idCaja);
          if($("#total-hoy").html()!="0.00"){
            $("#btn-guardar").attr("disabled",false);
            $("#btn-guardar").html('<i class="fas fa-save"></i> Cerrar caja');
          }
          consultarUltimaCaja();


    });
    

}

function guardarCompra(compra){
  $(compra).each(function(e){
   
    if(this[0]==1){
      console.log("cannabis-> "+this[1]+"x"+this[2]);
      var producto = new Array();

      //METE EN VARIABLE PRODUCTO NOMBRE Y CANTIDAD

      let total = (parseFloat(this[2])*parseFloat(this[3])).toFixed(2);
     
      producto.push(this[1], this[2], total, this[4]);
    //  alert(producto[2]);

      totalGeneticas+=parseFloat(this[3]*this[2]);
      //console.log(compra);

      var existe = false;
      if(geneticas.length!=0){
      
        $(geneticas).each(function(k){
          
        
          if(geneticas[k].includes(producto[0])){
            existe=true;

            this[1]=parseFloat(this[1])+parseFloat(producto[1]);
            this[2]=parseFloat(this[2])+parseFloat(producto[2]);

           // alert(total);
          // alert(this[2]);
          //  this[2]=(parseFloat(total)+parseFloat(this[2])).toFixed(2);
         //   alert(producto[2]);
            producto = [];
          }

        });
        if(!existe){
           geneticas.push(producto);
            
            
            producto = [];
        }
      } else {
        geneticas.push(producto);
            producto = [];
            
      }

     



    } else {
      console.log("producto-> "+this[1]);
      var producto = new Array();

      //METE EN VARIABLE PRODUCTO NOMBRE Y CANTIDAD
      producto.push(this[1], this[2], this[3]);


      totalProductos+=parseFloat(this[3]*this[2]);

      var existe = false;
      if(productos.length!=0){

        $(productos).each(function(k){
          
          if(productos[k].includes(producto[0])){
            existe=true;
            this[1]=parseFloat(this[1])+parseFloat(producto[1]);
            
            producto = [];
          }
        });
        if(!existe){
           productos.push(producto);
            
            
            producto = [];
        }
      } else {
        productos.push(producto);
            producto = [];
            
      }
       
    }
  });
 
}

function postVentasHoy(idCaja){

  return $.post("js/ajax/caja.php", {funcion:"ventasHoy", idCaja:idCaja}, function(result){

    resultado = result;
    
  });

}


function guardarCaja(){
  console.log(JSON.stringify(geneticas));
  console.log(JSON.stringify(productos));
  var texto = $(this).html();
  let tCuotas = parseFloat($("#total-cuotas").html());

  $(this).html('<img src="./img/cargandomini.gif" style="width:30px;">');
  $("#btn-guardar").attr("disabled","true");
  $.post("js/ajax/caja.php", {funcion:"guardarCaja", geneticas:JSON.stringify(geneticas), productos:JSON.stringify(productos), total_p:totalProductos.toFixed(2), total_c:totalGeneticas.toFixed(2),total_cuo:tCuotas, total:(totalGeneticas+totalProductos+tCuotas).toFixed(2), idCaja:idCaja}, function(result){
      
      if(result=="success"){
        localStorage.setItem("mensaje","La caja de hoy ha sido registrada correctamente");
        location.reload();
    
       
      } else {
        mostrarMensaje("Ha ocurrido un error al guardar la caja","error");
        console.log(result);
      }
      

  });
  

}

function consultarUltimaCaja(){

  $.post("js/ajax/caja.php",{funcion:"consultarUltimaCaja", idCaja:idCaja}, function(result){
    if(result!="false"){
      
      var datos = JSON.parse(result);
      $("#btn-guardar").attr("disabled","true");
      $("#btn-guardar").html("La última caja ha sido guardada a las "+datos["hora"]);
      $("#btn-eliminar-caja").fadeIn("500");
      $("#btn-c-e-caja").on("click", eliminarCaja);
    }
  });
}
 
function eliminarCaja(){

  $.post("js/ajax/caja.php",{funcion:"eliminarUltimaCaja", idCaja:idCaja}, function(result){
    if(result=="success"){

      $("#modalEliminarCaja").modal("hide");
      $(".container").fadeOut("500");
      mostrarMensaje("La caja se ha eliminado correctamente","success");
      setTimeout(function(){location.reload();},3000);
    

    } else {
      $("#modalEliminarCaja").modal("hide");
      mostrarMensaje("Ha ocurrido un error al eliminar la última caja","error");
      console.log(result);
    }
  });
}

function consultarCuotas(idCaja){
  $.post("js/ajax/caja.php", {funcion:"cuotasCaja", idCaja:idCaja}, function(result){
    let datos = JSON.parse(result);
    if(datos.length!=0){
      let totalCuotas = 0;
      $(datos).each(function(n){
        totalCuotas += parseFloat(datos[n]["cuota"]);
      });
      $("#n-cuotas").html(datos.length);
      $("#total-cuotas").html(totalCuotas);
      $("#total-hoy").html(parseFloat($("#total-hoy").html())+parseFloat(totalCuotas));
      if($("#total-hoy").html()!="0.00"){
        $("#btn-guardar").attr("disabled",false);
        $("#btn-guardar").html('<i class="fas fa-save"></i> Guardar');
      }

    }
  });
}

//TRADUCCIÓN DATATABLE
 var espanol={
                     "lengthMenu": "Mostrar _MENU_ registros por página",
                     "zeroRecords": "No se han encontrado datos",
                     "info": "Mostrando página _PAGE_ de _PAGES_",
                     "infoEmpty": "No se han encontrado registros.",
                     "infoFiltered": "(filtered from _MAX_ total records)",
                     "paginate": {
                            "first":      "Primero",
                            "last":       "Último",
                            "next":       ">",
                            "previous":   "<"
                        },
                     "search":         "Buscar:",
                };
