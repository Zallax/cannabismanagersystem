

  var totalGeneticas = 0;
  var totalProductos = 0;
  var tablas = false;
$(document).ready(function() {
  $("#btn-elim").on("click", function(){
    $("#cF1").html(fecha($("#inputF1").val()));
    $("#cF2").html(fecha($("#inputF2").val()));
    $("#modalEliminarDatos").modal("show");

  });


  $("#btn-c-e-datos").on("click", borrarDatos);

  $("#btn-impr").on("click", imprim1);

  $("#btn-ayer").on("click", function(){

    $(".tab-content").fadeOut("500", function(){
      $(".cargando").fadeIn("500");
      consultarVentas("","","ayer");
      consultarTotales("","","ayer");

    });
  });

  $("#btn-semana").on("click", function(){

    $(".tab-content").fadeOut("500", function(){
      $(".cargando").fadeIn("500");
      consultarVentas("","","semana");
      consultarTotales("","","semana");

    });
  });

  $("#btn-mes").on("click", function(){

    $(".tab-content").fadeOut("500", function(){
      $(".cargando").fadeIn("500");
      consultarVentas("","","mes");
      consultarTotales("","","mes");
    });
  });
  $("#btn-ano").on("click", function(){

    $(".tab-content").fadeOut("500", function(){
      $(".cargando").fadeIn("500");
      consultarVentas("","","ano");
      consultarTotales("","","ano");
    });
  });

  $("#btn-consultar").on("click", function(){
    var fecha1 = $("#inputF1").val();
    var fecha2 = $("#inputF2").val();

    if(fecha1!="" || fecha2!=""){
      
      
      $(".tab-content").fadeOut("500", function(){
        $(".cargando").fadeIn("500");
        consultarVentas(fecha1,fecha2,"");
        consultarTotales(fecha1,fecha2,"");

      });
    } else {
      mostrarMensaje("No has seleccionado ningún periodo","error");
    }

 
  });

  if($("#total-hoy").html()=="0.00"){
    $("#btn-guardar").attr("disabled","true");
    $("#btn-guardar").html("La caja está vacía");
  }



});

    var productos = new Array();
    var geneticas = new Array();

function consultarVentas(fecha1, fecha2, tipo){
  $("#zonaTablaGen").empty();
  $("#zonaTablaGen").html(' <table class="table table-hover table-light" id="tabla-caja-geneticas"><thead class="bg-white text-grey"><tr><th style="width:10%;">TIPO</th><th style="width:50%;">Nombre</th><th>Cantidad</th><th>Total</th></tr></thead><tbody></tbody></table>');
  $("#zonaTablaProd").empty();
  $("#zonaTablaProd").html('<table class="table table-hover table-light" id="tabla-caja-productos"><thead class="bg-white text-grey"><tr><th style="width:60%;">Nombre</th><th>Cantidad</th><th>Total</th></tr></thead><tbody></tbody></table>');

   totalGeneticas = 0;
    totalProductos = 0;
    productos = [];
    geneticas = [];
    var compras = new Array();
    postVentasFechas(fecha1,fecha2, tipo).done(function(data){
      compras = JSON.parse(data);

    }).done(function(){
      $(compras).each(function(n){
          guardarCompra(JSON.parse(compras[n]["geneticas"]), "genetica");
          guardarCompra(JSON.parse(compras[n]["productos"]), "producto");
      });
      console.log(totalGeneticas);
      console.log("N Productos-> "+productos.length);
      console.log("N Cannabis-> "+geneticas.length);
   
     
      
      
      $(geneticas).each(function(c){
        var tipo="";
    
        if(geneticas[c][3]=="v" || geneticas[c][3]=="o"){
          tipo = '<div class="tipo verde" tipo="v">v</div>';
        } else {
          tipo = '<div class="tipo marron" tipo="m">m</div>';
        }


        var td = '<tr>';
        td += '<td>';
        td += tipo;
        td += '</td>';
        td += '<td>';
        td += geneticas[c][0];
        td += '</td>';
        td += '<td class="text-right">';
        td += (parseFloat(geneticas[c][1])).toFixed(2);
        td += '</td><td class="text-right"><span class="precio-total-genetica">';
        //console.log(geneticas[c][0] + "-> "+geneticas[c][2]);
        console.log("Cantidad -> "+geneticas[c][1]+" x "+geneticas[c][2]);
        //console.log("Precio ->"+geneticas[c][2]);
        let total = parseFloat(geneticas[c][2]);
        td +=total.toFixed(2)+"</span> €</tr>";


        $("#tabla-caja-geneticas tbody").append(td);

        
       
      });


     
      //$("#tabla-caja-productos tbody").empty();
      $(productos).each(function(c){
        var td = '<tr>';
        td += '<td>';
        td += productos[c][0];
        td += '</td>';
        td += '<td class="text-right">';
        td += productos[c][1];
        td += '</td>';
        td += '</td><td class="text-right"><span class="precio-total-genetica">';
        td +=(parseFloat(productos[c][2]*productos[c][1])).toFixed(2)+"</span> €</tr>";
        $("#tabla-caja-productos tbody").append(td);

       
      });

          
       

          
            
         


          /* $("#total-geneticas").html(totalGeneticas.toFixed(2));
          $("#total-hoy").html((totalGeneticas+totalProductos).toFixed(2));
          $("#total-productos").html(totalProductos.toFixed(2));*/
    });
    setTimeout(function(){
      $(".cargando").fadeOut("500", function(){
      $(".tab-content").fadeIn("500");

         $("#tabla-caja-productos").DataTable( {
                    "language": espanol,
                    "paging":   false,
                    "info":     false,
                    

        });
         $("#tabla-caja-geneticas").DataTable( {
                     "order": [[ 1, "asc" ]],
                      "language": espanol,
                      "paging":   false,
                      "info":     false,


                                 
           }); 
      });

      }, 500);


}

function guardarCompra(compra, tipo){
  $(compra).each(function(e){
   
    if(tipo=="genetica"){
     // console.log("cannabis-> "+this[0]+"x"+this[1]);
      var producto = new Array();

      //METE EN VARIABLE PRODUCTO NOMBRE Y CANTIDAD
      producto.push(this[0], this[1], this[2], this[3]);

      totalGeneticas+=parseFloat(this[2]*this[1]);
      var existe = false;
      if(geneticas.length!=0){

        $(geneticas).each(function(k){
          
          if(geneticas[k].includes(producto[0])){
            existe=true;
            this[1]=parseFloat(this[1])+parseFloat(producto[1]);
            this[2]=parseFloat(this[2])+parseFloat(producto[2]);
            producto = [];
          }
        });
        if(!existe){
           geneticas.push(producto);
            
            
            producto = [];
        }
      } else {
        geneticas.push(producto);
            producto = [];
            
      }

     



    } else {
    //  console.log("producto-> "+this[0]);
      var producto = new Array();

      //METE EN VARIABLE PRODUCTO NOMBRE Y CANTIDAD
      producto.push(this[0], this[1], this[2]);

      totalProductos+=parseFloat(this[2]*this[1]);

      var existe = false;
      if(productos.length!=0){

        $(productos).each(function(k){
          
          if(productos[k].includes(producto[0])){
            existe=true;
            this[1]=parseFloat(this[1])+parseFloat(producto[1]);
            
            producto = [];
          }
        });
        if(!existe){
           productos.push(producto);
            
            
            producto = [];
        }
      } else {
        productos.push(producto);
            producto = [];
            
      }
       
    }
  });


  
}

function consultarTotales(fecha1,fecha2,tipo){

  $.post("js/ajax/caja.php", {funcion:"totalesVentas", fecha1:fecha1, fecha2:fecha2, tipo:tipo}, function(result){
      var datos = JSON.parse(result);
    
       $("#total-geneticas").html(datos[0]["total_gen"]);
      
      $("#total-productos").html(datos[0]["total_prod"]);

      $("#total-cuotas").html(datos[0]["total_cuotas"]);

      $("#total-hoy").html(datos[0]["total"]);

    });
}


function postVentasFechas(fecha1, fecha2, tipo){

  return $.post("js/ajax/caja.php", {funcion:"ventasFechas", fecha1:fecha1, fecha2:fecha2, tipo:tipo}, function(result){
    resultado = result;
    
  });

}


function borrarDatos(){
  $("#modalEliminarDatos").modal("hide");
  var fecha1 = $("#inputF1").val();
  var fecha2 = $("#inputF2").val();
  $.post("php/funciones.php", {funcion:"borrarDatos", fecha1:fecha1, fecha2:fecha2}, function(result){
    if(result!="error"){
        mostrarMensaje("Los datos han sido eliminados correctamente", "success");
    } else {
        mostrarMensaje("Ha ocurrido un error al eliminar los datos", "error");
    }
  });
}



//TRADUCCIÓN DATATABLE
 var espanol={
                     "lengthMenu": "Mostrar _MENU_ registros por página",
                     "zeroRecords": "No se han encontrado datos",
                     "info": "Mostrando página _PAGE_ de _PAGES_",
                     "infoEmpty": "No se han encontrado registros.",
                     "infoFiltered": "(filtered from _MAX_ total records)",
                     "paginate": {
                            "first":      "Primero",
                            "last":       "Último",
                            "next":       ">",
                            "previous":   "<"
                        },
                     "search":         "Buscar:",
                };
