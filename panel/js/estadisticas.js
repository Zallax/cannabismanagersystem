 var datos = new Array();

var ctx = document.getElementById("ultimaSemana").getContext('2d');
$.post("js/ajax/caja.php", {funcion:"ventasUltimaSemana"}, function(result){
   if(result!=""){
      datos = JSON.parse(result);
      var fechas = new Array();
      var ventas = new Array();
      $(datos).each(function(n){
         var temporal = "";
         temporal = this["fecha"].split("-");
         fechas.push(temporal[2]+"/"+temporal[1]+"/"+temporal[0]);
         ventas.push(datos[n]["total"]);
      });

     var ventasSemanal = new Chart(ctx, {
         type: 'bar',
         data: {
             labels: fechas,
             datasets: [{
                 label: '# ventas de la última semana (€)',
                 data: ventas,
                 backgroundColor: [
                     '#00a65a',
                     '#00a65a',
                     '#00a65a',
                     '#00a65a',
                     '#00a65a',
                     '#00a65a',
                     '#00a65a',
                     '#00a65a',
                     '#00a65a',
                     '#00a65a',
                     '#00a65a',
                     '#00a65a',
                     '#00a65a'
                 ],

                 borderWidth: 0
             }]
         },
         options: {
             scales: {
                 yAxes: [{
                     ticks: {
                         beginAtZero:true
                     }
                 }]
             }
         }
     });

   }
   console.log(datos.length);
   console.log(datos[0]["fecha"]);
});

//------------------------------------------------

consultarVentas();



    var productos = new Array();
    var geneticas = new Array();

function consultarVentas(){
   totalGeneticas = 0;
    totalProductos = 0;
    productos = [];
    geneticas = [];
    var compras = new Array();
    postVentasFechas().done(function(data){
      compras = JSON.parse(data);

    }).done(function(){
      $(compras).each(function(n){
          guardarCompra(JSON.parse(compras[n]["geneticas"]), "genetica");
          guardarCompra(JSON.parse(compras[n]["productos"]), "producto");
      });

      var arrayNGen = new Array();
      var arrayCant = new Array();

      $(geneticas).each(function(e){
          if(this[1] > 20){
            arrayNGen.push(this[0]);
            arrayCant.push(this[1]);
          }

      });
      console.log(arrayNGen.length);
      var ctx2 = document.getElementById("geneticasDispensadas").getContext('2d');
      var ventasGeneticas = new Chart(ctx2, {
          type: 'pie',
          data: {
              labels: arrayNGen,
              datasets: [{
                  label: '# ventas última semana',
                  data: arrayCant,
                  backgroundColor: [
                      '#fcd116',
                      '#e5053a',
                      '#0038a8',
                      '#008c82',
                      '#444f51',
                      '#60542b',
                      '#2d338e',
                      '#d12d33',
                      '#9ec400',

                  ],
                  borderWidth: 1
              }]
          }

      });













    });



}

function guardarCompra(compra, tipo){
  $(compra).each(function(e){

    if(tipo=="genetica"){
      console.log("cannabis-> "+this[0]+"x"+this[1]);
      var producto = new Array();

      //METE EN VARIABLE PRODUCTO NOMBRE Y CANTIDAD
      producto.push(this[0], this[1], this[2], this[3]);

      totalGeneticas+=parseFloat(this[2]*this[1]);
      var existe = false;
      if(geneticas.length!=0){

        $(geneticas).each(function(k){

          if(geneticas[k].includes(producto[0])){
            existe=true;
            this[1]=parseFloat(this[1])+parseFloat(producto[1]);

            producto = [];
          }
        });
        if(!existe){
           geneticas.push(producto);


            producto = [];
        }
      } else {
        geneticas.push(producto);
            producto = [];

      }





    }


  });



}

function postVentasFechas(){

  return $.post("js/ajax/caja.php", {funcion:"ventasTotales"}, function(result){
    resultado = result;

  });

}
