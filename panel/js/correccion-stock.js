let idCaja = $(".btn-tablet").attr("id-caja");
hayPesaje();

$(document).ready(function() {
    $(".input-stock").on("focusout", comprobarInputStock);
    $("#btn-rellenar").on("click", rellenarAuto);
  	

 });


 function comprobarInputStock(){
  if($(this).val()!=""){
    $(this).parent().parent().css("background-color","#ace8ac");

  } else {
    $(this).parent().parent().css("background-color","white");
  }
 }


function rellenarAuto(){
	$(".input-stock").each(function(n){
		$(this).val(parseFloat($(this).attr("anterior")));
		$(this).parent().parent().css("background-color","#ace8ac");
		setTimeout(function(){
			
		},500);
	});


}


 function guardarStock(){
 	var mod =  new Array();
 	var cambios = true;

 	
 	$(".input-stock").each(function(n){
 		var temporal =  new Array();
 		var id = parseInt($(this).attr("id"));
 		var anterior = parseFloat($(this).attr("anterior"));
 		var nuevo = parseFloat($(this).val());
 		



 		if($(this).val()==""){
 			
 			cambios = false;
 		} else {
 			temporal = [id,anterior,nuevo];

 			mod.push(temporal);
 			
 			
 		}
 	});
 	if(cambios){
    $("#btn-guardar-pesaje").attr("disabled","disabled");
    $("#btn-guardar-pesaje").html('<img src="./img/cargandomini.gif" style="width:30px;">');
 		$.post("js/ajax/caja.php", {funcion:"aStock", geneticas:mod, idCaja}, function(result){
 			if(result==""){
 				mostrarMensaje("El Stock se ha actualizado correctamente","success");
 				setTimeout(function(){
 				    location.reload();
 				},3000);
 			
 			} else {
 				mostrarMensaje("Ha ocurrido un error","error");
 				console.log(result);
 	
 			}
 		});
 	} else {
 		mostrarMensaje("Es necesario rellenar todos los campos.","error");
 	}
 	
 	
 }



 function hayPesaje(){

 	$.post("js/ajax/caja.php", {funcion:"ultimoPesaje", idCaja:idCaja}, function(result){
 		if(result=="false"){


 			$("#btn-guardar-pesaje").on("click", guardarStock);

 		} else {
 			console.log(result);
 			$("#btn-guardar-pesaje").attr("disabled","true");
 			if(result=="true2") {
 				$("#btn-guardar-pesaje").html("Ya has corregido el stock de la caja");
 			} else {
 				$("#btn-guardar-pesaje").html("La caja está cerrada");
 			}
 		}
 	});
 }