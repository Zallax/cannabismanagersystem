cargarUltimaCompra();
let idCaja = $(".btn-tablet").attr("id-caja");
hayPesaje();
setInterval(cargarUltimaCompraRepe, 3000);
alertas();
function cargarUltimaCompra(){
	
	$.post("php/ajax/vistaUltimaCompra.php", function(result){
		$(".zona-lista-tablet").hide().html(result).fadeIn("500");
		//$(".zona-lista-tablet").html(result);
	});
}

function cargarUltimaCompraRepe(){
	
	$.post("php/ajax/vistaUltimaCompra.php", function(result){
		
		$(".zona-lista-tablet").html(result);
	});
}

function alertas(){
	comprobarCajas();
}

if($(".btn-tablet").attr("abierta")=="no"){
  $("#dropdownMenuButton").prop("disabled","true");
  $("#accionesRap a").attr("href","#");
  $("#accionesRap").css("opacity","0.5");

}

function comprobarCajas(){
	$.post("php/funciones.php",{funcion:"comprobarCajasAbiertas"} ,function(result){
		if(result!=""){
			let cajas = JSON.parse(result);
			if(cajas.length!=0){
				let alert = '<a href="cajas-sin-cerrar.php"><div class="alert alert-danger text-center" role="alert"><h5 class="display-5">¡ATENCIÓN! Hay cajas sin cerrar ('+cajas.length+').</h5></div></a>'
				$(".zona-alertas").hide().append(alert).fadeIn("500");

			}
		}
	});
}

function hayPesaje(){

	$.post("js/ajax/caja.php", {funcion:"ultimoPesaje", idCaja:idCaja}, function(result){

		if(result=="false"){


			let alert = '<a href="pesaje.php"><div class="alert alert-danger" role="alert"><h3>Atención</h3></br> Aun no se ha realizado el pesaje de esta caja.</div></a>'
			$(".zona-alertas").hide().append(alert).fadeIn("500");

		} 
	});
}