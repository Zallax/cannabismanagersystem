$("#botonGuardarComentario").hide();

comprobarComentarios();

 /*IMAGENES
    */



$(document).ready(function() {

    $(".content-wrapper").fadeIn("500");

    $("#formAltaSocio").on("submit", function(e){
      e.preventDefault();
      cargarDatosModalConfirmar();
      $("#modalConfirmar").modal("show");

    })

    $("#btnRegistrarSocio").on("click", function(){
      guardarSocio();
    })
    $("#btnSubirFotoPersonal").on("click", subirFotoPersonal);
    $("#btnSubirDni1").on("click", function(){
      subirDni(1);
    });

    $("#btnSubirDni2").on("click", function(){
      subirDni(2);
    });

    $("#comentario").click(editarComentarios);
    $("#botonGuardarComentario").on("click", guardarComentarios);
    $("#inputNombre").on("focusout", function(){
      actualizarDatos("nombre", this);
    });

     $("#inputApellidos").on("focusout", function(){
      actualizarDatos("apellidos", this);
    });

    $("#inputNSocio").on("focusout", function(){
      actualizarDatos("nsocio", this);
    });

    $("#inputAvalador").on("focusout", function(){
      actualizarDatos("avalador", this);
    });

    $("#inputFechaSocio").on("focusout", function(){
      actualizarDatos("alta", this);
    });
    $("#dni").on("focusout", function(){
      actualizarDatos("dni", this);
    });
    $("#inputTel").on("focusout", function(){
      actualizarDatos("telefono", this);
    });
    $("#inputDir").on("focusout", function(){
      actualizarDatos("direccion", this);
    });
    $("#inputCon").on("focusout", function(){
      actualizarDatos("consumo", this);
    });
    $("#inputCorreo").on("focusout", function(){
      actualizarDatos("correo", this);
    });

      $('#check-descuento').click(function() {
        if ($(this).is(':checked')) {
          activarDescuento(1);
        } else {
          activarDescuento(0);
        }
      });

    $("#btnActivarSocio").on("click", activarSocio);
    $("#btn-eliminar-socio").on("click", modalEliminarSocio);
    $("#btn-renovar-manual").on("click", function(){

      renovacionManual($("#fechaRenov").val(), $("#mesesRenov").val(), $("#cuotaRenov").val());
    });


    $("#btnEliminarSocio").on("click", eliminarSocio);



    if($(".progress-bar").html()=="100%"){
        $(".progress-bar").addClass("bg-danger");
        $("#btn-ren-1").on("click", function(){
          renovacion(6);

        });

        $("#btn-ren-2").on("click", function(){
        renovacion(12);

      });
        $("#btn-ren-3").on("click", function(){
        $("#modalRenovaciones").modal("show");

      });

        $("#btn-ren-4").addClass("disabled");
        $("#btn-ren-4").unbind();



    } else if($(".progress-bar").html()=="Sin cuota%"){
      $(".progress-bar").html("Sin cuota");

      $(".progress-bar").css("width","100%");
      $("#btn-ren-1").addClass("disabled");
      $("#btn-ren-1").unbind();

      $("#btn-ren-2").addClass("disabled");
      $("#btn-ren-2").unbind();

    $("#btn-ren-3").on("click", function(){
    $("#modalRenovaciones").modal("show");

  });

      $("#btn-ren-4").addClass("disabled");
      $("#btn-ren-4").unbind();



    } else {

      $("#btn-ren-1").addClass("disabled");
      $("#btn-ren-1").unbind();

      $("#btn-ren-2").addClass("disabled");
      $("#btn-ren-2").unbind();

      $("#btn-ren-3").addClass("disabled");
      $("#btn-ren-3").unbind();

      $("#btn-ren-4").on("click", function(){
        var idSocio = $("#idSocio").val();
        anularRenovacion(idSocio);

       });


    }
    moment.locale('es');
    $.fn.dataTable.moment( 'L', 'es');


      $('#tabla-socios').DataTable( {
                 "language": espanol,
                 "autoWidth": false,
                 scrollY:        500,
                 scrollCollapse: true,
                 paging:         true,



            }

             );



} );

function guardarSocio(){

  var nsocio = $("#nsocio").val();
  var nombre = $("#nombre").val();
  var apellidos = $("#apellidos").val();
  var dni = $("#c-dni").val();
  var direccion = $("#direccion").val();
  var telefono = $("#telefono").val();
  var savalador = $("#savalador").val();
  var fAlta = $("#fAlta").val();
  var fPago = $("#fPago").val();
  var cuota = $("#rcuota").val();
  var requisitos = new Array();
  var req_docu = new Array();
  var correo = $("#correo").val();
  var consumo = $("#consumo").val();
  let cantidad = $("#rcant").val();

  if($("#check1").prop('checked')){
    requisitos[0] = 'req1:'+true;
  } else {
    requisitos[0] = 'req1:'+false;
  }
  if($("#check2").prop('checked')){
    requisitos[1] = 'req2:'+true;
  } else {
    requisitos[1] = 'req2:'+false;
  }
  if($("#check3").prop('checked')){
    requisitos[2] = 'req3:'+true;

  } else {
    requisitos[2] = 'req3:'+false;
  }
  if($("#check4").prop('checked')){
    requisitos[3] = 'req4:'+true;

  } else {
    requisitos[3] = 'req4:'+false;

   }


   //requisitos documentacion
   if($("#check5").prop('checked')){
     req_docu[0] = 'req1:'+true;
   } else {
     req_docu[0] = 'req1:'+false;
   }
   if($("#check6").prop('checked')){
     req_docu[1] = 'req2:'+true;
   } else {
     req_docu[1] = 'req2:'+false;
   }

   jsonRequisitos = JSON.stringify(requisitos);
   jsonReqDocu = JSON.stringify(req_docu);






    $.post("js/ajax/darAltaSocio.php", {nsocio:nsocio, nombre:nombre, apellidos:apellidos, dni:dni, savalador:savalador, cuota:cuota, cant:cantidad, fAlta:fAlta, fPago:fPago, telefono:telefono, direccion:direccion, correo:correo, requisitos:jsonRequisitos, consumo:consumo, req_docu:jsonReqDocu}, function(result){
              console.log(result);
              if(result==""){
                $("#modalConfirmar").modal("hide");
                $("html, body").animate({ scrollTop: 0 }, 600);
                $("#nombreCSocio").append(nombre+" "+apellidos+" ");

                $("#zonaAlta").fadeOut(500, "swing", function(){

                  $("#zonaAltaImagenes").fadeIn(1000, "swing");

                }).delay(500);

              }


          });




}

function activarDescuento(d){
  var idSocio = $("#idSocio").val();



    $.post("js/ajax/g_dato_socio.php", {
    d: "descuento",
    id: idSocio,
    valor: d
    }, function(result){

    if(d==1){
      mostrarMensaje('Se ha activado el descuento para este socio', "success");
    } else {
      mostrarMensaje('Se ha desactivado el descuento para este socio', "error");
    }

  });
}


function actualizarDatos(tipo, campo){
  /*if(tipo=="nsocio"){
   var textoInput=$("#inputNSocio").val();
  }
  if(tipo=="alta"){
   var textoInput=$("#inputFechaSocio").val();
  }
  if(tipo=="avalador"){
    var textoInput=$("#inputAvalador").val();
   }
   if(tipo=="dni"){
    var textoInput=$("#inputDni").val();
   }
   */
  var textoInput =  $(campo).val();


  if(validar(tipo,textoInput)){
      var idSocio = $("#idSocio").val();



        $.post("js/ajax/g_dato_socio.php", {
        d: tipo,
        id: idSocio,
        valor: textoInput
        }, function(result){

        mostrarMensaje('Se han guardado los cambios');

      });



  }else {
     mostrarMensaje('<i class="fa fa-times-circle" ></i> Ha ocurrido un error al guardar los cambios');

  }
}

function mostrarMensaje(mensaje, tipo){
    if(tipo=="success"){
      $("#mensajes").css("background-color","rgba(205, 255, 152, 0.89)");
    }

    if(tipo=="error"){
      $("#mensajes").css("background-color","rgba(255, 152, 152, 0.89)");
    }
     $("#mensajes").empty();

     $("#mensajes").append(mensaje);
     $("#mensajes").fadeIn().delay(3000).fadeOut(500);

}
function validar(tipo, texto){
  var bValido = true;
  if(tipo=="nsocio"){
    var exp = /[0-9]/;
    if(!exp.test(texto)){
      bValido=false;
    }
  }
  if(tipo=="avalador"){

  }

  return bValido;

}

function comprobarComentarios(){
  var comentario = $("#comentario").text();
  if (comentario!="Sin comentarios"){
    $("#inputComentarioPerfil").val($("#comentario").text());
  }
}

function editarComentarios(){
  $("#botonGuardarComentario").removeClass("oculto");
 $(".zonaComentario").addClass("oculto");
 $("#zonaInputComentario").removeClass("oculto");
 $("#botonGuardarComentario").show(200);

}

function guardarComentarios(){
   var textoInput=$("#inputComentarioPerfil").val();
   var idSocio = $("#idSocio").val();
   if(textoInput!=""){


     $.post("js/ajax/g_coment_perfil.php", {
      id: idSocio,
      comentario: textoInput
     }, function(){
      $("#comentario").text(textoInput);
      $(".zonaComentario").removeClass("oculto");
      $("#zonaInputComentario").addClass("oculto");
      $("#botonGuardarComentario").hide("fast");

    });

   } else {
     $.post("js/ajax/g_coment_perfil.php", {
      id: idSocio,
      comentario: textoInput
     }, function(){
    $("#comentario").text("Sin comentarios");
    $(".zonaComentario").removeClass("oculto");
    $("#zonaInputComentario").addClass("oculto");
    $("#botonGuardarComentario").hide("fast");

    });



   }
   mostrarMensaje('<i class="fa fa-check-circle""></i> Se han guardado los cambios', "success");


}

function myFunction() {
    // Declare variables
    var input, filter, table, tr, td, i;
    input = document.getElementById("inputBuscarSocio");
    filter = input.value.toUpperCase();
    table = document.getElementById("tablaSocios");
    tr = table.getElementsByTagName("tr");

    // Loop through all table rows, and hide those who don't match the search query
    for (i = 0; i < tr.length; i++) {
      td = tr[i].getElementsByTagName("td")[0];
      if (td) {
        if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
          tr[i].style.display = "";
        } else {
          tr[i].style.display = "none";
        }
      }

    }
  }


$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})


function subirFotoPersonal(){

  if($(".fotoSocio .kv-file-content").length>0){
      var imagen = $(".fotoSocio>.file-input>.file-preview>.file-drop-disabled>.file-preview-thumbnails>.file-preview-frame>.kv-file-content>.file-preview-image").attr("src");

      var dni = $("#dni").val();
      $.post("js/ajax/cargarFotoSocio.php",{imagen:imagen, dni:dni}, function(result){

        if (result=="success"){
          mostrarMensaje("La foto ha sido actualizada correctamente", "success");


        } else {
          mostrarMensaje('<i class="fa fa-times-circle" ></i> Ha ocurrido un error al guardar la imagen',"error");
        }



    });


  } else {
    mostrarMensaje('<i class="fa fa-times-circle" ></i> No has seleccionado ninguna imagen',"error");

  }
}

function subirDni(zona){

  if(zona==1){
    var selector = '.zona-frontal-dni';
  } else {
    var selector = '.zona-trasera-dni';
  }

  if($(selector+" .kv-file-content").length>0){
      var imagen = $(selector+" .file-preview-image").attr("src");


      var dni = $("#dni").val();
      $.post("js/ajax/cargarFotoDni.php",{tipo:zona, imagen:imagen, dni:dni}, function(result){

        if (result=="success"){
          mostrarMensaje("La foto ha sido actualizada correctamente");
          location.reload(true);

        } else {

          mostrarMensaje('<i class="fa fa-times-circle" ></i> Ha ocurrido un error al guardar la imagen');
        }



    });


  } else {
    mostrarMensaje('<i class="fa fa-times-circle" ></i> No has seleccionado ninguna imagen');

  }
}


function afterModalTransition(e) {
    e.setAttribute("style", "display: none !important;");
  }


  $('.fade2').on('hide.bs.modal', function (e) {
    setTimeout( () => afterModalTransition(this), 200);
})




function cargarModalActivacion(){

  var boton=$(this);
  var id = boton.attr("data-id");

  $.post("php/ajax/formActivarSocio.php",{id:id}, function(result){
    $("#contenidoActivarSocio").html(result);

    $('[data-toggle="popover"]').popover({
      html: true,
      trigger: 'focus'
    });
  });
}


function cargarActivaciones(){
  $.post("php/funciones.php",{funcion:"activaciones"},function(result){
    $("#zona-activaciones").html(result);
    $(".btn-activar-socio").on("click", cargarModalActivacion);
  });
}

function activarSocio(){

    var nsocio = $("#n-socio").val();
    let datos = $("#selectCuotas").val();
    let aDatos = datos.split(":");

    var meses = aDatos[0]
    var cant = aDatos[1]

    var validado = true;
    if(nsocio==""){
        $("#n-socio").addClass("is-invalid").focus();
        validado = false;
    }
    if(meses==null){
      $("#selectCuotas").addClass("is-invalid").focus();
      validado = false;
    }

    if($("#mesesRenov").val()!=""){
      meses = $("#mesesRenov").val();
      cant = $("#cantRenov").val();
      if($("#cantRenov").val()==""){
        $("#cantRenov").addClass("is-invalid").focus();
        validado = false;
      }
    } else {
      if($("#selectCuotas").val()=="manual"){
        $("#mesesRenov").addClass("is-invalid").focus();
        validado = false;
      }

    }

    if(validado){
      var id = $("#idSocio").val();


      $.post("js/ajax/socios.php",{funcion:"activar",id:id, n:nsocio, meses:meses, cant:cant},function(result){
        if(result=="success"){
          mostrarMensaje("El socio ha sido activado correctamente");
          cargarActivaciones();

            $("#modalActivarSocio").modal("hide");
          } else {
            mostrarMensaje("Ha ocurrido un error al activar al socio");
            console.log(result);
          }


      });

    }


}


function cargarTablaRetiradas(){
  var idSocio = $("#idSocio").val();
  var hayCannabis = false;
  var hayProductos = false;
  $.post("php/tablas.php",{id:idSocio}, function(result){

      if(result!=""){
        var retiradas = JSON.parse(result);
        $("#ultimas-retiradas").html("");
        $("#ultimas-retiradas").append('<div id="accordion"></div>');


        for (i=0;retiradas.length>i;i++){

          hayCannabis = false;
          hayProductos = false;
          var articulos = JSON.parse(retiradas[i]["articulos"]);
          var fechaYHora = retiradas[i]["fecha"].split(" ");
          var arrayFecha = fechaYHora[0].split('-');
          var fecha = arrayFecha[2]+"/"+arrayFecha[1]+"/"+arrayFecha[0];
          let total = 0;
          $("#ultimas-retiradas #accordion").append(

     '<div class="card retiradas-perfil '+retiradas[i]["id"]+'">'+
       '<div class="card-header bg-azul text-white" data-toggle="collapse" data-target="#'+retiradas[i]["id"]+'" aria-expanded="true" aria-controls="collapseOne">'+
         '<div class="row">'+
             '<div class="col-lg-3 col-md-3 col-4 text-left">'+
               '<span class="badge badge-dark " style="font-size:1.5em;margin-top:4px;">'+
                fecha+'</span>'+
             '</div>'+
             '<div class="col-lg-4 col-md-3 col-5 mostrador pl-4">'+

                 '<b>Dispensado por</b><br>'+
                retiradas[i]["nUsuario"]+' '+retiradas[i]["aUsuario"]+

             '</div>'+
             '<div class="col-lg-2 col-3 text-left" id="precioTotal">'+

               +retiradas[i]["total"]+'€'+

           '</div>'+
             '<div class="col-lg-2 col-6" id="idRetirada">'+
               '#'+retiradas[i]["id"]+
             '</div>'+

           '<div class="col-lg-1 col-6 text-right"><a class="btn btn-info btn-sm mt-1" href="modificar-transaccion.php?id='+retiradas[i]["id"]+'&m" data-toggle="tooltip" data-placement="bottom" title="EDITAR"><i class="fas fa-pen-square"></i></a></div>'+

         '</div>'+


        '</div>'+
       '<div id="'+retiradas[i]["id"]+'" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">'+
       '<div class="card card-cannabis" style="display:none">'+
       '<div class="card-header bg-secondary text-white cat-retirado">CANNABIS</div>'+
       '<ul class="lista-cannabis list-group list-group-flush">'+
      '</ul>'+
     '</div>'+
     '<div class="card card-productos" style="display:none;">'+
     '<div class="card-header bg-secondary text-white cat-retirado">PRODUCTOS</div>'+
     '<ul class="lista-productos list-group list-group-flush">'+
    '</ul>'+
   '</div>'+
       '</div>'+
       '</div>');


        for (k=0;k<articulos.length;k++){
          // alert("id: "+i+" "+articulos[i]);

          var idTransaccion =  retiradas[i]["id"];
          if(articulos[k][0]==1){

            mostrarArticulo(idTransaccion, articulos[k][1] ,"geneticas", articulos[k][2]);
            total=parseFloat(total)+parseFloat(articulos[k][2]);


          }

          if(articulos[k][0]==2){

            mostrarArticulo(idTransaccion, articulos[k][1] ,"articulos", articulos[k][2]);



          }
          //var aArticulo = JSON.parse(articulos[k]);
         // alert(aArticulo);
           // var aArticulo = articulos[k].split(",");
         //   alert(aArticulo[1]);
          }

          if(total>5){
            $("."+retiradas[i]["id"]+" .card-header").removeClass("bg-azul").addClass("bg-warning");
          }

        }

      }


  });
}

function mostrarArticulo(idTrans,id, tipo, cantidad){

    if(tipo=="geneticas"){
      $("#"+idTrans+" .card-cannabis").show();


      $("#"+idTrans+" .lista-cannabis").append('<li class="list-group-item">'+id+' ('+cantidad+' Gr)</li>');
    }

    if(tipo == "articulos"){

      $("#"+idTrans+" .card-productos").show();

      $("#"+idTrans+" .lista-productos").append('<li class="list-group-item">'+id+' ('+cantidad+' Ud)</li>');
    }


}

function renovacion(meses){
  $(".container").css("opacity","0.2");
  $(".animacion-carga").fadeIn("500");
  var idSocio = $("#idSocio").val();
  $.post("js/ajax/socios.php",{funcion:"renovar", id:idSocio, meses:meses},function(result){

    if(result=="success"){
      $(".content-wrapper").fadeOut("500", function(){
        location.reload(true);
      })
    } else {
      console.log(result);
    }
  })
}

function renovacionManual(fecha, meses, cuota){
  $("#modalRenovaciones").modal("hide");
  $(".container").css("opacity","0.2");
  $(".animacion-carga").fadeIn("500");
  var idSocio = $("#idSocio").val();
  $.post("js/ajax/socios.php",{funcion:"renovacionManual", id:idSocio, fecha:fecha, meses:meses, cuota:cuota},function(result){

    if(result=="success"){
      $(".content-wrapper").fadeOut("500", function(){
        location.reload(true);
      })
    } else {
      console.log(result);
    }
  })
}


function cargarDatosModalConfirmar(){
  let aDatos = "";
  $("#rnsocio").val($("#nsocio").val());
  $("#rnombre").val($("#nombre").val());
  $("#rapellidos").val($("#apellidos").val());
  $("#rc-dni").val($("#c-dni").val());
  $("#rdireccion").val($("#direccion").val());
  $("#rtelefono").val($("#telefono").val());
  $("#ravalador").val($("#savalador").val());
  $("#rfAlta").val($("#fAlta").val());
  $("#rfPago").val($("#fPago").val());
  aDatos = $("#cuota").val().split(":");
  $("#rcuota").val(aDatos[0]);
  $("#rcant").val(aDatos[1]);
  if($("#cuota").val()=="manual"){
     $("#rcuota").val($("#meses").val());
     $("#rcant").val($("#cant").val());
  } 

 
  

  $("#rcorreo").val($("#correo").val());
  $("#rconsumo").val($("#consumo").val());


  //CHEKS
  $("#rcheck1").attr('checked', $("#check1").prop("checked"));
  $("#rcheck2").attr('checked', $("#check2").prop("checked"));
  $("#rcheck3").attr('checked', $("#check3").prop("checked"));
  $("#rcheck4").attr('checked', $("#check4").prop("checked"));
  $("#rcheck5").attr('checked', $("#check5").prop("checked"));
  $("#rcheck6").attr('checked', $("#check6").prop("checked"));
}

function anularRenovacion(idSocio){

  $(".container").css("opacity","0.2");
  $(".animacion-carga").fadeIn("500");
  var idSocio = $("#idSocio").val();
  $.post("js/ajax/socios.php",{funcion:"anularRenovacion", id:idSocio},function(result){

    if(result=="success"){
      $(".content-wrapper").fadeOut("500", function(){
        location.reload(true);
      })
    } else {
      console.log(result);
    }
  })
}

function modalEliminarSocio(){
  $("#modalEliminarSocio").modal("show");
  $("#btn-c-e-socio").unbind().on("click", eliminarSocio);
}

function eliminarSocio(){
  var idSocio = $("#idSocio").val();
  var nuevoAval = $("#avaladores").val();
  $.post("js/ajax/socios.php", {funcion:"eliminarSocio", idSocio:idSocio, nuevoAval:nuevoAval}, function(result){
    if(result==""){
      $("#modalEliminarSocio").modal("hide");
      localStorage.setItem("mensaje", "El socio ha sido eliminado correctamente.")

        location.href ="socios.php";

    } else {
      mostrarMensaje("Ha ocurrido un error.", "success");
      console.log(result);
    }
  });
}


//TRADUCCIÓN DATATABLE
 var espanol={
                     "lengthMenu": "Mostrar _MENU_ registros por página",
                     "zeroRecords": "No se han encontrado datos",
                     "info": "Mostrando página _PAGE_ de _PAGES_",
                     "infoEmpty": "No se han encontrado registros.",
                     "infoFiltered": "(filtered from _MAX_ total records)",
                     "paginate": {
                            "first":      "Primero",
                            "last":       "Último",
                            "next":       ">",
                            "previous":   "<"
                        },
                     "search":         "Buscar:",
                };
