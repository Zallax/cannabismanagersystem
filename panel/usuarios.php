<?php
  require_once("php/config.php");
  require_once ("php/funciones.php");
  include "php/cabecera.php";
?>

 <?php cabecera("usuarios");
  $usuarios = consulta("select id, nombre, apellidos, tipo from usuarios;");

  if($_SESSION["tipo"]==1){
      





 ?>

  <div class="content-wrapper animated fadeIn">
    <div class="container">
        <div class="card border-morado mb-3">
          <div class="card-body">
          <div class="icono-fondo">
                      <i class="fas fa-user"></i>
          </div>
          <div class="titulo-seccion"><span>Usuarios</span></div>

        </div>
      </div>
        <?php
          if(ISSET($_GET["m"])){
            if($_GET["m"]==1){
              echo '<div class="alert alert-success alert-dismissible fade show" role="alert">
                      El usuario ha sido eliminado correctamente.
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
                    </div>';
            }
          }
        ?>
      <div class="row ">
        <div class="col-sm-3">

          <div class="btn-group-vertical btn-block" role="group" aria-label="Vertical button group">
              <a class="btn btn-outline-dark" href="nuevo-usuario.php"><i class="fas fa-user-plus"></i> Añadir usuario</a>
              <a class="btn btn-outline-dark" href="#"><i class="fas fa-clipboard-list"></i> Lista de accesos</a>

            </div>
        </div>
        <div class="col-sm-9">

          <div class="card p-2">
            <table class=" table table-hover table-striped table-responsive table-light">
              <thead class="bg-white">
                <tr>
                  <th width="20%">
                    Nombre
                  </th>
                  <th width="55%">
                    Apellidos
                  </th>
                  <th >
                    Rango
                  </th>
                  <th width="20%" class="text-center">
                    Acción
                  </th>
              </tr>
              </thead>
              <tbody>
                <?php
                  foreach ($usuarios as $indice=>$valor){
                    if($valor["tipo"]==1){
                      $rango = '<span class="badge badge-primary">Administrador</span>';
                    }
                    if($valor["tipo"]==2){
                      $rango = '<span class="badge badge-secondary">Gestor</span>';
                    }
                    if($valor["tipo"]==3){
                      $rango = '<span class="badge badge-info">Contable</span>';
                    }

                    echo '<tr>
                            <td>'.$valor["nombre"].'</td>
                            <td>'.$valor["apellidos"].'</td>
                            <td>'.$rango.'</td>
                           <td class="text-center"><a href="perfil-usuario.php?id='.$valor["id"].'" class="btn btn-muted-success" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Editar"><i class="fas fa-user-edit"></i></a></td>
                           </tr>';
                  }

                ?>
              </tbody>
            </table>
        </div>
    </div>
      </div>

      <?php
        }else {
            errorDePermisos();
        }

      ?>





        </div>
    <?php include "php/footer.php";?>
    <script src="js/socios.js"></script>

  </div>
</body>

</html>
