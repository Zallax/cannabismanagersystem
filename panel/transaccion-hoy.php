<?php
  require_once("php/config.php");
  require_once ("php/funciones.php");
  include "php/cabecera.php";
  extract($_GET);
  $fecha = consulta("select CURRENT_DATE as fecha");
        $fechaActual = $fecha[0]["fecha"];
  $caja=false;
  if($_SESSION["idCaja"]!=""){
    $consulta = consulta("select r.id as id, r.idSocio as idSocio, r.articulos as articulos, r.fecha as fecha, r.precio_total as precio_total, u.nombre as nUsuario, u.apellidos as aUsuario, r.idMod, r.manual, s.nombre as nSocio, s.apellidos as aSocio, s.nsocio as numSocio from retiradas r inner join usuarios u on r.idUsuario = u.id inner join socios s where idCaja = ".$_SESSION["idCaja"]." and r.idSocio = s.id order by id desc;");
    $caja=true;
  } 
  
 


?>

 <?php cabecera("historial-socios");?>
  <div class="content-wrapper animated fadeIn">
    <div class="container">
      <?php


      ?>
      <div class="card pb-2" role="alert">
        <div class="card-body">
        <div class="icono-fondo">
                    <i class="fas fa-fw fa-shopping-cart"></i>
        </div>
        <div class="titulo-seccion"><span class=>Transacciones de la caja</span></div>
</div>
      </div>



         <?php

          if(count($consulta)!=0 && $caja){
            foreach($consulta as $indice => $valor){
              $articulos = json_decode($valor["articulos"]);



              $fechaHora = explode(" ", $valor["fecha"]);
              $fecha = explode("-", $fechaHora[0]);
              $hora = explode(":", $fechaHora[1]);

              $total = 0;
              foreach($articulos as $indice2 => $producto){
                if($producto[0]=="1"){
                  $total+=$producto[2];
                }
              }
              if($total>5){
                $clase = 'bg-warning text-white';
                $alerta = '<i class="mr-2 mt-2 fas fa-exclamation-triangle" data-toggle="tooltip" data-placement="bottom" title="Dispensados más de 5 gramos."></i>';
              } else {
                $clase = 'bg-white text-grey';
                $alerta = '';
              }

              echo
           '
           <div class="tab-content mb-2 p-3">
          
            <div id="accordion">
            <div class="card retiradas-perfil">'.
             '<div class="card-header '.$clase.'"data-toggle="collapse" data-target="#'.$valor["id"].'" aria-expanded="true" aria-controls="collapseOne">'.
               '<div class="row no-gutters">'.
                   '<div class="col-lg-2 col-md-3 col-4 text-center">'.
                     '
                     <span class="badge badge-dark " style="font-size:1.5em;margin-top:4px;">'.
                      $fecha[2]."/".$fecha[1]."/".$fecha[0].'</span>'.
                   '</div>'.
                   '<div class="col-lg-2 col-md-3 col-8 mostrador pl-4">'.
                      '<b>Dispensado por</b><br>'.
                      $valor["nUsuario"].' '.$valor["aUsuario"].
                   '</div>';
                   echo '<div class="col-lg-2 col-md-3 col-6 mostrador pl-4">';
                  if($valor["idMod"]!=0){
                    $modificador = consulta("select nombre, apellidos from usuarios where id = ".$valor["idMod"].";");
                    if(count($modificador)!=0){
                      $nombre = $modificador[0]["nombre"]." ".$modificador[0]["apellidos"];
                    } else {
                      $nombre = "Usuario eliminado";
                    }

                      echo '<b>Modificado por</b><br>'.
                      $nombre;

                  }
                  echo '</div>';
                  echo '<div class="col-lg-1 col-md-3 col-6">';
                  if($valor["manual"]!=0){

                     echo '<span class="badge badge-warning">Manual</span>';


                 }
                 echo '</div>';

                   echo '<div class="col-lg-1 col-4 col-md-3 text-center" id="precioTotal">'.

                     $valor["precio_total"].'€'.

                 '</div>'.
                   '<div class="col-lg-2 col-4 col-md-3 text-center" id="idRetirada">'.
                     '#'.$valor["id"].
                   '</div>'.
                   '<div class="col-lg-1 col-md-3 col-4 text-right">'.$alerta.'<a class="btn btn-info btn-sm mt-1" href="modificar-transaccion.php?id='.$valor["id"].'&m" data-toggle="tooltip" data-placement="bottom" title="EDITAR"><i class="fas fa-pen-square"></i></a></div>'.
               '</div>'.


              '</div>'.
             '<div id="'.$valor["id"].'" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">'.
             '<div class="card card-cannabis">'.
             '<div class="card-header bg-secondary text-white cat-retirado">CANNABIS</div>'.
             '<ul class="lista-cannabis list-group list-group-flush">';
              $cannabis = false;
              foreach($articulos as $indice2 => $producto){
                if($producto[0]=="1"){
                  $cannabis = true;
                  echo '<li class="list-group-item">'.$producto[1].' ('.$producto[2].' Gr)</li>';
                }
              }
              if(!$cannabis){
                echo '<li class="list-group-item">Sin datos</li>';
              }



            echo '</ul>'.
           '</div>'.
           '<div class="card card-productos">'.
           '<div class="card-header bg-secondary text-white cat-retirado">PRODUCTOS</div>'.
           '<ul class="lista-productos list-group list-group-flush">';
           $productos = false;
           foreach($articulos as $indice2 => $producto){
             if($producto[0]=="2"){
               echo '<li class="list-group-item">'.$producto[1].' ('.$producto[2].' Ud)</li>';
               $productos = true;
             }
           }
           if(!$productos){
             echo '<li class="list-group-item">Sin datos</li>';
           }


           echo
          '</ul>'.
         '</div>'.
             '</div>'.
             ' <div class="card-footer">
              <a class="text-primary" href="perfil.php?s='.$valor["idSocio"].'"><h5>'.$valor["nSocio"].' '.$valor["aSocio"].' <span class="text-muted">#'.$valor["numSocio"].'</span></h5></a>
             </div>

             </div>

               </div>

      </div>';

            }
          } else {
            echo '<span class="text-muted">No hay datos disponibles</span>';
          }


         ?>








        </div>



       </div>
    <?php include "php/footer.php";?>
    <script src="js/correccion-stock.js"></script>



</div>
</body>

</html>
