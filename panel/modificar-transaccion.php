<?php
  require_once("php/config.php");
  require_once ("php/funciones.php");
  include "php/cabecera.php";

   extract($_GET);
  $consulta = consulta("select * from retiradas where id=$id");
  $datos = $consulta[0];
  $fechaYHora = explode(" ",$datos["fecha"]);
  $consulta2= consulta("select nombre, apellidos, nsocio, descuento from socios where id=".$datos["idSocio"]);

  if(!empty($consulta2)){
    $nombre = $consulta2[0]["nombre"]." ".$consulta2[0]["apellidos"];
    $nsocio = $consulta2[0]["nsocio"];
    $terapeutico = "";
    if($consulta2[0]["descuento"]==1){
      $terapeutico = '<h5 class="text-success descuento" descuento="'.$_SESSION["pdescuento"].'"><i class="fas fa-notes-medical"></i> Socio terapéutico</h5>';
    }
  } else {
    $nombre = "Socio eliminado";
    $nsocio = "?";
  }
?>

 <?php cabecera("inicio");?>

 <div class="modal fade" id="modalEliminarTransaccion" tabindex="-1" role="dialog" aria-labelledby="modalEliminarTransaccionLabel" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
     <div class="modal-content" >
       <div class="modal-header">
         <h5 class="modal-title" id="modalEliminarTransaccionLabel">Confirma la operación</h5>
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
           <span aria-hidden="true">&times;</span>
         </button>
       </div>
       <div class="modal-body">
         <div class="row">
           <div class="col-sm-12 col-md-3 col-lg-2 col-xl-2 text-danger text-center" style="font-size:4em;opacity:0.5">
           </div>
           <div class="col-sm-12 col-md-9 col-lg-10 col-xl-10">
             <h5 class="text-danger">Un momento...</h5>

             <div class="alert alert-danger" role="alert">
             Esta acción eliminará la transacción. Si la caja ha sido cerrada previamente, esta acción no quedará reflejada en dicha caja y se mantendrán las cuentas intactas.</br>
             <b>Estos datos no podrán recuperarse.</b> ¿Estás seguro de que deseas continuar?
             </div>
           </div>
         </div>
       </div>
       <div class="modal-footer">
         <button type="button" class="btn btn-secondary w-50 btn-lg" data-dismiss="modal"><i class="fas fa-times"></i></button>
         <button type="button" transaccion="<?php echo $datos["id"]; ?>" id="btn-c-e-trans" class="btn btn-danger w-50 btn-lg"><i class="fas fa-check"></i></button>
       </div>
     </div>
   </div>
 </div>
  <div class="content-wrapper animated fadeIn">
    <div class="container">
    <?php

   if(ISSET($_GET["id"])){

    

    $articulos = json_decode($datos["articulos"]);
    $cargando = "<div class='text-center'><img width='50px' height='50px' src='img/cargando.gif'></div>";
    
    $editarStock = "true";

    if($consulta[0]["idCaja"]!=$_SESSION["idCaja"]){
            echo '<div class="alert alert-danger" role="alert">
        ¡ATENCIÓN! Vas a modificar la transacción de una caja ya cerrada, estos cambios no modificará el total de la caja <b>ni afectará al stock</b>.
      </div>';

    $editarStock = "false";
    }


   ?>

      <input class="d-none" id="total" value="0">
      <input class="d-none" id="flagEditar" value="<?php echo $editarStock;?>">
      
      <div class="card mb-4">
      <div class="card-header bg-white">
        
          <h4 class="idTrans" id="<?php echo $datos["id"];?>">Modificando transacción #<?php echo $datos["id"];?></h4>

      </div> 
      <div class="card-body pt-1 pb-1">
       <h5> <i class="fas fa-user"></i> <?php echo $nombre; ?> <span class="text-muted">(#<?php echo $nsocio ?>)</span></h5>
       <?php echo $terapeutico; ?>
         <div class="input-group mb-3" style="width:290px;">
           <div class="input-group-prepend">
             <span class="input-group-text" id="basic-addon1"><i class="far fa-calendar-alt"></i></span>
           </div>
           <input type="date" class="form-control form-control-lg" placeholder="Fecha" id="fecha" value="<?php echo $fechaYHora[0]; ?>">
           <input style="display:none;" id="hora" value="<?php echo $fechaYHora[1];?>">
         </div>

          

        

      </div>
      <div class="card-footer text-muted text-left">
               <div class="row">
                 <div class="col-sm-6">
                   <div style="font-size:24px; font-weight:100;display:inline-block;color:black"><small class="text-muted">APORT.</small> <i class="fas fa-angle-right"> </i><span class="fadeIn animated" class="text-dark"> <span id="total-geneticas">0</span> gr.</span></div>  <div style="font-size:24px; font-weight:100; display:inline-block;margin-left:20px;color:black"><small class="text-muted">TOTAL</small> <i class="fas fa-angle-right"> </i> <span class="fadeIn animated" class="text-dark"><span id="total-precio">0</span> €</span></div>
                 </div>
                 <div class="col-sm-3">
                   
                 </div>
                 <div class="col-sm-3 text-right">
                     <button class="btn btn-outline-danger btn-block" href="alta.php" data-toggle="modal" data-target="#modalEliminarTransaccion"><i class="fa fa-trash"></i> Eliminar transacción
                   </button>
                 </div>
               </div>   
       </div>

 


  </div>

      <div class="tab-content">

      <div class="card lista-finalizar-cannabis">
        <div class="card-header">
          <h4 style="margin-bottom:0px; width:60%margin-bottom:0px;width: 60%;float:left;;float:left;">Cannabis</h4><div style="width:100px;float:right"><button class="btn btn-outline-dark btn-sm" id="btnACannabis" data-container="body" data-toggle="popover" data-placement="right" data-content="<?php echo $cargando; ?>'" data-html="true"><i class="fas fa-cart-plus"></i> Añadir</button></div>
        </div>

        <?php
          $listaCannabis = '<ul class="list-group list-group-flush" id="listaCannabis">';
          $listaProd = '<ul class="list-group list-group-flush" id="listaProd">';

          foreach ($articulos as $indice=>$valor){
            if($valor[0]==1){

                $listaCannabis.='<li class="list-group-item">
                <div class="row">

                  <div class="col-12 col-sm-6 col-md-5 divnombre" precio="'.$valor[3].'" tipo="'.$valor[4].'">
                    <h5>'.$valor[1].'</h5>
                  </div>

                  <div class="col-6 col-sm-6 col-md-3" id="divcantidad">
                   
                    <div class="btn-group btn-cantidad" style="border:1px solid #bababa;border-radius:5px;" role="group" aria-label="First group">
                                <button type="button" class="btn btn-secondary btn-menos">-</button>
                                 <input class="text-center input-cantidad" type="number" size="3" value="'.$valor[2].'"></input>
                                <button type="button" class="btn btn-secondary btn-mas">+</button>
                              </div>
                  </div>

                  <div class="col-6 col-sm-2 col-md-4 text-right">
                    <button class="btn btn-info mod-can" data-id="'.$indice.'" data-container="body" data-toggle="popover" data-placement="right" data-content="'.$cargando.'" data-html="true"><i class="fas fa-pencil-alt"></i>

                    </button>
                    <button class="btn btn-danger eli-can"><i class="fas fa-trash"></i>

                    </button>
                  </div>
                </div>
              </li>';
            }

            if($valor[0]==2){
              $listaProd.=' <li class="list-group-item">
              <div class="row">

                  <div class="col-12 col-sm-6 col-md-5 divnombrep" precio="'.$valor[3].'">
                    <h5>'.$valor[1].'</h5>
                  </div>

                  <div class="col-6 col-sm-6 col-md-3" id="divcantidadp">
                    <div class="btn-group btn-cantidad-p" style="border:1px solid #bababa;border-radius:5px;" role="group" aria-label="First group">
                                                    <button type="button" class="btn btn-secondary btn-menos">-</button>
                                                     <input class="text-center input-cantidad" type="number" size="3" value="'.$valor[2].'"></input>
                                                    <button type="button" class="btn btn-secondary btn-mas">+</button>
                                                  </div>
                                     
                  </div>


                <div class="col-6 col-sm-3 col-md-4 text-right">
                  <button class="btn btn-info mod-prod" data-container="body" data-content="hola" data-toggle="popover" data-placement="right" data-html="true"><i class="fas fa-pencil-alt"></i>

                  </button>
                  <button class="btn btn-danger eli-prod" data-id="'.$indice.'"><i class="fas fa-trash"></i>

                  </button>
                </div>
              </div>
            </li>';
            }
          }

          $listaCannabis.='</ul>';
          $listaProd.='</ul>';
          echo $listaCannabis;






        ?>




      </div>



   <div class="card lista-finalizar-productos">
        <div class="card-header">
          <h4 style="margin-bottom:0px; width:60%;float:left;">Productos</h4><div style="width:100px;float:right"><button class="btn btn-outline-dark btn-sm" id="btnAProducto" data-container="body" data-toggle="popover" data-placement="right" data-content="cargando" data-html="true"><i class="fas fa-cart-plus"></i> Añadir</button></div>
        </div>

       <?php
        echo $listaProd;

     ?>

      </div>
      <button class="btn btn-segundo btn-block btn-lg" id="btn-guardar"><i class="fas fa-save"></i> Guardar</button>
        </div>
        </div>
    <?php include "php/footer.php";?>
    <script type="text/javascript" src="js/socios.js"></script>


    <?php

   } else {
     echo "<p>No ha seleccionado ninguna transacción</p>";
   }

   ?>
   <script>
    var cargando="<div class='text-center'><img width='50px' height='50px' src='img/cargando.gif'></div>";
    $(document).ready(function(){

      $(".btn-cantidad .btn-mas").on("click", sumarCantidad);


      function sumarCantidad(){
        var boton = $(this);
        var input = boton.parent().find('.input-cantidad');
        let cant = parseFloat(input.val())+0.5;
        input.val(cant);
        actualizarPrecio();
        actualizarGramos();
        

      }

      actualizarPrecio();
      actualizarGramos();
      eventoPrecio();

      $(".btn-cantidad .btn-menos").on("click", restarCantidad);


      function restarCantidad(){
        var boton = $(this);
        var input = boton.parent().find('.input-cantidad');
        if((input.val()-0.5)>0){
          let cant = parseFloat(input.val())-0.5;
          input.val(cant);
          actualizarPrecio();
          actualizarGramos();
        }

        

      }



      //PRODUCTOS

        $(".btn-cantidad-p .btn-mas").on("click", sumarCantidadP);


        function sumarCantidadP(){
          var boton = $(this);
          var input = boton.parent().find('.input-cantidad');
          let cant = parseFloat(input.val())+1;
          input.val(cant);
           actualizarPrecio();
          

        }



        $(".btn-cantidad-p .btn-menos").on("click", restarCantidadP);


        function restarCantidadP(){
          var boton = $(this);
          var input = boton.parent().find('.input-cantidad');
          if((input.val()-1)>0){
            let cant = parseFloat(input.val())-1;
            input.val(cant);
          }
           actualizarPrecio();

          

        }


      //FUERA

      crearEventosEditarCannabis();
      crearEventosEditarProductos();

      /*
      $.post("php/ajax/vistaSeleccionCannabis.php", function(result){
          $(".mod-can").attr("data-content", result);

        });
      $('[data-toggle="popover"]').popover();

      */
      var anteriorPulsado;
      var ultimoPulsado;
      $('.mod-can').popover({
          trigger: 'click',
          template: '<div class="popover popover-body popover-a-can animated fadeIn">'+cargando+'</div>'
        });


      $('.mod-prod').popover({
        trigger: 'click',
        template: '<div class="popover popover-e-prod animated fadeIn">'+cargando+'</div>'
      });

      $('#btnACannabis').popover({
        trigger: 'click',
        template: '<div class="popover popover-a-can animated fadeIn">'+cargando+'</div>'
      });

      $('#btnAProducto').popover({
        trigger: 'click',
        template: '<div class="popover popover-a-can animated fadeIn">'+cargando+'</div>'
      });

     /* $(".mod-can").on("click", function(){


        /*$('[data-toggle="popover"]').popover("hide");
         actual.popover("toggle");


      });*/

      $('.mod-can').on("show.bs.popover",function(){
        $("#btnACannabis").popover("hide");
        $("#btnAProducto").popover("hide");
        $(".mod-prod").popover("hide");
        var actual = $(this);
        ultimoPulsado = actual;

        if(anteriorPulsado!=$(this)){
          $(".mod-can").popover('hide');
          anteriorPulsado=$(this);
        }



        $.post("php/ajax/vistaSeleccionCannabis.php", function(result){

            $('.popover-body').empty().append(result);
            $('.popover-body li').unbind().on("click", function(){
               var seleccionado = $(this).children("#nom-gen").html();
               var precio = $(this).children("#nom-gen").attr("precio");
               var tipo = $(this).children("#nom-gen").attr("tipo");

              ultimoPulsado.parent().parent().parent().css("background-color","#a0e6a0");
              ultimoPulsado.parent().parent().parent().addClass("animated pulse");
              ultimoPulsado.parent().siblings(".divnombre").find("h5").fadeOut("500", function(){
                ultimoPulsado.parent().siblings(".divnombre").attr("precio",precio);
                ultimoPulsado.parent().siblings(".divnombre").attr("tipo",tipo);
                ultimoPulsado.parent().siblings(".divnombre").find("h5").html(seleccionado);
                actualizarPrecio();
                $(this).fadeIn("500");

              });

              ultimoPulsado.popover("hide");

            });

            });
      });

       $('.mod-prod').on("show.bs.popover",function(){
        $(".mod-can").popover("hide");
        $("#btnACannabis").popover("hide");
        $("#btnAProducto").popover("hide");
        var actual = $(this);
        ultimoPulsado = actual;

        if(anteriorPulsado!=$(this)){
          $(".mod-prod").popover('hide');
          anteriorPulsado=$(this);
        }



        $.post("php/ajax/vistaSeleccionProductos.php", function(result){

            $('.popover-e-prod').empty().append(result);
            $('.popover-e-prod li').unbind().on("click", function(){
              var seleccionado = $(this).attr("nombre");
             var precio = $(this).attr("precio");
              ultimoPulsado.parent().parent().parent().css("background-color","#a0e6a0");

              ultimoPulsado.parent().parent().parent().addClass("animated pulse");
              ultimoPulsado.parent().siblings(".divnombrep").attr("precio",precio);
              ultimoPulsado.parent().siblings(".divnombrep").find("h5").fadeOut("500", function(){

                ultimoPulsado.parent().siblings(".divnombrep").find("h5").html(seleccionado);
                 actualizarPrecio();
                $(this).fadeIn("500");
              });

              ultimoPulsado.popover("hide");
            });

            });
      });


      $('#btnACannabis').on("show.bs.popover",function(){
        $(".mod-can").popover("hide");
        $(".mod-prod").popover("hide");
        $("#btnAProducto").popover("hide");

        $.post("php/ajax/vistaSeleccionCannabis.php", function(result){

            $('.popover-a-can').empty().append(result);
            $('.popover-a-can li').unbind().on("click", function(){
              var seleccionado = $(this).children("#nom-gen").html();
             var tipo = $(this).children("#nom-gen").attr("tipo");
              if($(".descuento").length==1 && (tipo=="v" || tipo=="m")){
                var cantDesc = parseFloat(7/100).toFixed(2);

                var precioTemp = $(this).children("#nom-gen").attr("precio");

                var descTemp = (precioTemp*cantDesc).toFixed(2);

                var precio = precioTemp - descTemp;
              } else {
                var precio = $(this).children("#nom-gen").attr("precio");
              }

             
              var tipo = $(this).children("#nom-gen").attr("tipo");
                var li = '<li style="display:none;background-color:rgb(160, 230, 160);" class="animated pulse list-group-item"><div class="row"><div class="col-12 col-sm-6 col-md-5 divnombre" tipo="'+tipo+'" precio="'+precio+'"><h5>'+seleccionado+'</h5></div><div class="col-6 col-sm-6 col-md-3" id="divcantidad"><div class="btn-group btn-cantidad" style="border:1px solid #bababa;border-radius:5px;" role="group" aria-label="First group"><button type="button" class="btn btn-secondary btn-menos">-</button><input class="text-center input-cantidad" type="number" size="3" value="1"></input><button type="button" class="btn btn-secondary btn-mas">+</button></div></div><div class="col-6 col-sm-3 col-md-4 text-right"><button class="btn btn-danger eli-can"><i class="fas fa-trash"></i></button></div></div></div></li>';
                if(($('#listaCannabis li').hasClass('sin-productos'))){
                  $("#listaCannabis").html(li);
                } else {
                  $("#listaCannabis").append(li);
                }
                actualizarPrecio();
                actualizarGramos();
                eventoPrecio();
                $(".btn-cantidad .btn-mas").unbind().on("click", sumarCantidad);
                $(".btn-cantidad .btn-menos").unbind().on("click", restarCantidad);
                $("#listaCannabis li:last-child").fadeIn();
                $("#listaCannabis li:last-child").css("background-color","white");

                $(this).fadeIn("500");
                $('#btnACannabis').popover("hide");
                crearEventosEditarCannabis();
              });


            });

            });

      $('#btnAProducto').on("show.bs.popover",function(){
        $(".mod-can").popover("hide");
        $(".mod-prod").popover("hide");
        $("#btnACannabis").popover("hide");

        $.post("php/ajax/vistaSeleccionProductos.php", function(result){

            $('.popover-a-can').empty().append(result);
            $('.popover-a-can li').unbind().on("click", function(){
              var seleccionado = $(this).attr("nombre");
              var precio = $(this).attr("precio");
                var li = '<li style="display:none;background-color:rgb(160, 230, 160);" class="animated pulse list-group-item"><div class="row"><div class="col-12 col-sm-6 col-md-5 divnombrep" precio="'+precio+'"><h5>'+seleccionado+'</h5></div><div class="col-6 col-sm-6 col-md-3" id="divcantidadp">  <div class="btn-group btn-cantidad-p" style="border:1px solid #bababa;border-radius:5px;" role="group" aria-label="First group"><button type="button" class="btn btn-secondary btn-menos">-</button><input class="text-center input-cantidad" type="number" size="3" value="1"></input><button type="button" class="btn btn-secondary btn-mas">+</button></div></div><div class="col-6 col-sm-3 col-md-4 text-right"><button class="btn btn-danger eli-prod"><i class="fas fa-trash"></i></button></div></div></div></li>';
                if(($('#listaProd li').hasClass('sin-productos'))){
                  $("#listaProd").html(li);
                } else {
                  $("#listaProd").append(li);
                }
                actualizarPrecio();
                $(".btn-cantidad-p .btn-mas").unbind().on("click", sumarCantidadP);
                $(".btn-cantidad-p .btn-menos").unbind().on("click", restarCantidadP);
                $("#listaProd li:last-child").fadeIn();
                setTimeout(function(){
                  $("#listaProd li:last-child").css("background-color","white");
                }, 1000);

                $(this).fadeIn("500");
                $('#btnAProducto').popover("hide");
                crearEventosEditarProductos();
              });


            });

            });


      var vacio = true;



      $("#btn-guardar").on("click", function(){
         $(this).attr("disabled",true);
         var precioTotal = parseFloat($("#total-precio").html());

         if(precioTotal!=0 || precioTotal!=0.00){
           var articulos = new Array();
           if(!($("#listaCannabis li").hasClass("sin-productos"))){
             $("#listaCannabis li").each(function(n){


               var zona = $(this).children();
              // var precio = parseFloat($(this).find(".divnombre").attr("precio"));
               //var cant = parseFloat($(this).find(".input-cantidad").val());
               //precioTotal = parseFloat(precioTotal)+parseFloat(precio*cant);


               var articulo = [1, zona.children().children().html()+"", zona.find('#divcantidad>.btn-cantidad>input').val()+"", parseFloat(zona.children().attr("precio")), zona.children().attr("tipo")];
               articulos.push(articulo);
              // articulos.push('{1,"'+zona.children().children().html()+'","'+zona.find('#divcantidad>input').val()+'"}');


             });
           }

           if(!($("#listaProd li").hasClass("sin-productos"))){
             $("#listaProd li").each(function(n){
              // var precio = parseFloat($(this).find(".divnombrep").attr("precio"));
              // var cant = parseFloat($(this).find(".input-cantidad").val());
              // precioTotal = parseFloat(precioTotal)+parseFloat(precio*cant);



               var zona = $(this).children();


               var articulo = [2, zona.children().children().html()+"", zona.find('#divcantidadp>.btn-cantidad-p>input').val(), parseFloat(zona.children().attr("precio"))];
               articulos.push(articulo);
                //articulos.push('{2,"'+zona.children().children().html()+'","'+zona.find('#divcantidadp>input').val()+'"}');


             });
               if($("#fecha").val()!=""){
                   var json = JSON.stringify(articulos);
               var id=$(".idTrans").attr("id");
               var fecha = $("#fecha").val();
               var hora = $("#hora").val();
               var fechaYHora = fecha+" "+hora;
               var flagEditar = $("#flagEditar").val();
                
               $("#btn-guardar").fadeOut("500");
               $.post("php/funciones.php", {funcion:"actualizarCompra", json:json, id:id, fecha:fechaYHora, total:precioTotal, flagEditar:flagEditar}, function(result){
                  console.log(result);
                 if(result=="success"){
                   $(".content-wrapper").css("opacity","0");
                   setTimeout(function(){
                     localStorage.setItem("mensaje", "La transacción se ha modificado correctamente")

                       location.href ="index.php";
                   },300)
                 } else {
                   $("#btn-guardar").fadeIn("500");
                   mostrarMensaje("No se ha realizado ningún cambio");
                   $(".btn-guardar").attr("disabled", false);
                  
                 }
               });

             } else {
               mostrarMensaje("El campo de fecha no puede estar vacío", "error");
             }
          }
        } else {
          mostrarMensaje("La transacción no puede estar vacía", "error");
        }
        

        


      });


      });


      function crearEventosEditarCannabis(){
       
        $(".eli-can").unbind().on("click", function(){
        var elemento =  $(this).parent().parent().parent();


       elemento.css("background-color","#ff5353");
       elemento.addClass("animated bounceOut");
       //elemento.css("opacity","0");

        setTimeout( function() {
           elemento.remove();
           actualizarGramos();
           actualizarPrecio();

           if($("#listaCannabis li").length==0){
            $('#listaCannabis').append('<li class="list-group-item list-group-item-action sin-productos">Sin genéticas</li>');
             
           }

       }, 700);

      });
      }

      function crearEventosEditarProductos(){

        $(".eli-prod").unbind().on("click", function(){
        var elemento =  $(this).parent().parent().parent();
       elemento.css("background-color","#ff5353");
       elemento.addClass("animated bounceOut");
        setTimeout( function() {
           elemento.remove();
           actualizarPrecio();
           if($("#listaProd li").length==0){
            $('#listaProd').append('<li class="list-group-item list-group-item-action sin-productos">Sin productos</li>');
           }
       }, 700);



      });
      }

      $("#btn-c-e-trans").on("click", function(){

       // var id = $(this).attr("transaccion");
         var precioTotal = 0;
        var articulos = new Array();
        if(!($("#listaCannabis li").hasClass("sin-productos"))){
          $("#listaCannabis li").each(function(n){


            var zona = $(this).children();
            var precio = parseFloat($(this).find(".divnombre").attr("precio"));
            var cant = parseFloat($(this).find(".input-cantidad").val());
            precioTotal = parseFloat(precioTotal)+parseFloat(precio*cant);


            var articulo = [1, zona.children().children().html()+"", zona.find('#divcantidad>.btn-cantidad>input').val()+"", parseFloat(zona.children().attr("precio")), zona.children().attr("tipo")];
            articulos.push(articulo);
           // articulos.push('{1,"'+zona.children().children().html()+'","'+zona.find('#divcantidad>input').val()+'"}');


          });
        }

        if(!($("#listaProd li").hasClass("sin-productos"))){
          $("#listaProd li").each(function(n){
            var precio = parseFloat($(this).find(".divnombrep").attr("precio"));
            var cant = parseFloat($(this).find(".input-cantidad").val());
            precioTotal = parseFloat(precioTotal)+parseFloat(precio*cant);



            var zona = $(this).children();


            var articulo = [2, zona.children().children().html()+"", zona.find('#divcantidadp>.btn-cantidad-p>input').val(), parseFloat(zona.children().attr("precio"))];
            articulos.push(articulo);
             //articulos.push('{2,"'+zona.children().children().html()+'","'+zona.find('#divcantidadp>input').val()+'"}');


          });
       }

       
        var json = JSON.stringify(articulos);
        var id=$(".idTrans").attr("id");
        
        
       
        var flagEditar = $("#flagEditar").val();
        
        $("#btn-guardar").fadeOut("500");
        $.post("php/funciones.php", {funcion:"eliminarTransaccion", json:json, id:id, flagEditar:flagEditar}, function(result){

          if(result=="success"){
            $(".content-wrapper").css("opacity","0");
            setTimeout(function(){
              localStorage.setItem("mensaje", "La transacción se ha eliminado correctamente")

                location.href ="index.php";
            },300)
          } else {
            $("#btn-guardar").fadeIn("500");
            $(".modalEliminarTransaccion").modal("hide");
            mostrarMensaje("Ha ocurrido un error al eliminar la transacción");
            console.log(result);
          }
        });

      




       /* $.post("php/funciones.php", {funcion:"eliminarTransaccion", id:id}, function(result){
          if(result=="success"){
            localStorage.setItem("mensaje", "La transacción se ha eliminado correctamente")

              location.href ="index.php";
          } else {
            console.log(result);
            $(".modalEliminarTransaccion").modal("hide");
            mostrarMensaje("Ha ocurrido un error al eliminar la transacción");
          }
        });*/
      });

      function eventoPrecio(){
        $(".input-cantidad").change(function(){
            actualizarPrecio();
            actualizarGramos();
        });
      }

      function actualizarPrecio(){
        let total = parseFloat(0);
        console.log("cambio "+total);
        $(".divnombre").each(function(e){
          let precio = $(this).attr("precio");
          let cantidad = parseFloat($(this).parent().find('.input-cantidad').val());
          let temp = precio * cantidad;
       
          total = (total+temp);
          
        });
        $(".divnombrep").each(function(e){
          let precio = $(this).attr("precio");
          let cantidad = parseFloat($(this).parent().find('.input-cantidad').val());
          let temp = precio * cantidad;
        
          total = (total+temp);
          
        });
        $("#total-precio").fadeOut('100', function(){
          $(this).html((total).toFixed(2)).fadeIn('100');
        });

      }
      function actualizarGramos(){
        let total = parseFloat(0);
        
        $(".divnombre").each(function(e){
          
          let cantidad = parseFloat($(this).parent().find('.input-cantidad').val());
          
       
          total = (total+cantidad);
          
        });
        $("#total-geneticas").fadeOut('100', function(){
          $(this).html((total).toFixed(2)).fadeIn('100');
        });

      }




  </script>

</body>

</html>
