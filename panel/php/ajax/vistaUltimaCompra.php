
<?php
require_once("../conexion.php");
session_start();
if(ISSET($_SESSION["id"])){


$hoy = getdate();

$fechahoy = $hoy["year"].'-'.$hoy["mon"].'-'.$hoy["mday"];
if(ISSET($_SESSION["idCaja"]) && $_SESSION["idCaja"]!=""){
  $idCaja = $_SESSION["idCaja"];

} else {
  $idCaja = 0;
}
$comp = consulta ("select * from cajas where hora ='00:00:00' and id = $idCaja;");
if(count($comp)!=0){
  $idCaja = $comp[0]["id"];
  $consulta= consulta("select r.id as id, s.id as idS, s.nombre as nSocio, s.apellidos as aSocio, s.nsocio as numSocio, s.comentarios as comentarioSocio, r.fecha as fechaCompra, r.articulos as articulos, r.precio_total as precio from retiradas r inner join socios s on s.id = r.idSocio where r.id = (select max(id) as id from retiradas) and fecha > '$fechahoy 00:00:00' and idCaja = $idCaja");

 

  if(count($consulta)!=0){


    $idRetirada = $consulta[0]["id"];

    $articulos = json_decode($consulta[0]["articulos"]);
    count($articulos);

    $divFechaHora = explode(" ", $consulta[0]["fechaCompra"]);
    $divFecha = explode("-", $divFechaHora[0]);
    $divHora = explode(":", $divFechaHora[1]);
    $total = 0;
    foreach($articulos as $valor => $indice){
     if($indice[0]==1){
       $total+=$indice[2];

     }
    }

    if($total>5){
      $clase = 'warning';
    } else {
      $clase = 'info';
    }

    ?>



    <div class="card text-white bg-<?php echo $clase; ?> mb-3">
      <div class="card-header">Actividad dispositivo de carta</div>
      <div class="card-body">
       <div class="row">
        <div class="col-md-2" style="border-right:1px solid #00000059">
          <h5><?php echo $consulta[0]["nSocio"].' '.$consulta[0]["aSocio"].' (#'.$consulta[0]["numSocio"].')';?></h5>
        <hr>
          <p><i class="fas fa-calendar-alt"></i> <?php echo $divFecha[2].'/'.$divFecha[1].'/'.$divFecha[0];?> <i class="fas fa-clock ml-4"></i> <?php echo $divHora[0].':'.$divHora[1];?></p>
        <hr>
          <h1 class="text-center"><?php echo $consulta[0]["precio"];?>€</h1><hr>
          <a href="modificar-transaccion.php?id=<?php echo $idRetirada;?>" class="btn-block btn btn-outline-secondary">MODIFICAR</a>
        </div>
        <div class="col-md-5">
          <h5>Cannabis</h5>
          <ul class="list-group lista-tablet l-cannabis">
            <?php
            $gramos = 0;
            foreach($articulos as $valor => $indice){
             if($indice[0]==1){

                if($indice[4]=="v"){
                  $imagen = '<div class="circulo-verde"></div>';
                } else if($indice[4]=="v"){
                    $imagen = '<div class="circulo-marron"></div>';
                } else {
                  $imagen = '<div class="circulo-otros"></div>';
                }

                echo ' <li class="list-group-item d-flex justify-content-between align-items-center">'
                      .$imagen.$indice[1].'
                      <span class="badge badge-'.$clase.' badge-pill"><span class="gen" style="font-size:20px">'.$indice[2].'</span> Gr.</span>
                    </li>';
             }
            }
            ?>
          </ul>
        </div>
         <div class="col-md-5">
          <h5>Productos</h5>
          <ul class="list-group lista-tablet">
            <?php
            foreach($articulos as $valor => $indice){
             if($indice[0]==2){
                echo ' <li class="list-group-item d-flex justify-content-between align-items-center">
                      '.$indice[1].'
                      <span class="badge badge-info badge-pill"><span style="font-size:20px">'.$indice[2].'</span> Ud.</span>
                    </li>';
             }
            }
            ?>
          </ul>
        </div>
        

        
         

       </div>
      </div>
    
          <?php 
           if($consulta[0]["comentarioSocio"]!=""){
             ?>
                <div class="card-footer alert-warning text-white">
                 <?php echo $consulta[0]["comentarioSocio"];?>
               </div>
            
             <?php
           }

        ?>
      
    </div>
   
    

    <?php
  } else {
    echo '<div class="alert alert-warning" role="alert">
    <i class="fas fa-exclamation-triangle"></i> No hay compras registradas hoy.
  </div>';
  }
} else {
    echo '<div class="alert alert-secondary" role="alert">
    <i class="fas fa-exclamation-triangle"></i> La caja está cerrada.
  </div>';
}

}

?>

<script type="text/javascript">
/*  $(document).ready(function(){
    var gramos = 0;
    $(".gen").each(function(n){
      var actual = parseFloat($(this).html());
      gramos=parseFloat(gramos)+actual;

    });
    if(gramos>5){
      $(".card").removeClass("bg-info").addClass("bg-warning");
    }
  });*/
</script>
