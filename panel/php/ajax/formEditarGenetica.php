<?php
	require_once("../conexion.php");
	extract($_POST);
	$consulta = consulta("select * from geneticas where id=$id");
  if($consulta[0]["destacada"]==0){
    $check = "";
  } else {
    $check = "checked";
  }

  $oculto="";
session_start();
        if($_SESSION["tipo"]==2){
            $oculto ="d-none";
        }
          echo $oculto;


 

?>
<div class="row">
	<div class="col-sm-3">
		<label for="imagenGenetica">
	      <center>
        <input type="text" id="ruta-imagen" class="d-none" value="<?php echo $consulta[0]["img"]; ?>">
        <div class="fotoGenetica">
            <div class="file-loading">
                <input id="imagen-genetica" name="imagen-genetica" type="file" accept="image/*" required>
            </div>
        </div>
        </center>
		</label>
		 <input style="display:none;" id="imagenGenetica" type="file" value="Cambiar datos" />

	</div>

	<div class="col-sm-9">
	<input type="text" style="display:none;" id="idGenetica" placeholder="Nombre" value="<?php echo $consulta[0]["id"]; ?>">
  <div class="form-group no-gutters">


       <label for="colFormLabelLg" class="">Nombre</label>
      <input type="text" class="form-control form-control-lg" id="nombreGenetica" placeholder="Nombre" value="<?php echo $consulta[0]["nombre"]; ?>">

  </div>
  <div class="form-group no-gutters">

    <label for="colFormLabelLg" class="">Descripción</label>
      <textarea style="height:100px;" type="text" class="form-control form-control-lg" id="descripcionGenetica" placeholder="Descripción"><?php echo $consulta[0]["descripcion"]; ?></textarea>

  </div>
  <div class="form-group row">
    <div class="col-sm-6 mt-2 <?php echo $oculto;?>">
      <label for="colFormLabelLg" class="text-center">Precio (€)</label>
      <input type="text" class="form-control form-control-lg" id="precioGenetica" placeholder="Introduce el precio en Euros" value="<?php echo $consulta[0]["precio"]; ?>">

      </div>
      <div class="col-sm-6 mt-2 <?php echo $oculto;?>">
    <label for="colFormLabelLg" class="text-center">Stock (Gr)</label>
      <input type="number" class="form-control form-control-lg" id="stockGenetica" placeholder="Introduce el precio en Euros" value="<?php echo $consulta[0]["stock"]; ?>">
    </div>

    <div class="col-sm-6 mt-2">
      <label for="colFormLabelLg" class="text-center">Banco</label>
      <input type="text" class="form-control form-control-lg" id="bancoGenetica" placeholder="Introduce el banco" value="<?php echo $consulta[0]["banco"]; ?>">
    </div>


    <div class="col-sm-6 mt-2">
      <label for="colFormLabelLg" class="text-center">Gusto</label>
      <input type="text" class="form-control form-control-lg" id="gustoGenetica" placeholder="Introduce el gusto" value="<?php echo $consulta[0]["gusto"]; ?>">
    </div>


  </div>




  <div class="form-group row">

    <div class="col-sm-3">
      <label for="colFormLabelLg" class="text-center"><small>% ÍND</small></label>
      <input type="text" class="form-control form-control-lg" id="pindicaGenetica" placeholder="Porcentaje índica" value="<?php echo $consulta[0]["pindica"]; ?>">
    </div>

    <div class="col-sm-3">
      <label for="colFormLabelLg" class="text-center"><small>% SAT</small></label>
      <input type="text" class="form-control form-control-lg" id="psativaGenetica" placeholder="Porcentaje sativa" value="<?php echo $consulta[0]["psativa"]; ?>">
    </div>

    <div class="col-sm-3">
      <label for="colFormLabelLg" class="text-center"><small>% CBD</small></label>
      <input type="text" class="form-control form-control-lg" id="pcbdGenetica" placeholder="Porcentaje CBD" value="<?php echo $consulta[0]["pcbd"]; ?>">
    </div>

    <div class="col-sm-3">
      <label for="colFormLabelLg" class="text-center"><small>% THC</small></label>
      <input type="text" class="form-control form-control-lg" id="pthcGenetica" placeholder="Porcentaje THC" value="<?php echo $consulta[0]["pthc"]; ?>">
    </div>
  </div>
  <div class="custom-control custom-switch">
              <input type="checkbox" class="custom-control-input" id="check1" <?php echo $check;?>>
              <label class="custom-control-label" for="check1">DESTACAR ESTA GENÉTICA EN LA CARTA</label>
</div>
 </div>
</div>
<div class="row">
  <div class="col-sm-3">

  </div>
  <div class="col-sm-9">
     <button class="btn btn-outline-primary btn-block mt-3" type="button" data-toggle="collapse" data-target="#collapseReg" aria-expanded="false" aria-controls="collapseReg">
        Registro de cambios
      </button>

      <div class="collapse" id="collapseReg">
    <div class="row mt-3">
      <div class="col-12 zona-registro-gen">
        
        <?php 
          $registro = consulta("select * from reg_gen where idGen = $id");
          if(!empty($registro)){
            ?>
            <ul class="list-group border-primary">
              <?php
                foreach ($registro as $indice => $valor){
                  $usuario = consulta("select nombre, apellidos from usuarios where id = ".$valor["idUsuario"].";");
                  if(empty($usuario)){
                    $nombreUsuario = "Usuario eliminado";
                  } else {
                    $nombreUsuario = $usuario[0]["nombre"]." ".$usuario[0]["apellidos"];

                  }


                
                echo '<li class="list-group-item list-group-item-primary">
                    <span class="mr-1"><b>'.$valor["fechaHora"].'</b></span> Registro modificado de <b>'.$valor["cantAnterior"].'Gr.</b> a <b>'.$valor["cant"].'Gr.</b> por '.$nombreUsuario.'
                </li>';
              }
              ?>
              
             
            </ul>

            <?php

          }
        ?>

      
      </div>
      </div>
    </div>
  </div>
  </div>



<script>
   var fotoGenetica = $("#ruta-imagen").val();

                var btnCust = '<button id="btnSubirFotoPersonal" type="button" class="btn btn-info btn-block" title="Guardar imagen" >' +
                '<i class="fa fa-edit"></i> Guardar' +
                '</button>';
                  $("#imagen-genetica").fileinput({
                      theme: "fa",
                      overwriteInitial: false,
                      maxFileSize: 5000,
                      showClose: false,
                      showCaption: false,
                      showBrowse: false,
                      browseOnZoneClick: true,

                      elErrorContainer: '#kv-avatar-errors-2',
                      msgErrorClass: 'alert alert-block alert-danger',
                      defaultPreviewContent: '<img class="img-thumbnail" src="img/geneticas/'+fotoGenetica+'" style="width:100%;height:100%;">',
                      layoutTemplates: {main2: '{preview} Pulsa para editar'},
                      allowedFileExtensions: ["jpg"]
                  });


</script>
