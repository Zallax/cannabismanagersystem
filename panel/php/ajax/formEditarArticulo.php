<?php
	require_once("../conexion.php");
	extract($_POST);
	/*$consulta = consulta("select a.nombre as nombre, a.descripcion as descripcion, a.precio as precio, a.stock as stock, c.nombre as nombreCat, c.id as idCat from articulos a inner join categorias_articulos c on a.categoria = c.id where a.id=$id");*/
    $consulta = consulta("select * from articulos where id=$id");
    $articulo = $consulta[0];

    $categorias = consulta("select * from categorias_articulos");

?>
<input class="d-none" id="id-articulo" value="<?php echo $articulo["id"]; ?>">
<div class="form-group">
    <label for="nombre-articulo">Nombre</label>
                   
    <input type="text" class="form-control" id="nombre-articulo" placeholder="Introduce el nombre" value="<?php echo $articulo["nombre"]; ?>" required>

</div>


<div class="form-group">
    <label for="desc-articulo">Descripción</label>
                   
    <textarea type="text" class="form-control" id="desc-articulo" placeholder="Introduce la descripción" required><?php echo $articulo["descripcion"]; ?></textarea>
</div>

<div class="row">
    <div class="form-group col-6">
        <label for="precio-articulo">Precio</label>
                    
        <input type="text" class="form-control" id="precio-articulo" placeholder="Introduce el precio" value="<?php echo $articulo["precio"]; ?>" required>

    </div>
    <div class="form-group col-6">
        <label for="stock-articulo">Stock</label>
                    
        <input type="text" class="form-control" id="stock-articulo" placeholder="Introduce el stock" value="<?php echo $articulo["stock"]; ?>" required>

    </div>
</div>


<div class="form-group">
    <label for="categoria-articulo">Categoría</label>
                   
    <select class="form-control" id="categoria-articulo" required>
        <?php
            for($i=0;$i<count($categorias);$i++){
                if($categorias[$i]["id"]==$articulo["categoria"]){
                    $seleccionado = 'selected="true"';
                } else {
                    $seleccionado = "";
                }
                echo '<option '.$seleccionado.' value="'.$categorias[$i]["id"].'">'.$categorias[$i]["nombre"].'</option>';
            }

        ?>
        
    </select>

</div>