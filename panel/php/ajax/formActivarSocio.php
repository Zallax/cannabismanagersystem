<?php
//  require_once("../conexion.php");
  require_once("../funciones.php");
  require_once("../../js/ajax/comprobarNSocioLibre.php");
  require_once("../config.php");
  extract($_POST);
  $consulta = consulta("select * from socios where id=$id");
  $nombre = $consulta[0]["nombre"];
  $apellidos = $consulta[0]["apellidos"];


  $avalador = consulta("select foto, nsocio, nombre, apellidos from socios where id = '".$consulta[0]["avalador"]."';");
  $fechaFormato = explode('-',$consulta[0]["alta"]);

?>
<div class="infoSocioActivacion">
<input id="idSocio" value="<?php echo $id; ?>" style="display:none"></input>
<h1><?php echo $nombre.' '.$apellidos; ?></h1>
<div class="row">
  <div class="col-sm-6">

    <div class="alert alert-primary" role="alert">
     <div class="row">
        <div class="col-sm-9"> <b>Asigna nº de socio. </b> </br><small>Números disponibles: <?php ultimoNSocio();?>.</br>Último disponible: #<?php maximoSocio(); ?></small></div>
        <div class="col-sm-3"><input id="n-socio" class="form-control form-control-lg" placeholder="Nº de socio" type="number" style="width:100%;background-color:white !important;"></div>
      </div>
      </div>
    <div class="activaciones dato">
        <i class="fa fa-users"></i> Avalador
        <div class="alert bg-light" role="alert">
          <button type="button" class="btn text-secondary btn-lg" id="fotoSocio" data-toggle="popover" data-placement="right" data-content="<img width='100%' src='img/socios/foto-personal/<?php echo $avalador[0]["foto"]; ?>'>">
            <?php echo $avalador[0]["nombre"]." ".$avalador[0]["apellidos"].' (#'.$avalador[0]["nsocio"].')'; ?>
          </button>
        </div>
    </div>
    <div class="activaciones dato">
        <i class="fa fa-calendar"></i> Fecha de Alta
        <div class="alert bg-light text-secondary" role="alert">
          <?php echo $fechaFormato[2].'/'.$fechaFormato[1].'/'.$fechaFormato[0]; ?>
        </div>
    </div>
    <div class="activaciones dato">
        <i class="fa fa-address-card"></i> DNI
        <div class="alert bg-light text-secondary" role="alert">
          <span id="z-dni"><?php echo $consulta[0]["dni"]; ?></span>
        </div>
    </div>
    <div class="activaciones dato">
        <i class="fa fa-phone"></i> Teléfono
        <div class="alert bg-light text-secondary" role="alert">
          <?php echo $consulta[0]["telefono"]; ?>
        </div>
    </div>
    <div class="activaciones dato">
        <i class="fa fa-map-marker"></i> Dirección
        <div class="alert bg-light text-secondary" role="alert">
          <?php echo $consulta[0]["direccion"]; ?>
        </div>
    </div>
    <div class="activaciones dato">
        <i class="fa fa-map-marker"></i> Email
        <div class="alert bg-light text-secondary" role="alert">
          <?php echo $consulta[0]["email"]; ?>
        </div>
    </div>







  </div>
  <div class="col-sm-6">
    <div class="alert alert-primary" role="alert">

      <div class="form-group">
         <label for="exampleFormControlSelect1">Selecciona la cuota</label>
         <select class="form-control form-control-lg" id="selectCuotas">
          <option disabled selected>Selecciona</option>
           <?php
              $precios = getCuotas();

              foreach ($precios as $indice => $valor){
                echo '<option value="'.$indice.':'.$valor.'" >'.$indice.' meses ('.$valor.'€)</option>';
              }
           ?>
           <option value="manual">Elegir otra...</option>
         </select>
       </div>
       <div id="zona-meses" style="display:none;">

        <label>
          Meses
        </label>
        
          <input class="form-control form-control-lg mb-2" id="mesesRenov" type="number"></input>
          <label>
          Cantidad (€)
        </label>
        
          <input class="form-control form-control-lg" id="cantRenov" type="number"></input>
        
       </div>
    </div>

    <h4>Declara ser</h4>
    <?php
    $requisitos = json_decode($consulta[0]["requisitos"]);

    mostrarRequisitosSocio($requisitos);


     ?>

     <h4 class="mt-5">Documentación que aportará</h4>
     <?php


     $requisitos2 = json_decode($consulta[0]["req_docu"]);


     mostrarDocuAportada($requisitos2);
      ?>
     <!-- <p class="text-right">Puedes añadir la copia del DNI ahora o hacerlo en otro momento desde el perfil de Socio</p>-->
      <div class="row">
                    <div class="col-sm-12">
                      <h5 class="mt-3">Firma</h5>
                      <img src="img/socios/firma/<?php echo $consulta[0]["firma"];?>" style="width:100%;">
                    </div>
                   <!-- <div class="col-sm-3 zona-frontal-dni">
                      <strong>Parte delantera</strong>

                      <div class="file-loading">
                          <input id="dni-1" name="dni-1" type="file" accept="image/*" required>
                      </div>
                    </div>
                    <div class="col-sm-3 zona-trasera-dni">
                      <strong>Parte trasera</strong>

                      <div class="file-loading">
                          <input id="dni-2" name="dni-2" type="file" accept="image/*" required>
                      </div>
                    </div>-->
                  </div>
  </div>
</div>
</div>





<script>
/*  var btnDelDni1 = '<button id="btnSubirDni1" type="button" class="btn btn-outline-dark" title="Guardar imagen" >' +
      '<i class="fa fa-edit"></i> Guardar' +
      '</button>  ';
  var btnDelDni2 = '<button id="btnSubirDni2" type="button" class="btn btn-outline-dark" title="Guardar imagen" >' +
      '<i class="fa fa-edit"></i> Guardar' +
      '</button>';

  $("#dni-1").fileinput({
      theme: "fa",
      overwriteInitial: true,
      maxFileSize: 3500,
      showClose: false,
      showCaption: false,
      showBrowse: true,
      browseOnZoneClick: true,
      browseClass: "btn btn-outline-dark",
      browseLabel: "Subir",
      browseIcon: "<i class=\"fas fa-file-upload\"></i> ",
      elErrorContainer: '#kv-avatar-errors-2',
      msgErrorClass: 'alert alert-block alert-danger',

      layoutTemplates: {main2: '{preview} ' + ' {browse} '  + btnDelDni1},
      allowedFileExtensions: ["jpg"]
  });
  $("#dni-2").fileinput({
      theme: "fa",
      overwriteInitial: true,
      maxFileSize: 3500,
      showClose: false,
      showCaption: false,
      showBrowse: true,
      browseOnZoneClick: true,
      browseClass: "btn btn-outline-dark",
      browseLabel: "Subir",
      browseIcon: "<i class=\"fas fa-file-upload\"></i> ",

      elErrorContainer: '#kv-avatar-errors-2',
      msgErrorClass: 'alert alert-block alert-danger',

      layoutTemplates: {main2: '{preview} ' + ' {browse} '  + btnDelDni2},
      allowedFileExtensions: ["jpg"]
  });

  $("#btnSubirDni1").on("click", function(){
    subirDni2(1);
  });

  $("#btnSubirDni2").on("click", function(){
    subirDni2(2);
  });

  function subirDni2(zona){

    if(zona==1){
      var selector = '.zona-frontal-dni';
    } else {
      var selector = '.zona-trasera-dni';
    }

    if($(selector+" .kv-file-content").length>0){
        var imagen = $(selector+" .file-preview-image").attr("src");


        var dni = $("#z-dni").html();
        $.post("js/ajax/cargarFotoDni.php",{tipo:zona, imagen:imagen, dni:dni}, function(result){

          if (result=="success"){
            mostrarMensaje("Se ha añadido el DNI correctamente.");


          } else {
            console.log(result);
            mostrarMensaje('<i class="fa fa-times-circle" ></i> Ha ocurrido un error al guardar la imagen');
          }



      });


    } else {
      mostrarMensaje('<i class="fa fa-times-circle" ></i> No has seleccionado ninguna imagen');

    }
  }
  */

  $("#selectCuotas").change(function(){
    if($(this).val()=="manual"){
      $("#zona-meses").fadeIn("500");
    } else {
      $("#zona-meses").fadeOut("500");
      $("#mesesRenov").val("");
    }
  });
</script>
