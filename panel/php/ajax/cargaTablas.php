<?php
session_start();
require_once("../conexion.php");
extract($_POST);

tablaVerde($tipo,$categoria);

function tablaVerde($tipo,$categoria){
        $genetica=consulta("SELECT * from geneticas where tipo='$tipo' and categoria='$categoria' order by nombre;");
        count($genetica)>0  ? $datos=true : $datos=false;
        if($datos){

            echo '<table class="table table-hover table-light display compact table-striped table-responsive" id="tablagenetica'.$categoria.'">
                <thead class="thead-dark">
                    <tr>
                    
                    <th scope="col" class="zonaTablaFoto"></th>
                    <th scope="col">Nombre</th>
                    
                    <th scope="col">Precio</th>
                    <th scope="col">Gramos</th>
                    <th scope="col">Banco</th>
                    <th class="text-center" scope="col">Editar</th>
                    </tr>
                </thead>
                <tbody>';

                    for($i=0;$i<count($genetica);$i++){
                        $stock = $genetica[$i]["stock"];
                         if($_SESSION["tipo"]!=1){
                            $stock = " - ";
                         }
                        $mensaje='<p class="text-muted">En Stock</p>';
                        $estado="";
                        if($genetica[$i]["stock"]<10){
                            $estado = "table-warning";
                            $mensaje='<p class="text-muted">Apunto de agotarse</p>';
                        } 
                        if($genetica[$i]["stock"]<2 ){
                            $estado = "table-danger";
                            $mensaje='<p class="text-muted">Agotado</p>';
                        } 
                     
                        echo '
                       <tr class="'.$estado.'">
                        
                        <td><div class="rounded" style="width:70px;height:70px;background-size:100%;background-repeat:no-repeat;background-position:center;background-image:url(img/geneticas/'.$genetica[$i]["img"].')"></div></td>
                        <td><div class="ngenetica-tabla">'.$genetica[$i]["nombre"].'</div> '.$mensaje.'</td>
                        <td class="pgenetica-tabla">'.$genetica[$i]["precio"].' €</td>
                        <td>'.$stock.' </td>
                        <td><span class="text-muted">'.$genetica[$i]["banco"].'</span></td>
                        <td class="text-center"><button type="button" class="btnEditarGenetica btn btn-primary" data-toggle="modal" data-id="'.$genetica[$i]["id"].'" data-target="#modalGenetica">
                          <i class="fa fa-pencil-square-o"></i>
                        </button></td>
                        </tr>';
                    }
                
                    echo '
                </tbody>
                </table>';
            } else {
                echo "<small style='margin-bottom:30px' class='text-muted'>No hay datos disponibles</small>";
            }

        
        
    }

?>