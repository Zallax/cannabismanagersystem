<?php
require_once("../conexion.php");
extract($_POST);
    tablaVerde($categoria);
    function tablaVerde($categoria){
             $genetica=consulta("select * from articulos where categoria=$categoria;");
             count($genetica)>0 ? $datos=true : $datos=false;
             if($datos){
                 echo '<table class="table table-white table-hover display compact table-striped" id="tablaCategoria'.$categoria.'">
                     <thead class="thead-white">
                         <tr>
                         
                         
                         <th style="width:90% !important" scope="col">Nombre</th>
                         
                         <th scope="col">Precio</th>
                         <th scope="col">Stock</th>
                         <th class="text-center" scope="col">Editar</th>
                         </tr>
                     </thead>
                     <tbody class="table-white">';

                         for($i=0;$i<count($genetica);$i++){
                             $mensaje='<p style="margin-bottom:0px;" class="text-muted">En Stock</p>';
                             $estado="";
                             if($genetica[$i]["stock"]<3){
                                 $estado = "table-warning";
                                 $mensaje='<p class="text-muted" style="margin-bottom:0px;">Apunto de agotarse</p>';
                             } 
                             if($genetica[$i]["stock"]<1 ){
                                 $estado = "table-danger";
                                 $mensaje='<p style="margin-bottom:0px;" class="text-muted">Agotado</p>';
                             } 
                          
                             echo '
                            <tr class="'.$estado.'">
                             
                           
                             <td><div class="narticulo-tabla">'.$genetica[$i]["nombre"].'</div> '.$mensaje.'</td>
                             <td class="pgenetica-tabla">'.$genetica[$i]["precio"].' €</td>
                             <td>'.$genetica[$i]["stock"].' Ud.</td>
                            
                             <td class="text-right"><button type="button" class="btnEditarProducto btn btn-primary" data-toggle="modal" data-id="'.$genetica[$i]["id"].'" data-target="#modalProducto">
                               <i class="fa fa-pencil-square-o"></i>
                             </button></td>
                             </tr>';
                         }
                     
                         echo '
                     </tbody>
                     </table>';
                 } else {
                     echo "<small class='text-muted'>No hay datos disponibles</small>";
                 }

             
             
         }

?>