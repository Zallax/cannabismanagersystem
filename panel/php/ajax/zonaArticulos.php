<?php
    require_once("../conexion.php");
    extract($_POST);

     if($accion=="categorias"){
         cargarCategorias();
     }
    
     if($accion=="contenedores"){
         cargarContenedores();
     }


     function cargarCategorias(){
        $resultado = consulta ("select * from categorias_articulos order by nombre");
        if(count($resultado)>0){
            echo ' <a class="nav-link active" data-id-cat="'.$resultado[0]["id"].'" id="cat'.$resultado[0]["id"].'" data-toggle="pill" href="#seccionCat'.$resultado[0]["id"].'" role="tab" aria-controls="#seccionCat'.$resultado[0]["id"].'" aria-selected="true"><i class="fas fa-chevron-right"></i> <span class="n-cat">'.$resultado[0]["nombre"].'</span><button data-toggle="modal" data-target="#modalEliminarCat" class="btn-eliminar-cat"><i class="fa fa-trash"></i></button><button data-toggle="modal" data-target="#modalModCat" class="btn-editar-cat"><i class="fas fa-pencil-alt"></i></button></a>';

            for ($i=1;$i<count($resultado);$i++){
                echo ' <a class="nav-link" data-id-cat="'.$resultado[$i]["id"].'" id="cat'.$resultado[$i]["id"].'" data-toggle="pill" href="#seccionCat'.$resultado[$i]["id"].'" role="tab" aria-controls="#seccionCat'.$resultado[$i]["id"].'"><span><i class="fas fa-chevron-right"></i> <span class="n-cat">'.$resultado[$i]["nombre"].'</span></span><button data-toggle="modal" data-target="#modalEliminarCat" class="btn-eliminar-cat"><i class="fa fa-trash"></i></button><button data-toggle="modal" data-target="#modalModCat" class="btn-editar-cat"><i class="fas fa-pencil-alt"></i></button></a>';
            }
        } else {
            echo "No se han creado categorías.";
        }
 
     }

     function cargarContenedores(){
        $resultado = consulta ("select * from categorias_articulos order by nombre");
        if(count($resultado)>0){
            echo '<div class="tab-pane fade show active" data-id-cat="'.$resultado[0]["id"].'" id="seccionCat'.$resultado[0]["id"].'" role="tabpanel" aria-labelledby="seccionCat'.$resultado[0]["id"].'">';
           tablaVerde($resultado[0]["id"]);
          echo '</div>';
    
          for ($i=1;$i<count($resultado);$i++){
            echo '<div class="tab-pane fade" data-id-cat="'.$resultado[$i]["id"].'" id="seccionCat'.$resultado[$i]["id"].'" role="tabpanel" aria-labelledby="seccionCat'.$resultado[$i]["id"].'">';
            tablaVerde($resultado[$i]["id"]);
         echo '</div>';
            }   
        } else {
            echo "No hay datos disponibles.";
        }
     }


     //tablaVerde($categoria);

     function tablaVerde($categoria){
             $genetica=consulta("select * from articulos where categoria=$categoria;");
             count($genetica)>0 ? $datos=true : $datos=false;
             if($datos){
                 echo '<table class="table table-white table-hover display compact table-striped" id="tablaCategoria'.$categoria.'">
                     <thead class="thead-white">
                         <tr>
                         
                         
                         <th style="width:90% !important" scope="col">Nombre</th>
                         
                         <th scope="col">Precio</th>
                         <th scope="col">Stock</th>
                         <th class="text-center" scope="col">Editar</th>
                         </tr>
                     </thead>
                     <tbody class="table-white">';

                         for($i=0;$i<count($genetica);$i++){
                             $mensaje='<p style="margin-bottom:0px;" class="text-muted">En Stock</p>';
                             $estado="";
                             if($genetica[$i]["stock"]<3){
                                 $estado = "table-warning";
                                 $mensaje='<p class="text-muted" style="margin-bottom:0px;">Apunto de agotarse</p>';
                             } 
                             if($genetica[$i]["stock"]<1 ){
                                 $estado = "table-danger";
                                 $mensaje='<p style="margin-bottom:0px;" class="text-muted">Agotado</p>';
                             } 
                          
                             echo '
                            <tr class="'.$estado.'">
                             
                           
                             <td><div class="narticulo-tabla">'.$genetica[$i]["nombre"].'</div> '.$mensaje.'</td>
                             <td class="pgenetica-tabla">'.$genetica[$i]["precio"].' €</td>
                             <td>'.$genetica[$i]["stock"].' Ud.</td>
                            
                             <td class="text-right"><button type="button" class="btnEditarProducto btn btn-primary" data-toggle="modal" data-id="'.$genetica[$i]["id"].'" data-target="#modalProducto">
                               <i class="fa fa-pencil-square-o"></i>
                             </button></td>
                             </tr>';
                         }
                     
                         echo '
                     </tbody>
                     </table>';
                 } else {
                     echo "<small class='text-muted'>No hay datos disponibles</small>";
                 }

             
             
         }



?>