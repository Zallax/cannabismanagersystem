<?php
	require_once("../conexion.php");
	$consulta = consulta("select * from geneticas where stock>0 order by nombre");
    $indicas = '<ul class="list-group">';
    $sativas = '<ul class="list-group">';
    $hibridas = '<ul class="list-group">';
    $extraccion = '<ul class="list-group list-group-item-action">';
    $otros = '<ul class="list-group list-group-item-action">';

    $consulta2 = consulta("select * from geneticas where stock>0");
    $indicasm = '<ul class="list-group">';
    $sativasm = '<ul class="list-group">';
    $hibridasm = '<ul class="list-group">';
    $extraccionm = '<ul class="list-group list-group-item-action">';

    foreach($consulta as $indice=>$valor){
        
        if($valor["tipo"]=="verde"){

            if($valor["categoria"]=="indica"){
                $indicas.='<li class="list-group-item list-group-item-action"><span id="nom-gen" tipo="v" precio="'.$valor["precio"].'">'.$valor["nombre"].'</span> (<span id="max-stock">'.$valor["stock"].'</span> Gr.)</li>';
            }
            if($valor["categoria"]=="sativa"){
                $sativas.='<li class="list-group-item list-group-item-action"><span id="nom-gen" tipo="v" precio="'.$valor["precio"].'">'.$valor["nombre"].'</span> (<span id="max-stock">'.$valor["stock"].'</span> Gr.)</li>';
            }
            if($valor["categoria"]=="hibrida"){
                $hibridas.='<li class="list-group-item list-group-item-action"><span id="nom-gen" tipo="v" precio="'.$valor["precio"].'">'.$valor["nombre"].'</span> (<span id="max-stock">'.$valor["stock"].'</span> Gr.)</li>';
            }
            if($valor["categoria"]=="extraccion"){
                $extraccion.='<li class="list-group-item list-group-item-action"><span id="nom-gen" tipo="v" precio="'.$valor["precio"].'">'.$valor["nombre"].'</span> (<span id="max-stock">'.$valor["stock"].'</span> Gr.)</li>';
            }
           
        }
        if($valor["categoria"]=="otros"){
            $otros.='<li class="list-group-item list-group-item-action"><span id="nom-gen" tipo="o" precio="'.$valor["precio"].'">'.$valor["nombre"].'</span> (<span id="max-stock">'.$valor["stock"].'</span> Gr.)</li>';
        }
        if($valor["tipo"]=="marron"){

          if($valor["categoria"]=="indica"){
              $indicasm.='<li class="list-group-item list-group-item-action"><span id="nom-gen" tipo="m" precio="'.$valor["precio"].'">'.$valor["nombre"].'</span> (<span id="max-stock">'.$valor["stock"].'</span> Gr.)</li>';
          }
          if($valor["categoria"]=="sativa"){
              $sativasm.='<li class="list-group-item list-group-item-action"><span id="nom-gen" tipo="m" precio="'.$valor["precio"].'">'.$valor["nombre"].'</span> (<span id="max-stock">'.$valor["stock"].'</span> Gr.)</li>';
          }
          if($valor["categoria"]=="hibrida"){
              $hibridasm.='<li class="list-group-item list-group-item-action"><span id="nom-gen" tipo="m" precio="'.$valor["precio"].'">'.$valor["nombre"].'</span> (<span id="max-stock">'.$valor["stock"].'</span> Gr.)</li>';
          }
          if($valor["categoria"]=="extraccion"){
              $extraccionm.='<li class="list-group-item list-group-item-action"><span id="nom-gen" tipo="m" precio="'.$valor["precio"].'">'.$valor["nombre"].'</span> (<span id="max-stock">'.$valor["stock"].'</span> Gr.)</li>';
          }
      }

    }
    $indicas .= '</ul>';
    $sativas .= '</ul>';
    $hibridas .= '</ul>';
    $extraccion .= '</ul>';
     $otros .= '</ul>';


    $indicasm .= '</ul>';
    $sativasm .= '</ul>';
    $hibridasm .= '</ul>';
    $extraccionm .= '</ul>';





?>

<div id="accordion">
  <div class="card">
    <div class="card-header" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"> 
      <h5 class="mb-0">
     
          <div class="circulo-verde"></div> Índicas

      </h5>
    </div>

    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
      <div class="card-body" style="max-height:200px; overflow:scroll;padding:0px;">
        <?php
            echo $indicas;

        ?>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTwo" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
      <h5 class="mb-0">
       
      <div class="circulo-verde"></div> Sativas
 
      </h5>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
      <div class="card-body" style="max-height:200px; overflow:scroll;padding:0px;">
      <?php
            echo $sativas;

        ?>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingThree"  data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
      <h5 class="mb-0">

          <div class="circulo-verde"></div> Hibridas
     
      </h5>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
      <div class="card-body" style="max-height:200px; overflow:scroll;padding:0px;">
      <?php
            echo $hibridas;

        ?>
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header" id="headingFour"  data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
      <h5 class="mb-0">

          <div class="circulo-verde"></div> Extracción
     
      </h5>
    </div>
    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
      <div class="card-body" style="max-height:200px; overflow:scroll;padding:0px;">
      <?php
            echo $extraccion;

        ?>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingOtros"  data-toggle="collapse" data-target="#collapseOtros" aria-expanded="false" aria-controls="collapseOtros">
      <h5 class="mb-0">

          <div class="circulo-verde"></div> Otros
     
      </h5>
    </div>
    <div id="collapseOtros" class="collapse" aria-labelledby="headingOtros" data-parent="#accordion">
      <div class="card-body" style="max-height:200px; overflow:scroll;padding:0px;">
      <?php
            echo $otros;

        ?>
      </div>
    </div>
  </div>

  <!-- HACHIS -->
  <div class="card">
    <div class="card-header" id="heading1"  data-toggle="collapse" data-target="#collapse1" aria-expanded="false" aria-controls="collapse1">
      <h5 class="mb-0">

          <div class="circulo-marron"></div> Índicas
     
      </h5>
    </div>
    <div id="collapse1" class="collapse" aria-labelledby="heading1" data-parent="#accordion">
      <div class="card-body" style="max-height:200px; overflow:scroll;padding:0px;">
      <?php
            echo $indicasm;

        ?>
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header" id="heading2"  data-toggle="collapse" data-target="#collapse2" aria-expanded="false" aria-controls="collapse2">
      <h5 class="mb-0">

          <div class="circulo-marron"></div> Sativas
     
      </h5>
    </div>
    <div id="collapse2" class="collapse" aria-labelledby="heading2" data-parent="#accordion">
      <div class="card-body" style="max-height:200px; overflow:scroll;padding:0px;">
      <?php
            echo $sativasm;

        ?>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="heading3"  data-toggle="collapse" data-target="#collapse3" aria-expanded="false" aria-controls="collapse3">
      <h5 class="mb-0">

          <div class="circulo-marron"></div> Híbridas
     
      </h5>
    </div>
    <div id="collapse3" class="collapse" aria-labelledby="heading3" data-parent="#accordion">
      <div class="card-body" style="max-height:200px; overflow:scroll;padding:0px;">
      <?php
            echo $hibridasm;

        ?>
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header" id="heading4"  data-toggle="collapse" data-target="#collapse4" aria-expanded="false" aria-controls="collapse4">
      <h5 class="mb-0">

          <div class="circulo-marron"></div> Extracción
     
      </h5>
    </div>
    <div id="collapse4" class="collapse" aria-labelledby="heading4" data-parent="#accordion">
      <div class="card-body" style="max-height:200px; overflow:scroll;padding:0px;">
      <?php
            echo $extraccionm;

        ?>
      </div>
    </div>
  </div>
  


</div>

<script>

</script>