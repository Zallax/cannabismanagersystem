<div class="row sel-socio" style="overflow-y:scroll;max-height:calc(100vh - 225px);">
<?php
	require_once("../conexion.php");
	

	if(ISSET($_POST)){

		extract($_POST);

		if(ISSET($nombre)){
			$consulta = consulta("select * from socios where activado=1 and (nombre like '$nombre%' or apellidos like '$nombre%') order by nsocio;");
		} else if(ISSET($nsocio)){
			$consulta = consulta("select * from socios where activado=1 and nsocio like '$nsocio%' order by nsocio;");
			
		} else {
			$consulta = consulta("select * from socios where activado=1 order by nsocio");
		}
	}

	if(count($consulta)!=0){
		foreach($consulta as $indice => $valor){

			if($valor["foto"]==""){
				$foto = "perfil.png";
			} else {
				$foto = $valor["foto"];
			}



?>


	<div class="col-lg-3 col-sm-4 mt-3" style="display:table-cell;">
		<div class="card socio" style="width: 100%;">
		  <img class="card-img-top" src="img/socios/foto-personal/<?php echo $foto; ?>" alt="Card image cap">
		  <div class="card-body">
		    <span class="card-text font-weight-bold nombre"><?php echo $valor["nombre"].' '.$valor["apellidos"];?></span></br>
		    <span class="text-muted nsocio">#<?php echo $valor["nsocio"] ?></span>
		    <input class="d-none id" value="<?php echo $valor["id"]; ?>">
		     <input class="d-none descuento" value="<?php echo $valor["descuento"]; ?>">
		  </div>
		</div>
	</div>	


<?php
		}
	}
?>
</div>

<script>	
	$(".socio").on("click", function(){
	  var id = $(this).find("input.id").val();
	  var nombre = $(this).find(".nombre").html();
	  var nsocio = $(this).find(".nsocio").html();
	  var imagen = $(this).find(".card-img-top").attr("src");
	  var descuento = $(this).find("input.descuento").val();
	  seleccionarSocio(id, nombre, nsocio, imagen, descuento);
	});

</script>