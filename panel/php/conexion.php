<?php



function connectDB()
{
    try
    {
        $opc=array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
        $dsn="mysql:host=localhost;dbname=gstr_islabonita";
        $usuario="root";
        $contrasena="root";
        $base=new PDO($dsn,$usuario,$contrasena,$opc);
    }
    catch (PDOException $e)
    {
        die ("Error".$e->getMessage());
        $resultado=null;
    }
    return $base;
}

function ejecutaConsulta($sql)
{
		//recibe una cadena conteniendo una instruccion SELECT y devuelve un resultset

		$miconexion=connectDB();
		return $miconexion->query($sql);

}

function consulta($sql)
{

		//recibe una cadena conteniendo una instruccion SELECT y devuelve un array con la fila de datos
		$datos=[];
		$resultset=ejecutaConsulta($sql);
		while($fila=$resultset->fetch(PDO::FETCH_ASSOC))
		{
			$datos[]=$fila;
		}
		return $datos;


}
function consultaSegura($sql, $user, $pass)
{
	$miconexion=connectDB();
		$statement = $miconexion->prepare($sql);
		$statement ->setFetchMode(PDO::FETCH_ASSOC);
		$statement->bindParam(':mail', $user, PDO::PARAM_STR);
		$statement->bindParam(':pass', $pass, PDO::PARAM_STR);
		if(!$statement->execute()){
			echo "ERROR";
			$miconexion->close();
		}
		$datos=[];
		while($fila=$statement->fetch())
		{
			$datos[]=$fila;
		}
		return $datos;


}

function insert($sql)
{
		/*recibe una cadena conteniendo una instruccion DML, la ejecuta y
		devuelve el nº de filas afectadas por dicha instruccion*/
		$miconexion=connectDB();
		$accion = $miconexion->prepare($sql);
		$accion->execute();
		return $accion->rowCount();
		//return "1";
}



?>
