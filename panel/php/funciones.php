<?php
    
    include "conexion.php";
    extract($_POST);

    function fecha($fecha){
        $divFecha = explode("-", $fecha);
        return $divFecha[2].'/'.$divFecha[1].'/'.$divFecha[0];
    }



    if(ISSET($funcion)){
        if($funcion=="activaciones"){
            tablaSociosSinActivar();
        }
        if($funcion=="botonPanico"){
            botonPanico($idU);
        }

        if($funcion=="actualizarCompra"){
            actualizarCompra($json, $id, $fecha, $total, $flagEditar);
        }
        if($funcion == "registrarCompra"){
            registrarCompra($json, $id, $total, $idCaja);
        }
        if($funcion == "eliminarTransaccion"){
            eliminarTransaccion($id, $json, $flagEditar);
        } 
        if($funcion == "comprobarCajasAbiertas"){
            comprobarCajasAbiertas();
        } 
         if($funcion == "subirDocu1"){
            subirDocu(1,$id);
        } 
         if($funcion == "subirDocu2"){
            subirDocu(2,$id);
        } 
         if($funcion == "comprobarCajaAbierta"){
            comprobarCaja();
        } 
        if($funcion == "borrarDatos"){
            borrarDatos($fecha1,$fecha2);
        } 
        if($funcion == "modificarStock"){
            modificarStock($accion, $idGen, $cantidad, $asunto, $user);
        }
    }
    function tablaSocios(){
        $socios=consulta('SELECT * from socios where activado=1 order by nsocio');



        echo '<table class="table table-hover table-striped table-white" id="tabla-socios">
            <thead class="thead-white">
                <tr>
                <th class="text-center" width="50px" scope="col">Nº</th>
                <th scope="col">Foto</th>
                <th scope="col">Nombre</th>
                <th scope="col">Apellidos</th>
                <th scope="col" style="width:100px;">Alta</th>
                <th scope="col" style="width:100px;">DNI</th>

                <th class="text-center" scope="col">Perfil</th>
                </tr>
            </thead>
            <tbody>';

                for($i=0;$i<count($socios);$i++){
                    $divFecha = explode("-", $socios[$i]["alta"]);


                    if($socios[$i]["foto"]==""){
                        $foto="perfil.png";
                    } else {
                        $foto=$socios[$i]["foto"];
                    }

                    $datosFaltantes = consultarDatosFaltantes($socios[$i]);

                    if($datosFaltantes != "nada"){
                        $cadenaDatosFaltantes = "Faltan datos (";
                        foreach ($datosFaltantes as $indice => $valor){
                            if($indice == 0){
                                $cadenaDatosFaltantes.=$valor;
                            }
                             else if($indice!=count($datosFaltantes)-1){
                                $cadenaDatosFaltantes.=", ".$valor;
                            } else {
                                $cadenaDatosFaltantes.=" y ".$valor;
                            }

                        }
                        $cadenaAlerta = '<span data-toggle="tooltip" data-placement="bottom" title="'.$cadenaDatosFaltantes.')"><i class="far fa-bell"></i></span>';
                    } else {
                        $cadenaAlerta = "";
                    }

                    echo '
                    <tr>
                    <th class="text-center scope="row">'.$socios[$i]["nsocio"].'</th>
                    <td width="70px" class="text-center" style="padding:0.01em;"><div style="background-image:url(img/socios/foto-personal/'.$foto.');width:100%;height:70px;background-position: center;background-size: 100%;background-repeat:no-repeat;"></div></td>
                    <td>'.$socios[$i]["nombre"].'</td>
                    <td>'.$socios[$i]["apellidos"].'</td>
                    <td>'.$divFecha[2].'/'.$divFecha[1].'/'.$divFecha[0].'</td>
                    <td>'.$socios[$i]["dni"].'</td>

                    <td class="text-center" style="width:80px;">'.$cadenaAlerta.'<a class="btn btn-primary" href="perfil.php?s='.$socios[$i]["id"].'"><i class="fa fa-pencil-square-o"></i></a></td>
                    </tr>';
                }

                echo '
            </tbody>
            </table>';
    }


    function tablaSociosSinRenovar(){
        //$socios=consulta('select c.id as id, c.fechafin as fechafin, c.fechapago as fechapago, s.nombre as nombre, s.apellidos as apellidos, s.id as ids, s.foto as foto, s.nsocio as nsocio from cuotas c  inner join socios s on c.idSocio = s.id where fechafin < CURRENT_DATE');

        $socios = consulta('select c.id as id, c.fechafin as fechafin, c.fechapago as fechapago, s.nombre as nombre, s.apellidos as apellidos, s.id as ids, s.foto as foto, s.nsocio as nsocio from cuotas c inner join socios s on c.idSocio = s.id WHERE c.id IN (SELECT MAX(id) FROM cuotas GROUP BY idSocio) and c.fechafin < CURRENT_DATE');



        echo '<table class="table table-hover table-striped table-danger" id="tabla-socios">
            <thead class="bg-white">
                <tr>
                <th class="text-center" width="50px" scope="col">Nº</th>
                <th scope="col">Foto</th>
                <th scope="col">Nombre</th>
                <th scope="col">Apellidos</th>
                <th scope="col" style="width:100px;">Último pago</th>
                <th scope="col" style="width:100px;">Fin de cuota</th>

                <th class="text-center" scope="col">Perfil</th>
                </tr>
            </thead>
            <tbody>';

                for($i=0;$i<count($socios);$i++){



                    if($socios[$i]["foto"]==""){
                        $foto="perfil.png";
                    } else {
                        $foto=$socios[$i]["foto"];
                    }

                    $datosFaltantes = consultarDatosFaltantes($socios[$i]);



                    echo '
                    <tr>
                    <th class="text-center scope="row">'.$socios[$i]["nsocio"].'</th>
                    <td width="70px" class="text-center" style="padding:0.01em;"><div style="background-image:url(img/socios/foto-personal/'.$foto.');width:100%;height:70px;background-position: center;background-size: 100%;background-repeat:no-repeat;"></div></td>
                    <td>'.$socios[$i]["nombre"].'</td>
                    <td>'.$socios[$i]["apellidos"].'</td>
                    <td>'.fecha($socios[$i]["fechapago"]).'</td>
                    <td>'.fecha($socios[$i]["fechafin"]).'</td>

                    <td class="text-center" style="width:80px;"><a class="btn btn-primary" href="perfil.php?s='.$socios[$i]["ids"].'"><i class="fa fa-pencil-square-o"></i></a></td>
                    </tr>';
                }

                echo '
            </tbody>
            </table>';
    }

    function tablaSociosUltimasVisitas(){
        $socios=consulta('select id, ultimo_acceso, nombre, apellidos, foto, nsocio from socios order by ultimo_acceso desc');



        echo '<table class="table table-hover table-striped table-info" id="tabla-socios">
            <thead class="thead-dark">
                <tr>
                <th class="text-center" width="50px" scope="col">Nº</th>
                <th scope="col">Foto</th>
                <th scope="col">Nombre</th>
                <th scope="col">Apellidos</th>
                <th scope="col" style="width:100px;">Último acceso</th>


                <th class="text-center" scope="col">Perfil</th>
                </tr>
            </thead>
            <tbody>';

                for($i=0;$i<count($socios);$i++){
                    if($socios[$i]["ultimo_acceso"]!="0000-00-00 00:00:00"){
                    $fechaYHora = explode(" ", $socios[$i]["ultimo_acceso"]);
                    $fechaD = explode("-", $fechaYHora[0]);
                    $fecha = $fechaD[2]."/".$fechaD[1]."/".$fechaD[0];
                    $horaBruto = explode(":", $fechaYHora[1]);
                    $hora = $horaBruto[0].":".$horaBruto[1];


                    if($socios[$i]["foto"]==""){
                        $foto="perfil.png";
                    } else {
                        $foto=$socios[$i]["foto"];
                    }

                    $datosFaltantes = consultarDatosFaltantes($socios[$i]);



                    echo '
                    <tr>
                    <th class="text-center scope="row">'.$socios[$i]["nsocio"].'</th>
                    <td width="70px" class="text-center" style="padding:0.01em;"><div style="background-image:url(img/socios/foto-personal/'.$foto.');width:100%;height:70px;background-position: center;background-size: 100%;background-repeat:no-repeat;"></div></td>
                    <td>'.$socios[$i]["nombre"].'</td>
                    <td>'.$socios[$i]["apellidos"].'</td>
                    <td>'.$fecha.'</td>


                    <td class="text-center" style="width:80px;"><a class="btn btn-primary" href="perfil.php?s='.$socios[$i]["id"].'"><i class="fa fa-pencil-square-o"></i></a></td>
                    </tr>';
                }
            }

                echo '
            </tbody>
            </table>';
    }

    function tablaSociosProxRenov(){
        $socios=consulta('select c.id as id, c.fechafin as fechafin, s.nombre as nombre, s.apellidos as apellidos, s.id as ids, s.foto as foto, s.nsocio as nsocio from cuotas c  inner join socios s on c.id = s.id where fechafin between date_sub(now(), interval 0 month) and date_add(now(), interval 1 month) order by fechafin asc');



        echo '<table class="table table-hover table-striped table-danger" id="tabla-socios">
            <thead class="thead-dark">
                <tr>
                <th class="text-center" width="50px" scope="col">Nº</th>
                <th scope="col">Foto</th>
                <th scope="col">Nombre</th>
                <th scope="col">Apellidos</th>

                <th scope="col" style="width:100px;">Fecha renovación</th>

                <th class="text-center" scope="col">Perfil</th>
                </tr>
            </thead>
            <tbody>';

                for($i=0;$i<count($socios);$i++){



                    if($socios[$i]["foto"]==""){
                        $foto="perfil.png";
                    } else {
                        $foto=$socios[$i]["foto"];
                    }

                    $datosFaltantes = consultarDatosFaltantes($socios[$i]);



                    echo '
                    <tr>
                    <th class="text-center scope="row">'.$socios[$i]["nsocio"].'</th>
                    <td width="70px" class="text-center" style="padding:0.01em;"><div style="background-image:url(img/socios/foto-personal/'.$foto.');width:100%;height:70px;background-position: center;background-size: 100%;background-repeat:no-repeat;"></div></td>
                    <td>'.$socios[$i]["nombre"].'</td>
                    <td>'.$socios[$i]["apellidos"].'</td>

                    <td>'.fecha($socios[$i]["fechafin"]).'</td>

                    <td class="text-center" style="width:80px;"><a class="btn btn-primary" href="perfil.php?s='.$socios[$i]["id"].'"><i class="fa fa-pencil-square-o"></i></a></td>
                    </tr>';
                }

                echo '
            </tbody>
            </table>';
    }

    function sociosConAnotaciones(){
        $socios=consulta('select id, ultimo_acceso, nombre, apellidos, foto, nsocio, comentarios from socios where comentarios != "" order by id desc');



        echo '<table class="table table-hover table-striped table-danger" id="tabla-socios">
            <thead class="thead-dark">
                <tr>
                <th class="text-center" width="50px" scope="col">Nº</th>
                <th scope="col">Foto</th>
                <th scope="col">Nombre</th>
                <th scope="col">Apellidos</th>

                <th scope="col">Anotación</th>

                <th class="text-center" scope="col">Perfil</th>
                </tr>
            </thead>
            <tbody>';

                for($i=0;$i<count($socios);$i++){



                    if($socios[$i]["foto"]==""){
                        $foto="perfil.png";
                    } else {
                        $foto=$socios[$i]["foto"];
                    }

                    $datosFaltantes = consultarDatosFaltantes($socios[$i]);



                    echo '
                    <tr>
                    <th class="text-center scope="row">'.$socios[$i]["nsocio"].'</th>
                    <td width="70px" class="text-center" style="padding:0.01em;"><div style="background-image:url(img/socios/foto-personal/'.$foto.');width:100%;height:70px;background-position: center;background-size: 100%;background-repeat:no-repeat;"></div></td>
                    <td>'.$socios[$i]["nombre"].'</td>
                    <td>'.$socios[$i]["apellidos"].'</td>

                    <td>'.$socios[$i]["comentarios"].'</td>

                    <td class="text-center" style="width:80px;"><a class="btn btn-primary" href="perfil.php?s='.$socios[$i]["id"].'"><i class="fa fa-pencil-square-o"></i></a></td>
                    </tr>';
                }

                echo '
            </tbody>
            </table>';
    }


    function consultarDatosFaltantes($socios){
        $faltan=false;
        $valores = array();

        foreach($socios as $indice => $valor){
            if ($valor==""){
                if($indice=="foto_dni"){
                    array_push($valores, "Fotocopia del DNI");
                    $faltan=true;
                }
                if($indice=="foto"){
                    array_push($valores, "Foto personal");
                    $faltan=true;
                }

            }
        }

        if($faltan){
            return $valores;
        } else {
            return "nada";
        }
    }





    function tablaSociosSinActivar(){
        $socios=consulta('SELECT * from socios where activado=0 order by nsocio');


        echo '<table class="table table-hover table-striped bg-white" id="tabla-socios">
            <thead class="bg-white">
                <tr>


                <th scope="col" width="200px">Nombre</th>
                <th scope="col">Apellidos</th>
                <th scope="col" width="70px">Alta</th>
                <th scope="col" width="70px">DNI</th>


                <th class="text-center" scope="col">Activación</th>
                </tr>
            </thead>
            <tbody>';

                for($i=0;$i<count($socios);$i++){


                    echo '
                    <tr>


                    <td>'.$socios[$i]["nombre"].'</td>
                    <td width="30%">'.$socios[$i]["apellidos"].'</td>
                    <td width="15%">'.$socios[$i]["alta"].'</td>
                    <td width="10%">'.$socios[$i]["dni"].'</td>

                    <td class="text-center" style="width:100px;"><button class="btn btn-primary btn-activar-socio" data-id="'.$socios[$i]["id"].'" data-toggle="modal" data-target="#modalActivarSocio"><i class="fa fa-pencil-square-o"></i> Activar</button></td>
                    </tr>';
                }

                echo '
            </tbody>
            </table>';
    }

    function existeCuota($id){
        $resultado = consulta("select * from cuotas where idSocio=$id");

        if(count($resultado)!=0){
            return true;
        } else {
            return false;
        }
    }

    function tablaCuotasSocio ($s){
        $datos = consulta("SELECT id, fechapago, fechafin, meses from cuotas where idSocio=$s order by id desc");
        if(!empty($datos)){
            echo '<table class="table table-responsive">
                      <thead class="thead-dark">
                        <tr>
                          <th scope="col" width="30">Fecha de Pago</th>
                          <th scope="col" width="30%">Fin de Cuota</th>
                          <th scope="col" width="20%">Meses</th>
                          <th style="text-align:center;" scope="col" width="20%">Recibos</th>
                          
                        </tr>
                      </thead>
                      <tbody>';
                            
                        foreach ($datos as $indice => $valor){
                           
                            echo '<tr>
                          <th scope="row">'.fecha($valor["fechapago"]).'</th>
                          <td>'.fecha($valor["fechafin"]).'</td>
                          <td style="text-align:center;">'.$valor["meses"].'</td>
                          <td style="text-align:center;"><a target="_blank" href="dompdf/recibo.php?id='.$valor["id"].'"><i class="material-icons">
print
</i></a></td>
                          
                        </tr>';
                        }
                        
                    echo '</tbody>
                </table>';


        } else {
            echo '<small class="text-muted">No hay datos disponibles</small>';
        }
    }


    function getFechaPago($s,$tipo){

        if(existeCuota($s)){
            if($tipo=="pago"){
                 $fecha = consulta("SELECT fechapago, meses from cuotas where idSocio=$s order by id desc limit 1;");
                  $divFecha = explode("-", $fecha[0]["fechapago"]);
             } else {
                 $fecha = consulta("SELECT fechafin, meses from cuotas where idSocio=$s order by id desc limit 1;");
                  $divFecha = explode("-", $fecha[0]["fechafin"]);
             }




            if ($fecha[0]["meses"]==0){
                echo "No renovado";
            } else {
                echo $divFecha[2].'/'.$divFecha[1].'/'.$divFecha[0];
            }
        } else {
            echo "Sin cuota";
        }




    }

    function selectSocios(){
        $resultado = consulta("select id, nsocio, nombre, apellidos from socios where activado=1 order by nsocio;");
        if (count($resultado)!=0){
            echo '<option value="0">Sin aval</option>';
            for($i=0;$i<count($resultado);$i++){
                echo '<option value="'.$resultado[$i]["id"].'">'.$resultado[$i]["nsocio"].' - '.$resultado[$i]["nombre"].' '.$resultado[$i]["apellidos"];
            }
        } else {
              echo '<option value="1">Primer socio</option>';
        }

    }

    function selectGeneticas(){
        
        $resultado = consulta("select id, stock, nombre from geneticas order by nombre asc;");
        if (count($resultado)!=0){
             
            foreach ($resultado as $indice=>$valor){
                if($valor["stock"]==0){
                    echo '<option disabled>'.$valor["nombre"]." (Sin Stock)</option>";
                } else {
                     echo '<option value="'.$valor["id"].'">'.$valor["nombre"]."</option>";
                }
            }
        } else {
              echo '<option disabled>No hay genéticas</option>';
        }

    }
    function selectGeneticasTodas(){
        
        $resultado = consulta("select id, stock, nombre from geneticas order by nombre asc;");
        if (count($resultado)!=0){
             
            foreach ($resultado as $indice=>$valor){
               
                     echo '<option value="'.$valor["id"].'">'.$valor["nombre"]."</option>";
                
            }
        } else {
              echo '<option disabled>No hay genéticas</option>';
        }

    }

    function comprobarCuota($id){
        $resultado = consulta("select * from cuotas where idSocio=$id order by id desc limit 1");

        if(count($resultado)!=0){
            $fecha_actual = strtotime(date("Y-m-d"));
            $fecha_entrada = strtotime($resultado[0]["fechafin"]);

            if($fecha_actual > $fecha_entrada)
                {
                return false;
                }else{
                    return true;
                }
        } else {
            return false;
        }



    }

    function porcentajeCuota($id){

        $consulta = consulta("select * from cuotas where idSocio=$id order by id desc limit 1;");

        if(count($consulta)!=0){

             $numero = new dateTime(date("Y-m-d"));
             $numero2 = new DateTime($consulta[0]["fechapago"]);
             $numero3 = new DateTime($consulta[0]["fechafin"]);
             $diferencia1 =$numero3->diff($numero2)->format("%a");
             $diferencia2 = $numero->diff($numero2)->format("%a");


            // $resultado = $numero3->diff($numero2);
            $division =$diferencia2* 100;
             $resultado = round($division/$diferencia1,0);
             if($resultado>100){
                 $resultado=100;
             }
             return $resultado;

        } else {
            return "Sin cuota";
        }


    }


    function nSociosSinActivar(){
        $numero = consulta("select activado from socios where activado=0");
        echo count($numero);
    }

    function botonPanico($idU){
        session_start();
    
        insert("insert into tablet (estado, fecha, hora, usuario) values (3, CURRENT_DATE, CURRENT_TIME, $idU);");
        session_destroy();

    }
    function ultimosMovimientosGeneticas(){
        $consulta = consulta("select * from reg_gen order by id desc limit 20");

        if(!empty($consulta)){
            echo '<li class="list-group-item kust-group-item-action">
                               <div class="row no-gutters">
                                <div class="col-sm-2 no-gutters">
                                  <b>Fecha y Hora</b>

                                  </div> 
                               <div class="col-sm-1">
                               <b>Cantidad anterior</b>
                                </div>
                                <div class="col-sm-1">
                            <b>Cantidad nueva</b>
                                </div>
                                <div class="col-sm-2">
                                 <b>Nombre genética</b>
                                 </div>
                                <div class="col-sm-2">
                                 <b>Usuario</b>
                                 </div>
                                 <div class="col-sm-2">
                                   <b>Asunto</b>
                                 </div>
                               </div>
                           </li>';

        
        foreach($consulta as $valor){
         
           $usuario = consulta("select nombre, apellidos from usuarios where id = ".$valor["idUsuario"].";");
           $gen = consulta("select nombre from geneticas where id = ".$valor["idGen"].";");

           $clase = "list-group-item-danger";
           $fechaYHora = explode(" ", $valor["fechaHora"]);
           $fechaD = explode("-", $fechaYHora[0]);
           $fecha = $fechaD[2]."/".$fechaD[1]."/".$fechaD[0];
           $horaBruto = explode(":", $fechaYHora[1]);
           $hora = $horaBruto[0].":".$horaBruto[1];


           if(empty($usuario)){
             $nombreUsuario = "Usuario eliminado";
           } else {
             $nombreUsuario = $usuario[0]["nombre"]." ".$usuario[0]["apellidos"];

           }

           if(empty($gen)){
             $nombreGen = "Genética eliminada";
           } else {
             $nombreGen = $gen[0]["nombre"];

           }
            

           if($valor["cantAnterior"] < $valor["cant"]){
            $clase = "list-group-item-success";
           }



            echo '<li class="list-group-item kust-group-item-action '.$clase.'">
                               <div class="row no-gutters">
                                <div class="col-sm-2 no-gutters">
                                  <b><i class="fas fa-calendar-alt mr-1"></i>  '.$fecha.' | '.$hora.'</b>

                                  </div> 
                               <div class="col-sm-1">
                               <i class="fas fa-chevron-circle-left mr-1"></i> '.$valor["cantAnterior"].'Gr. 
                                </div>
                                <div class="col-sm-1">
                            <i class="fas fa-chevron-circle-right ml-3 mr-1"></i>'.$valor["cant"].'Gr.
                                </div>
                                <div class="col-sm-2">
                                 <i">'.$nombreGen.'</i>
                                 </div>
                                <div class="col-sm-2">
                                 <i class="fas fa-user"></i> '.$nombreUsuario.'
                                 </div>
                                 <div class="col-sm-2">
                                    <i class="fas fa-file-alt mr-2"></i> '.$valor["asunto"].'
                                 </div>
                               </div>
                           </li>';
        }
    } else {
        echo '<p>No hay registros</p>';
    }

    }

    function productosApuntoAcabar(){
        $consulta = consulta("select a.nombre, a.stock,ca.nombre as categoria from articulos a inner join categorias_articulos ca on a.categoria = ca.id where a.stock <= 5 order by a.stock");


        for($i=0;$i<count($consulta);$i++){
                $clase="info";
                $cantidad = $consulta[$i]["stock"] . " ud.";
            if($consulta[$i]["stock"]<=3){
                $clase = "warning";
            } if($consulta[$i]["stock"]<=0){
                $clase = "danger";

            }



            echo '<li class="list-group-item list-group-item-action"><span class="badge badge-'.$clase.' mr-2">'.$cantidad.'</span>'.$consulta[$i]["nombre"].'<span class="text-muted categoria">'.$consulta[$i]["categoria"].'</span></li>';
        }

    }

    function dispensarioApuntoAcabar(){
        $consulta = consulta("select nombre, categoria, stock from geneticas where stock <=10 order by stock");


         for($i=0;$i<count($consulta);$i++){
                $clase="info";
                $cantidad = $consulta[$i]["stock"] . " ud.";
            if($consulta[$i]["stock"]<=3){
                $clase = "warning";
            } if($consulta[$i]["stock"]<=0){
                $clase = "danger";

            }



            echo '<li class="list-group-item list-group-item-action"><span class="badge badge-'.$clase.' mr-2">'.$cantidad.'</span>'.$consulta[$i]["nombre"].'<span class="text-muted categoria">'.$consulta[$i]["categoria"].'</span></li>';
        }

    }

     function ultimosSociosRegistrados(){
        $consulta = consulta("select * from socios where activado=1 order by alta desc limit 5");



        for($i=0;$i<count($consulta);$i++){
            $fecha = fecha($consulta[$i]["alta"]);
            if($consulta[$i]["foto"]==""){
                $foto = "perfil.png";
            } else {
                $foto = $consulta[$i]["foto"];
            }
            echo '<a href="perfil.php?s='.$consulta[$i]["id"].'"><li class="list-group-item list-group-item-action">
        <div class="row ultimos-socios">
        <div class="col-1 col-xl-1 col-lg-2">
        <img width="100%" src="img/socios/foto-personal/'.$foto.'">
        </div>
        <div class="col-2 n-socio col-xl-2 col-lg-2">
        #'.$consulta[$i]["nsocio"].'
        </div>
         <div class="col-6 col-xl-6 col-lg-4">
         '.$consulta[$i]["nombre"].' '.$consulta[$i]["apellidos"].'
        </div>
         <div class="col-3 col-xl-3 col-lg-4 text-right">
         '.$fecha.'
        </div>
        </div></li></a>';
        }

    }



    function actualizarCompra($json, $id, $fecha, $total, $flagEditar){
        
        if($flagEditar=="true"){
            deshacerCompra($id);
            actualizarStock($json);
        }
        
        session_start();
        $idMod = $_SESSION["id"];
         if(insert("update retiradas set articulos='".$json."', precio_total = $total, idMod = $idMod, fecha = '$fecha' where id=$id;")==1){
            echo "success";
        } else {
            echo "error";
        }
    }

    function deshacerCompra($id){
        $consulta = consulta("select articulos from retiradas where id=$id;");
        $articulos = json_decode($consulta[0]["articulos"]);
        foreach($articulos as $indice=>$valor){
            if($valor[0]==1){
                insert("update geneticas set stock = stock + ".$valor[2]." where nombre='".$valor[1]."';");

            }
            if($valor[0]==2){
                insert("update articulos set stock = stock + ".$valor[2]." where nombre='".$valor[1]."';");

            }
        }

    }

    function actualizarStock($json){

        $articulos = json_decode($json);
        foreach($articulos as $indice=>$valor){
            if($valor[0]==1){
                insert("update geneticas set stock = stock - ".$valor[2]." where nombre='".$valor[1]."';");
            }
            if($valor[0]==2){
                insert("update articulos set stock = stock - ".$valor[2]." where nombre='".$valor[1]."';");
            }
        }

    }
    function actualizarStockEliminar($json){

        $articulos = json_decode($json);
        foreach($articulos as $indice=>$valor){
            if($valor[0]==1){
                insert("update geneticas set stock = stock + ".$valor[2]." where nombre='".$valor[1]."';");
            }
            if($valor[0]==2){
                insert("update articulos set stock = stock + ".$valor[2]." where nombre='".$valor[1]."';");
            }
        }

    }


    function tablaStock($tipo){
        $consulta=consulta("SELECT * from cajas where fecha = CURRENT_DATE;");

       /* if(count($consulta)!=0){
            echo '<table class="table table-hover table-responsive table-light" style="border-radius:10px;padding:10px;" id="tabla-'.$tipo.'">
                <thead class="bg-info text-white">
                    <tr>

                    <th style="width:70%;" scope="col">Nombre</th>
                    <th class="text-center" style="width:20%;" scope="col">Actual</th>
                    <th style="width:10%;" scope="col">Corrección</th>
                    </tr>
                </thead>
                <tbody>';
                $geneticas = json_decode($consulta[0]["geneticas"]);
                if($tipo=="v"){
                    $t = "verde";
                } else {
                    $t = "marron";
                }
                    for($i=0;$i<count($geneticas);$i++){
                        if($geneticas[$i][3]==$tipo){
                            $consulta2 = consulta("select stock, id from geneticas where nombre = '".$geneticas[$i][0]."' and tipo = '$t'");

                            echo '
                            <tr class="correccion-stock">

                            <td>'.$geneticas[$i][0].'</td>
                            <td class="text-center">'.$consulta2[0]["stock"].' Gr.</td>
                            <td ><input type="number" class="form-control input-stock form-control-sm" anterior="'.$consulta2[0]["stock"].'" id="'.$consulta2[0]["id"].'"></td>


                            </tr>';
                        }

                    }

                    echo '
                </tbody>
                </table>';
        } else {
            echo "Sin genéticas";
        }
    */

        $consulta = consulta("select * from geneticas order by nombre ASC;");

        if(!empty($consulta)){
            echo '<table class="table table-hover table-responsive" style="border-radius:10px;" id="tabla-'.$tipo.'">
                <thead class="bg-white text-grey">
                    <tr>

                    <th style="width:70%;" scope="col">Nombre</th>
                    <th class="text-center d-none" style="width:20%;" scope="col">Actual</th>
                    <th style="width:40%;" scope="col">Peso</th>
                    </tr>
                </thead>
                <tbody>';
                
                $geneticas = $consulta;
                if($tipo=="v"){
                    $t = "verde";
                } else if($tipo=="m"){
                    $t = "marron";
                } else if($tipo=="o"){
                    $t = "otros";
                }

                foreach ($geneticas as $indice=>$valor){
                    if($valor["tipo"]==$t){
                        echo '
                        <tr class="correccion-stock">

                        <td>'.$valor["nombre"].'</td>
                        <td class="text-center d-none">'.$valor["stock"].' Gr.</td>
                        <td ><input type="number" class="form-control input-stock form-control-sm" anterior="'.$valor["stock"].'" id="'.$valor["id"].'"></td>


                        </tr>';
                    }
                }

/*
                    for($i=0;$i<count($geneticas);$i++){
                        if($geneticas[$i][3]==$tipo){
                            $consulta2 = consulta("select stock, id from geneticas where nombre = '".$geneticas[$i][0]."' and tipo = '$t'");

                            echo '
                            <tr class="correccion-stock">

                            <td>'.$geneticas[$i][0].'</td>
                            <td class="text-center">'.$consulta2[0]["stock"].' Gr.</td>
                            <td ><input type="number" class="form-control input-stock form-control-sm" anterior="'.$consulta2[0]["stock"].'" id="'.$consulta2[0]["id"].'"></td>


                            </tr>';
                        }

                    }*/

                    echo '
                </tbody>
                </table>';
        } else {
            echo "Sin genéticas";
        }
        


    }

    function getHistorialPesos(){
        $consulta = consulta("select * from correcciones order by id desc;");

        if(count($consulta)!=0){
            foreach($consulta as $indice=>$valor){
               
                $fechas = explode("-",$valor["fecha"]);
                $fecha = $fechas[2]."/".$fechas[1]."/".$fechas[0];
                $cPersona = consulta("select u.nombre, u.apellidos from usuarios u where id = (select idUsuario from cajas idUsuario where id = ".$valor["id_caja"].")");
                if(count($cPersona)!=0){
                    $nUsuario = $cPersona[0]["nombre"]." ".$cPersona[0]["apellidos"];
                } else {
                    $nUsuario = "Usuario eliminado";
                }

                echo '<div class="bg-white p-3 m-3" style="border-radius:10px;">';
                echo '<div id="accordion">';
                echo '<div class="card">';
                echo ' <div class="card-header bg-success text-white" id="heading'.$valor["id"].'" data-toggle="collapse" data-target="#collapse'.$valor["id"].'" aria-expanded="true" aria-controls="collapse'.$valor["id"].'">';
                echo '<h5 class="float-left mb-0">'.$fecha.'</h5>';
                echo '<span class="float-right text-muted"> <i class="fas fa-user-tag"></i> '.$nUsuario.'</span>';
                echo '</div>';
                echo '<div id="collapse'.$valor["id"].'" class="collapse" aria-labelledby="heading'.$valor["id"].'" data-parent="#accordion">
                     <div class="card-body p-0">';
                echo '<table class="table table-hover table-striped table-responsive">';

                echo '<thead class="bg-dark text-white">';
                echo '<tr>';
                echo '<th style="width:80%">Nombre</th>';

                echo '<th class="text-center">Almacenado</th>';
                echo '<th class="text-center">Corregido</th>';
                echo '<th class="text-center">Margen</th>';
                echo '</tr>';
                echo '</thead>';
                echo '<tbody>';

                $datos = json_decode($valor["geneticas"]);

                foreach($datos as $indice2 => $valor2){
                    $cNombre = consulta("select nombre from geneticas where id = ".$valor2[0].";");
                    if(count($cNombre)!=0){
                        $nombre = $cNombre[0]["nombre"];
                    } else {
                        $nombre = "Genética eliminada";
                    }
                    echo '<tr>';
                    echo '<td>'.$nombre.'</td>';
                    if($valor2[1]<$valor2[2]){
                        $clase = " text-success";
                    } else if($valor2[1]>$valor2[2]) {
                        $clase = " text-danger";
                    } else {
                        $clase = "";
                    }
                    echo '<td class="text-center font-weight-bold">'.$valor2[1].'</td>';
                    echo '<td class="text-center font-weight-bold'.$clase.'">'.$valor2[2].'</td>';
                    echo '<td class="text-center font-weight-bold'.$clase.'">'.round(($valor2[2]-$valor2[1]),2).'</td>';
                    echo '</tr>';
                }

                echo '</tbody>';
                echo '</table>';
                echo '</div>';
                echo '</div>';
                echo '</div>';
                echo '</div>';
                echo '</div>';

            }

        } else {
            echo '<span class="text-muted">No hay datos disponibles</span>';
        }
    }

    function registrarCompra($json, $id, $total, $idCaja){

        session_start();
        //$tiempo = date('Y-m-d H:i:s');
        $id_usuario = $_SESSION["id"];
        $correcto = true;
        $productos = json_decode($json);

        for($i=0;$i<count($productos);$i++){
            nombreProducto($productos[$i][0],$productos[$i][1], $productos[$i][2]);

            if($i==count($productos)-1){

                $articulos = json_encode($productos);
                if(insert("insert into retiradas (idSocio, articulos, fecha, idUsuario, precio_total, manual, idCaja) values ($id, '$articulos', CURRENT_TIMESTAMP, $id_usuario, $total, 1, $idCaja)")==1){
                    echo "success";
                } 
            }
        }
    }

    function nombreProducto($idProducto, $producto, $cantidad){

        if($idProducto=="1"){

            $stockP = consulta("select nombre, stock from geneticas where nombre='$producto';");
            if($stockP[0]["stock"]!=0){
                $nuevoStock = $stockP[0]["stock"] - $cantidad;
                insert("update geneticas set stock=$nuevoStock where nombre='$producto';");

            }


        }

        if($idProducto=="2"){

            $stockP = consulta("select nombre, stock from articulos where nombre='$producto';");
            if($stockP[0]["stock"]!=0){
                $nuevoStock = $stockP[0]["stock"] - $cantidad;
                insert("update articulos set stock=$nuevoStock where nombre='$producto';");
            }
        }



    }

    function sociosApuntoCaducar(){
        $consulta = consulta("select c.idSocio as id, c.fechafin as fechafin, s.nombre as nombre, s.apellidos as apellidos, s.id as ids, s.foto as foto, s.nsocio as nsocio from cuotas c  inner join socios s on c.idSocio = s.id where fechafin between date_sub(now(), interval 0 month) and date_add(now(), interval 1 month) order by fechafin asc limit 5");


        if(count($consulta)!=0){

          foreach($consulta as $indice=>$valor){
            $fechaD = explode("-", $valor["fechafin"]);
            $fecha = $fechaD[2]."/".$fechaD[1]."/".$fechaD[0];
            $socio = $valor["nombre"]." ".$valor["apellidos"];
            if($valor["foto"]==""){
                $foto = "perfil.png";

            } else {
                $foto = $valor["foto"];
            }
            echo '<a href="perfil.php?s='.$valor["id"].'"><li class="list-group-item list-group-item-warning list-group-item-action">
                    <div class="row ultimos-socios">
                    <div class="col-1 col-xl-1 col-lg-2">
                    <img width="100%" src="img/socios/foto-personal/'.$foto.'">
                    </div>
                    <div class="col-2 n-socio col-xl-2 col-lg-2">
                    #'.$valor["nsocio"].'
                    </div>
                     <div class="col-6 col-xl-6 col-lg-4">
                     '.$socio.'
                    </div>
                     <div class="col-3 col-xl-3 col-lg-4 text-right">
                     '.$fecha.'
                    </div>
                    </div></li></a>';
                    }
          }

          echo '<a href="socios.php?f=proximas-renovaciones"><li class="list-group-item list-group-item-action text-center">
                    Ver más <i class="fas fa-angle-right"></i></li></a>';
        }

        function ultimasVisitas(){
            $consulta = consulta("select id, ultimo_acceso, nombre, apellidos, foto, nsocio from socios order by ultimo_acceso desc limit 5");


            if(count($consulta)!=0){

              foreach($consulta as $indice=>$valor){
                $fechaYHora = explode(" ", $valor["ultimo_acceso"]);
                $fechaD = explode("-", $fechaYHora[0]);
                $fecha = $fechaD[2]."/".$fechaD[1]."/".$fechaD[0];
                $horaBruto = explode(":", $fechaYHora[1]);
                $hora = $horaBruto[0].":".$horaBruto[1];
                $socio = $valor["nombre"]." ".$valor["apellidos"];
                if($valor["foto"]==""){
                    $foto = "perfil.png";

                } else {
                    $foto = $valor["foto"];
                }
                if($valor["ultimo_acceso"]!="0000-00-00 00:00:00"){
                    echo '<a href="perfil.php?s='.$valor["id"].'"><li class="list-group-item list-group-item-action">
                            <div class="row ultimos-socios">
                            <div class="col-1 col-xl-1 col-lg-2">
                            <img width="100%" src="img/socios/foto-personal/'.$foto.'">
                            </div>
                            <div class="col-2 n-socio col-xl-2 col-lg-2">
                            #'.$valor["nsocio"].'
                            </div>
                             <div class="col-6 col-xl-6 col-lg-4">
                             '.$socio.'
                            </div>
                             <div class="col-3 col-xl-3 col-lg-4 text-right">
                             <small>'.$fecha.' a las '.$hora.'</small>
                            </div>
                            </div></li></a>';
                }


                        }
              }

              echo '<a href="socios.php?f=ultima-visita"><li class="list-group-item list-group-item-action text-center">
                        Ver más <i class="fas fa-angle-right"></i></li></a>';
            }


function listaSociosAvalados($id){
    $consulta = consulta("select nombre, apellidos, id from socios where avalador = '$id' order by nombre;");
    if(count($consulta)!=0){
        echo '<ul class="list-group">';
        foreach($consulta as $indice => $valor){
            echo '<a href="perfil.php?s='.$valor["id"].'"><li class="list-group-item list-group-item-action list-group-item-light">';
            echo $valor["nombre"]." ".$valor["apellidos"];
            echo "</li></a>";
        }
        echo "</ul>";
    } else {
        echo '<small class="text-muted">No ha avalado a nadie.</small>';
    }

}


function datosInicio($n){
    
    if($n==1){
        $fecha = consulta("select CURRENT_DATE as fecha");
        $fechaActual = $fecha[0]["fecha"];
      //  $consulta = consulta("select id from retiradas where fecha between '$fechaActual 00:00:00' and '$fechaActual 23:59:59';");
        if($_SESSION["idCaja"]!=""){
            $consulta = consulta("select id from retiradas where idCaja = ".$_SESSION["idCaja"].";");
            echo count($consulta);
        } else {
            echo "0";
        }
       
    }

    if($n==2){

        $consulta = consulta("select id from socios where alta = CURRENT_DATE");
        echo count($consulta);
    }

    if($n==3){
        if($_SESSION["idCaja"]!=""){
            $fecha = consulta("select CURRENT_DATE as fecha");
            $fechaActual = $fecha[0]["fecha"];
            $consulta = consulta("select precio_total from retiradas where idCaja = ".$_SESSION["idCaja"].";");
            $consulta2 = consulta("select cuota from cuotas where idCaja = ".$_SESSION["idCaja"].";");
            $total = 0;
            if(count($consulta)!=0){
                
                foreach($consulta as $indice => $valor){
                    $total+=$valor["precio_total"];
                }
                
            } 
            if(!empty($consulta2)){
                foreach ($consulta2 as $indice => $valor){
                    $total+=$valor["cuota"];
                }
            }

            echo $total;

        } else {
            echo "0";
        }
        

    }
}

function eliminarTransaccion($id, $json, $flagEditar){
    if($flagEditar=="true"){
        actualizarStockEliminar($json);
    }
    $insert = insert("delete from retiradas where id=$id;");
    if($insert==1){
        echo "success";
    } else {
        echo "error";
    }
}

function mostrarRequisitosSocio($re){
    $r1 = explode(":", $re[0]);
    $r2 = explode(":", $re[1]);
    $r3 = explode(":", $re[2]);
    $r4 = explode(":", $re[3]);

    if($r1[1]=="true"){
        echo '<div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input" id="check1" checked disabled>
        <label class="custom-control-label" for="check1">SER CONSUMIDOR/A DE CANNABIS SATIVA L.</label>
      </div>';
    }
    if($r2[1]=="true"){
        echo '<div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input" id="check1" checked disabled>
        <label class="custom-control-label" for="check2">SER CONSUMIDOR/A DE TABACO</label>
      </div>';
    }
    if($r3[1]=="true"){
        echo '<div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input" id="check1" checked disabled>
        <label class="custom-control-label" for="check3">SER CONSUMIDOR/ A DE PLANTAS CON PROPIEDADES TERAPÉUTICAS</label>
      </div>';
    }
    if($r4[1]=="true"){
        echo '<div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input" id="check1" checked disabled>
        <label class="custom-control-label" for="check4">SER CONSUMIDOR/A HABITUAL DE CANNABIS POR RAZONES TERAPÉUTICAS, <small>y tener diagnosticada una enfermedad para la que el cannabis tiene efectos terapéuticos según la IACM, o que el uso paliativo de los cannabinoides ha sido probado científicamente y aportar certificado médico acreditativo de la solicitud de uso de cannabis por parte del paciente sin oposición médica, o tener una licencia de uso de cannabis de cualquier país del Mundo.</small></label>
      </div>';
    }

}
function mostrarDocuAportada($re){
    $r1 = explode(":", $re[0]);
    $r2 = explode(":", $re[1]);

    if($r1[1]=="true"){
        echo '<div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input" id="check1" checked disabled>
        <label class="custom-control-label" for="check1">Copia del DNI</label>
      </div>';
    }
    if($r2[1]=="true"){
        echo '<div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input" id="check1" checked disabled>
        <label class="custom-control-label" for="check2">Documentación médica</label>
      </div>';
    }


}


function nAvaladores($socio){
    $consulta = consulta("select id from socios where avalador=$socio;");
    return count($consulta);
}


function comprobarCajasAbiertas(){

    $cFecha = consulta("SELECT date_sub(CURRENT_DATE, interval 1 week) as fecha, date_sub(CURRENT_DATE, interval 1 day) as hoy");
    $fecha = $cFecha[0]["fecha"];
    $hoy = $cFecha[0]["hoy"];
    $consulta = consulta("select * from cajas where hora='00:00:00' and fecha!=CURRENT_DATE order by id desc");
    $cajas=[];
    $ultimo="";
    //echo "select * from retiradas where fecha BETWEEN '".$fecha." 00:00:00' and '".$hoy." 23:59:59' order by id desc</br>";
    foreach($consulta as $indice => $valor){
       array_push($cajas, $valor);
    }

    echo json_encode($cajas);
}

function getGastosSocio($id){
    $consulta = consulta("select precio_total from retiradas where idSocio = $id;");
    $total = 0;
    foreach ($consulta as $valor){
        $total+=$valor["precio_total"];
    }
    return $total;
}

function subirDocu($zona,$id){
  
    $estado = consulta("select dni, documentacion from socios where id=$id;");

    if($zona==1){

        if($estado[0]["documentacion"]==""){
        
        $json = '["'.$id.'-1.png",""]';
        insert("update socios set documentacion='$json' where id=$id;");

        $nombre_archivo =$_FILES['docu1']['name'];
        $tipo_archivo = $_FILES['docu1']['type'];
        $tamano_archivo = $_FILES['docu1']['size'];
        $archivo= $_FILES['docu1']['tmp_name'];

        if (!((strpos($tipo_archivo, "jpeg") || strpos($tipo_archivo, "png")) && ($tamano_archivo  < 50000000))) 
        {
            $error = "El tamaño de los archivos no es correcta. Se permiten archivos de 5 Mb máximo.";
        }
        else
        {
            $file = $_FILES['docu1']['name'];
            
            $nombre= $id."-1.png";
           
            $dirtemp = "../img/socios/documentacion/".$nombre;//Directorio temporaral para subir el fichero

            if (is_uploaded_file($_FILES['docu1']['tmp_name'])) {
                copy($_FILES['docu1']['tmp_name'], $dirtemp);

              //  unlink($dirtemp); //Borrar el fichero temporal
               }
            else
            {
               $error = "Ocurrió algún error al subir el fichero. No pudo guardarse.";
            }

        }

        if($error==""){

            header("Location: ../perfil.php?s=".$id);
        } else {
            header("Location: ../perfil.php?s=".$id."&e=1");
        }



       /* define('UPLOAD_DIR', '../../img/socios/dni/');

        $img = $imagen;
        $img = str_replace('data:image/jpeg;base64,', '', $img);
        $data = base64_decode($img);
        $file = UPLOAD_DIR . $estado[0]["id"] . '-1.png';
        $success = file_put_contents($file, $data);
        

            echo "success";*/


    } else {
        $array = json_decode($estado[0]["documentacion"]);
        $array[0]= $id."-1.png";

        $json = json_encode($array);

        insert("update socios set documentacion='$json' where id=$id;");

         $nombre_archivo =$_FILES['docu1']['name'];
        $tipo_archivo = $_FILES['docu1']['type'];
        $tamano_archivo = $_FILES['docu1']['size'];
        $archivo= $_FILES['docu1']['tmp_name'];

        if (!((strpos($tipo_archivo, "jpeg") || strpos($tipo_archivo, "png")) && ($tamano_archivo  < 50000000))) 
        {
            $error = "El tamaño de los archivos no es correcta. Se permiten archivos de 5 Mb máximo.";
        }
        else
        {
            $file = $_FILES['docu1']['name'];
            
            $nombre= $id."-1.png";
           
            $dirtemp = "../img/socios/documentacion/".$nombre;//Directorio temporaral para subir el fichero

            if (is_uploaded_file($_FILES['docu1']['tmp_name'])) {
                copy($_FILES['docu1']['tmp_name'], $dirtemp);

              //  unlink($dirtemp); //Borrar el fichero temporal
               }
            else
            {
               $error = "Ocurrió algún error al subir el fichero. No pudo guardarse.";
            }

        }

        

        if($error==""){

            header("Location: ../perfil.php?s=".$id);
        } else {
            header("Location: ../perfil.php?s=".$id."&e=1");
        }
        /*
        define('UPLOAD_DIR', '../../img/socios/dni/');

        $img = $imagen;
        $img = str_replace('data:image/jpeg;base64,', '', $img);
        $data = base64_decode($img);
        $file = UPLOAD_DIR . $estado[0]["id"] . '-1.png';
        $success = file_put_contents($file, $data);
        

            echo "success";


    }*/
    }
    

}

    if($zona==2){

        if($estado[0]["documentacion"]==""){
        
        $json = '["","'.$id.'-2.png"]';
        insert("update socios set documentacion='$json' where id=$id;");

        $nombre_archivo =$_FILES['docu2']['name'];
        $tipo_archivo = $_FILES['docu2']['type'];
        $tamano_archivo = $_FILES['docu2']['size'];
        $archivo= $_FILES['docu2']['tmp_name'];

        if (!((strpos($tipo_archivo, "jpeg") || strpos($tipo_archivo, "png")) && ($tamano_archivo  < 50000000))) 
        {
            $error = "El tamaño de los archivos no es correcta. Se permiten archivos de 5 Mb máximo.";
        }
        else
        {
            $file = $_FILES['docu2']['name'];
            
            $nombre= $id."-2.png";
           
            $dirtemp = "../img/socios/documentacion/".$nombre;//Directorio temporaral para subir el fichero

            if (is_uploaded_file($_FILES['docu2']['tmp_name'])) {
                copy($_FILES['docu2']['tmp_name'], $dirtemp);

              //  unlink($dirtemp); //Borrar el fichero temporal
               }
            else
            {
               $error = "Ocurrió algún error al subir el fichero. No pudo guardarse.";
            }

        }

        if($error==""){

            header("Location: ../perfil.php?s=".$id);
        } else {
            header("Location: ../perfil.php?s=".$id."&e=1");
        }



       /* define('UPLOAD_DIR', '../../img/socios/dni/');

        $img = $imagen;
        $img = str_replace('data:image/jpeg;base64,', '', $img);
        $data = base64_decode($img);
        $file = UPLOAD_DIR . $estado[0]["id"] . '-1.png';
        $success = file_put_contents($file, $data);
        

            echo "success";*/


    } else {
        $array = json_decode($estado[0]["documentacion"]);
        $array[1]= $id."-2.png";

        $json = json_encode($array);

        insert("update socios set documentacion='$json' where id=$id;");

         $nombre_archivo =$_FILES['docu2']['name'];
        $tipo_archivo = $_FILES['docu2']['type'];
        $tamano_archivo = $_FILES['docu2']['size'];
        $archivo= $_FILES['docu2']['tmp_name'];

        if (!((strpos($tipo_archivo, "jpeg") || strpos($tipo_archivo, "png")) && ($tamano_archivo  < 50000000))) 
        {
            $error = "El tamaño de los archivos no es correcta. Se permiten archivos de 5 Mb máximo.";
        }
        else
        {
            $file = $_FILES['docu2']['name'];
            
            $nombre= $id."-2.png";
           
            $dirtemp = "../img/socios/documentacion/".$nombre;//Directorio temporaral para subir el fichero

            if (is_uploaded_file($_FILES['docu2']['tmp_name'])) {
                copy($_FILES['docu2']['tmp_name'], $dirtemp);

              //  unlink($dirtemp); //Borrar el fichero temporal
               }
            else
            {
               $error = "Ocurrió algún error al subir el fichero. No pudo guardarse.";
            }

        }

        

        if($error==""){

            header("Location: ../perfil.php?s=".$id);
        } else {
            header("Location: ../perfil.php?s=".$id."&e=1");
        }
        /*
        define('UPLOAD_DIR', '../../img/socios/dni/');

        $img = $imagen;
        $img = str_replace('data:image/jpeg;base64,', '', $img);
        $data = base64_decode($img);
        $file = UPLOAD_DIR . $estado[0]["id"] . '-1.png';
        $success = file_put_contents($file, $data);
        

            echo "success";


    }*/
    }
    

}
}

function errorDePermisos(){
              echo ' <div class="content-wrapper animated fadeIn">
            <div class="container"><div class="alert alert-danger alert-dismissible fade show" role="alert">
                              No tienes permisos para acceder a esta página.
                              
                            </div></div></div>';
}

function comprobarCaja(){
    $datos = consulta("select hora from cajas where fecha = CURRENT_DATE and hora = '00:00:00';");

    if(empty($datos)){
        
        echo "false";
    } else {
        echo "true";
    }
}

function borrarDatos($fecha1,$fecha2){
    $con1 = insert("delete from cajas where fecha between '".$fecha1."' and '".$fecha2."';");
    $con2 = insert("delete from retiradas where fecha BETWEEN '".$fecha1." 00:00:00' and '".$fecha2." 23:59:59';");
    $con3 = insert("delete from correcciones wherefe fecha BETWEEN '".$fecha1."' and '".$fecha2."';");

    if($con1>0 && $con2>0){
        echo "ok";
    } else {
        echo "error";
    }


}

function modificarStock($accion, $idGen, $cantidad, $asunto, $user){
    $ok = true;
    $ant = consulta("select stock from geneticas where id = $idGen;");
    $anterior = $ant[0]["stock"];

    if($accion=="retirar"){
        $resta = $anterior - $cantidad;
        if($resta>=0){
            $i1 = insert("insert into reg_gen (idGen, cant, cantAnterior, idUsuario, fechaHora, asunto) values ($idGen, $resta, $anterior, $user, CURRENT_TIMESTAMP, '$asunto');");
            $i2 = insert("update geneticas set stock = $resta where id=$idGen;");
        } else {
            $ok = false;
        }
       

    } else {
        $suma = $anterior + $cantidad;
        $i1 = insert("insert into reg_gen (idGen, cant, cantAnterior, idUsuario, fechaHora, asunto) values ($idGen, $suma, $anterior, $user, CURRENT_TIMESTAMP, '$asunto');");
        $i2 = insert("update geneticas set stock = $suma where id=$idGen;");
    }

    if($i1!=1){
        $ok = false;
    }

    if($i2!=1){
        $ok = false;
    }

    echo $ok;
}


?>
