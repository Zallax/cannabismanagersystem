<?php

     require_once("php/config.php");
  require_once ("php/funciones.php");
  require_once ("php/conexion.php");
  require_once("php/sesion.php");
  require_once("php/notificaciones.php");

    function cabecera($seccion){
        $nombre = getNombre();
        if($seccion=="inicio"){
          $inicioActivo=" inicioActivo";
          $sel[0]="menu-seleccionado";
        } else {
          $inicioActivo="";
        }
  $idUsuario = $_SESSION["id"];
  $tablet = consulta("select *, CURRENT_DATE as f_actual from tablet where id = (select max(id) from tablet);");
  if($_SESSION["tipo"]!=1){
  
  $caja = consulta("select * from cajas where id = (select max(id) from cajas where idUsuario = $idUsuario) and fecha = CURRENT_DATE; ");
 
  } else {
    $caja = consulta("select * from cajas where id = (select max(id) from cajas where hora = '00:00:00') and fecha = CURRENT_DATE; ");
  }
  if (!empty($caja)){
    $idCaja = $caja[0]["id"];
    $_SESSION["idCaja"]=$idCaja;
  } else {
    $idCaja="";
  }
  $consultaConfig = consulta("select * from config;");
  $nombreA = $consultaConfig[0]["valor"];
  $nombreC = $consultaConfig[1]["valor"];
  $_SESSION["pdescuento"]=$consultaConfig[2]["valor"];
 
  if(count($tablet)==1){

    if($tablet[0]["fecha"] != $tablet[0]["f_actual"]){

      $estadoTablet = '<i class="fas fa-lock-open"></i> <small>ABRIR CAJA</small>';
      $eTablet = "no";
    } else {
      if($tablet[0]["estado"] == 0){
        $estadoTablet = '<i class="fas fa-lock-open"></i> <small>ABRIR CAJA</small>';
        $eTablet = "no";
      } else if($tablet[0]["estado"] == 2){
         $estadoTablet = '<i class="fas fa-lock"></i> <small> ABRIR CAJA</small>'; 
         $eTablet = "no";
      } else if($tablet[0]["estado"] == 3){
         $estadoTablet = '<i class="fas fa-lock-open"></i> <small> DESBLOQUEAR TABLET</small>'; 
         $eTablet = "bloqueada";
      } else {
        $estadoTablet = '<i class="fas fa-lock"></i> <small>CAJA ABIERTA</small>';
        $eTablet = "si";

      }
    }
  } else {
    $estadoTablet = '<i class="fas fa-lock-open"></i> <small>CAJA ABIERTA</small>';
    $eTablet = "no";
  }

        ?>
<!DOCTYPE html>
<html lang="es">

<head>
  <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Abel|Roboto|Roboto+Condensed" rel="stylesheet">

  <meta name="robots" content="noindex, nofollow">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title><?php echo $nombreA;?></title>
  <link rel="icon" type="image/png" href="img/logo.png" />
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
            rel="stylesheet">
  <!-- Bootstrap core CSS-->
  <!--<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">-->
  <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
  <!-- Custom fonts for this template-->
   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">


  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

  <!-- Page level plugin CSS-->
  <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/animate.css">
</head>

<body class="fixed-nav sticky-footer sidenav-toggled" style="background-color:#f9fafb !important;" id="page-top">

  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">¿Deseas cerrar la sesión?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body"><p>Al cerrar sesión, el dispositivo de carta quedará bloqueado.</p><p>Para desbloquearlo vuelve a iniciar sesión.</p></div>
        <div class="modal-footer">
          
          <a class="btn btn-primary" href="login.php">Cerrar sesión</a>
        </div>
      </div>
    </div>
  </div>
  <!-- Navigation--> <nav class="navbar navbar-expand-lg navbar-light bg-white fixed-top" id="mainNav">

    <a class="navbar-brand" href="index.php"><img src="img/logo.png" class="mr-3" width="30px"><?php echo $nombreC;?></a> <button class="btn btn-dark btn-tablet" id-caja="<?php echo $idCaja; ?>" usuario="<?php echo $_SESSION["id"]; ?>" abierta="<?php echo $eTablet; ?>"><?php echo $estadoTablet ?></button>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav" id="zonaMenu">


        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Inicio">
          <a class="nav-link <?php echo $sel[0]; ?>" href="index.php">
            <i class="fa fa-fw fa-dashboard <?php echo $inicioActivo;?>"></i>
            <span class="nav-link-text <?php echo $inicioActivo;?>">Inicio</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Socios">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#expandir-mostrador" data-parent="#expandir-mostrador">
            <i class="fas fa-cash-register text-danger"></i>
            <span class="nav-link-text"> Mostrador</span>
          </a>
          <ul class="sidenav-second-level collapse" id="expandir-mostrador">
            
            <li>
              <a href="caja.php" id="m-caja"><i class="fas fa-chevron-right"></i> <span class="nav-link-text"> Caja</span></a>
            </li>
            <li>
              <a href="pesaje.php" id="m-pesaje"><i class="fas fa-chevron-right"></i> <span class="nav-link-text"> Pesaje</span></a>
            </li>
            <?php
              if($_SESSION["tipo"]==1){


            ?>
            <li>
              <a href="ver-cajas.php" id="d-cajas">
                <i class="fas fa-chevron-right"></i>
                <span class="nav-link-text">Historial de Cajas</span>
              </a>
            </li>
            <li>
              <a href="ventas.php" id="d-ventas">
                <i class="fas fa-chevron-right"></i>
                <span class="nav-link-text">Datos de ventas</span>
              </a>
            </li>
            <li>
              <a href="transacciones.php" id="d-trans">
                <i class="fas fa-chevron-right"></i>
                <span class="nav-link-text">Transacciones</span>
              </a>
            </li>
            <?php
              }
            ?>
          </ul>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Socios">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#expandir" data-parent="#expandir">
            <i class="fa fa-fw fa-users text-info"></i>
            <span class="nav-link-text">Socios</span>
          </a>
          <ul class="sidenav-second-level collapse" id="expandir">
            <li>
              <a href="socios.php" id="l-socios">
               <i class="fas fa-chevron-right"></i>
             Lista de Socios</a>
            </li>
            <li>
              <a href="activaciones.php" id="activaciones">
                 <i class="fas fa-chevron-right"></i>Activaciones <span class="badge badge-pill" style="border-radius:5px; background-color:#80af5b9c;"> <?php nSociosSinActivar(); ?></span></a>
            </li>

            
          </ul>
        </li>



        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Genéticas">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#expandir-dispensario" data-parent="#zonaMenu">
            <i class="fa fa-fw fa-pagelines text-success"></i>
            <span class="nav-link-text">Dispensario</span>
          </a>
          <ul class="sidenav-second-level collapse" id="expandir-dispensario">
          
            <li>
              <a href="marihuana.php" id="m-marihuana">
               <i class="fas fa-chevron-right"></i>Marihuana</a>
            </li>
            <li>
              <a href="marihuana.php?m" id="m-hachis">
               <i class="fas fa-chevron-right"></i>Hachis</a>
            </li>
           
            <li>
              <a href="retirar-stock.php" id="m-r-stock">
               <i class="fas fa-plus"></i>Modificar stock</a>
            </li>
            <li>
              <a href="crear-transaccion.php" id="t-socios">
               <i class="fas fa-plus"></i>Crear transacción</a>
            </li>
          </ul>
        </li>
       

        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Link">
          <a class="nav-link" href="articulos.php">
            <i class="fas fa-shopping-cart text-warning"></i>
            <span class="nav-link-text">Artículos</span>
          </a>
        </li>
        <?php
          if($_SESSION["tipo"]==1){


        ?>

        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Link">
          <a class="nav-link" href="usuarios.php">
             <i class="fas fa-user-alt" style="color:#941894"></i>
            <span class="nav-link-text">Usuarios</span>
          </a>
        </li>


        
         <?php
        }
        ?>


        <?php
          if($_SESSION["tipo"]==1){


        ?>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Control de tablet">
          <a class="nav-link" href="historial-tablet.php">
            <i class="fas fa-mobile-alt" style="color:#123c71;"></i>
            <span class="nav-link-text">Control de Tablet</span>
          </a>
        </li>
         <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Configuracion">
          <a class="nav-link" href="configuracion.php">
            <i class="fas fa-cogs" style="color:#821e1e;"></i>
            <span class="nav-link-text">Configuración</span>
          </a>
        </li>
        <?php
        }
        ?>
       
      </ul>
      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
        
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle mr-lg-2" id="alertsDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-fw fa-bell"></i>
            <span class="d-lg-none">Notificaciones
              <?php

             
              if($_SESSION["notificaciones"]){
                echo '<span class="badge badge-pill badge-warning">nuevas</span>';
              }
              

              ?>
            </span>
            <?php
            if($_SESSION["notificaciones"]){
              echo '<span class="indicator text-warning d-none d-lg-block">
              <i class="fa fa-fw fa-circle"></i>
            </span>';
            }
            
            ?>
            
          </a>
          <div class="dropdown-menu animated zoomIn dos" id="zona-notificaciones" aria-labelledby="alertsDropdown">
            <img src="img/cargando.svg">
          </div>
        </li>
        <!--<li class="nav-item">
          <form class="form-inline my-2 my-lg-0 mr-lg-2">
            <div class="input-group">
              <input class="form-control" type="text" placeholder="Buscar...">
              <span class="input-group-append">
                <button class="btn btn-primary" type="button">
                  <i class="fa fa-search"></i>
                </button>
              </span>
            </div>
          </form>
        </li>-->
        <li class="nav-item">
          <div class="dropdown show">
            <a class="btn text-dark dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fa fa-fw fa-user"></i> <?php echo $_SESSION["nombre"]; ?>
            </a>

            <div class="dropdown-menu animated zoomIn dos dropdown-menu-right" aria-labelledby="dropdownMenuLink">
              <a class="dropdown-item" href="perfil-usuario.php?id=<?php echo $_SESSION["id"]; ?>"><i class="fa fa-fw fa-user"></i> Mi perfil</a>
              <a class="dropdown-item" data-toggle="modal" data-target="#exampleModal" href="#"><i class="fa fa-fw fa-sign-out"></i> Salir</a>
            </div>
          </div>

        </li>
        <li class="nav-item">
           <button style="margin-top:1px;margin-left:10px;" data-toggle="tooltip" data-placement="bottom" title="¡PÁNICO!" id="btnPanico" data-id="<?php echo $_SESSION["id"];?>" class="btn btn-danger"><i class="fa fa-exclamation-triangle"></i></button>

        </li>
      </ul>
    </div>
  </nav>
  <div id="mensajes"></div>    
  <?php

}
?>