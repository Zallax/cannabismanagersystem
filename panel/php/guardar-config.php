<?php
	  require_once ("conexion.php");
	if(count($_POST)!=0){
	  $error="";
	  extract($_POST);
	  if($nombreA!=""){
	    insert("update config set valor='$nombreA' where id=1");
	  }
	  if($ncorto!=""){
	    insert("update config set valor='$ncorto' where id=2");
	  }
	  if(isset($_FILES["logo"])){
	    $nombre_archivo =$_FILES['logo']['name'];
	           $tipo_archivo = $_FILES['logo']['type'];
	           $tamano_archivo = $_FILES['logo']['size'];
	           $archivo= $_FILES['logo']['tmp_name'];
	       } else{
	           $nombre_archivo="";
	       }

	       if ($nombre_archivo!="")
	       {
	           //Limitar el tipo de archivo y el tamaño    
	           if (!((strpos($tipo_archivo, "gif") || strpos($tipo_archivo, "jpeg") || strpos($tipo_archivo, "png")) && ($tamano_archivo  < 50000000))) 
	           {
	               $error = "El tamaño de los archivos no es correcta. Se permiten archivos de 5 Mb máximo.";
	           }
	           else
	           {
	               $file = $_FILES['logo']['name'];
	              
	               $nombre= "logo.png";
	               $dirtemp = "../img/".$nombre;//Directorio temporaral para subir el fichero

	               if (is_uploaded_file($_FILES['logo']['tmp_name'])) {
	                   copy($_FILES['logo']['tmp_name'], $dirtemp);

	                 //  unlink($dirtemp); //Borrar el fichero temporal
	                  }
	               else
	               {
	                  $error = "Ocurrió algún error al subir el fichero. No pudo guardarse.";
	               }

	           }
	  }
	  
	 	if($error==""){
	 		header("Location: ../configuracion.php?s");
	 	} else {
	 		header("Location: ../configuracion.php?e");
	 	}
	} else {
		header("Location: ../configuracion.php?e");
	}

?>