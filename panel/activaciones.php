<?php
  require_once("php/config.php");
  require_once ("php/funciones.php");
  include "php/cabecera.php";
?>

 <?php cabecera("activaciones");?>
           <nav>
            <div class="modal fade2" style="display: none !important;" id="modalActivarSocio" tabindex="-1" role="dialog" aria-labelledby="modalActivarSocioLabel" aria-hidden="true">
              <div class="modal-dialog" role="document" style="max-width: 95%;" >
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="modalActivarSocioLabel">Activar Socio</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body" id="contenidoActivarSocio">
                    <div class="text-center"><img src="img/cargando.gif" style="height:128px;"></div>
                  </div>
                  <div class="modal-footer" style="justify-content: normal;">

                    <button type="button" id="btnEliminarSocio" class="btn btn-danger" style="width:100px;"><i class="fa fa-trash"></i> Eliminar</button>
                   <div style="width:100%;">

                    <button style="float:right;" type="button" id="btnActivarSocio" class="btn btn-primary"><i class="fa fa-save"></i> Activar Socior</button>
                  </div>
                </div>
                </div>
              </div>
            </div>
            </nav>
  <div class="content-wrapper animated fadeIn">
    <div class="container">

      
        <div class="card border-info">
          <div class="card-body">
          <div class="icono-fondo" style="top:3% !important;">
                      <i class="fas fa-users"></i>
          </div>
          <div class="titulo-seccion"><span>Activaciones</span></div>

        <div id="zona-activaciones">
          <div class="text-center"><img src="img/cargando.svg"></div>
        </div>

        </div>
       </div>


    <nav>
          <div class="modal fade2" style="display: none !important;" id="modalDatosSocio" tabindex="-1" role="dialog" aria-labelledby="modalDatosSocioLabel" aria-hidden="true">
            <div class="modal-dialog" role="document" style="max-width: 95%;" >
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="modalDatosSocioLabel">Información del Socio</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body" id="contenidoModalDatosSocio">
                  <div class="text-center"><img src="img/cargando.gif" style="height:128px;"></div>
                </div>
                <div class="modal-footer">
                  <div id="zonaEliminar">
                  <button type="button" id="btnEliminarGenetica" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                  </div>
                  <div id="confirmarEliminar" style="display:none">
                  <span style="margin-right:10px">¿Estás seguro?</span>
                  <button class="btn btn-dark" id="btnSiEliminar"><i class="fa fa-check"></i></button>
                  <button class="btn btn-danger" id="btnNoEliminar"><i class="fa fa-times"></i></button>
                  </div>
                  <button type="button" id="btnEditarGenetica" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
                </div>
              </div>
            </div>
          </div>
          </nav>

  <?php include "php/footer.php";?>
    <script src="js/socios.js"></script>
    <script type="text/javascript">
      cargarActivaciones();
      if(opcionLateral!="si"){
        $("#activaciones").addClass("menu-seleccionado");
        $("#expandir").collapse('toggle');
      }
    </script>
  </div>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/plugins/piexif.min.js" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/plugins/sortable.min.js" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/plugins/purify.min.js" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/fileinput.min.js"></script>

  <script src="js/locales/es.js"></script>

  <script src="themes/fa/theme.js"></script>
</body>

</html>
