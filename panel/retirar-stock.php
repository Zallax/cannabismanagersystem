<?php

  include "php/cabecera.php";
?>

 <?php cabecera("cannabis");?>
 
 

  <div class="content-wrapper animated fadeIn background-container">
    <div class="container">








        <div class="row">
          <div class="col-sm-12 boton-a-genetica">
            <div class="card border-success" >
              <div class="card-body">
                <div class="icono-fondo">
                          <i class="fas fa-weight"></i>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="titulo-seccion"><h5>Modificar Stock</h5>
                    <p class="text-muted">Registra los cambios de pesaje para los preparados de cannabis.</p>
                  </div>
                  
                </div>
                
              </div>
              </div>
            </div>


          </div>

        </div>
       
      <div class="card mb-3 mt-3">
              <div class="card-header bg-white">
               <h5 class="card-title">Retirar Marihuana</h5>
              </div>
              <div class="text-center"><img src="img/cargando.svg" id="cargando-retirar" style="height:128px;display:none;"></div>
              <div class="card-body row" id="zona-retirar">
                <div class="col-12 mb-2 text-muted">
                    Selecciona la genética, la cantidad en gramos a retirar del Stock y el motivo por el que retiras la cantidad.
                </div>

                <div class="form-group col-sm-4 ">
                   <label for="s-genetica">Genética</label>
                   <select class="form-control s-genetica">
                   <option value="sin">Selecciona una genética...</option>
                     <?php selectGeneticas(); ?>
                   </select>
                 </div>
                 <div class="form-group col-sm-2">
                     <label for="cantidad">Cantidad</label>
                     <input type="number" min="0.1" max="500" class="form-control cantidad">
                     
                   </div>
                   <div class="form-group col-sm-6">
                       <label for="asunto">Asunto</label>
                       <input type="text" class="form-control asunto" >
                       
                     </div>
              </div>
              <button class="btn btn-lg btn-success btn-block btn-guardar-retirar">Crear registro</button>

            </div>
            <div class="card mb-3 mt-3">
                    <div class="card-header bg-white">
                     <h5 class="card-title">Aportar Marihuana</h5>
                    </div>
                    <div class="text-center"><img src="img/cargando.svg" id="cargando-aportar" style="height:128px;display:none;"></div>
                    <div class="card-body row" id="zona-aportar">
                      <div class="col-12 mb-2 text-muted">
                          Selecciona la genética, la cantidad en gramos a retirar del Stock y el motivo por el que aportas la cantidad.
                      </div>
                      <div class="form-group col-sm-4 ">
                                         <label for="s-genetica">Genética</label>
                                         <select class="form-control s-genetica">
                                         <option value="sin">Selecciona una genética...</option>
                                           <?php selectGeneticasTodas(); ?>
                                         </select>
                                       </div>
                                       <div class="form-group col-sm-2">
                                           <label for="cantidad">Cantidad</label>
                                           <input type="number" min="0.1" max="500" class="form-control cantidad">
                                           
                                         </div>
                                         <div class="form-group col-sm-6">
                                             <label for="asunto">Asunto</label>
                                             <input type="text" class="form-control asunto" >
                                             
                                           </div>
                    </div>
                    <button class="btn btn-lg btn-success btn-block btn-guardar-aportar">Crear registro</button>

                  </div>
 


   <?php include "php/footer.php";?>
 

      <script type="text/javascript">
        if(opcionLateral!="si"){
          $("#expandir-dispensario").collapse('toggle');
            $("#m-r-stock").addClass("menu-seleccionado");
         
        }


        $(".btn-guardar-retirar").on("click", function(){
          $(this).attr("disabled", true);
          modificarStock("retirar", $("#zona-retirar .s-genetica"), $("#zona-retirar .cantidad"), $("#zona-retirar .asunto"));
        });

        $(".btn-guardar-aportar").on("click", function(){
          $(this).attr("disabled", true);
         modificarStock("aportar", $("#zona-aportar .s-genetica"), $("#zona-aportar .cantidad"), $("#zona-aportar .asunto")); 
        });


        function modificarStock(funcion, idGen, cantidad, asunto){
          let idGenAux = idGen.val();
          let cantidadAux = cantidad.val();
          let asuntoAux = asunto.val();


         // idGen.attr("selected", true);
          


          if(validado(funcion, idGen, cantidad, asunto)){

            $("#zona-"+funcion).fadeOut("100", function(){
              $("#cargando-"+funcion).fadeIn("500");
            idGen.val("sin");
             cantidad.val("");
             asunto.val("");
            
           

          enviarDatos(funcion, idGenAux, cantidadAux, asuntoAux).done(function(data){
            console.log(data);
            if(data==1){ 
              $("#cargando-"+funcion).fadeOut("100", function(){

                                $("#zona-"+funcion).fadeIn("500");
                                mostrarMensaje("Los cambios han sido guardados","success");
                                $(".btn-guardar-"+funcion).attr("disabled",false);
             });

            } else {
                
                 $("#cargando-"+funcion).fadeOut("100", function(){

                 $("#zona-"+funcion).fadeIn("500");
                 mostrarMensaje("Ha ocurrido un error al guardar los cambios","error");
                 $(".btn-guardar-"+funcion).attr("disabled",false);
                });
            }
       

        }); 

        });
        
        } else {
          mostrarMensaje("Debes completar todos los datos correctamente.","error");
          $(".btn-guardar-"+funcion).attr("disabled",false);
        }
      }

        function enviarDatos(funcion, idGen, cantidad, asunto){
          return $.post("php/funciones.php", {funcion:"modificarStock", accion:funcion, idGen:idGen, cantidad:cantidad, asunto:asunto, user:<?php echo $_SESSION["id"]; ?>}, function(result){
            resultado = result;
          });
        }

        function validado(funcion, idGen, cantidad, asunto){
    
          let validado = true;
          idGen.removeClass("is-invalid");
          cantidad.removeClass("is-invalid");
          asunto.removeClass("is-invalid");

          if(idGen.val()=='sin'){
            idGen.addClass("is-invalid");
            validado = false;
          }

          if (cantidad.val()<0.1 || cantidad.val() > 500){
            cantidad.addClass("is-invalid");
            validado = false;
          }

          if(asunto.val()=="" || asunto.val()==" "){
           asunto.addClass("is-invalid");
            validado = false;
          }
      
          return validado;
        }


      </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/fileinput.min.js"></script>

    <script src="js/locales/es.js"></script>

    <script src="themes/fa/theme.js"></script>

  </div>
</body>

</html>
