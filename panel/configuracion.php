<?php
  require_once("php/config.php");
  require_once ("php/funciones.php");
  include "php/cabecera.php";
?>

 <?php cabecera("config");
 $date = time();

  $consultaConfig = consulta("select * from config;");
  $nombreA = $consultaConfig[0]["valor"];
  $nombreC = $consultaConfig[1]["valor"];
  $error ="";
 
  if($_SESSION["tipo"]==1){
      



 ?>

  <div class="content-wrapper animated fadeIn">
    <div class="container">
        <div class="alert alert-info" role="alert">
          <div class="icono-fondo">
                      <i class="fas fa-cogs"></i>
          </div>
          <div class="titulo-seccion"><span>Configuración del sistema</span></div>

        </div>
        <?php
        
            if(isset($_GET["s"])){
              echo '<div class="alert alert-success alert-dismissible fade show" role="alert">
                      Los datos se han guardado correctamente
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
                    </div>';
            } 

            if(isset($_GET["e"])) {
              echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                      Ha ocurrido un error al guardar los datos.
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>';
            }
          
        ?>
        <form method="POST" action="php/guardar-config.php" enctype="multipart/form-data" id="form-config">
      <div class="row">
        <div class="col-sm-12">
            <div class="row mt-4">
                <div class="col-lg-3">
                    <b>Nombre de la asociación</b></br>
                    <small class="text-muted">Introduce aquí el nombre completo de la asociación. Este nombre aparecerá en los documentos y formularios.</small>
                </div>
                <div class="col-lg-9">
                    <input class="form-control" name="nombreA" value="<?php echo $nombreA; ?>" required></input>
                </div>

            </div>  

            <div class="row mt-5">
              
              <div class="col-lg-3">
                  <b>Nombre corto</b></br>
                  <small class="text-muted">Introduce aquí el nombre corto de la asociación. Este nombre se utilizará para el panel y otros campos.</small>
              </div>
              <div class="col-lg-9">
                  <input class="form-control" name="ncorto" value="<?php echo $nombreC; ?>" required></input>
              </div>
            </div>
             <div class="row mt-5">
              
              <div class="col-lg-3">
                  <b>Logotipo</b></br>
                  <small class="text-muted">Introduce aquí el logotipo de la asociación. (Medidas recomendadas: 512x512)</small>
              </div>
              <div class="col-lg-9">
                  <img style="max-width:200px;" src="img/logo.png?<?php echo $date; ?>">
                  <div class="form-group">
                    <label for="exampleFormControlFile1">Selecciona un logotipo</label>
                    <input type="file" name="logo" class="form-control-file" id="logo">
                  </div>
              </div>
            </div>

            <button class="btn btn-info btn-block btn-lg" type="submit"><i class="fas fa-save"></i> Guardar cambios</button>
        
        
        </div>
        

      </div>
    </form>




        </div>
          <?php
        } else {
          errorDePermisos();
        }

          ?>
    <?php include "php/footer.php";?>
    <script src="js/socios.js"></script>

  </div>

  
</body>

</html>
