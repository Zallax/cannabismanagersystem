<?php
  require_once("php/config.php");
  require_once ("php/funciones.php");
  include "php/cabecera.php";
?>

 <?php cabecera("inicio");?>
  <div class="content-wrapper animated fadeIn">
    <div class="container-fluid">




        <div class="row widgets">
          <div class="col-sm-12 zona-lista-tablet">

          </div>

           <div class="col-sm-12 zona-alertas">

          </div>
            
              <div class="col-xl-3 col-sm-6 mb-3 mt-3">
                
                <div class="row" id="accionesRap">
                  <div class="col-sm-12">
                      <a href="caja.php"> <div class="btn-rapido caja text-center"><i class="fas fa-cash-register"></i></br>CERRAR CAJA</div></a>
                  </div>
                 
                  <div class="col-sm-6">
                      <a href="crear-transaccion.php"> <div class="btn-rapido transaccion text-center"><i class="fas fa-shopping-basket"></i></br>TRANSACCIÓN</div></a>
                  </div>
                  <div class="col-sm-6">
                    <a href="activaciones.php"> <div class="btn-rapido activacion text-center"><i class="fas fa-user-plus"></i></br>ACTIVACIONES</div></a>
                  </div>
                </div>

                            <!--<a href="crear-transaccion.php" class="btn btn-dark btn-lg btn-block mt-2 text-left"><i class="fas fa-chevron-circle-right"></i> Crear transacción</a>
                              <a href="caja.php" class="btn btn-dark btn-lg btn-block mt-2 text-left"><i class="fas fa-chevron-circle-right"></i> Cerrar caja</a>
                              <a href="activaciones.php" class="btn btn-dark btn-lg btn-block mt-2 text-left"><i class="fas fa-chevron-circle-right"></i> Activar socio</a>-->
            </div>
            <div class="col-xl-3 col-sm-6 mb-3 mt-3">
                          <div class="card text-white bg-success o-hidden h-100">
                                          <div class="card-body">
                                            <div class="card-body-icon">
                                              <i class="fas fa-fw fa-shopping-cart"></i>
                                            </div>
                                             <p class="lead">Transacciones</p>
                                            <h1 class="display-4"><?php datosInicio(1); ?></h1>
                                          </div>
                                          <a class="card-footer text-white clearfix small z-1" href="transaccion-hoy.php">
                                            <span class="float-left">Ver todas</span>
                                            <span class="float-right">
                                              <i class="fas fa-angle-right"></i>
                                            </span>
                                          </a>
                                        </div>
          </div>
            <div class="col-xl-3 col-sm-6 mb-3 mt-3">
              <div class="card text-white bg-warning o-hidden h-100">
                <div class="card-body">
                  <div class="card-body-icon">
                    <i class="fa fa-fw fa-users"></i>
                  </div>
                  <p class="lead">Nuevos socios</p>
                  <h1 class="display-4"><?php datosInicio(2); ?></h1>
                </div>
                <a class="card-footer text-white clearfix small z-1" href="socios.php">
                  <span class="float-left">Ver todos</span>
                  <span class="float-right">
                    <i class="fas fa-angle-right"></i>
                  </span>
                </a>
              </div>
            </div>
            <div class="col-sm-6 col-xl-3 mb-3 mt-3">

                    <div class="card text-white bg-danger o-hidden h-100">
                                    <div class="card-body">
                                      <div class="card-body-icon">
                                        <i class="fas fa-cash-register"></i>
                                      </div>
                                      <p class="lead">Total de la caja</p>
                                      <h1 class="display-4"><?php datosInicio(3); ?>€</h1>
                                    </div>
                                    <a class="card-footer text-white clearfix small z-1" href="caja.php">
                                      <span class="float-left">Ver detalles</span>
                                      <span class="float-right">
                                        <i class="fas fa-angle-right"></i>
                                      </span>
                                    </a>
                                  </div>






          </div>

      <?php
        if($_SESSION["tipo"]==1){


      ?>

          <div class="col-12">
            <h3 class="fino">Estadísticas</h3>
          </div>
          <div class="col-sm-12 col-xl-6 mb-2">
            <div class="card">
              <div class="card-body">
                <h5 class="card-title">Cajas anteriores</h5>
                <canvas id="ultimaSemana"></canvas>
              </div>
            </div>
          
          </div>
          <div class="col-sm-12 col-xl-6 mb-2">
            
            <div class="card">
              <div class="card-body">
                <h5 class="card-title">Lo más dispensado este mes</h5>
               <canvas id="geneticasDispensadas"></canvas>
              </div>
            </div>
            </div>


      <?php

        }
      ?>

            <div class="col-12 mt-2">
            <h3 class="fino">Socios</h3>
          </div>
          <div class="col-xl-4 col-lg-6">

            <div class="card mb-3">
              <div class="card-header bg-white">
               <h5 class="card-title">Próximas renovaciones</h5>
              </div>
              <ul class="list-group list-group-flush">
                 <?php sociosApuntoCaducar();?>
             </ul>

            </div>
          </div>
          <div class="col-xl-4 col-lg-6">

            <div class="card mb-3">
              <div class="card-header bg-white">
                <h5 class="card-title">Últimos socios registrados</h5>
              </div>
              <ul class="list-group list-group-flush">
                 <?php ultimosSociosRegistrados();?>
             </ul>

            </div>
          </div>
          <div class="col-xl-4 col-lg-12">

            <div class="card mb-3">
              <div class="card-header bg-white">
                <h5 class="card-title">Últimas visitas</a></h5>
              </div>
              <ul class="list-group list-group-flush">
                 <?php ultimasVisitas();?>
             </ul>

            </div>
          </div>
          <?php
            if($_SESSION["tipo"]==1){


          ?>
          <div class="col-12 mt-2">
          <h3 class="fino">Dispensario</h3>
        </div>
        <div class="col-lg-12">

      

           <div class="card mb-3" style="max-height:300px;">
             <div class="card-header bg-white">
              <h5 class="card-title">Últimos movimientos<div class="vermas">Ver todo <i class="fas fa-angle-right"></i></div></h5>
             </div>
             <ul class="list-group list-group-flush p-3" style="max-height:350px;overflow-y:scroll;">
                <?php ultimosMovimientosGeneticas();?>
            </ul>

           </div>

        
        </div>
          <div class="col-lg-6">

            <div>

             <div class="card mb-3">
               <div class="card-header bg-white">
                <h5 class="card-title">Productos con poco stock<div class="vermas">Ver todo <i class="fas fa-angle-right"></i></div></h5>
               </div>
               <ul class="list-group list-group-flush p-3" style="max-height:350px;overflow-y:scroll;">
                  <?php productosApuntoAcabar();?>
              </ul>

             </div>

            </div>
          </div>

          <div class="col-lg-6">
            <div id="datosStock">
              <div class="card mb-3">
                <div class="card-header bg-white">
                  <h5 class="card-title">Genéticas con poco Stock<div class="vermas">Ver todo <i class="fas fa-angle-right"></i></div></h5>
                </div>
                <ul class="list-group list-group-flush p-3" style="max-height:350px;overflow-y:scroll;">
                   <?php dispensarioApuntoAcabar();?>
               </ul>

              </div>


            </div>
          </div>
          <?php

        }
        ?>

        </div>

        </div>

   <?php include "php/footer.php";?>
       <script src="js/Chart.js"></script>
   <script src="js/estadisticas.js">

   </script>
   <script src="js/inicio.js"></script>
  </div>
</body>

</html>
