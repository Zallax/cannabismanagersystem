<?php
  require_once("php/config.php");
  require_once ("php/funciones.php");
  include "php/cabecera.php";
?>

 <?php cabecera("lista-socios");?>
  <div class="content-wrapper animated fadeIn">
    <div class="container" id="zona-lista-socios">
      <div class="card">
        <div class="card-body">
        <div class="icono-fondo">
                    <i class="fas fa-users"></i>
        </div>
       
        <div class="titulo-seccion"><span>Lista de Socios</span></div>

        
      <div class="text-right mb-2">
        

         <div class="btn-group btn-group-md" role="group">
           <button id="btnGroupDrop1" type="button" class="btn btn-outline-dark dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
             Filtrar
           </button>
           <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
            <a class="btn btn-danger dropdown-item " href="socios.php?f=sin-renovar">Sin renovar</a>
                 <a class="btn btn-warning dropdown-item" href="socios.php?f=proximas-renovaciones">Próximas renovaciones</a>
                 <a class="btn btn-dark dropdown-item" href="socios.php?f=ultima-visita">Última visita</a>
                 <a class="btn btn-secondary dropdown-item" href="socios.php?f=anotaciones">Con anotaciones</a>
           </div>
           <a class="btn btn-success" href="alta.php"><i class="fa fa-user-plus"></i> Nuevo socio</a>
         </div>
       

        
      
    </div>
      
     
      <div class="tab-content">
       
        <?php 

          if (ISSET($_GET["f"])){
              
                if($_GET["f"]=="sin-renovar"){
                tablaSociosSinRenovar();
                }
                if($_GET["f"]=="ultima-visita"){
                tablaSociosUltimasVisitas();
                }
                if($_GET["f"]=="proximas-renovaciones"){
                tablaSociosProxRenov();
                }
                if($_GET["f"]=="anotaciones"){
                  sociosConAnotaciones();
                  
                }
              
          } else {
            tablaSocios();
            
          }
          

          ?>
          </div>
</div>
        </div>
        </div>
    <?php include "php/footer.php";?>
    <script src="js/socios.js"></script>
    <script>
      if(opcionLateral!="si"){
        $("#l-socios").addClass("menu-seleccionado");
        $("#expandir").collapse('toggle');
      }
      
      </script>

  </div>
</body>

</html>
