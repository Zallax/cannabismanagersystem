<?php
  require_once("php/config.php");
  require_once ("php/funciones.php");
  include "php/cabecera.php";
  extract($_GET);
  $consulta = consulta("select t.id as id, t.estado as estado, t.fecha as fecha, t.hora as hora, u.nombre as nombre, u.apellidos as apellidos from tablet t inner join usuarios u where t.usuario = u.id order by id desc;");
?>

 <?php cabecera("hist-tablet");?>
  <div class="content-wrapper animated fadeIn">
    <div class="container">

      <div class="alert alert-info" role="alert">
        <div class="titulo-seccion"><span>Control de accesos</span></div>
        
      </div>
    
     
      <div class="tab-content">
      <ul class="list-group">
         

         <?php
          
          

          if(count($consulta)!=0){
            foreach($consulta as $indice => $valor){
   
              $fecha = explode("-", $valor["fecha"]);
              $hora = explode(":", $valor["hora"]);

              if($valor["estado"]==0){
                $clase = "list-group-item-danger";
                $texto = '<i class="fas fa-calendar-alt"></i> '.$fecha[2].'/'.$fecha[1].'/'.$fecha[0].'  <i class=" ml-2 fas fa-clock"></i> '.$hora[0].':'.$hora[1].' <i class="fas fa-angle-right"></i> Cerrada por '.$valor["nombre"].' '.$valor["apellidos"];
              }

              if($valor["estado"]==1){
                $clase = "list-group-item-success";
                $texto = '<i class="fas fa-calendar-alt"></i> '.$fecha[2].'/'.$fecha[1].'/'.$fecha[0].'  <i class="ml-2 fas fa-clock"></i> '.$hora[0].':'.$hora[1].' <i class="fas fa-angle-right"></i> Abierta por '.$valor["nombre"].' '.$valor["apellidos"];
              }

              if($valor["estado"]==2){
                $clase = "list-group-item-info";
                $texto = '<i class="fas fa-calendar-alt"></i> '.$fecha[2].'/'.$fecha[1].'/'.$fecha[0].'  <i class="ml-2 fas fa-clock"></i> '.$hora[0].':'.$hora[1].' <i class="fas fa-angle-right"></i> Cerrada al cerrar la caja por '.$valor["nombre"].' '.$valor["apellidos"];
              }
              if($valor["estado"]==3){
                $clase = "list-group-item-warning";
                $texto = '<i class="fas fa-calendar-alt"></i> '.$fecha[2].'/'.$fecha[1].'/'.$fecha[0].'  <i class="ml-2 fas fa-clock"></i> '.$hora[0].':'.$hora[1].' <i class="fas fa-angle-right"></i> Botón Pánico pulsado por '.$valor["nombre"].' '.$valor["apellidos"];
              }

              echo '<li class="list-group-item list-group-item-action '.$clase.'">'.$texto.'</li>';
  

             
             }

           }
         ?>
         </ul>
       </div>
            
      </div>


        
       
         
        

        


       </div>
    <?php include "php/footer.php";?>
    <script src="js/correccion-stock.js"></script>

      
  
</div>
</body>

</html>
