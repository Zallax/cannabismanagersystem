<?php
  require_once("php/config.php");
  require_once ("php/funciones.php");
  include "php/cabecera.php";

  extract($_POST);
  $textoFecha = "";

  if(ISSET($fechaSelect)){
  
  $fechaActual = $fechaSelect;
  $consulta = consulta("select r.id as id, r.idSocio as idSocio, r.articulos as articulos, r.fecha as fecha, r.precio_total as precio_total, u.nombre as nUsuario, u.apellidos as aUsuario, r.idMod, r.manual, s.nombre as nSocio, s.apellidos as aSocio, s.nsocio as numSocio from retiradas r inner join usuarios u on r.idUsuario = u.id inner join socios s where fecha between '$fechaActual 00:00:00' and '$fechaActual 23:59:59' and r.idSocio = s.id order by id desc;");
    $textoFecha = "del ".fecha($fechaActual);
  }
  
?>

 <?php cabecera("historial-socios");
 if($_SESSION["tipo"]==1){
  ?>
  <div class="content-wrapper animated fadeIn">
    <div class="container">
      <?php


      ?>
      <div class="col-sm-12 boton-a-genetica">
                  <div class="card border-danger">
                    <div class="card-body">
                      <div class="icono-fondo">
                               <i class="fas fa-fw fa-shopping-cart"></i>
                    </div>
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="titulo-seccion"><span class="">Transacciones <?php echo $textoFecha;?></span>
                          <p class="text-muted">En esta sección puedes ver todas las transacciones realizadas en una fecha concreta</p>
                        </div>
                        
                      </div>
                      
                    </div>
                    </div>
                  </div>


                </div>

      

      <form method="post" action="transacciones.php" class="row mt-3">
        <div class="col-lg-4"></div>
        <div class="col-lg-4"><div class="input-group mb-3 text-center">
          <input type="date" name="fechaSelect" class="form-control form-control-lg" placeholder="Selecciona una fecha" value="<?php echo $fechaSelect; ?>" required>
          <div class="input-group-append">
            <button class="btn btn-dark" type="submit"><i class="fas fa-search"></i></button>
          </div>
        </div></div>
        <div class="col-lg-4"></div>


        

      </form>



         <?php
         if(ISSET($fechaSelect)){

         
          if(count($consulta)!=0){
            foreach($consulta as $indice => $valor){
              $articulos = json_decode($valor["articulos"]);



              $fechaHora = explode(" ", $valor["fecha"]);
              $fecha = explode("-", $fechaHora[0]);
              $hora = explode(":", $fechaHora[1]);

              $total = 0;
              foreach($articulos as $indice2 => $producto){
                if($producto[0]=="1"){
                  $total+=$producto[2];
                }
              }
              if($total>5){
                $clase = 'bg-warning';
                $alerta = '<i class="mr-2 mt-2 fas fa-exclamation-triangle" data-toggle="tooltip" data-placement="bottom" title="Dispensados más de 5 gramos."></i>';
              } else {
                $clase = 'bg-azul';
                $alerta = '';
              }

              echo
           '
           <div class="tab-content bg-white mb-2 p-3" style="border-radius:20px;">
            <a class="text-primary" href="perfil.php?s='.$valor["idSocio"].'"><h5>'.$valor["nSocio"].' '.$valor["aSocio"].' <span class="text-muted">#'.$valor["numSocio"].'</span></h5></a>
            <div id="accordion">
            <div class="card retiradas-perfil">'.
             '<div class="card-header '.$clase.' text-white"data-toggle="collapse" data-target="#'.$valor["id"].'" aria-expanded="true" aria-controls="collapseOne">'.
               '<div class="row no-gutters">'.
                   '<div class="col-lg-2 col-md-3 col-4 text-center">'.
                     '
                     <span class="badge badge-dark " style="font-size:1.5em;margin-top:4px;">'.
                      $fecha[2]."/".$fecha[1]."/".$fecha[0].'</span>'.
                   '</div>'.
                   '<div class="col-lg-2 col-md-3 col-8 mostrador pl-4">'.
                      '<b>Dispensado por</b><br>'.
                      $valor["nUsuario"].' '.$valor["aUsuario"].
                   '</div>';
                   echo '<div class="col-lg-2 col-md-3 col-6 mostrador pl-4">';
                  if($valor["idMod"]!=0){
                    $modificador = consulta("select nombre, apellidos from usuarios where id = ".$valor["idMod"].";");
                    if(count($modificador)!=0){
                      $nombre = $modificador[0]["nombre"]." ".$modificador[0]["apellidos"];
                    } else {
                      $nombre = "Usuario eliminado";
                    }

                      echo '<b>Modificado por</b><br>'.
                      $nombre;

                  }
                  echo '</div>';
                  echo '<div class="col-lg-1 col-md-3 col-6">';
                  if($valor["manual"]!=0){

                     echo '<span class="badge badge-warning">Manual</span>';


                 }
                 echo '</div>';

                   echo '<div class="col-lg-1 col-4 col-md-3 text-center" id="precioTotal">'.

                     $valor["precio_total"].'€'.

                 '</div>'.
                   '<div class="col-lg-2 col-4 col-md-3 text-center" id="idRetirada">'.
                     '#'.$valor["id"].
                   '</div>'.
                   '<div class="col-lg-1 col-md-3 col-4 text-right">'.$alerta.'<a class="btn btn-info btn-sm mt-1" href="modificar-transaccion.php?id='.$valor["id"].'&m" data-toggle="tooltip" data-placement="bottom" title="EDITAR"><i class="fas fa-pen-square"></i></a></div>'.
               '</div>'.


              '</div>'.
             '<div id="'.$valor["id"].'" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">'.
             '<div class="card card-cannabis">'.
             '<div class="card-header bg-secondary text-white cat-retirado">CANNABIS</div>'.
             '<ul class="lista-cannabis list-group list-group-flush">';
              $cannabis = false;
              foreach($articulos as $indice2 => $producto){
                if($producto[0]=="1"){
                  $cannabis = true;
                  echo '<li class="list-group-item">'.$producto[1].' ('.$producto[2].' Gr)</li>';
                }
              }
              if(!$cannabis){
                echo '<li class="list-group-item">Sin datos</li>';
              }



            echo '</ul>'.
           '</div>'.
           '<div class="card card-productos">'.
           '<div class="card-header bg-secondary text-white cat-retirado">PRODUCTOS</div>'.
           '<ul class="lista-productos list-group list-group-flush">';
           $productos = false;
           foreach($articulos as $indice2 => $producto){
             if($producto[0]=="2"){
               echo '<li class="list-group-item">'.$producto[1].' ('.$producto[2].' Ud)</li>';
               $productos = true;
             }
           }
           if(!$productos){
             echo '<li class="list-group-item">Sin datos</li>';
           }


           echo
          '</ul>'.
         '</div>'.
             '</div>'.
             '</div>
               </div>

      </div>';

            }
          } else {
            echo '<div class="text-center"><span class="text-muted">No hay datos disponibles</span></div>';
          }
        } else {
          echo '<h4 class="text-muted text-center">Selecciona una fecha</h4>';
        }


         ?>








        </div>



       </div>
       <?php
          } else {
            errorDePermisos();
          }
       ?>
    <?php include "php/footer.php";?>
    <script src="js/correccion-stock.js"></script>
    <script>
      if(opcionLateral!="si"){
        $("#d-trans").addClass("menu-seleccionado");
        $("#expandir-mostrador").collapse('toggle');
      }
      </script>


</div>
</body>

</html>
