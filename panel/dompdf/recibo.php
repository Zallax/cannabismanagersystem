<?php ob_start();
session_start();

if(!ISSET($_SESSION["id"])){
	header("Location: ../login.php");
}

//require_once("../php/conexion.php");
require_once("../php/funciones.php");
extract($_GET);
$consultaConfig = consulta("select * from config;");
$nombreA = $consultaConfig[0]["valor"];
$nombreC = $consultaConfig[1]["valor"];

$consulta = consulta("select * from cuotas where id=$id");
$idSocio = $consulta[0]["idSocio"];
$consulta2 = consulta("select * from socios where id=$idSocio");
$datos = $consulta2[0];




?>
<head>
    <style>
        p{
            margin:5px 0;
        }
        .text-uppercase{
            text-transform:uppercase;
        }
    </style>
    
</head>
<body style="font-family: Calibri,Candara,Segoe,Segoe UI,Optima,Arial,sans-serif;font-size:15px;">
    <img style="float:right;width:200px;" src="../../img/logo.png">
<h2 style="font-size:30px; text-align:center;margin-bottom:70px;"><u>RECIBO DE CUOTA</u></h2>


<p ><b><?php echo $datos["nombre"]." ".$datos["apellidos"];?></b>, con Número de Socio <b><?php echo $datos["nsocio"];?></b> ha entregado la cantidad de <b><?php echo $consulta[0]["cuota"];?>€</b> en concepto de inscripción (<b><?php echo $consulta[0]["meses"]?> meses</b>) en la <b><?php echo $nombreA; ?></b>.</p>
</br></br></br></br></br>
<ul style="margin-top:50px;margin-bottom:50px;">
    <li><b>Fecha de Pago:</b> <?php echo fecha($consulta[0]["fechapago"]); ?></li>
    <li><b>Fecha de Caducidad:</b> <?php echo fecha($consulta[0]["fechafin"]); ?></li>
</ul>
</br></br></br></br></br>
<!--<table style="margin-left:120px">
    <tr>
        <td ></td><td>Firma miembro de la Junta Directiva:</td>
    </tr>
</table>-->
<p style="margin-top:400px;font-size:10px;"><i>Se recomienda la conservación de este documento con el fin de poder demostrar, en caso de ser necesario, que la cuota ha sido abonada.</i></p>

</body>
<?php
require_once 'dompdf/autoload.inc.php';
use Dompdf\Dompdf;
$dompdf = new DOMPDF();
$dompdf->load_html(ob_get_clean());
$dompdf->render();
$pdf = $dompdf->output();
$filename = "Recibo.pdf";
file_put_contents($filename, $pdf);
$dompdf->stream($filename);
?>
