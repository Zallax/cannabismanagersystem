<?php ob_start();
session_start();

if(!ISSET($_SESSION["id"])){
	header("Location: ../login.php");
}

//require_once("../php/conexion.php");
require_once("../php/funciones.php");
extract($_GET);
$consultaConfig = consulta("select * from config;");
$nombreA = $consultaConfig[0]["valor"];
$nombreC = $consultaConfig[1]["valor"];

$consulta = consulta("select * from socios where id=$id");
$datos = $consulta[0];
if($datos["firma"]!=""){
    $firma = '<img src="../img/socios/firma/'.$datos["firma"].'" alt="" width="200px">';
} else  {
    $firma = '';
}
$consulta2 = consulta("select * from socios where id = ".$datos["avalador"].";");
if(!empty($consulta2)){
    $avalador = $consulta2[0];
    if($avalador["firma"]!=""){
        $firma2 = '<img src="../img/socios/firma/'.$avalador["firma"].'" alt="" width="200px">';
    } else  {
        $firma2 = '';
    }
}

$checks = json_decode($datos["requisitos"]);

if($datos["req_docu"]!=""){
    $checks2 = json_decode($datos["req_docu"]);
    $req5= explode(":", $checks2[0]);
    $req6= explode(":", $checks2[1]);

} else {
    $req5[1] = "false;";
    $req6[1] = "false;";
}


$req1 = explode(":", $checks[0]);
$req2 = explode(":", $checks[1]);
$req3= explode(":", $checks[2]);
$req4= explode(":", $checks[3]);

$activado = '<img src="check.png" style="width:10px;height:10px;margin-right:10px;margin-left:-10px;">';
$desactivado = '<img src="square.png" style="width:9px;height:9px;margin-right:11px;margin-left:-10px;">';
$chk1 = $desactivado;
$chk2 = $desactivado;
$chk3 = $desactivado;
$chk4 = $desactivado;
$chk5 = $desactivado;
$chk6 = $desactivado;

if($req1[1]=="true"){
    $chk1 = $activado;
}
if($req2[1]=="true"){
    $chk2 = $activado;
}
if($req3[1]=="true"){
    $chk3 = $activado;
}
if($req4[1]=="true"){
    $chk4 = $activado;
}
if($req5[1]=="true"){
    $chk5 = $activado;
}
if($req6[1]=="true"){
    $chk6 = $activado;
}

?>
<head>
    <style>
        p{
            margin:5px 0;
        }
        .text-uppercase{
            text-transform:uppercase;
        }
    </style>
</head>
<body style="text-align:justify;font-family: Calibri,Candara,Segoe,Segoe UI,Optima,Arial,sans-serif;font-size:10px;">
<h2 style="font-size:14px; "><u>ASOCIACIÓN CANNÁBICA________________________________________SOLICITUD DE ADMISIÓN SOCIO/A</u></h2>
</br></br></br>
<table width="100%" style="margin:20px; 0 0 20%;font-size:9px;">
    <tr>
        <td style="padding:5px;"><u><b>Nº TARJETA:</b></u> <span style="text-transform:uppercase"><?php echo $datos["nsocio"] ?></span></td>
    </tr>
    <tr>
        <td style="padding:5px;"><b>APELLIDOS: </b><span style="text-transform:uppercase"><?php echo $datos["apellidos"] ?></span></td>
        <td style="padding:5px;"><b>DNI:</b> <span style="text-transform:uppercase"><?php echo $datos["dni"] ?></span></td>
    </tr>
    <tr>
        <td style="padding:5px;"><b>NOMBRE:</b> <span style="text-transform:uppercase"><?php echo $datos["nombre"] ?></span></td>
        <td style="padding:5px;"><b>EMAIL:</b> <span style="text-transform:uppercase"><?php echo $datos["email"] ?></span></td>
    </tr>
    <tr>
        <td style="padding:5px;"><b>DOMICILIO:</b> <span style="text-transform:uppercase"><?php echo $datos["direccion"] ?></span></td>
        <td style="padding:5px;"><b>TELEFONO:</b> <span style="text-transform:uppercase"><?php echo $datos["telefono"] ?></span></td>
    </tr>
</table>

<p>Por la presente, solicito a la Junta directiva de la asociación ser admitido como socio/a al cumplir con los requisitos establecidos en los estatutos, y manifiesto ser mayor de 21 años y:</p>

<ul style="list-style-type: none">
    <li><b><?php echo $chk1 ?>SER CONSUMIDOR/A DE CANNABIS SATIVA L.</b></li>
    <li><b><?php echo $chk2 ?>SER CONSUMIDOR/A DE TABACO</b></li>
    <li><b><?php echo $chk3 ?>SER CONSUMIDOR/A DE PLANTAS CON PROPIEDADES TERAPÉUTICAS</b></li>
    <li><b><?php echo $chk4 ?>SER CONSUMIDOR/A HABITUAL DE CANNABIS POR RAZONES TERAPÉUTICAS</b>, y tener diagnosticada una enfermedad para la que el cannabis tiene efectos terapéuticos según la IACM, o que el uso paliativo de los cannabinoides ha sido probado científicamente y aportar certificado médico acreditativo de la solicitud de uso de cannabis por parte del paciente sin oposición médica, o tener una licencia de uso de cannabis de cualquier país del Mundo.</li>

</ul>

<p>
    <b>AFIRMO</b> mi voluntad de incorporarme como SOCIO/A de la <span class="text-uppercase"><?php echo $nombreA ?></span> conociendo los estatutos y objetivos de ésta y me comprometo a cumplir con ellos, con las normas de funcionamiento interno y con la legislación española, con especial atención a:
</p>

<p>
    El artículo 19 de la Ley Orgánica 1/1992, sobre Protección de la Seguridad ciudadana: 1) Los agentes de las Fuerzas y Cuerpos de Seguridad podrán limitar o restringir, por el tiempo imprescindible, la circulación o permanencia en vías o lugares públicos en supuestos de alteración del orden, la seguridad ciudadana o la pacífica convivencia, cuando fuere necesario para su restablecimiento. Así mismo  podrán ocupar preventivamente los  efectos o instrumentos susceptibles de  ser  utilizados  para  acciones ilegales, dándoles el  destino que legalmente proceda. 2) Para el descubrimiento y detención de los participantes en un hecho delictivo causante de grave alarma social y para la recogida de los instrumentos, efectos o pruebas del mismo, se podrán establecer controles en la vías, lugares o establecimientos públicos, en la medida indispensable a los fines de este apartado, al objeto de proceder a la identificación de las personas que transiten o se encuentren en ellos, al registro de los vehículos y al control superficial de los efectos personales con el fin de comprobar que no se portan sustancias o instrumentos prohibidos o peligrosos. El resultado de la diligencia se pondrá de inmediato en conocimiento del Ministerio Fiscal.
</p>
<p>
El artículo 25.1 de la Ley Orgánica 1/1992, sobre Protección de la Seguridad ciudadana: Constituyen infracciones graves a la seguridad ciudadana el consumo en lugares, vías, establecimientos o transportes públicos, así como la tenencia ilícita, aunque no estuviera destinada al tráfico de drogas tóxicas, estupefacientes o sustancias psicotrópicas, siempre que no constituya infracción penal, así como el abandono en los sitios mencionados de útiles o instrumentos utilizados para su consumo.
</p>
<p>
El artículo 368 del Código Penal Español, Ley Orgánica 5/2010: Los que ejecuten actos de cultivo, elaboración o tráfico, o de otro modo promuevan,  favorezcan  o  faciliten  el  consumo  ilegal  de  drogas  tóxicas,  estupefacientes  o  sustancias  psicotrópicas,  o  las  posean  con aquellos fines, serán castigados con las penas de prisión de tres a seis años y multa del tanto al triplo del valor de la droga objeto del delito si se tratase de sustancias o productos que causen grave daño a la salud, y de prisión de uno a tres años y multa del tanto al duplo en los demás casos.
</p>
<p>
El artículo 18 de la Constitución Española: 1. Se garantiza el derecho al honor, a la intimidad personal y familiar y a la propia imagen. 2. El domicilio es inviolable. Ninguna entrada o registro podrá hacerse en él sin el consentimiento del titular o resolución judicial, salvo en caso de  flagrante  delito.  3.  Se  garantiza  el  secreto  de  las  comunicaciones  y,  en  especial,  de  las  postales,  telegráficas  y  telefónicas,  salvo resolución judicial. 4. La Ley limitará el uso de la informática para garantizar la intimidad personal y familiar de los ciudadanos y el pleno ejercicio de sus derechos.
</p>

<table style="width:100%;">
    <tr>
        <td style="text-align:center;">DOCUMENTOS ADJUNTOS:</td><td><ul style="list-style:none;"> <li><?php echo $chk5 ?>COPIA DNI/NIE</li></ul></td>

    </tr>
        <tr><td></td><td><ul style="list-style:none;"> <li> <?php echo $chk6 ?>CERTIFICADO MÉDICO</li></ul></td></tr>

</table>
<p><u>Nº DE SOCIO/A AVALADOR:</u> <?php echo $avalador["nsocio"];?></p>
<p>YO  <span style="text-transform:uppercase"><?php echo $avalador["nombre"]." ".$avalador["apellidos"]; ?></span>  FECHA  DE  INGRESO  <span style="text-transform:uppercase"><?php echo fecha($avalador["alta"]) ?></span>  ,  avalo  el  ingreso  del solicitante en la <span class="text-uppercase"><?php echo $nombreA ?></span> a <span style="text-transform:uppercase"><?php echo fecha($datos["alta"]); ?></span>.</p>
    </br></br>
<table style="width:100%;">
    <tr>
        <td style="text-align:center;"><u>FIRMA SOLICITANTE</u></td><td style="text-align:center;"><u>FIRMA SOCIO/A AVALISTA</u></td>

    </tr>
    <tr>
    <td><?php echo $firma; ?></td>
    <td><?php echo $firma2; ?></td>
    </tr>


</table>
<p><u>DE LA JUNTA DIRECTIVA SE ACEPTA LA PRESENTE SOLICITUD:</u><span style="margin-left:50px;border-radius:100px;border:1px solid black;padding:5px;">SI</span><span style="margin-left:100px;">NO</span></a</p>
<p style="margin-top:20px;"><u>CON FECHA:</u> <?php echo fecha($datos["alta"]); ?> <u style="margin-left:100px;">FECHA DE INGRESO:</u> <?php echo fecha($datos["alta"]); ?></p>
</br><p style="font-size:8px;margin-top:20px;">Declara dar su consentimiento expreso a que los datos recogidos en esta ficha serán incorporados a un fichero informático cuyo titular es la <span class="text-uppercase"><?php echo $nombreA ?></span>, siendo tratados con la debida confidencialidad y reserva apropiadas, conforme a la Ley Orgánica 15/1999, de 13 de diciembre, de Protección de Datos de Carácter Personal, para su exclusiva utilización con fines de gestión interna de la Asociación. Usted podrá ejercer los derechos de acceso, rectificación y cancelación de sus datos mediante comunicación escrita dirigida a cualquier delegación de la Asociación.              </p>

<h2 style="text-align:center;margin:0 20px;">SOLICITUD DE PARTICIPACIÓN EN EL PROGRAMA DE CULTIVO COLECTIVO
DE LA ASOCIACIÓN CANNÁBICA</h2>
<div style="margin-left:40px;margin-top:30px;margin-bottom:30px;"><p><u><b>NOMBRE:</b></u><span style="text-transform:uppercase"> <?php echo $datos["nombre"] ?></span></p>
<p><u><b>APELLIDOS:</b></u><span style="text-transform:uppercase"> <?php echo $datos["apellidos"] ?></span></p>
<p><u><b>DNI:</b></u><span style="text-transform:uppercase"> <?php echo $datos["dni"] ?></span></p>
<p><u><b>Nº SOCIO/A:</b></u><span style="text-transform:uppercase"> <?php echo $datos["nsocio"] ?></span></p>

</div>
<p>Por la presente manifiesto ser mayor de 21 años y mi condición de consumidor habitual de Cannabis Sativa L. y solicito participar en el programa de cultivo colectivo con los miembros de la asociación asumiendo mi corresponsabilidad en el cultivo junto al resto de personas participantes en el mismo.</p>
<p><b>El producto de mi participación en el cultivo asociativo en ÚNICA Y EXCLUSIVAMENTE PARA MI CONSUMO PERSONAL EN EL ÁMBITO PRIVADO DEL LOCAL SOCIAL, asumiendo cualquier responsabilidad de mis actos contrarios a la Ley que se pudieran derivar y eximiendo de ello a la Asociación, Asociados/as y Junta Directiva.</b></p>
<ul>
	<li>Para este efecto adjunta el patrocinio/aval de UN/A SOCIO/A de la Asociación a la presente solicitud que manifiesta ser conocedora del consumo de cannabis por parte de la persona que solicita su incorporación a la Asociación o documentación que acredite que se trata de una persona consumidora.</li>
	<li>Solicito participar en la cantidad de (<?php echo $datos["consumo"];?>) gr. de cannabis al mes, cantidad que será revisada o confirmada por el/la socio/a a cada trimestre. En caso de no confirmar o revisar se prorrogará la cantidad ya declarada.</li>
	<li>El/la socio/a autoriza a los/las socios/as activos/as colaboradores de la Asociación a cultivar, recoger y repartir entre los/las socios/as el Cannabis proveniente del cultivo colectivo asociativo para el consumo.</li>
	<li>El/la socio/a se obliga a comunicar de inmediato, en el momento que decida abandonar su participación en la Asociación, la solicitud de baja de la Asociación. Anualmente deberá ratificarse la condición de socio/a.</li>
	<li>El/la socio/a comunicará a la Asociación la notificación de sanción en base al artículo 25.1 de la Ley Orgánica 1/1992, sobre Protección Ciudadana, o la imputación del delito tipificado en el artículo 3668 del Código Penal, pudiendo implicar en el primer caso e implicando en el segundo la expulsión del socio/a.</li>
	<li>El/la socio/a se compromete a no ceder su carnet de socio/a, el cual es intransferible; la transferencia del carnet de socio/a será motivo de expulsión de la asociación. La persona socia siempre exhibirá su documento de identidad y el carnet de socio/a para retirar su material.</li>
	<li>Cualquier incumplimiento de los compromisos adquiridos mediante la firma del presente podrá implicar o implicará la expulsión de la Asociación mediante el correspondiente procedimiento fijado en los estatutos.</li>
	<li style="list-style:none;margin-left:-10px;margin-top:10px;"><b><u><?php echo $activado;?>El/la solicitante manifiesta haber leído los estatutos y la presente solicitud de participación en el programa de cultivo colectivo.</b></u></li>
</ul>

<p style="margin-top:10px;"><u><b>IDENTIDAD SOCIO/A AVALADOR/A (NOMBRE):</b></u> <span style="text-transform:uppercase"> <?php echo $avalador["nombre"]." ".$avalador["apellidos"] ?></span></p>
<p><u><b>NÚMERO DE SOCIO/A AVALADOR/A:</b></u> <span style="text-transform:uppercase"> <?php echo $avalador["nsocio"] ?></span></p>

<table style="margin-bottom:20px;">
	<tr>
		<td style="font-size:10px;"><u><b>FIRMA SOCIO/A AVALADOR/A:</b></u> </td><td><?php echo $firma; ?></td>
	</tr>
	<tr>
		<td style="font-size:10px;"><u><b>FIRMA INTERESADO:</b></u> </td><td><?php echo $firma2; ?></td>
	</tr>
</table>
</hr>
<div style="width:100%;height:1px;background-color:black;margin-bottom:10px;"></div><p></p>
	<p style="margin-top:20px;">DE LA ADMINISTRACIÓN:</p>
<p style="margin-top:10px;">SE ACEPTA LA PRESENTE SOLICITUD:<span style="margin-left:50px;border-radius:100px;border:1px solid black;padding:5px;">SI</span><span style="margin-left:100px;">NO</span></a</p>

<p style="margin-top:10px;">CON FECHA <span style="text-transform:uppercase"><?php echo fecha($datos["alta"]) ?></span></p></br>
<p style="margin-bottom:100px;">FIRMA MIEMBRO JUNTA DIRECTIVA:</p>
<div style="width:100%;height:1px;background-color:black;margin-bottom:5px;"></div><p></p>
<p> </p>
<p style="font-size:8px;">Declara dar su consentimiento expreso a que los datos recogidos en esta ficha serán incorporados a un fichero informático, siendo tratados con la debida confidencialidad y reserva apropiadas, conforme a la Ley Orgánica 15/1999, de 13 de diciembre, de Protección de Datos de Carácter Personal, para su gestión interna de la asociación. Usted podrá ejercer los derechos de acceso, rectificación y cancelación de sus datos mediante comunicación escrita dirigida a cualquier delegación de la <span class="text-uppercase"><?php echo $nombreA ?></span></p>

</body>
<?php
require_once 'dompdf/autoload.inc.php';
use Dompdf\Dompdf;
$dompdf = new DOMPDF();
$dompdf->load_html(ob_get_clean());
$dompdf->render();
$pdf = $dompdf->output();
$filename = "Ficha de socio.pdf";
file_put_contents($filename, $pdf);
$dompdf->stream($filename);
?>
