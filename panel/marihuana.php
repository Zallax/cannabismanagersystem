<?php

  include "php/cabecera.php";
?>

 <?php cabecera("cannabis");?>
 <?php
    $tipo = "verde";
    $titulo = "Marihuana";
    $subtitulo = "Gestiona la Marihuana de tu dispensario";
   if(ISSET($_GET["m"])){
         $tipo = "marron";
         $titulo = "Hachis";
         $subtitulo = "Gestiona el hachis de tu dispensario";
       }
    

 ?>
 <input class="d-none" value="<?php echo $tipo; ?>" id="tipo">
 <nav>

   <div class="modal fade2" style="display: none !important;" id="modalGenetica" tabindex="-1" role="dialog" aria-labelledby="modalGeneticaLabel" aria-hidden="true">
     <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 95%;" >
       <div class="modal-content">
         <div class="modal-header">
           <h5 class="modal-title" id="modalGeneticaLabel">Editar Genética</h5>
           <button type="button" class="close" data-dismiss="modal" aria-label="Close">
             <span aria-hidden="true">&times;</span>
           </button>
         </div>
         <div class="modal-body" id="contenidoModalGenetica">
           <div class="text-center cargando"><img src="img/cargando.svg" style="height:128px;"></div>
           <div class="contenido"></div>
         </div>
         <div class="modal-footer">
            <?php
        if($_SESSION["tipo"]==1){


      ?><div id="zonaEliminar">
           <button type="button" id="btnEliminarGenetica" class="btn btn-danger"><i class="fa fa-trash"></i></button>
           </div>
           <?php
              }
           ?>
           <div id="confirmarEliminar" style="display:none">
           <span style="margin-right:10px">¿Estás seguro?</span>
           <button class="btn btn-dark" id="btnSiEliminar"><i class="fa fa-check"></i></button>
           <button class="btn btn-danger" id="btnNoEliminar"><i class="fa fa-times"></i></button>
           </div>
           <button type="button" id="btnEditarGenetica" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
         </div>
       </div>
     </div>
   </div>
   </nav>
   <nav>
   <div class="modal fade2 bg-verde" style="display: none !important;" id="modalAGenetica" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
     <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 95%;" >
       <div class="modal-content">
         <div class="modal-header bg-dark text-white">
           <h5 class="modal-title" id="exampleModalLabel">Añadir Genética</h5>
           <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
             <span aria-hidden="true">&times;</span>
           </button>
         </div>
         <div class="modal-body anadirGenetica">
         <form id="formAnadirGenetica" action="js/ajax/cargarNuevaGenetica.php" method="POST" enctype="multipart/form-data">
            <input class="d-none" value="<?php echo $tipo; ?>" name="tipo">

        <div class="row">
             <div class="col-sm-4">

                <div class="fotoGenetica">
                   <!-- <div class="file-loading">
                        <input id="foto-a-genetica" name="avatar-1" type="file" accept="image/*" required>
                    </div>-->
                    <div class="tipoDato">Imagen</div>
                    <input type="file" class="custom-file-input" id="inputImg1" name="imgGen" lang="es" required="">
                                                          <label style="left: 20px;width:80%;top:32px;" class="custom-file-label" for="inputImg1" data-browse="Abrir">Seleccionar...</label>
                </div>
             </div>
             <div class="col-sm-8">
               <div class="descripcion">
                  <div class="tipoDato">Nombre</div>
                 <input type="text" placeholder="NOMBRE" name="ngenetica" required class="nombre-a-genetica" pattern="[A-Za-z0-9\s]+" minlength="4" maxlength="40" title="Sólo puedes introducir letras y números"></input>

                  <div class="tipoDato">Descripción</div>
                 <textarea placeholder="Descripción" name="dgenetica" required class="comentario-a-genetica"></textarea>
               </div>
           </div>
         </div>
         <div class="row" style="margin-top:30px">
           <div class="col-sm-6">
               <div class="tipoDato">Gusto</div>
               <input placeholder="GUSTO" type="text" name="ggenetica" class="dato"></input>
           </div>

           <div class="col-sm-6">
               <div class="tipoDato">Banco</div>
               <input placeholder="BANCO" type="text" name="bgenetica" class="dato"></input>
           </div>


         </div>
         <div class="row" style="margin-top:30px">
           <div class="col-sm-3">
               <div class="tipoDato">% Índica</div>
               <input placeholder="% ÍNDICA" type="text" name="pigenetica" class="dato">
           </div>
           <div class="col-sm-3">
               <div class="tipoDato">% Sativa</div>
               <input placeholder="% SATIVA" type="text" name="psgenetica" class="dato">
           </div>
           <div class="col-sm-3">
               <div class="tipoDato">% THC</div>
               <input placeholder="% THC" type="text" name="ptgenetica" class="dato">
           </div>
           <div class="col-sm-3">
               <div class="tipoDato">% CBD</div>
               <input placeholder="% CBD" type="text" name="pcgenetica" class="dato">
           </div>

         </div>
         <div class="row" style="margin-top:30px">
           <div class="col-sm-3">
               <div class="tipoDato">Stock <small style="display:inline-block">(Gr)</small></div>
               <input required placeholder="Stock" type="number" min="0" step=any name="sgenetica" class="dato">
           </div>
           <div class="col-sm-3">
               <div class="tipoDato">Precio <small style="display:inline-block">(€)</small></div>
               <input required placeholder="Precio" type="number" min="0" step="any" name="pgenetica" class="dato">
           </div>
           <div class="col-sm-6">
             <div><h2>TIPO</h2>
           <div class="btn-group btn-group-toggle" id="botonesTipo" data-toggle="buttons">
               <label class="btn btn-secondary btn-lg">
                 <input type="radio" name="categoria" value="indica" autocomplete="off" required> ÍNDICA
               </label>
               <label class="btn btn-secondary btn-lg">
                 <input type="radio" name="categoria" value="sativa" autocomplete="off" required> SATIVA
               </label>
               <label class="btn btn-secondary btn-lg">
                 <input type="radio" name="categoria" value="hibrida" autocomplete="off" required> HÍBRIDA
               </label>
               <label class="btn btn-secondary btn-lg">
                 <input type="radio" name="categoria" value="extraccion" autocomplete="off" required> EXTRACCIÓN
               </label>
               <label class="btn btn-secondary btn-lg">
                 <input type="radio" name="categoria" value="otros" autocomplete="off" required> OTROS
               </label>
             </div>
           </div>
           </div>
          </div>







         </div>
         <div class="modal-footer">
         <button id="btnAnadirGenetica" type="submit" class="btn btn-dark btn-block btn-lg">Guardar</button>
         </form>
         </div>
       </div>
     </div>
   </div>
   </nav>
  <div class="content-wrapper animated fadeIn background-container">
    <div class="container">








        <div class="row">
          <div class="col-sm-12 boton-a-genetica">
            <div class="card border-success" >
              <div class="card-body">
                <div class="icono-fondo">
                          <i class="fas fa-weight"></i>
              </div>
              <div class="row">
                <div class="col-sm-10">
                  <div class="titulo-seccion"><h5><?php echo $titulo; ?></h5>
                    <p class="text-muted"><?php echo $subtitulo; ?></p>
                  </div>
                  
                </div>
                <div class="col-sm-2">
                  <button type="button" class="btn btn-success btn-lg mt-3" data-toggle="modal" data-target="#modalAGenetica"><i class="fas fa-plus"></i> Añadir Genética</button>

                </div>
              </div>
              </div>
            </div>


          </div>

        </div>
        <div class="nav nav-tabs nav-justified nav-geneticas" id="nav-tab" role="tablist">

          <a class="nav-item nav-link active" id="btnMostrarIndicas" data-toggle="tab" href="#nav-indicas" role="tab" aria-controls="nav-indicas" aria-selected="true">Índica</a>
          <a class="nav-item nav-link" id="btnMostrarSativas" data-toggle="tab" href="#nav-sativas" role="tab" aria-controls="nav-sativas" aria-selected="false">Sativa</a>
          <a class="nav-item nav-link" id="btnMostrarHibridas" data-toggle="tab" href="#nav-hibridas" role="tab" aria-controls="nav-hibridas" aria-selected="false">Hibridas</a>
          <a class="nav-item nav-link" id="btnMostrarExtraccion" data-toggle="tab" href="#nav-extraccion" role="tab" aria-controls="nav-extraccion" aria-selected="false">Extracción</a>
          <a class="nav-item nav-link" id="btnMostrarOtros" data-toggle="tab" href="#nav-otros" role="tab" aria-controls="nav-otros" aria-selected="false">Otros</a>

        </div>
      <div class="card">
        <div class="card-body">
       <?php
        if(ISSET($_SESSION["mensaje"])){
          if($_SESSION["mensaje"]!=""){
              ?>
              <div class="alert alert-success" role="alert">
                <?php echo $_SESSION["mensaje"]; ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
              </div>

              <?php

            $_SESSION["mensaje"]="";

          }
        }

       ?>
      <div class="tab-content" id="nav-tabContent">
        <div class="tab-pane fade show active" id="nav-indicas" role="tabpanel" aria-labelledby="nav-home-tab">

          <div class="container-fluid margenArriba tablaIndicas">
            <div class="text-center"><img src="img/cargando.svg" style="height:128px;"></div>
          </div>
        </div>
        <div class="tab-pane fade" id="nav-sativas" role="tabpanel" aria-labelledby="nav-profile-tab">
           <div class="container-fluid margenArriba tablaSativas">
            <div class="text-center"><img src="img/cargando.svg" style="height:128px;"></div>
          </div>

        </div>
        <div class="tab-pane fade" id="nav-hibridas" role="tabpanel" aria-labelledby="nav-contact-tab">
           <div class="container-fluid margenArriba tablaHibridas">
            <div class="text-center"><img src="img/cargando.svg" style="height:128px;"></div>
          </div>

        </div>
        <div class="tab-pane fade" id="nav-extraccion" role="tabpanel" aria-labelledby="nav-contact-tab">
           <div class="container-fluid margenArriba tablaExtraccion">
            <div class="text-center"><img src="img/cargando.svg" style="height:128px;"></div>
          </div>

        </div>
        <div class="tab-pane fade" id="nav-otros" role="tabpanel" aria-labelledby="nav-contact-tab">
           <div class="container-fluid margenArriba tablaOtros">
            <div class="text-center"><img src="img/cargando.svg" style="height:128px;"></div>
          </div>

        </div>
      </div>
       </div>
        </div>



   <?php include "php/footer.php";?>
     <script src="js/verde.js"></script>

      <script type="text/javascript">
        if(opcionLateral!="si"){
          $("#expandir-dispensario").collapse('toggle');
          if($("#tipo").val()=="verde"){
            $("#m-marihuana").addClass("menu-seleccionado");
            
          } else {
            $("#m-hachis").addClass("menu-seleccionado");
          }
         
        }
      </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/fileinput.min.js"></script>

    <script src="js/locales/es.js"></script>

    <script src="themes/fa/theme.js"></script>

  </div>
</body>

</html>
