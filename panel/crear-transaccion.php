<?php
  require_once("php/config.php");
  require_once ("php/funciones.php");
  include "php/cabecera.php";
?>

 <?php cabecera("crear-trans");?>
 <div class="modal fade2" style="display: none !important;" id="modalSocio" tabindex="-1" role="dialog" aria-labelledby="modalSocioLabel" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 800px;" >
     <div class="modal-content">
       <div class="modal-header">
         <h5 class="modal-title" id="modalSocioLabel">Seleccionar socio</h5>
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
           <span aria-hidden="true">&times;</span>
         </button>
       </div>
       <div class="modal-body" id="contenidoModalSocios">
         <div class="text-center cargando"><img src="img/cargando.svg"></div>
         <div class="row mb-2" id="campos-buscar" style="display:none;">
            <div class="col-sm-12">Buscar por</div>
            <div class="col-sm-2"><input  type="number" class="form-control" id="buscar-numero" placeholder="Nº Socio"></div>
            <div class="col-sm-10"><input type="text" class="form-control" id="buscar-nombre" placeholder="Nombre, apellidos..."></div>
         </div>
         <div id="resultados" style="display:none;"></div>
       </div>
      
     </div>
   </div>
 </div>

<div class="modal fade2" style="display: none !important;" id="modalFinalizar" tabindex="-1" role="dialog" aria-labelledby="modalFinalizarLabel" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 570px;" >
     <div class="modal-content">
       <div class="modal-header">
         <h5 class="modal-title" id="modalFinalizarLabel">Confirmar</h5>
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
           <span aria-hidden="true">&times;</span>
         </button>
       </div>
       <div class="modal-body text-center" id="contenidoModalFinalizar">
          <div class="descuento" style="display:none;"> 
            <span class="font-weight-bold mb-2">Aplicado descuento del <?php echo $_SESSION["pdescuento"];?>% por socio terapéutico</span>
            <input id="pdescuento" style="display:none;" value="<?php echo $_SESSION["pdescuento"];?>">
     
      
      </div>
          <span>TOTAL</span>
        </br>
        <span style="font-weight:bold;font-size:30px;"><span id="total"></span>€</span>
       
       </div>
        <div class="modal-footer">
          <button class="btn btn-success btn-block btn-lg" id="btn-guardar"><i class="fas fa-save"></i> Guardar</button>
        </div>
     </div>
   </div>
 </div>


  <div class="content-wrapper animated fadeIn">
    <div class="container">
      <h5>Crear transacción</h5>

      <div class="card" id="sel-socio">
        <div class="card-body">
          <span id="cambiar-socio">Pulsa aquí para seleccionar un socio.</span>
          <div class="socio-seleccionado" style="display:none;">
            <div class="row">
              <div class="col-sm-2">
                <img src="" class="img-thumbnail">
              </div>
              <div class="col-sm-10">
                <h1 class="card-title"></h1>
                <h4 class="card-subtitle mb-2 text-muted"></h4><input class="d-none" id="id-socio-sel" value=""><input class="d-none" id="descuento" value="">
              </div>
            </div>
        </div>
        </div>
      </div>
     
      <div class="tab-content mt-3">
       
      <div class="card lista-finalizar-cannabis">
        <div class="card-header">
          <h4 style="margin-bottom:0px; width:60%;float:left;">Cannabis</h4><div style="width:100px;float:right"><button class="btn btn-outline-dark btn-sm" id="btnACannabis" data-container="body" data-toggle="popover" data-placement="right" data-content="<?php echo $cargando; ?>'" data-html="true"><i class="fas fa-cart-plus"></i> Añadir</button></div>
          

        </div>
        <ul class="list-group list-group-flush" id="listaCannabis">
          <li class="list-group-item sin-productos">Añade genéticas</li>
        </ul>

      </div>



   <div class="card lista-finalizar-productos">
        <div class="card-header">
          <h4 style="margin-bottom:0px; width:60%;float:left;">Productos</h4><div style="width:100px;float:right"><button class="btn btn-outline-dark btn-sm" id="btnAProducto" data-container="body" data-toggle="popover" data-placement="right" data-content="cargando" data-html="true"><i class="fas fa-cart-plus"></i> Añadir</button></div>
        </div>
      <ul class="list-group list-group-flush" id="listaProd">
        <li class="list-group-item sin-productos">Añade productos</li>
      </ul>

      </div>
      
      <button class="btn btn-success btn-block btn-lg" id="btn-total"><i class="fas fa-save"></i> Ver total</button>
        </div>
        </div>

       
    <?php include "php/footer.php";?>
    <script type="text/javascript" src="js/socios.js"></script>


   <script>
    let idCaja = $(".btn-tablet").attr("id-caja");

    if(idCaja==""){
      $("#btn-total").prop("disabled","true").removeClass("btn-segundo").addClass("btn-danger").html("Sólo la persona que ha abierto la caja puede registrar transacciones");

    }
    var cargando="<div class='text-center'><img width='50px' height='50px' src='img/cargando.gif'></div>";
    $(document).ready(function(){
      crearEventosEditarCannabis();
      crearEventosEditarProductos();

      /*
      $.post("php/ajax/vistaSeleccionCannabis.php", function(result){
          $(".mod-can").attr("data-content", result);
         
        });
      $('[data-toggle="popover"]').popover();

      */
      var anteriorPulsado;
      var ultimoPulsado;
      

      
     

      $('#btnACannabis').popover({
        trigger: 'click',
        template: '<div class="popover popover-a-can animated fadeIn">'+cargando+'</div>'
      });

      $('#btnAProducto').popover({
        trigger: 'click',
        template: '<div class="popover popover-a-can animated fadeIn">'+cargando+'</div>'
      });
     
     /* $(".mod-can").on("click", function(){
        
         
        /*$('[data-toggle="popover"]').popover("hide");
         actual.popover("toggle");
         

      });*/



      $('#btnACannabis').on("show.bs.popover",function(){
        $(".mod-can").popover("hide");
        $(".mod-prod").popover("hide");
        $("#btnAProducto").popover("hide");

        $.post("php/ajax/vistaSeleccionCannabis.php", function(result){
        
            $('.popover-a-can').empty().append(result); 
            $('.popover-a-can li').unbind().on("click", function(){
              var seleccionado = $(this).children("#nom-gen").html();
              var precio = $(this).children("#nom-gen").attr("precio");
              var tipo = $(this).children("#nom-gen").attr("tipo");

                var li = '<li style="display:none;background-color:rgb(160, 230, 160);" class="animated pulse list-group-item"><div class="row"><div class="col-12 col-sm-6 col-md-6 divnombre" tipo="'+tipo+'" precio="'+precio+'"><h5>'+seleccionado+'</h5></div><div class="col-6 col-sm-6 col-md-2" id="divcantidad"><div class="btn-group btn-cantidad" style="border:1px solid #bababa;border-radius:5px;" role="group" aria-label="First group"><button type="button" class="btn btn-secondary btn-menos">-</button><input class="text-center input-cantidad" size="3" value="1"></input><button type="button" class="btn btn-secondary btn-mas">+</button></div></div><div class="col-6 col-sm-3 col-md-4 text-right"><button class="btn btn-danger eli-can"><i class="fas fa-trash"></i></button></div></div></div></li>';
                if(($('#listaCannabis li').hasClass('sin-productos'))){
                  $("#listaCannabis").html(li);
                } else {
                  $("#listaCannabis").append(li);
                }
                $(".btn-cantidad .btn-mas").unbind().on("click", sumarCantidad);
                $(".btn-cantidad .btn-menos").unbind().on("click", restarCantidad);
                $("#listaCannabis li:last-child").fadeIn();
                $("#listaCannabis li:last-child").css("background-color","white");
                
                $(this).fadeIn("500");
                $('#btnACannabis').popover("hide");
                crearEventosEditarCannabis();

              });
              
  
            });
            
            });

      $('#btnAProducto').on("show.bs.popover",function(){
        $(".mod-can").popover("hide");
        $(".mod-prod").popover("hide");
        $("#btnACannabis").popover("hide");

        $.post("php/ajax/vistaSeleccionProductos.php", function(result){
      
            $('.popover-a-can').empty().append(result); 
            $('.popover-a-can li').unbind().on("click", function(){
              var seleccionado = $(this).attr("nombre");
              var precio = $(this).attr("precio");
                var li = '<li style="display:none;background-color:rgb(160, 230, 160);" class="animated pulse list-group-item"><div class="row"><div class="col-12 col-sm-6 col-md-6 divnombrep" precio="'+precio+'"><h5>'+seleccionado+'</h5></div><div class="col-6 col-sm-2 col-md-2" id="divcantidadp"><div class="btn-group btn-cantidad-p" style="border:1px solid #bababa;border-radius:5px;" role="group" aria-label="First group"><button type="button" class="btn btn-secondary btn-menos">-</button><input class="text-center input-cantidad" size="3" value="1"></input><button type="button" class="btn btn-secondary btn-mas">+</button></div></div><div class="col-6 col-sm-3 col-md-4 text-right"><button class="btn btn-danger eli-prod"><i class="fas fa-trash"></i></button></div></div></div></li>';
                if(($('#listaProd li').hasClass('sin-productos'))){
                  $("#listaProd").html(li);
                } else {
                  $("#listaProd").append(li);
                }
                $(".btn-cantidad-p .btn-mas").unbind().on("click", sumarCantidadP);
                $(".btn-cantidad-p .btn-menos").unbind().on("click", restarCantidadP);
                $("#listaProd li:last-child").fadeIn();
                setTimeout(function(){
                  $("#listaProd li:last-child").css("background-color","white");
                }, 1000);
                
                $(this).fadeIn("500");
                $('#btnAProducto').popover("hide");
                crearEventosEditarProductos();

              });
              
      
            });
            
            });

      $("#btn-total").on("click", function(){
        var precioTotal = 0;
        var vacio = true;
        var socio = false;
        if(!($("#listaCannabis li").hasClass("sin-productos"))){
          vacio = false;
          let pdescuento = parseFloat($("#pdescuento").val()/100);
          
          $("#listaCannabis li").each(function(n){
            var tipo = $(this).find(".divnombre").attr("tipo");
            let precio = parseFloat($(this).find(".divnombre").attr("precio"));
            var cant = parseFloat($(this).find(".input-cantidad").val());
            if($("#descuento").val()==1 && (tipo=="v" || tipo=="m")){
              
              let precioDesc = parseFloat(precio*pdescuento);
              let precioFinal = parseFloat(precio-precioDesc);
              precioTotal = parseFloat(precioTotal)+parseFloat(precioFinal*cant);
            } else {
              precioTotal = parseFloat(precioTotal)+parseFloat(precio*cant);
            } 
            
          });
        }
        if(!($("#listaProd li").hasClass("sin-productos"))){
          vacio = false;

          $("#listaProd li").each(function(n){
            var precio = parseFloat($(this).find(".divnombrep").attr("precio"));
            var cant = parseFloat($(this).find(".input-cantidad").val());
            precioTotal = parseFloat(precioTotal)+parseFloat(precio*cant);
          });
        }

        if($(".card-title").html()!=""){
          socio = true;
        }
        if(!vacio && socio){
          if($("#descuento").val()==1){
            $(".descuento").show();
            
           
       
            $("#total").html((precioTotal).toFixed(2));
            $("#modalFinalizar").modal("show");

          } else {
            $(".descuento").hide();
            $("#total").html((precioTotal).toFixed(2));
            $("#modalFinalizar").modal("show");
          }
     
        }
        
      });

      $("#btn-guardar").on("click", function(){
        $(this).attr("disabled",true);
        var articulos = new Array();
        if(!($("#listaCannabis li").hasClass("sin-productos"))){
          $("#listaCannabis li").each(function(n){
            var zona = $(this).children();
               let pdescuento = parseFloat($("#pdescuento").val()/100);
               if($("#descuento").val()==1){
                 /*
                 let precioDesc = parseFloat(precio*pdescuento);
                 let precioFinal = parseFloat(precio-precioDesc);
                 precioTotal = parseFloat(precioTotal)+parseFloat(precioFinal*cant);*/

                 let precio = parseFloat(zona.children().attr("precio"));
                 let precioDesc = parseFloat(precio*pdescuento).toFixed(2);
                 let totalPrecio = parseFloat(precio-precioDesc).toFixed(2);
                var articulo = [1, zona.children().children().html()+"", zona.find('#divcantidad>.btn-cantidad>input').val()+"", totalPrecio, zona.children().attr("tipo")];

               } else {
                let precio = parseFloat(zona.children().attr("precio"));
                 var articulo = [1, zona.children().children().html()+"", zona.find('#divcantidad>.btn-cantidad>input').val()+"", precio, zona.children().attr("tipo")];
               } 
            
            articulos.push(articulo);
           // articulos.push('{1,"'+zona.children().children().html()+'","'+zona.find('#divcantidad>input').val()+'"}');
          
            
          });
        }

        if(!($("#listaProd li").hasClass("sin-productos"))){
          $("#listaProd li").each(function(n){

            var zona = $(this).children();
            

            var articulo = [2, zona.children().children().html()+"", zona.find('#divcantidadp>.btn-cantidad-p>input').val(), parseFloat(zona.children().attr("precio"))];
            articulos.push(articulo);
             //articulos.push('{2,"'+zona.children().children().html()+'","'+zona.find('#divcantidadp>input').val()+'"}');
            
            
          });
       }
        
        
        var json = JSON.stringify(articulos);
        var id=$("#id-socio-sel").val();
        
        var total = $("#total").html();
        $("#btn-guardar").fadeOut("500");
        
        
        $.post("php/funciones.php", {funcion:"registrarCompra", json:json, id:id, total:total, idCaja:idCaja}, function(result){
      
          if(result=="success"){
            $(".content-wrapper").css("opacity","0");
            setTimeout(function(){
              localStorage.setItem("mensaje", "Se ha añadido la transacción correctamente.")
            
                location.href ="index.php";
            },300)
          } else {
            $("#btn-guardar").fadeIn("500");
            mostrarMensaje("Ha ocurrido un error.");
            $(".btn-guardar").attr("disabled", false);
            console.log(result);
          }
        });
     

      });


      });


      function crearEventosEditarCannabis(){
        $(".eli-can").unbind().on("click", function(){
        var elemento =  $(this).parent().parent().parent();

       elemento.css("background-color","#ff5353");
       elemento.addClass("animated bounceOut");
       //elemento.css("opacity","0");
        setTimeout( function() {
           elemento.remove();
           if($("#listaCannabis li").length==0){
            $('#listaCannabis').append('<li class="list-group-item list-group-item-action sin-productos">Sin genéticas</li>');
           }
       }, 700);
     
      });


      }

      function crearEventosEditarProductos(){
         
        $(".eli-prod").unbind().on("click", function(){
        var elemento =  $(this).parent().parent().parent();
       elemento.css("background-color","#ff5353");
       elemento.addClass("animated bounceOut");
        setTimeout( function() {
           elemento.remove();
           
           if($("#listaProd li").length==0){
            $('#listaProd').append('<li class="list-group-item list-group-item-action sin-productos">Sin productos</li>');
           }
       }, 700);

        
     
      });
      }

    
/// MODAL SELECCION SOCIO
    $("#sel-socio").on("click", function(){
      $("#buscar-nombre").unbind().on("keyup", function(){
        var texto = $(this).val();
        $.post("php/ajax/vistaSeleccionSocio.php", {nombre:texto}, function(result){
          $("#contenidoModalSocios #resultados").html(result);
        });
      });
      $("#buscar-numero").unbind().on("keyup", function(){
        var texto = $(this).val();
        $.post("php/ajax/vistaSeleccionSocio.php", {nsocio:texto}, function(result){
          $("#contenidoModalSocios #resultados").html(result);
        });
      });


      $.post("php/ajax/vistaSeleccionSocio.php", function(result){
        $(".cargando").fadeOut("500", function(){
          $("#contenidoModalSocios #campos-buscar").fadeIn("500");
          $("#contenidoModalSocios #resultados").html(result);
          $("#contenidoModalSocios #resultados").fadeIn("500");
        });
        
        
      });
      $("#modalSocio").modal("show");
  
    });

    function seleccionarSocio(id, nombre, nsocio, imagen, descuento){
      $("#modalSocio").modal("hide");
      $("#sel-socio #cambiar-socio").fadeOut("500", function(){
          $(".card-title").html(nombre);
          $(".card-subtitle").html(nsocio);
          $("#id-socio-sel").attr("value", id);
          $("#descuento").attr("value", descuento);
          console.log(id);
          $(".socio-seleccionado .img-thumbnail").attr("src", imagen);
          $("#sel-socio .socio-seleccionado").fadeIn("500");
      });

    }


      if(opcionLateral!="si"){
        $("#t-socios").addClass("menu-seleccionado");
        $("#expandir-dispensario").collapse('toggle');
      }



      function sumarCantidad(){
        var boton = $(this);
        var input = boton.parent().find('.input-cantidad');
        let cant = parseFloat(input.val())+0.5;
        input.val(cant);

        

      }





      function restarCantidad(){
        var boton = $(this);
        var input = boton.parent().find('.input-cantidad');
        if((input.val()-0.5)>0){
          let cant = parseFloat(input.val())-0.5;
          input.val(cant);
        }

        

      }


      //PRODUCTOS

        


        function sumarCantidadP(){
          var boton = $(this);
          var input = boton.parent().find('.input-cantidad');
          let cant = parseFloat(input.val())+1;
          input.val(cant);

          

        }



        

        function restarCantidadP(){
          var boton = $(this);
          var input = boton.parent().find('.input-cantidad');
          if((input.val()-1)>0){
            let cant = parseFloat(input.val())-1;
            input.val(cant);
          }

          

        }



  </script>
  
</body>

</html>
