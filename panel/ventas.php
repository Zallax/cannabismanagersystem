<?php
  require_once("php/config.php");
  require_once ("php/funciones.php");
  include "php/cabecera.php";
  $hoy = getdate();
?>

 <?php cabecera("ventas");
 if($_SESSION["tipo"]==1){

 ?>


  <div class="content-wrapper animated fadeIn background-container">
    <div class="container">
      <div class="card mb-2">
        <div class="card-body row">
          <div class="col-sm-2">
            <div class="icono-fondo" style="top:0%; left:30%;">
                        <i class="fas fa-euro-sign"></i>
            </div>
          </div>
         <div class="col-sm-10">
          <h3 class="card-title mt-3">Ventas</h3>
          <p>Selecciona el periódo de ventas que quieres consultar.</p>
         </div>
          
         


        </div>
        <div class="card-footer">
          <div class="row ">



            <div class="col-lg-6">
              <p class="font-weight-bold">Acceso rápido:</p>
              <div class="btn-group btn-block mb-3" role="group" aria-label="acceso-rapido">
                <button type="button" class="btn btn-outline-dark" id="btn-ayer">Ayer</button>
                <button type="button" class="btn btn-outline-dark" id="btn-semana">Última semana</button>
                <button type="button" class="btn btn-outline-dark" id="btn-mes">Último mes</button>
                <button type="button" class="btn btn-outline-dark" id="btn-ano">Último año</button>
              </div>
            </div>
            <div class="col-lg-6">
             <p class="font-weight-bold">Selecciona el periódo de ventas que quieres consultar.</p>

              <button class="btn btn-outline-info btn-block" type="button" data-toggle="collapse" data-target="#fechas" aria-expanded="false" aria-controls="fechas">
                  <i class="fas fa-calendar-alt"></i> Elegir fechas
                </button>
              <div class="collapse" id="fechas">
                <div class="row mt-3">
                  <div class="col-sm-6">
                    <div class="input-group input-group-lg">
                      <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-lg">Del</span>
                      </div>
                      <input type="date" class="form-control" id="inputF1" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-lg">
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="input-group input-group-lg">
                      <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-lg">Al</span>
                      </div>
                      <input type="date" class="form-control" id="inputF2" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-lg">
                    </div>

                  </div>

                  <div class="col-sm-12 mt-3">
                    <button class="btn btn-outline-primary btn-block btn-lg" id="btn-consultar">Consultar</button>

                  </div>

                </div>
              </div>
            </div>
            <!-- -->
              


          </div>
        </div>

      </div>

      <div class="text-center mt-5 cargando" style="display:none;"><img src="img/cargando.svg"></div>
      <div class="tab-content" style="display:none;">

        <div class="row">
          <div class="col-lg-3">

                  <div class="card mb-2">
                    <div class="card-header bg-white">
                      <h5 class="card-title">Aportaciones de cultivo</h5>

                  
                  
                </div>
                <div class="card-footer text-muted text-right">
                   <div style="font-size:24px; font-weight:100;"><small class="text-muted">TOTAL</small> <i class="fas fa-angle-right"> </i><span class="text-dark"><span id="total-geneticas">0</span> €</span></div>
                 </div>
              </div>

            <!--<div class="alert text-center" role="alert">
               <small class="text-muted">TOTAL GENÉTICAS</small></br><span class="badge badge-pill badge-success" style="font-size:50px; font-weight:100;"><span id="total-geneticas"></span> €</span>
            </div>-->
          </div>
          <div class="col-lg-3">
                <div class="card mb-2">
                  <div class="card-header bg-white">
                    <h5 class="card-title">Total productos</h5>

                
                
              </div>
              <div class="card-footer text-muted text-right">
                 <div style="font-size:24px; font-weight:100;"><small class="text-muted">TOTAL</small> <i class="fas fa-angle-right"> </i><span class="text-dark"><span id="total-productos">0</span> €</span></div>
               </div>
            </div>


            <!--<div class="alert text-center" role="alert">
              <small class="text-muted">TOTAL PRODUCTOS</small></br><span class="badge badge-pill badge-success" style="font-size:50px; font-weight:100;"><span id="total-productos"></span> €</span>
            </div>-->


          </div>
          <div class="col-lg-3">

              <div class="card mb-2">
                <div class="card-header bg-white">
                  <h5 class="card-title">Total Cuotas</h5>

              
              
            </div>
            <div class="card-footer text-muted text-right">
               <div style="font-size:24px; font-weight:100;"><small class="text-muted">TOTAL</small> <i class="fas fa-angle-right"> </i><span class="text-dark"><span id="total-cuotas">0</span> €</span></div>
             </div>

           <!-- <div class="alert text-center" role="alert">
            <small class="text-muted">TOTAL PERÍODO</small></br><div class="cant-total badge badge-pill badge-info"><span id="total-hoy">0</span>€</span></div>
            </div>-->
          </div>
        </div>

          <div class="col-lg-3">

                <div class="card mb-2">
                  <div class="card-header">
                    <h5 class="card-title">Total general</h5>

                
                
              </div>
              <div class="card-footer bg-success  text-muted text-right">
                 <div style="font-size:24px; font-weight:100;"><small class="text-muted">TOTAL</small> <i class="fas fa-angle-right"> </i> <span class="text-white"> <span id="total-hoy">0</span> €</span></div>
               </div>



            <!--<div class="alert text-center" role="alert">
            <small class="text-muted">TOTAL PERÍODO</small></br><div class="cant-total badge badge-pill badge-info"><span id="total-hoy">0</span>€</span></div>-->

            </div>
          </div>
          <div class="col-sm-12 text-right">
            <button id="btn-elim" class="btn btn-danger mr-3"><i class="material-icons md-48 text-white">
                 delete
                 </i></br>ELIMINAR</button>
               <button id="btn-impr" class="btn btn-blanco"><i class="material-icons md-48 text-dark">
                    print
                    </i></br>IMPRIMIR</button>
                    
          </div>
        </div>

        <div class="row mt-3">
          <div class="col-lg-6 col-12">

            <div class="card mb-2">
              <div class="card-header bg-white">
                <h5 class="card-title">Genéticas dispensadas</h5>
              </div>
              <div class="class-body">
                
                <div id="zonaTablaGen"></div>
              </div>
            </div>
            <!--
            <h3>Genéticas dispensadas</h3>
           <div id="zonaTablaGen"></div>-->


          </div>
          <div class="col-lg-6 col-12">
           <div class="card">
                         <div class="card-header bg-white">
                           <h5 class="card-title">Productos vendidos</h5>
                         </div>
                         <div class="class-body">
                           
                           <div id="zonaTablaProd"></div>
                         </div>
                       </div>
            </div>

          </div>




        </div>








        </div>
      </div>
    </div>

    <div class="modal fade" id="modalEliminarDatos" tabindex="-1" role="dialog" aria-labelledby="modalEliminarDatosLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content" >
          <div class="modal-header">
            <h5 class="modal-title" id="modalEliminarDatosLabel">Confirma la operación</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-sm-12 col-md-3 col-lg-2 col-xl-2 text-danger text-center" style="font-size:4em;opacity:0.5">
                <i class="fas fa-exclamation-triangle"></i>
              </div>
              <div class="col-sm-12 col-md-9 col-lg-10 col-xl-10">
                <h5 class="text-danger">Un momento...</h5>

                <div class="alert alert-danger" role="alert">
                  <h2>Esta acción eliminará toda la información entre el <span id="cF1" class="font-weight-bold"></span> y el <span id="cF2" class="font-weight-bold"></span></h2>
                   </br>
                   <b>Estos datos no podrán recuperarse.</b></br> ¿Estás seguro de que deseas continuar?
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary w-50 btn-lg" data-dismiss="modal"><i class="fas fa-times"></i></button>
            <button type="button" id="btn-c-e-datos" class="btn btn-danger w-50 btn-lg"><i class="fas fa-check"></i></button>
          </div>
        </div>
      </div>
    </div>

 <?php
      } else {
        errorDePermisos();
      }
   ?>

    <?php include "php/footer.php";?>
    <script src="js/ventas.js"></script>
    <script>
      if(opcionLateral!="si"){
        $("#d-ventas").addClass("menu-seleccionado");
        $("#expandir-mostrador").collapse('toggle');
      }

      function imprim1(){
      //var printContents = $(".tab-content").html();
      var fecha1 = $("#inputF1").val();
      var fecha2 = $("#inputF2").val();
      var header = '<html><head><style>table{font-size:12px;}th{background-color:black;color:white}</style></head><body>';
      var cabecera = '<h3>Informe entre el '+fecha1+' y el '+fecha2+'</h3>';
      var tablaTotales = '<table border="1" style="width:100%;"><tr><td>Aportaciones</td><td>Productos</td><td>Cuotas</td><td>Total</td></tr>'+
      '<tr><td>'+$("#total-geneticas").html()+'€</td><td>'+$("#total-productos").html()+'€</td><td>'+$("#total-cuotas").html()+'€<td>'+$("#total-hoy").html()+'€</td></tr></table>';

      var tituloGen = '<div style="float:left;width:55%;"><h4>Genéticas dispensadas</h4>';
      var tablaGen = '<table border="1">';
      $("#tabla-caja-geneticas tr").each(function(n){
        tablaGen+='<tr>';
        tablaGen+=$(this).html();
        tablaGen+='</tr>';
      });
      tablaGen+='</table></div>';
      var tituloProd = '<div style="float:right;width:43%;"><h4>Productos vendidos</h4>';
      var tablaProd = '<table border="1">';

      $("#tabla-caja-productos tr").each(function(n){
        tablaProd+='<tr>';
        tablaProd+=$(this).html();
        tablaProd+='</tr>';
      });
      tablaProd+='</table></div>';
      var fin = '</body></html>'
      //var tablaGen = $("#tabla-caja-geneticas").html();
     // var tablaProd = $("#tabla-productos").html();

      var salida = header+cabecera+tablaTotales+tituloGen+tablaGen+tituloProd+tablaProd+fin;
              w = window.open();
              w.document.write(salida);
              w.document.close(); // necessary for IE >= 10
              w.focus(); // necessary for IE >= 10
          w.print();
          w.close();
              return true;}
      </script>
      </script>
  </div>


</body>

</html>
