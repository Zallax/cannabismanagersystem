<?php
session_start();
session_destroy();

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Isla Bonita | Panel de Gestión</title>
  <!-- Bootstrap core CSS-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <!-- Custom fonts for this template-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">
  <link rel="stylesheet" href="css/animate.css">
</head>

<body>
  <div class="container">
  <div class="logo text-center" style="margin-top:20px;">
  <!--<img width="200px" height="200px" src="img/logo.png">-->
  <h3 style="color:white;">PANEL DE GESTIÓN</H3>
  </div>

    <div class="card card-login mx-auto mt-2">
      
      <div class="card-header">Iniciar sesión</div>
      <div class="card-body">
          <div class="alert alert-danger" style="display:none;" role="alert">
            Los datos no son correctos.
          </div>
          <div class="form-group">
            <label for="inputMail">Email</label>
            <input class="form-control" id="inputMail" type="email" aria-describedby="emailHelp" placeholder="Introduce tu email">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Contraseña</label>
            <input class="form-control" id="inputPass" type="password" placeholder="Introduce tu contraseña">
          </div>
          <div class="form-group">
            <div class="form-check">
              <label class="form-check-label">
                <input class="form-check-input" id="recordar-mail" type="checkbox"> Recordar email</label>
            </div>
          </div>
          <button id="btnAcceder" class="btn btn-info btn-block">Acceder</button>


      </div>
    </div>
  </div>
  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="js/sesion.js"></script>
</body>

</html>
