<?php
  require_once("php/config.php");
  require_once ("php/funciones.php");
  include "php/cabecera.php";
?>

 <?php cabecera("inicio");?>
  <div class="content-wrapper animated fadeIn">
    <div class="container zona-nuevo-usuario">
      <div class="alert alert-info" role="alert">
        <div class="icono-fondo">
                    <i class="fas fa-users"></i>
        </div>
        <div class="titulo-seccion"><h3>Nuevo usuario</h3></div>

      </div>
      <?php
        if(ISSET($_GET["e"])){
          if($_GET["e"]==1){
            echo '<div class="alert alert-danger" role="alert">
                    Ha ocurrido un error al registrar a este usuario
                  </div>';
          }
        }
      ?>
      <div class="tab-content">
        <form action="js/ajax/registrar-usuario.php" method="POST">
          <input style="display:none" type="password" name="fakepasswordremembered"/>
          <input style="display:none" type="email" name="fakepasswordremembered"/>
          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="inputNombre">Nombre</label>
              <input required type="text" class="form-control" name="n" id="inputNombreU" placeholder="Introduce el nombre" autocomplete="ÑÖcompletes">
            </div>
            <div class="form-group col-md-6">
              <label for="inputApellidos">Apellidos</label>
              <input required type="text" class="form-control" name="a" id="inputApellidosU" placeholder="Introduce los apellidos" autocomplete="off">
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="inputEmail">Email</label>
              <input required type="email" class="form-control" name="m" id="inputEmailU" placeholder="Introduce el Email" autocomplete="ÑÖcompletes">
            </div>
            <div class="form-group col-md-6">
              <label for="inputPass">Contraseña</label>
              <input required type="password" class="form-control" name="p" id="inputPass" placeholder="Introduce la contraseña" spellcheck="false">
            </div>
          </div>

          <div class="form-row">

            <div class="form-group col-md-12">
              <label for="inputState">Tipo de cuenta</label>
              <select id="inputState" name="t" class="form-control" required>
                <option selected value="">Elige un tipo de cuenta...</option>
                <option value="1">Administrador</option>
                <option value="2">Gestor</option>

              </select>
            </div>

          </div>
          <div class="text-right">
          <button type="submit" class="btn btn-segundo btn-block btn-lg"><i class="fas fa-file-alt"></i> Añadir usuario</button>
        </div>
        </form>
    </div>
        </div>
    <?php include "php/footer.php";?>
    <script src="js/socios.js"></script>

  </div>
</body>

</html>
