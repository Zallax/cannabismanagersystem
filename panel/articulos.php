<?php

  include "php/cabecera.php";
?>

 <?php cabecera("articulos");?>
 <nav>
        <!-- MODAL ELIMINAR CATEGORIA -->


        <div class="modal fade2" style="display:none !important;" id="modalEliminarCat" tabindex="-1" role="dialog" aria-labelledby="modalEliminarCatLabel" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 500px;" >
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="modalEliminarCatLabel">Eliminar Categoría</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body" id="contenidoModalCategoria">
                <div class="text-center"><img src="img/cargando.gif" style="height:128px;"></div>
              </div>
              <div class="modal-footer">
                <div id="zonaEliminar">
                <button type="button" id="btnEliminarCat" class="btn btn-danger"><i class="fa fa-trash"></i> Eliminar categoría</button>
                </div>


              </div>
            </div>
          </div>
        </div>

        <!-- MODAL EDITAR CATEGORIA -->


        <div class="modal fade2" style="display:none !important;" id="modalModCat" tabindex="-1" role="dialog" aria-labelledby="modalModCatLabel" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 500px;" >
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="modalEliminarCatLabel">Editar Categoría</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body" id="contenidoEditarCategoria">
                <label for="i-mod-cat">Nombre</label>
                <input type="text" class="form-control" id="i-mod-cat" idCat="" value="" placeholder="Nombre de Categoría">
                <label for="desc" class="mt-4">Descripción</label>
                    <textarea class="form-control" id="t-mod-cat" rows="3"></textarea>
              </div>
              <div class="modal-footer">
                <div id="zonaGuardar">
                <button type="button" id="btnEditarCat"  class="btn btn-success btn-block"><i class="fas fa-save"></i> Guardar cambios</button>
                </div>


              </div>
            </div>
          </div>
        </div>

        <!-- MODAL AÑADIR CATEGORIA -->


        <div class="modal fade2" style="display:none !important;" id="modalNuevaCat" tabindex="-1" role="dialog" aria-labelledby="modalNuevaCatLabel" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 500px;" >
            <div class="modal-content">
              <form id="formNuevaCategoriaArticulo">
              <div class="modal-header">
                <h5 class="modal-title" id="modalNuevaCatLabel">Añadir Categoría</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body" id="contenidoNuevaCategoria">

                
                <label for="i-mod-cat">Nombre</label>
                <input type="text" class="form-control" id="nombre-cat" placeholder="Ej. Refrescos" required value="" placeholder="Nombre de Categoría" required>
                <label for="desc" class="mt-4">Descripción</label>
                    <textarea class="form-control" id="desc-cat" placeholder="Ej. Bebidas frías y otros" rows="3" required></textarea>
                
              </div>
              <div class="modal-footer">
                <div id="zonaGuardar">
                <button id="btnGuardarCat" type="submit" class="btn btn-success btn-block"><i class="fas fa-save"></i> Guardar cambios</button>
                </div>



              </div>
            </form>
            </div>
          </div>
        </div>


        <!-- MODAL PRODUCTO -->
        <div class="modal fade2" style="display:none !important;" id="modalProducto" tabindex="-1" role="dialog" aria-labelledby="modalGeneticaLabel" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document" style="min-width: 30%;" >
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="modalGeneticaLabel">Editar Artículo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body" id="contenidoModalArticulo">
                <div class="text-center"><img src="img/cargando.gif" style="height:128px;"></div>
              </div>
              <div class="modal-footer">
                <div id="zonaEliminarProd">
                <button type="button" id="btnEliminarProducto" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                </div>
                <div id="confirmarEliminar" style="display:none">
                <span style="margin-right:10px">¿Estás seguro?</span>
                <button class="btn btn-dark" id="btnSiEliminar"><i class="fa fa-check"></i></button>
                <button class="btn btn-danger" id="btnNoEliminar"><i class="fa fa-times"></i></button>
                </div>
                <button type="button" id="btnGuardarProducto" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
              </div>
            </div>
          </div>
        </div>
        </nav>
  <div class="content-wrapper animated fadeIn">
    <div class="container contenedor-articulos">











        <div class="row justify-content-center h-100">
          <div class="col-sm-12">
            <div class="alert alert-light border-warning" role="alert">
            <div class="icono-fondo">
                        <i class="material-icons md-48 text-warning">
                        shopping_basket
                        </i>

            </div>
              <div class="titulo-seccion"><span>Artículos</span><p>Gestiona los artículos disponibles en el inventario.</p></div>
          </div>
          </div>
<!--  
        <div class="col-sm-3">

          <div class="card">
            <div class="card-header">
             Nueva Categoría
            </div>
            <div class="card-body">
            <form id="formNuevaCategoriaArticulo">
            <div class="form-group">
            <label for="nombre-cat">Nombre</label>
            <input type="text" class="form-control" id="nombre-cat" placeholder="Ej. Refrescos" required>
            </div>
            <div class="form-group">
            <label for="nombre-cat">Descripción</label>
            <textarea type="text" class="form-control" id="desc-cat" placeholder="Ej. Bebidas frías y otros"></textarea>
            <div class="margenArriba text-right"><button type="submit" class="btn btn-primary">Guardar</button></div>
            </div>
            </form>
        </div>
      </div>
    </div>-->
    <div class="col-sm-3 zona-categorias">
     <h5 class="ml-4">Categorías</h5>
      <div class="nav flex-column nav-pills" id="zonaCategoriasArticulos" role="tablist" aria-orientation="vertical">

      Cargando...

      </div>
      <div class="text-center">

      <button class="btn btn-secondary w-50" data-toggle="modal" data-target="#modalNuevaCat"><i class="fas fa-plus"></i> Añadir</button>
    </div>
    </div>
    <div class="col-sm-9 zona-articulos">

        <div class="row zona-a-articulos">
          <div class="col-sm-12">
            <h5 style="float:left" class="ml-4">Artículos</h5>
            <button class="btn btn-info mb-2" type="button" style="float:right" data-toggle="collapse" data-target="#collapseArticulo" aria-expanded="false" aria-controls="collapseArticulo">
              <i class="fas fa-plus"></i> AÑADIR
            </button>

          </div>
        </div>
            <div class="collapse" style="margin-bottom:20px" id="collapseArticulo">
                  <div class="mt-4">
                    <form id="formNuevoArticulo">
                  <div class="form-group row">
                    <label for="nombre-articulo" class="col-sm-2 col-form-label">Nombre</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="nombre-articulo" placeholder="Introduce el nombre" required>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="desc-articulo" class="col-sm-2 col-form-label">Descripción</label>
                    <div class="col-sm-10">
                      <textarea type="text" class="form-control" id="desc-articulo" placeholder="Introduce la descripción" required></textarea>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="precio-articulo" class="col-sm-2 col-form-label">Precio (€)</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="precio-articulo" placeholder="Introduce el precio" required>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="nombre-articulo" class="col-sm-2 col-form-label">Stock</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="stock-articulo" placeholder="Introduce el nombre" required>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="selectCategorias" class="col-sm-2 col-form-label">Categoría</label>
                    <div class="col-sm-10">
                      <select class="form-control" id="selectCategorias" required>


                                </select>
                    </div>
                  </div>

                  <div class="margenArriba text-right"><button type="submit" class="btn btn-primary">Guardar</button></div>
        </form>
                </div>
                </div>
      <div class="tab-content" id="zonaContenedoresArticulos">
      Cargando...


      </div>
    </div>
  </div>


   <?php include "php/footer.php";?>

    <script src="js/articulos.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/plugins/piexif.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/plugins/sortable.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/plugins/purify.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/fileinput.min.js"></script>

    <script src="js/locales/es.js"></script>

    <script src="themes/fa/theme.js"></script>
    <script type="text/javascript">

    </script>
  </div>
</body>

</html>
