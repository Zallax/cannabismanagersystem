<?php

  include "php/cabecera.php";
?>

 <?php cabecera("hachis");?>

 <nav>
        <div class="modal fade2" style="display: none !important;" id="modalGenetica" tabindex="-1" role="dialog" aria-labelledby="modalGeneticaLabel" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 95%;" >
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="modalGeneticaLabel">Editar Genética</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body" id="contenidoModalGenetica">
                <div class="text-center"><img src="img/cargando.gif" style="height:128px;"></div>
              </div>
              <div class="modal-footer">
                <div id="zonaEliminar">
                <button type="button" id="btnEliminarGenetica" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                </div>
                <div id="confirmarEliminar" style="display:none">
                <span style="margin-right:10px">¿Estás seguro?</span> 
                <button class="btn btn-dark" id="btnSiEliminar"><i class="fa fa-check"></i></button>
                <button class="btn btn-danger" id="btnNoEliminar"><i class="fa fa-times"></i></button>
                </div>
                <button type="button" id="btnEditarGenetica" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
              </div>
            </div>
          </div>
        </div>
        </nav>


        <nav>
        <div class="modal fade2 bg-verde" style="display: none !important;" id="modalAGenetica" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 95%;" >
            <div class="modal-content">
              <div class="modal-header bg-dark text-white">
                <h5 class="modal-title" id="exampleModalLabel">Añadir Genética</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body anadirGenetica">
              <form id="formAnadirGenetica">
        
              
             <div class="row">
                  <div class="col-sm-4">

                     <div class="fotoGenetica">
                         <div class="file-loading">
                             <input id="foto-a-genetica" name="avatar-1" type="file" accept="image/*" required>
                         </div>
                     </div>
                  </div>
                  <div class="col-sm-8">
                    <div class="descripcion">
                       <div class="tipoDato">Nombre</div>
                      <input type="text" placeholder="NOMBRE" id="ngenetica" required class="nombre-a-genetica" pattern="[A-Za-z0-9\s]+" minlength="4" maxlength="40" title="Sólo puedes introducir letras y números"></input>
                     
                       <div class="tipoDato">Descripción</div>
                      <textarea placeholder="Descripción" id="dgenetica" required class="comentario-a-genetica"></textarea>
                    </div>
                </div>
              </div>
              <div class="row" style="margin-top:30px">
                <div class="col-sm-6">
                    <div class="tipoDato">Gusto</div>
                    <input placeholder="GUSTO" type="text" id="ggenetica" class="dato">
                </div>
               
                <div class="col-sm-6">
                    <div class="tipoDato">Banco</div>
                    <input placeholder="BANCO" type="text" id="bgenetica" class="dato">
                </div>

                
              </div>
              <div class="row" style="margin-top:30px">
                <div class="col-sm-3">
                    <div class="tipoDato">% Índica</div>
                    <input placeholder="% ÍNDICA" type="text" id="pigenetica" class="dato">
                </div>
                <div class="col-sm-3">
                    <div class="tipoDato">% Sativa</div>
                    <input placeholder="% SATIVA" type="text" id="psgenetica" class="dato">
                </div>
                <div class="col-sm-3">
                    <div class="tipoDato">% THC</div>
                    <input placeholder="% THC" type="text" id="ptgenetica" class="dato">
                </div>
                <div class="col-sm-3">
                    <div class="tipoDato">% CBD</div>
                    <input placeholder="% CBD" type="text" id="pcgenetica" class="dato">
                </div>

              </div>
              <div class="row" style="margin-top:30px">
                <div class="col-sm-3">
                    <div class="tipoDato">Stock <small style="display:inline-block">(Gr)</small></div>
                    <input required placeholder="Stock" type="number" step=any id="sgenetica" class="dato">
                </div>
                <div class="col-sm-3">
                    <div class="tipoDato">Precio <small style="display:inline-block">(€)</small></div>
                    <input required placeholder="Precio" type="number" step=any id="pgenetica" class="dato">
                </div>
                <div class="col-sm-6">
                  <div><h2>TIPO</h2>
                  <div class="btn-group btn-group-toggle" id="botonesTipo" data-toggle="buttons">
                    <label class="btn btn-secondary btn-lg">
                      <input type="radio" name="options" id="indica" autocomplete="off"> ÍNDICA
                    </label>
                    <label class="btn btn-secondary btn-lg">
                      <input type="radio" name="options" id="sativa" autocomplete="off"> SATIVA
                    </label>
                    <label class="btn btn-secondary btn-lg">
                      <input type="radio" name="options" id="hibrida" autocomplete="off"> HÍBRIDA
                    </label>
                    <label class="btn btn-secondary btn-lg">
                      <input type="radio" name="options" id="extraccion" autocomplete="off"> EXTRACCIÓN
                    </label>
                  </div>
                </div>
                </div>
               </div>
   

         

                  
          
       
              </div>
              <div class="modal-footer">
              <button id="btnAnadirGenetica" type="submit" class="btn btn-dark btn-block btn-lg">Guardar</button>
              </form>
              </div>
            </div>
          </div>
        </div>
        </nav>
  <div class="content-wrapper animated fadeIn">
    <div class="container">
    


     
   
        <div class="row">
          <div class="col-sm-12 margenArriba boton-a-genetica">
            <h2 style="float:left">Hachis</h2>
            <button type="button" class="btn btn-info btn-lg" style="float:right" data-toggle="modal" data-target="#modalAGenetica"><i class="fas fa-plus"></i> Añadir Genética</button>

          </div>

        </div>
        <div class="nav nav-tabs nav-justified nav-geneticas" id="nav-tab" role="tablist">
          
          <a class="nav-item nav-link active" id="btnMostrarIndicas" data-toggle="tab" href="#nav-indicas" role="tab" aria-controls="nav-indicas" aria-selected="true">Índica</a>
          <a class="nav-item nav-link" id="btnMostrarSativas" data-toggle="tab" href="#nav-sativas" role="tab" aria-controls="nav-sativas" aria-selected="false">Sativa</a>
          <a class="nav-item nav-link" id="btnMostrarHibridas" data-toggle="tab" href="#nav-hibridas" role="tab" aria-controls="nav-hibridas" aria-selected="false">Hibridas</a>
          <a class="nav-item nav-link" id="btnMostrarExtraccion" data-toggle="tab" href="#nav-extraccion" role="tab" aria-controls="nav-extraccion" aria-selected="false">Extracción</a>

        </div>
   
      <div class="tab-content" id="nav-tabContent">
        <div class="tab-pane fade show active" id="nav-indicas" role="tabpanel" aria-labelledby="nav-home-tab">

          <div class="container-fluid margenArriba tablaIndicas">
            <div class="text-center"><img src="img/cargando.gif" style="height:128px;"></div>
          </div>
        </div>
        <div class="tab-pane fade" id="nav-sativas" role="tabpanel" aria-labelledby="nav-profile-tab">
           <div class="container-fluid margenArriba tablaSativas">
            <div class="text-center"><img src="img/cargando.gif" style="height:128px;"></div>
          </div>

        </div>
        <div class="tab-pane fade" id="nav-hibridas" role="tabpanel" aria-labelledby="nav-contact-tab">
           <div class="container-fluid margenArriba tablaHibridas">
            <div class="text-center"><img src="img/cargando.gif" style="height:128px;"></div>
          </div>

        </div>
        <div class="tab-pane fade" id="nav-extraccion" role="tabpanel" aria-labelledby="nav-contact-tab">
           <div class="container-fluid margenArriba tablaExtraccion">
            <div class="text-center"><img src="img/cargando.gif" style="height:128px;"></div>
          </div>

        </div>
      </div>



   <?php include "php/footer.php";?>
    <script src="js/marron.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/fileinput.min.js"></script>
  
    <script src="js/locales/es.js"></script>

    <script src="themes/fa/theme.js"></script>
    <script type="text/javascript">

    </script>
  </div>
</body>

</html>
