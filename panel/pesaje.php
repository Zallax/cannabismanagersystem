<?php
  require_once("php/config.php");
  require_once ("php/funciones.php");
  include "php/cabecera.php";
  $hoy = getdate();
  
?>

 <?php cabecera("pesaje");?>
  <div class="content-wrapper animated fadeIn">
    <div class="container">
      <?php
        if($_SESSION["idCaja"]!=""){


      ?>

      <div class="card">
      <div class="card-body">
          <div class="icono-fondo">
                    <i class="fas fa-weight"></i>
        </div>
        <div class="row">
          <div class="col-sm-10">
            <div class="titulo-seccion"><span>Corrección de pesaje</span> <p>Sólo se mostrarán las genéticas dispensadas en la caja de hoy.</p></div>
          </div>
          <div class="col-sm-2">
            <a href="historial-pesaje.php" class="btn btn-block btn-dark btn-sm"><i class="fas fa-history"></i> Historial</a>
          </div>
        </div>

      </div>
          

      </div>


      

        <div class="row">
          <div class="col-12 mb-3">
            <?php cabecera("ventas");
            if($_SESSION["tipo"]==1){

            ?>
            <button id="btn-rellenar" class="btn btn-success mb-3 mt-3">Igualar todo el stock</button>

            <?php
              }
            ?>
          </div>
          <div class="col-sm-6">
            <div class="card mt-2">
              <div class="card-header bg-white">
                <h5 class="card-title">Marihuana</h5>
              </div>
              <div class="class-body">
                
                <?php tablaStock("v");?>
              </div>
            </div>
            
          </div>
          <div class="col-sm-6">
          
          


            <div class="card mt-2">
              <div class="card-header bg-white">
                <h5 class="card-title">Hachis</h5>
              </div>
              <div class="class-body">
                
                 <?php tablaStock("m");?>
              </div>
            </div>

            <div class="card mt-2">
              <div class="card-header bg-white">
                <h5 class="card-title">Otros</h5>
              </div>
              <div class="class-body">
                
                 <?php tablaStock("o");?>
              </div>
            </div>



          </div>

        </div>



        <button class="btn btn-success btn-lg btn-block mt-3 mb-3" id="btn-guardar-pesaje">Guardar</button>



        
        <?php
          } else {
            ?>
            <div class="card border-danger" role="alert">
              <div class="class-body">
                
                <div class="icono-fondo">
                            <i class="fas fa-weight"></i>
                </div>
                  <div class="titulo-seccion mt-5 text-danger"><span>Corrección de pesaje</span> <p>Para corregir el pesaje, primero es necesario abrir la caja</p></div>

              </div>
              
            </div>
              <?php cabecera("ventas");
            if($_SESSION["tipo"]==1){

            ?>
            <div class="text-center mt-2">
              <a href="historial-pesaje.php" class="btn btn-outline-dark btn-lg" style="opacity:0.4"><i class="fas fa-history" style="font-size:150px;"></i></br>Historial</a>
            </div>


            <?php
          }
        }

        ?>
        <!-- -->

       </div>
    <?php include "php/footer.php";?>
    <script src="js/correccion-stock.js"></script>
    <script>
      if(opcionLateral!="si"){
        $("#m-pesaje").addClass("menu-seleccionado");
        $("#expandir-mostrador").collapse('toggle');
      }
      </script>


</div>
</body>

</html>
