<?php
  require_once("php/config.php");
  require_once ("php/funciones.php");
  include "php/cabecera.php";
  extract($_GET);
  $consulta = consulta("select fecha from cajas where id = $id");
  $fecha = $consulta[0]["fecha"];
?>

 <?php cabecera("caja");?>
 <div class="modal fade" id="modalEliminarCaja" tabindex="-1" role="dialog" aria-labelledby="modalEliminarCajaLabel" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
     <div class="modal-content" >
       <div class="modal-header">
         <h5 class="modal-title" id="modalEliminarCajaLabel">Confirma la operación</h5>
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
           <span aria-hidden="true">&times;</span>
         </button>
       </div>
       <div class="modal-body">
         <div class="row">
           <div class="col-sm-12 col-md-3 col-lg-2 col-xl-2 text-danger text-center" style="font-size:4em;opacity:0.5">
             <i class="fas fa-exclamation-triangle"></i>
           </div>
           <div class="col-sm-12 col-md-9 col-lg-10 col-xl-10">
             <h5 class="text-danger">Un momento...</h5>

             <div class="alert alert-danger" role="alert">
               Esta acción eliminará lo siguiente:
               </br>
              <ul>
                <li>  El registro de esta caja. </li>
                <li>  El registro del pesaje de hoy (si se ha realizado). </li>
              </ul> <b>Estos datos no podrán recuperarse.</b> ¿Estás seguro de que deseas continuar?
             </div>
           </div>
         </div>
       </div>
       <div class="modal-footer">
         <button type="button" class="btn btn-secondary w-50 btn-lg" data-dismiss="modal"><i class="fas fa-times"></i></button>
         <button type="button" id="btn-c-e-caja" class="btn btn-danger w-50 btn-lg"><i class="fas fa-check"></i></button>
       </div>
     </div>
   </div>
 </div>
   <div class="content-wrapper animated fadeIn background-container" id="zona-caja">
    <div class="container">

      <div class="alert alert-light" role="alert">
        <div class="icono-fondo">
                    <i class="fas fa-cash-register"></i>
        </div>
          <div class="titulo-seccion"><span>Guardar caja</span><h5><i class="far fa-calendar-alt"></i> <?php echo fecha($fecha); ?></h5> <p> Si alguna caja ha quedado abierta, puedes cerrarla desde aquí.</p></div>
      </div>


      <div class="tab-content">

        <div class="row">
          <div class="col-sm-6 mb-2">
            <div class="card">
              <div class="card-body">
                <h5 class="card-title">Genéticas dispensadas</h5>
           
            <table class="table table-hover table-white nowrap responsive" id="tabla-caja-geneticas">
              <thead class="bg-white text-grey">
                <tr>
                  <th style="width:10%;">TIPO</th>
                  <th style="width:50%;">Nombre</th>
                  <th>Aport.</th>
                  <th>Total</th>
                </tr>
              </thead>
              <tbody>

              </tbody>
            </table>
            

          </div>
          <div class="card-footer text-muted text-right">
             <div style="font-size:24px; font-weight:100;"><small class="text-muted">TOTAL</small> <i class="fas fa-angle-right"> </i><span class="text-dark"><span id="total-geneticas">0</span> €</span></div>
           </div>
        </div>
          </div>
          <div class="col-sm-6 mb-2">
            <div class="card">
              <div class="card-body">
                <h5 class="card-title">Productos vendidos</h5>

            <table class="table table-hover table-white" id="tabla-caja-productos" style="width:100%;">
              <thead class="bg-white text-grey">
                <tr>
                  <th style="width:60%;">Nombre</th>
                  <th>Aport.</th>
                  <th>Total</th>
                </tr>
              </thead>
              <tbody>

              </tbody>
            </table>
            
          </div>
          <div class="card-footer text-muted text-right">
             <div style="font-size:24px; font-weight:100;"><small class="text-muted">TOTAL</small> <i class="fas fa-angle-right"> </i><span class="text-dark"><span id="total-productos">0</span> €</span></div>
           </div>
        </div>
      </div>
        
          <div class="col-sm-6 ">
              <div class="card mb-2">
                <h5 class="card-title" style="margin-top:20px;">Cuotas</h5>
                <div class="class-body" style="padding:0 20px 0 20px">
                  <span>Socios renovados/nuevos:</span> <i class="fas fa-angle-right"> </i> <span id="n-cuotas">0</span>
            </br>
              <h5 class="font-weight-bold">Total Cuotas:</span> <i class="fas fa-angle-right"> </i> <div class="badge badge-success"><span id="total-cuotas">0</span> €</div></h5>
                </div>
                
            </div>
           
        </div>
         

          <div class="col-sm-6">
            <div class="" role="alert">
           
              <div class="card">
                <h5 class="card-title" style="margin-top:10px;"><div style="font-size:24px; font-weight:100;"><small class="text-muted">TOTAL DE TODO</small> <i class="fas fa-angle-right"> </i><span class="text-dark"><span id="total-hoy">0</span> €</span></div></h5>
                <div class="card-footer" style="padding:0px;">
                  <button class="btn btn-success btn-lg btn-block" disabled id="btn-guardar">La caja está vacía</button>
                </div>
            </div>
          </div>
          <div class="text-center mt-3" style="width:100%;">
                    <button data-toggle="modal" data-target="#modalEliminarCaja" id="btn-eliminar-caja" class="btn btn-outline-secondary2"><i class="fas fa-trash-alt"></i> Eliminar esta caja</button>
                   </div>

        </div>





        
         

        </div>
      </div>
    </div>

    <?php include "php/footer.php";?>
    <script type="text/javascript">
      

  var idCaja = "<?php echo $id;?>";
        

  var f = new Date();
 // var fecha = f.getFullYear()+"-"+(f.getMonth()+1)+"-"+f.getDate();
  var totalGeneticas = 0;
  var totalProductos = 0;
$(document).ready(function() {

  $("#btn-guardar").on("click", guardarCaja);
  $("#btn-c-e-caja").on("click", eliminarCaja);


});
//let idCaja = $(".btn-tablet").attr("id-caja");
if(idCaja != ""){
  consultarVentas(idCaja);

  var productos = new Array();
  var geneticas = new Array();

}
    
function consultarVentas(idCaja){
    
    var compras = new Array();
    postVentasHoy(idCaja).done(function(data){
      compras = JSON.parse(data);


    }).done(function(){
      $(compras).each(function(n){
          guardarCompra(JSON.parse(compras[n]["articulos"]));
      });
      console.log(totalGeneticas);
      console.log("N Productos-> "+productos.length);
      console.log("N Cannabis-> "+geneticas.length);

      //let fecha2 = compras[0]["fecha"].split(" ");
      //let fecha1 = fecha2[0].split("-");
      //let fecha = fecha1[2]+"/"+fecha1[1]+"/"+fecha1[0];
      //$("#fCaja").html(fecha);
      $(geneticas).each(function(c){
        var tipo="";
    
        if(geneticas[c][3]=="v"){
          tipo = '<div class="tipo verde" tipo="v">v</div>';
        } else {
          tipo = '<div class="tipo marron" tipo="m">m</div>';
        }
        var td = '<tr>';
        td += '<td>';
        td += tipo;
        td += '</td>';
        td += '<td>';
        td += geneticas[c][0];
        td += '</td>';
        td += '<td class="text-right">';
        td += geneticas[c][1];
        td += '</td><td class="text-right"><span class="precio-total-genetica">';
      //  alert(geneticas[c][2]+" x "+geneticas[c][1]+" = "+geneticas[c][2]*geneticas[c][1]);
     // alert(geneticas[c][2]);
        td +=(geneticas[c][2])+"</span> €</tr>";

        $("#tabla-caja-geneticas tbody").append(td);

        
       
      });

      $("#tabla-caja-geneticas").DataTable( {
                  "order": [[ 1, "asc" ]],
                   "language": espanol,
                   "paging":   false,
                   "info":     false,
                   responsive: true,
                   "autoWidth": false,
                   scrollY:        500,
                   scrollCollapse: true,


                              
        }); 
      $("#total-geneticas").html(totalGeneticas.toFixed(2));

      $(productos).each(function(c){
        var td = '<tr>';
        td += '<td>';
        td += productos[c][0];
        td += '</td>';
        td += '<td class="text-right">';
        td += productos[c][1];
        td += '</td>';
        td += '</td><td class="text-right"><span class="precio-total-genetica">';
        td +=productos[c][2]*productos[c][1]+"</span> €</tr>";
        $("#tabla-caja-productos tbody").append(td);

       
      });


        $("#total-productos").html(totalProductos.toFixed(2));

          $("#tabla-caja-productos").DataTable( {
                     "language": espanol,
                     "paging":   false,
                     "info":     false,
                     responsive: true,
                     "autoWidth": false,
                     scrollY:        500,
                     scrollCollapse: true,
                     

         });
          let total = parseFloat($("#total-hoy").html())+(totalGeneticas+totalProductos);
       
          $("#total-hoy").html(total.toFixed(2));
          consultarCuotas(idCaja);
          if($("#total-hoy").html()!="0.00"){
            $("#btn-guardar").attr("disabled",false);
            $("#btn-guardar").html('<i class="fas fa-save"></i> Cerrar caja');
          }
          consultarUltimaCaja();


    });
    

}

function guardarCompra(compra){
  $(compra).each(function(e){
   
    if(this[0]==1){
      console.log("cannabis-> "+this[1]+"x"+this[2]);
      var producto = new Array();

      //METE EN VARIABLE PRODUCTO NOMBRE Y CANTIDAD

      let total = (parseFloat(this[2])*parseFloat(this[3])).toFixed(2);
     
      producto.push(this[1], this[2], total, this[4]);
    //  alert(producto[2]);

      totalGeneticas+=parseFloat(this[3]*this[2]);
      //console.log(compra);

      var existe = false;
      if(geneticas.length!=0){
      
        $(geneticas).each(function(k){
          
        
          if(geneticas[k].includes(producto[0])){
            existe=true;

            this[1]=parseFloat(this[1])+parseFloat(producto[1]);
            this[2]=parseFloat(this[2])+parseFloat(producto[2]);

           // alert(total);
          // alert(this[2]);
          //  this[2]=(parseFloat(total)+parseFloat(this[2])).toFixed(2);
         //   alert(producto[2]);
            producto = [];
          }

        });
        if(!existe){
           geneticas.push(producto);
            
            
            producto = [];
        }
      } else {
        geneticas.push(producto);
            producto = [];
            
      }

     



    } else {
      console.log("producto-> "+this[1]);
      var producto = new Array();

      //METE EN VARIABLE PRODUCTO NOMBRE Y CANTIDAD
      producto.push(this[1], this[2], this[3]);


      totalProductos+=parseFloat(this[3]*this[2]);

      var existe = false;
      if(productos.length!=0){

        $(productos).each(function(k){
          
          if(productos[k].includes(producto[0])){
            existe=true;
            this[1]=parseFloat(this[1])+parseFloat(producto[1]);
            
            producto = [];
          }
        });
        if(!existe){
           productos.push(producto);
            
            
            producto = [];
        }
      } else {
        productos.push(producto);
            producto = [];
            
      }
       
    }
  });
 
}


function postVentasHoy(idCaja){

  return $.post("js/ajax/caja.php", {funcion:"ventasHoy", idCaja:idCaja}, function(result){

    resultado = result;
    
  });

}


function guardarCaja(){
  console.log(JSON.stringify(geneticas));
  console.log(JSON.stringify(productos));
  var texto = $(this).html();
  let tCuotas = parseFloat($("#total-cuotas").html());

  $(this).html('<img src="./img/cargandomini.gif" style="width:30px;">');
  $("#btn-guardar").attr("disabled","true");
  $.post("js/ajax/caja.php", {funcion:"guardarCaja", geneticas:JSON.stringify(geneticas), productos:JSON.stringify(productos), total_p:totalProductos.toFixed(2), total_c:totalGeneticas.toFixed(2),total_cuo:tCuotas, total:(totalGeneticas+totalProductos+tCuotas).toFixed(2), idCaja:idCaja}, function(result){
      
      if(result=="success"){
        localStorage.setItem("mensaje","La caja de hoy ha sido registrada correctamente");
        location.reload();
    
       
      } else {
        mostrarMensaje("Ha ocurrido un error al guardar la caja","error");
        console.log(result);
      }
      

  });
  

}

function consultarUltimaCaja(){

  $.post("js/ajax/caja.php",{funcion:"consultarUltimaCaja", idCaja:idCaja}, function(result){
    if(result!="false"){
      
      var datos = JSON.parse(result);
      $("#btn-guardar").attr("disabled","true");
      $("#btn-guardar").html("La última caja ha sido guardada a las "+datos["hora"]);
      
    }
  });
}
 
function eliminarCaja(){

  $.post("js/ajax/caja.php",{funcion:"eliminarCaja", idCaja:idCaja}, function(result){
    if(result=="success"){

      $("#modalEliminarCaja").modal("hide");
      $(".container").fadeOut("500");
      mostrarMensaje("La caja se ha eliminado correctamente","success");
      setTimeout(function(){window.location.href = "index.php";
;},3000);
    

    } else {
      $("#modalEliminarCaja").modal("hide");
      mostrarMensaje("Ha ocurrido un error al eliminar la última caja","error");
      console.log(result);
    }
  });
}

function consultarCuotas(idCaja){
  $.post("js/ajax/caja.php", {funcion:"cuotasCaja", idCaja:idCaja}, function(result){
    let datos = JSON.parse(result);
    if(datos.length!=0){
      let totalCuotas = 0;
      $(datos).each(function(n){
        totalCuotas += parseFloat(datos[n]["cuota"]);
      });
      $("#n-cuotas").html(datos.length);
      $("#total-cuotas").html(totalCuotas);
      $("#total-hoy").html(parseFloat($("#total-hoy").html())+parseFloat(totalCuotas));
      if($("#total-hoy").html()!="0.00"){
        $("#btn-guardar").attr("disabled",false);
        $("#btn-guardar").html('<i class="fas fa-save"></i> Guardar');
      }

    }
  });
}

//TRADUCCIÓN DATATABLE
 var espanol={
                     "lengthMenu": "Mostrar _MENU_ registros por página",
                     "zeroRecords": "No se han encontrado datos",
                     "info": "Mostrando página _PAGE_ de _PAGES_",
                     "infoEmpty": "No se han encontrado registros.",
                     "infoFiltered": "(filtered from _MAX_ total records)",
                     "paginate": {
                            "first":      "Primero",
                            "last":       "Último",
                            "next":       ">",
                            "previous":   "<"
                        },
                     "search":         "Buscar:",
                };


    </script>


  </div>


</body>

</html>
