<?php
  require_once("php/config.php");
  require_once ("php/funciones.php");
  include "php/cabecera.php";
?>

 <?php cabecera("inicio");?>
 <div class="modal fade" id="modalEliminarUsuario" tabindex="-1" role="dialog" aria-labelledby="modalEliminarUsuarioLabel" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
     <div class="modal-content" >
       <div class="modal-header">
         <h5 class="modal-title" id="modalEliminarUsuarioLabel">Confirma la operación</h5>
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
           <span aria-hidden="true">&times;</span>
         </button>
       </div>
       <div class="modal-body">
         <div class="row">
           <div class="col-sm-12 col-md-3 col-lg-2 col-xl-2 text-danger text-center" style="font-size:4em;opacity:0.5">
             <i class="fas fa-exclamation-triangle"></i>
           </div>
           <div class="col-sm-12 col-md-9 col-lg-10 col-xl-10">
             <h5 class="text-danger">Un momento...</h5>

             <div class="alert alert-danger" role="alert">
               Esta acción eliminará toda la información almacenada sobre este usuario.
               <p>Todas sus transacciones pasarán a ser marcadas como "Usuario eliminado".</p>

                <b>Estos datos no podrán recuperarse.</b></br> ¿Estás seguro de que deseas continuar?
             </div>
           </div>
         </div>
       </div>
       <div class="modal-footer">
         <button type="button" class="btn btn-secondary w-50 btn-lg" data-dismiss="modal"><i class="fas fa-times"></i></button>
         <button type="button" id="btn-c-e-usuario" class="btn btn-danger w-50 btn-lg"><i class="fas fa-check"></i></button>
       </div>
     </div>
   </div>
 </div>
  <div class="content-wrapper animated fadeIn">
    <div class="container zona-nuevo-usuario">

      <?php
        if(ISSET($_GET["id"])){
          extract($_GET);
          $permiso = true;
          if($_SESSION["tipo"]!=1 && $_SESSION["id"]!=$id){
            $permiso = false;
          }
        }

          if($permiso){
          $consulta = consulta("select nombre, apellidos, fecha_registro, correo, tipo from usuarios where id = $id");
          $datos = $consulta[0];
      ?>
      <div class="card border-morado mb-3">
        <div class="card-body">
        <div class="icono-fondo">
                    <i class="fas fa-users"></i>
        </div>
        <div class="titulo-seccion"><h3>Perfil de <?php echo $datos["nombre"]." ".$datos["apellidos"];?></h3></div>
        </div>
      </div>
      <?php
        if(ISSET($_GET["e"])){
          if($_GET["e"]==1){
            echo '<div class="alert alert-danger" role="alert">
                    Ha ocurrido un error al guardar los cambios.
                  </div>';
          }
        }
                  if(ISSET($_GET["m"])){
            if($_GET["m"]==1){
              echo '<div class="alert alert-success alert-dismissible fade show" role="alert">
                      Los datos se han guardado correctamente
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
                    </div>';
            }
          }
      ?>

      <div class="card">
        <div class="card-body">
          <form action="js/ajax/modificar-usuario.php" method="POST" autocomplete="false">
          <input class="d-none" name="funcion" value="1">
          <input style="display:none" type="password" name="fakepasswordremembered"/>
          <input style="display:none" type="email" name="fakepasswordremembered"/>
          <input style="display:none" name="id" id="id" value="<?php echo $id;?>">
          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="inputNombre">Nombre</label>
              <input required type="text" class="form-control" name="n" id="inputNombreU" placeholder="Introduce el nombre" value="<?php echo $datos["nombre"];?>">
            </div>
            <div class="form-group col-md-6">
              <label for="inputApellidos">Apellidos</label>
              <input required type="text" class="form-control" name="a" id="inputApellidosU" placeholder="Introduce los apellidos" value="<?php echo $datos["apellidos"];?>">
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="inputEmail">Email</label>
              <input required type="email" class="form-control" name="m" id="inputEmailU" placeholder="Introduce el Email" value="<?php echo $datos["correo"];?>">
            </div>
            <div class="form-group col-md-6">
              <label for="inputPass">Contraseña</label>
              <input type="password" class="form-control" name="p" id="inputPass" placeholder="Introduce la contraseña" spellcheck="false">
            </div>
          </div>

          <div class="form-row">
            <?php

             if($_SESSION["tipo"]==1){?>
            <div class="form-group col-md-12">
              <label for="inputState">Tipo de cuenta</label>
              <select id="inputState" name="t" class="form-control">
                <option selected value="0">Elige un tipo de cuenta...</option>
                <option value="1">Administrador</option>
                <option value="2">Gestor</option>

              </select>
            </div>
            <?php
              }
            ?>

          </div>
          <div class="text-right">

          <button type="submit" class="btn btn-success btn-block btn-lg"><i class="fas fa-file-alt"></i> Guardar cambios</button>
          <?php
          
            if($id!==$_SESSION["id"]){


          ?>

          <div class="text-center"><button type="button" class="btn btn-muted-danger btn-lg" id="btn-eliminar-usuario" data-toggle="tooltip" data-placement="bottom" title="Eliminar usuario"><i class="fas fa-trash"></i></button>
            <?php
              }
             ?>
</div>
        </div>
        </form>
    </div>
        </div>
        </div>



    <?php
      }

    include "php/footer.php";?>
    <script>

      $("#btn-eliminar-usuario").on("click", function(){
        $("#modalEliminarUsuario").modal("show");
        let id = $("#id").val();
        $("#btn-c-e-usuario").unbind().on("click", function(){
            $.post("js/ajax/modificar-usuario.php", {id:id}, function(result){
              $(".modal-body").fadeOut(function(){

                $(this).html('<div class="text-center"><img src="img/cargando.gif" style="height:128px;"></div>').fadeIn();

                
              });
                if(result=="success"){
                 
                  $("#modalEliminarUsuario").modal("hide");
                  location.href ="usuarios.php?m=1";
                } else {
                  $(".modal-body").html('<div class="alert alert-danger alert-dismissible fade show" role="alert">Ha ocurrido un error al eliminar a este usuario.</div>');
                    console.log(result);
                }
            });

            
        });
      });
    </script>
    <script src="js/socios.js"></script>

  </div>
</body>

</html>
