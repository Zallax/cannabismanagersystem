<?php
  require_once("php/config.php");
  require_once ("php/funciones.php");
  include "php/cabecera.php";
  $hoy = getdate();
?>

 <?php cabecera("caja");?>
 <div class="modal fade" id="modalEliminarCaja" tabindex="-1" role="dialog" aria-labelledby="modalEliminarCajaLabel" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
     <div class="modal-content" >
       <div class="modal-header">
         <h5 class="modal-title" id="modalEliminarCajaLabel">Confirma la operación</h5>
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
           <span aria-hidden="true">&times;</span>
         </button>
       </div>
       <div class="modal-body">
         <div class="row">
           <div class="col-sm-12 col-md-3 col-lg-2 col-xl-2 text-danger text-center" style="font-size:4em;opacity:0.5">
             <i class="fas fa-exclamation-triangle"></i>
           </div>
           <div class="col-sm-12 col-md-9 col-lg-10 col-xl-10">
             <h5 class="text-danger">Un momento...</h5>

             <div class="alert alert-danger" role="alert">
               Esta acción eliminará lo siguiente:
               </br>
              <ul>
                <li>  El registro de la caja de hoy. </li>
                <li>  El registro del pesaje de hoy (si se ha realizado). </li>
              </ul> <b>Estos datos no podrán recuperarse.</b> ¿Estás seguro de que deseas continuar?
             </div>
           </div>
         </div>
       </div>
       <div class="modal-footer">
         <button type="button" class="btn btn-secondary w-50 btn-lg" data-dismiss="modal"><i class="fas fa-times"></i></button>
         <button type="button" id="btn-c-e-caja" class="btn btn-danger w-50 btn-lg"><i class="fas fa-check"></i></button>
       </div>
     </div>
   </div>
 </div>
  <div class="content-wrapper animated fadeIn background-container" id="zona-caja">
    <div class="container">

      <div class="alert alert-light border-danger" role="alert">
        <div class="icono-fondo">
                    <i class="material-icons md-48 text-danger">
                    store_mall_directory
                    </i>

        </div>
          <div class="titulo-seccion"><span>Guardar caja</span><h5>Fecha: <?php echo date("d").'/'.date("m").'/'.date("Y");?></h5> <p> Esta acción generará las estadísticas de hoy, almacenará las ventas y <b>bloqueará el dispositivo de carta</b></p></div>
      </div>


      <div class="tab-content">

        <div class="row">
          <div class="col-sm-6 mb-2">
            <div class="card">
              <div class="card-body">
                <h5 class="card-title">Genéticas dispensadas</h5>
           
            <table class="table table-hover table-white nowrap responsive" id="tabla-caja-geneticas">
              <thead class="bg-white text-grey">
                <tr>
                  <th style="width:10%;">TIPO</th>
                  <th style="width:50%;">Nombre</th>
                  <th>Aport.</th>
                  <th>Total</th>
                </tr>
              </thead>
              <tbody>

              </tbody>
            </table>
            

          </div>
          <div class="card-footer text-muted text-right">
             <div style="font-size:24px; font-weight:100;"><small class="text-muted">TOTAL</small> <i class="fas fa-angle-right"> </i><span class="text-dark"><span id="total-geneticas">0</span> €</span></div>
           </div>
        </div>
          </div>
          <div class="col-sm-6 mb-2">
            <div class="card">
              <div class="card-body">
                <h5 class="card-title">Productos vendidos</h5>

            <table class="table table-hover table-white" id="tabla-caja-productos" style="width:100%;">
              <thead class="bg-white text-grey">
                <tr>
                  <th style="width:60%;">Nombre</th>
                  <th>Aport.</th>
                  <th>Total</th>
                </tr>
              </thead>
              <tbody>

              </tbody>
            </table>
            
          </div>
          <div class="card-footer text-muted text-right">
             <div style="font-size:24px; font-weight:100;"><small class="text-muted">TOTAL</small> <i class="fas fa-angle-right"> </i><span class="text-dark"><span id="total-productos">0</span> €</span></div>
           </div>
        </div>
      </div>
        
          <div class="col-sm-6 ">
              <div class="card mb-2">
                <h5 class="card-title" style="margin-top:20px;">Cuotas</h5>
                <div class="class-body" style="padding:0 20px 0 20px">
                  <span>Socios renovados/nuevos:</span> <i class="fas fa-angle-right"> </i> <span id="n-cuotas">0</span>
            </br>
              <h5 class="font-weight-bold">Total Cuotas:</span> <i class="fas fa-angle-right"> </i> <div class="badge badge-success"><span id="total-cuotas">0</span> €</div></h5>
                </div>
                
            </div>
           
        </div>
         

          <div class="col-sm-6">
            <div class="" role="alert">
           
              <div class="card">
                <h5 class="card-title" style="margin-top:10px;"><div style="font-size:24px; font-weight:100;"><small class="text-muted">TOTAL DE TODO</small> <i class="fas fa-angle-right"> </i><span class="text-dark"><span id="total-hoy">0</span> €</span></div></h5>
                <div class="card-footer" style="padding:0px;">
                  <button class="btn btn-success btn-lg btn-block" disabled id="btn-guardar">La caja está vacía</button>
                </div>
            </div>
          </div>


        </div>





        
         <div class="text-center mt-3" style="width:100%;">
          <button data-toggle="modal" data-target="#modalEliminarCaja" id="btn-eliminar-caja" class="btn btn-outline-secondary2" style="display:none;"><i class="fas fa-trash-alt"></i> Eliminar la última caja</button>
         </div>


        </div>
      </div>
    </div>

    <?php include "php/footer.php";?>
    <script src="js/caja.js"></script>
    <script>
      if(opcionLateral!="si"){
        $("#m-caja").addClass("menu-seleccionado");
        $("#expandir-mostrador").collapse('toggle');
      }
      </script>

  </div>


</body>

</html>
