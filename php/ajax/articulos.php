<?php
extract($_POST);
require_once("../../panel/php/conexion.php");

if(ISSET($funcion)){
  if($funcion=="catArticulos"){
    cargarCategoriasArticulos();
  }
  if($funcion=="listaArt"){
    cargarListaArticulos();
  }
  if($funcion=="contenidoArticulos"){
  	contenidoArticulos($categoria);
  }
}

function cargarCategoriasArticulos(){
  $resultado = consulta ("select * from categorias_articulos order by nombre");
  if(count($resultado)>0){
      echo ' <a class="nav-link active" data-id-cat="'.$resultado[0]["id"].'" id="cat'.$resultado[0]["id"].'" data-toggle="pill" href="#seccionCat'.$resultado[0]["id"].'" role="tab" aria-controls="#seccionCat'.$resultado[0]["id"].'" aria-selected="true">'.$resultado[0]["nombre"].'</a>';

      for ($i=1;$i<count($resultado);$i++){
          echo ' <a class="nav-link" data-id-cat="'.$resultado[$i]["id"].'" id="cat'.$resultado[$i]["id"].'" data-toggle="pill" href="#seccionCat'.$resultado[$i]["id"].'" role="tab" aria-controls="#seccionCat'.$resultado[$i]["id"].'">'.$resultado[$i]["nombre"].'</a>';
      }
  } else {
      echo "No se han creado categorías.";
  }
}

function cargarListaArticulos(){
   $resultado = consulta ("select * from categorias_articulos order by nombre;");
   if(count($resultado)>0){
       echo '<div class="tab-pane fade show active" data-id-cat="'.$resultado[0]["id"].'" id="seccionCat'.$resultado[0]["id"].'" role="tabpanel" aria-labelledby="seccionCat'.$resultado[0]["id"].'">
       <p>'.$resultado[0]["descripcion"].'</p>
     </div>';

     for ($i=1;$i<count($resultado);$i++){
       echo '<div class="tab-pane fade" data-id-cat="'.$resultado[$i]["id"].'" id="seccionCat'.$resultado[$i]["id"].'" role="tabpanel" aria-labelledby="seccionCat'.$resultado[$i]["id"].'">
       <p>'.$resultado[$i]["descripcion"].'</p>
     </div>';
       }
   } else {
       echo "No hay datos disponibles.";
   }
}





function contenidoArticulos($categoria){
        $genetica=consulta("select * from articulos where categoria=$categoria order by nombre;");
        count($genetica)>0 ? $datos=true : $datos=false;
        if($datos){

        		echo ' <ul class="list-group">';
                    for($i=0;$i<count($genetica);$i++){
                        if($genetica[$i]["stock"]==0){
                          $claseAgotado = " pAgotado";
                          $disabled = " disabled";
                        } else {
                          $claseAgotado = "";
                          $disabled = "";
                        }

                        echo '
                       		<li class="list-group-item'.$claseAgotado.'"><div class="row">
                       		<div class="col-sm-4'.$claseAgotado.'">'.$genetica[$i]["nombre"].'

                       		</div>
                       		<div class="col-sm-6 text-center zonaCantidad">
                            <small style="font-size:10px;">CANTIDAD</small>
                       			<div class="btn-group'.$claseAgotado.' mr-2 btn-cantidad" style="border:1px solid #bababa;border-radius:5px;" role="group" aria-label="First group">
                       			    <button'.$disabled.' type="button" class="btn btn-secondary btn-menos">-</button>
                       			    <button'.$disabled.' type="button" class="btn contador">1</button>
                       			    <button'.$disabled.' type="button" class="btn btn-secondary btn-mas">+</button>
                       			  </div>
                       		</div>
                       		<div class="col-sm-2 text-right">
                       		 <button'.$disabled.' class="btn btn-lg btn-dark btn-anadir-articulo'.$claseAgotado.'" data-id="'.$genetica[$i]["id"].'" data-precio="'.$genetica[$i]["precio"].'">
                       		                       		<i class="fa fa-shopping-bag"></i>
                       		</button>
                       		</div>
                       		</div>
                       		</li>
                        ';
                    }
                echo '</ul>';

            } else {
                echo "<small class='text-muted'>No hay datos disponibles</small>";
            }



    }

?>
