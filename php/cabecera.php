<?php
  
  require_once("php/functions.php");

  $consultaConfig = consulta("select * from config;");
  $nombreA = $consultaConfig[0]["valor"];
  $nombreC = $consultaConfig[1]["valor"];
  function comprobarRevision(){
    if(ISSET($_SESSION["firma"]) && $_SESSION["firma"]=="no"){
        header("Location: revision.php");
    }
  }
	function mostrarCabecera($titulo){
      $consultaConfig = consulta("select * from config;");
  $nombreA = $consultaConfig[0]["valor"];
  $nombreC = $consultaConfig[1]["valor"];
	echo '<!doctype html>
<html lang="es">
  <head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="noindex, nofollow">
    <!-- Bootstrap CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://kit.fontawesome.com/8056c7c5bd.js" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/animate.css">
    <title>'.$nombreC.' | Dispensario Virtual</title>
    <link rel="icon" type="image/png" href="img/logo.png" />
  </head>';
  }
  function cargarHeader(){
    echo '  <div class="row animated fadeInDown" id="row-header">
        <div class="col-sm-12 col-md-12 col-lg-7">
        <a href="inicio.php"><img class="logo-banner" src="img/logo-banner.png" style="max-width:100%;height:auto;"></a>

        </div>

         <div class="col-sm-12 col-md-12 col-lg-5">
          <div class="row zonaUsuario">
          
           '.cargarZonaUsuario().'
          
             
            
          </div>
         </div>
        </div>
      ';
  }


?>
