<?php
require_once("panel/php/conexion.php");

extract($_POST);


if(ISSET($funcion)){
  if($funcion=="botonPanico"){
    botonPanico();
  }




}

function ultimaCaja(){
  $consulta = consulta("select id, hora from cajas where id = (select max(id) from cajas)");
  if(!empty($consulta)){
    if($consulta[0]["hora"]=="00:00:00"){
      return $consulta[0]["id"];
    } else {
      return "";
    }
  } else {
    return "";
  }
}

function fecha($fecha){
  $divFecha = explode("-", $fecha);
  return $divFecha[2].'/'.$divFecha[1].'/'.$divFecha[0];
}

function cargarUltimasGeneticas(){
  $claseTipo;
  $ultimasGeneticas = consulta("select id, nombre, tipo, categoria, fecha from geneticas where destacada=1 order by id desc;");
  if(count($ultimasGeneticas)!=0){
    for($i=0;$i<count($ultimasGeneticas);$i++){
      if($ultimasGeneticas[$i]["tipo"]=="verde"){
        $claseTipo="verde";
      } else {
        $claseTipo="marron";
      }

  		echo '<li class="list-group-item" data-id="'.$ultimasGeneticas[$i]["id"].'"><div class="'.$claseTipo.'"></div><span class="n-genetica">'.$ultimasGeneticas[$i]["nombre"].'</span><div style="display:inline-block;float:right;" class="text-right text-muted">'.$ultimasGeneticas[$i]["categoria"].'</div></li>';
  	}
  } else {
    echo '<li class="list-group-item" data-id="no" ><small>No hay genéticas destacadas</small></li>';
  }



}

function cargarZonaUsuario(){
	$date = time();

  return '
  <div class="col-8 col-sm-9 col-md-10 col-lg-9">
  <div class="infoUser text-right">
 <div class="nombreSocio">'.$_SESSION["nombre"].' '.$_SESSION["apellidos"].'</div>
 <button class="lista-articulos btn-dark btn-lg btn"><i class="fa fa-shopping-bag" style="margin-right:10px;"></i> <span id="n-articulos">0</span>
</button>
<a href="perfil.php" class="btn-marron btn-lg btn"><i class="fas fa-user"></i> <span id="tHome4">Mi Perfil</span>
</a>
</div>

</div>
  <div class="col-4 col-sm-3 col-md-2 col-lg-3">
                 <div class="fotoUser" style="background-image:url(panel/img/socios/foto-personal/'.$_SESSION["foto"].'?time='.$date.')">

              </div>
            </div>
           ';
}

function comprobarUltimoAcceso(){

  $fechaUltimoAcceso=consulta("select *, CURRENT_DATE as f_actual from tablet where id = (select max(id) from tablet);");

  if (count($fechaUltimoAcceso)!=0){
    if($fechaUltimoAcceso[0]["fecha"]!=$fechaUltimoAcceso[0]["f_actual"]){
      return false;
    } else {
      if($fechaUltimoAcceso[0]["estado"]!=1){
        return false;
      } else {
        return true;
      }

    }
  } else {
    return false;
  }


}



function botonPanico(){
  insert("update sesiones set estado=0 where id=max(id);");
}

function cargarListaArticulosCarrito(){
  ?>
  <div class="contenedor--menu">

         <div class="contenido-menu-articulos">

           <h1 class="tituloCategoriaArticulos">LISTA DE ARTÍCULOS<div style="float:right;font-size:30px;margin:-30px -69px 0 0;"><i style="font-size:37px;background:none !important;" class="lista-articulos fa fa-times"></i></div></h1>

           <h3>Cannabis</h3>

           <div class="text-muted" id="sm1">No se han añadido genéticas</div>
           <table class="tablaListaArticulos 2" id="tablaCannabis">
             <thead>
               <th>Nombre</th>
               <th width="50px">Cantidad</th>
                <th width="30px"></th>
             </thead>
             <tbody>

             </tbody>
           </table>

           <h3 style="margin-top:20px;">PRODUCTOS</h3>
           <div class="text-muted" id="sm2">No se han añadido productos</div>
           <table class="tablaListaArticulos 1" id="tablaProductos">
             <thead>
               <th>Nombre</th>
               <th width="50px">Cantidad</th>
                <th width="30px"></th>
             </thead>
             <tbody>

             </tbody>
           </table>
           
         </div>
         <button class="btn btn-danger mt-5 btn-block" id="vaciar-cesta" style="display:none;">BORRAR TODO</button>

     </div>

  <?php
}


function porcentajeCuota($id){

        $consulta = consulta("select * from cuotas where idSocio=$id order by id desc limit 1;");

        if(count($consulta)!=0){

             $numero = new dateTime(date("Y-m-d"));
             $numero2 = new DateTime($consulta[0]["fechapago"]);
             $numero3 = new DateTime($consulta[0]["fechafin"]);
             $diferencia1 =$numero3->diff($numero2)->format("%a");
             $diferencia2 = $numero->diff($numero2)->format("%a");


            // $resultado = $numero3->diff($numero2);
            $division =$diferencia2* 100;
             $resultado = round($division/$diferencia1,0);
             if($resultado>100){
                 $resultado=100;
             }
             return $resultado;

        } else {
            return "Sin cuota";
        }


    }

function ultimasGeneticas($id){
  $consulta = consulta("select * from retiradas where id=$id order by fecha desc LIMIT 1");
  if(count($consulta)!=0){
    echo '<ul class="list-group lista-historial">';
    foreach($consulta as $indice => $valor){
      $articulos = json_decode($valor["articulos"]);
      foreach($articulos as $indice2 => $valor2){
        if($valor2[0]==1){
          $cid = consulta("select id, precio, tipo, stock from geneticas where nombre='".$valor2[1]."';");
          if(count($cid)!=0){
            $idgen = $cid[0]["id"];
            $precio = $cid[0]["precio"];
            $stock = $cid[0]["stock"];
            if($stock >= $valor2[2]){
              $disabled = "";
            } else {
              $disabled = "disabled";
             
            }
            
            if($cid[0]["tipo"]=="verde"){
              $tipo = "v";
            } else {
              $tipo = "m";
            }
          } else {
            $idgen = "error";
            $disabled = "disabled";
            $precio = "";
            $tipo = "";
            $stock="";
          } 
          echo '<li class="list-group-item historial">'.$valor2[1].' (<span class="cant">'.$valor2[2].'</span> Gr)<button class="float-right btn btn-sm btn-dark btn-anadir" data-stock="'.$stock.'" data-tipo="'.$tipo.'" data-precio="'.$precio.'" data-id="'.$idgen.'" '.$disabled.'>AÑADIR</button></li>';
        }
      }
    }
    echo '</ul>';
  }
}

function ultimosProductos($id){
  $consulta = consulta("select * from retiradas where id=$id order by fecha desc LIMIT 1");
  if(count($consulta)!=0){
    echo '<ul class="list-group lista-historial">';
    foreach($consulta as $indice => $valor){
      $articulos = json_decode($valor["articulos"]);
      foreach($articulos as $indice2 => $valor2){
        if($valor2[0]==2){

          $cid = consulta("select id, precio, stock from articulos where nombre='".$valor2[1]."';");
          if(count($cid)!=0){
            $idgen = $cid[0]["id"];
            $precio = $cid[0]["precio"];
            $stock = $cid[0]["stock"];
            if($stock >= $valor2[2]){
              $disabled = "";
            } else {
              $disabled = "disabled";
             
            }
            
           
          } else {
            $idgen = "error";
            $disabled = "disabled";
            $precio = "";
            $tipo = "";
            $stock="";
          } 


          echo '<li class="list-group-item historial">'.$valor2[1].' (<span class="cant">'.$valor2[2].'</span> Ud)<button class="float-right btn btn-sm btn-dark btn-anadir-prod" data-stock="'.$stock.'" data-precio="'.$precio.'" data-id="'.$idgen.'" '.$disabled.'>AÑADIR</button></li>';
        }
      }
    }
    echo '</ul>';
  }
}
/*
function tieneDescuento($id){
  $consulta = consulta("select descuento from socios where id=$id");
  echo $consulta[0]["descuento"];

}*/

?>
