
<?php
    require_once("php/cabecera.php");

    mostrarCabecera("index");
?>
  <body>
    <div class="mensaje-firma" style="display:none;">Antes de finalizar tu alta, debes firmar.</div>
    <div class="zona-input-ampliado" style="display:none;"><span id="nombre-input"></span><div class="input-ampliado">Introduce la información</div></div>
    <div class="container-fluid" id="zona-alta">

       <div id="alta-0" class="animated fadeInUp">



         <h1 class="text-white text-center mt-5" id="tAlta1">¡Bienvenido!</h1>
         <h4 class="text-white text-center mt-5" id="tAlta2">Te damos la bienvenida a nuestra asociación.</br></br>Queremos que te sientas como en casa, por lo que te
         pedimos que <b>respetes todo el material inmobiliario</b>, así como al <b>resto de socios</b> para lograr entre todos una convivencia agradable.
       </br></br> A continuación vamos a solicitarte una serie de datos personales. </br> Estos datos se almacenan con el único objetivo de gestionar tu membresía a
     nivel interno. Únicamente la junta directiva tendrá acceso a estos datos, que están debidamente protegidos.</br></br>Puedes consultar cualquier tipo de duda
   con el personal de la asociación.</h4>
        <button class="btn btn-dark btn-block btn-lg mt-5" id="btn-paso0">Comenzar</button>
       </div>





      <div class="card mb-3" style="max-width:100%;margin:3% 0;display:none; border-radius:20px;">
        <div class="card-header text-white" style="background-color: #0000007a; border-radius: 20px 20px 0 0;">

          <div class="row">
              <div class="col-sm-4">
          <h1 class="t-nuevosocio">NUEVO SOCIO</h1>

        </div>
        <div class="col-sm-2"><div id="z1" class="btn-zona-alta activo">Paso 1</div>
        </div>
        <div class="col-sm-2"><div id="z2" class="btn-zona-alta">Paso 2</div>
        </div>
        <div class="col-sm-2"><div id="z3" class="btn-zona-alta">Paso 3</div>
        </div>
        <div class="col-sm-2"><div id="z4" class="btn-zona-alta">Paso 4</div>
        </div>

        </div>

        </div>
        <div class="card-body">
          <div class="formulario">
            <form id="formAltaSocio" autocomplete="off">

              <div id="alta-1">
                <h3 id="tAlta3">Información personal</h3>
                <div class="row">
                  <div class="col-sm-6">
                  <div class="form-group">
                    <label for="nombre-socio" id="tAlta4">Nombre</label>
                    <input type="text" class="form-control form-control-lg" id="nombre-socio" placeholder="Introduce tu nombre">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="apellidos-socio" id="tAlta5">Apellidos</label>
                    <input type="text" class="form-control form-control-lg" id="apellidos-socio" placeholder="Introduce tus apellidos">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="dni-socio" id="tAlta6">DNI</label>
                    <input type="text" class="form-control form-control-lg" id="dni-socio" placeholder="Introduce tu DNI">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="email-socio" id="tAlta7">Email</label>
                    <input type="email" class="form-control form-control-lg" id="email-socio" placeholder="Introduce tu dirección de correo">
                  </div>
                </div>
                  <div class="col-sm-6">
                  <div class="form-group">
                    <label for="direccion-socio" id="tAlta8">Domicilio</label>
                    <input type="text" class="form-control form-control-lg" id="direccion-socio" placeholder="Introduce tu domicilio">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="tel-socio" id="tAlta9">Teléfono</label>
                    <input type="number" class="form-control form-control-lg" id="tel-socio" placeholder="Introduce tu teléfono">
                  </div>
                </div>
                <div class="col-sm-2">

                </div>
                <div class="col-sm-8">

                </div>
                <div class="col-sm-10">
                  <p class="mensaje-error-val">Debes completar correctamente todos los campos</p>
                </div>
                <div class="col-sm-2 text-right">
                  <button type="button" id="boton-siguiente" class="btn btn-dark btn-block btn-lg"><i class="fas fa-arrow-circle-right"></i></button>
                </div>
              </div>
            </div>
            <div id="alta-2" style="display:none;">
                <h2>Requisitos</h2>
                <div class="row contenido" style="overflow-y:scroll;overflow-x: hidden;max-height:430px;">
                  <div class="col-sm-12">
                     <label class="checks"><span id="tAlta10">SER CONSUMIDOR/A DE CANNABIS SATIVA L.</span>
                      <input id="check1" type="checkbox" checked="checked">
                      <span class="checkmark"></span>
                    </label>

                    <label class="checks"><span id="tAlta11">SER CONSUMIDOR/A DE TABACO</span>
                      <input id="check2" type="checkbox">
                      <span class="checkmark"></span>
                    </label>

                    <label class="checks"><span id="tAlta12">SER CONSUMIDOR/ A DE PLANTAS CON PROPIEDADES TERAPÉUTICAS</span>
                      <input id="check3" type="checkbox">
                      <span class="checkmark"></span>
                    </label>

                    <label class="checks"><span id="tAlta13">SER CONSUMIDOR/A HABITUAL DE CANNABIS POR RAZONES TERAPÉUTICAS</span>, <small><span class="muted" id="tAlta14">y tener diagnosticada una enfermedad para la que el cannabis tiene efectos terapéuticos según la IACM, o que el uso paliativo de los cannabinoides ha sido probado científicamente y aportar certificado médico acreditativo de la solicitud de uso de cannabis por parte del paciente sin oposición médica, o tener una licencia de uso de cannabis de cualquier país del Mundo.</span></small>
                      <input id="check4" type="checkbox">
                      <span class="checkmark"></span>
                    </label>
                    <h2 id="tAlta15">¿Que documentación vas a aportar?</h2>
                    <div class="row">
                      <div class="col-sm-6">
                        <label class="checks"><span id="tAlta16">COPIA DNI/NIE</span>
                          <input id="check5" type="checkbox">
                          <span class="checkmark"></span>
                        </label>
                      </div>
                      <div class="col-sm-6">
                        <label class="checks"><span id="tAlta17">CERTIFICADO MÉDICO</span>
                          <input id="check6" type="checkbox">
                          <span class="checkmark"></span>
                        </label>
                      </div>
                    </div>
                    <p>
                      <span id="tAlta18"><b>AFIRMO</b> mi voluntad de incorporarme como SOCIO/A de la </span>"<span class="text-uppercase"><?php echo $nombreA ?></span>" <span id="tAlta19">conociendo los estatutos y objetivos de ésta y me comprometo a cumplir con ellos, con las normas de funcionamiento interno y con la legislación española, con especial atención a:</span></br>
                      <b id="tAlta20">El artículo 19 de la Ley Orgánica 1/1992, sobre Protección de la Seguridad ciudadana:</b>
                        <ol>
                          <li id="tAlta21">Los agentes de las Fuerzas y Cuerpos de Seguridad podrán limitar o restringir, por el tiempo imprescindible, la circulación o permanencia en vías o lugares públicos en supuestos de alteración del orden, la seguridad ciudadana o la pacífica convivencia, cuando fuere necesario para su restablecimiento. Así mismo podrán ocupar preventivamente los efectos o instrumentos susceptibles de ser utilizados para acciones ilegales, dándlies el destino que legalmente proceda.</li>
                          <li id="tAlta22">Para el descubrimiento y detención de los participantes en un hecho delictivo causante de grave alarma social y para la recogida de los instrumentos, efectos o pruebas del mismo, se podrán establecer contrlies en la vías, lugares o establecimientos públicos, en la medida indispensable a los fines de este apartado, al objeto de proceder a la identificación de las personas que transiten o se encuentren en ellos, al registro de los vehículos y al contrli superficial de los efectos personales con el fin de comprobar que no se portan sustancias o instrumentos prohibidos o peligrosos. El resultado de la diligencia se pondrá de inmediato en conocimiento del Ministerio Fiscal.</li>
                      </ol>


                       <b id="tAlta23">El artículo 25.1 de la Ley Orgánica 1/1992, sobre Protección de la Seguridad ciudadana:</b></br>
                       <span id="tAlta24">Constituyen infracciones graves a la seguridad ciudadana el consumo en lugares, vías, establecimientos o transportes públicos, así como la tenencia ilícita, aunque no estuviera destinada al tráfico de drogas tóxicas, estupefacientes o sustancias psicotrópicas, siempre que no constituya infracción penal, así como el abandono en los sitios mencionados de útiles o instrumentos utilizados para su consumo.</span></br>

                      <b id="tAlta25">El artículo 368 del Código Penal Español, Ley Orgánica 5/2010:</b></br>
                      <span id="tAlta26">
                      Los que ejecuten actos de cultivo, elaboración o tráfico, o de otro modo promuevan, favorezcan o faciliten el consumo ilegal de drogas tóxicas, estupefacientes o sustancias psicotrópicas, o las posean con aquellos fines, serán castigados con las penas de prisión de tres a seis años y multa del tanto al triplo del valor de la droga objeto del delito si se tratase de sustancias o productos que causen grave daño a la salud, y de prisión de uno a tres años y multa del tanto al duplo en los demás casos.</span>
                    </br>
                      <b id="tAlta27">El artículo 18 de la Constitución Española:</b></br>
                      <ol>
                        <li id="tAlta28">Se garantiza el derecho al honor, a la intimidad personal y familiar y a la propia imagen.</li>
                        <li id="tAlta29">El domicilio es inviliable. Ninguna entrada o registro podrá hacerse en él sin el consentimiento del titular o resliución judicial, salvo en caso de flagrante delito.</li>
                        <li id="tAlta30">Se garantiza el secreto de las comunicaciones y, en especial, de las postales, telegráficas y telefónicas, salvo resliución judicial.</li>
                        <li id="tAlta31">La Ley limitará el uso de la informática para garantizar la intimidad personal y familiar de los ciudadanos y el pleno ejercicio de sus derechos.</li>
                    </ol>


                    </p>
                  </div>

                     <div class="col-sm-2">
                      <button type="button" id="boton-atras" class="btn btn-dark btn-block btn-lg"><i class="fas fa-arrow-circle-left"></i></button>
                    </div>
                     <div class="col-sm-8">
                    </div>
                     <div class="col-sm-2">
                      <button type="button" id="boton-siguiente" class="btn btn-dark btn-block btn-lg"><i class="fas fa-arrow-circle-right"></i></button>
                    </div>

                </div>


            </div>
            <div id="alta-3" style="display:none;">

                <div class="contenido" style="overflow-y:scroll;overflow-x: hidden;max-height:430px;">
                <h2 id="tAlta32">SOLICITUD DE PARTICIPACIÓN EN EL PROGRAMA DE CULTIVO COLECTIVO
                DE LA ASOCIACIÓN CANNÁBICA
                </h2>

                <div class="form-group">
                    <div class="row">
                      <div class="col-sm-6"> <label for="tel-socio" id="tAlta33">¿Con qué cantidad deseas participar en el programa de cultivo?</br><small class="text-muted" id="tAlta34">Esta cantidad debe ser una aproximación y no debe superar los 60 gramos.</small>
                      </label>
                    </div>
                      <div class="col-sm-6">
                        <input type="number" class="form-control form-control-lg" id="cant-socio" min="0" max="60" placeholder="Gramos" style="width:100px;" maxlength="3">
                      </div>
                    </div>


                  </div>

                <p id="tAlta35">Por la presente manifiesto ser mayor de 21 años y mi condición de consumidor habitual de Cannabis Sativa L. y solicito participar en el programa de cultivo colectivo con los miembros de la asociación asumiendo mi corresponsabilidad en el cultivo junto al resto de personas participantes en el mismo.</p>
                <p class="font-weight-bold" id="tAlta36">
                El producto de mi participación en el cultivo asociativo en ÚNICA Y EXCLUSIVAMENTE PARA MI CONSUMO PERSONAL EN EL ÁMBITO PRIVADO DEL LOCAL SOCIAL, asumiendo cualquier responsabilidad de mis actos contrarios a la Ley que se pudieran derivar y eximiendo de ello a la Asociación, Asociados/as y Junta Directiva.</p>
                <ul>
                  <li id="tAlta37">Para este efecto adjunta el patrocinio/aval de UN/A SOCIO/A de la Asociación a la presente solicitud que manifiesta ser conocedora del consumo de cannabis por parte de la persona que solicita su incorporación a la Asociación o documentación que acredite que se trata de una persona consumidora.</li>
                  <li id="tAlta38">Solicito participar en la cantidad de <span class="font-weight-bold" id="cant-elegida">(SIN SELECCIONAR)</span> gr. de cannabis al mes, cantidad que será revisada o confirmada por el/la socio/a a cada trimestre. En caso de no confirmar o revisar se prorrogará la cantidad ya declarada.</li>
                  <li id="tAlta39">El/la socio/a autoriza a los/las socios/as activos/as colaboradores de la Asociación a cultivar, recoger y repartir entre los/las socios/as el Cannabis proveniente del cultivo colectivo asociativo para el consumo.</li>
                  <li id="tAlta40">El/la socio/a se obliga a comunicar de inmediato, en el momento que decida abandonar su participación en la Asociación, la solicitud de baja de la Asociación. Anualmente deberá ratificarse la condición de socio/a.</li>
                  <li id="tAlta41">El/la socio/a comunicará a la Asociación la notificación de sanción en base al artículo 25.1 de la Ley Orgánica 1/1992, sobre Protección Ciudadana, o la imputación del delito tipificado en el artículo 3668 del Código Penal, pudiendo implicar en el primer caso e implicando en el segundo la expulsión del socio/a.
                  </li>
                  <li id="tAlta42">El/la socio/a se compromete a no ceder su carnet de socio/a, el cual es intransferible; la transferencia del carnet de socio/a será motivo de expulsión de la asociación. La persona socia siempre exhibirá su documento de identidad y el carnet de socio/a para retirar su material.
                  </li>
                  <li id="tAlta43">
                    Cualquier incumplimiento de los compromisos adquiridos mediante la firma del presente podrá implicar o implicará la expulsión de la Asociación mediante el correspondiente procedimiento fijado en los estatutos.
                  </li>
                </ul>
                <label class="checks"><span id="tAlta44">El/la solicitante manifiesta haber leído los estatutos y la presente solicitud de participación en el programa de cultivo colectivo.</span>
                 <input type="checkbox" checked="checked" id="check-estatutos">
                 <span class="checkmark"></span>
               </label>
                <div class="row">
                   <div class="col-sm-2">
                    <button type="button" id="boton-atras" class="btn btn-dark btn-block btn-lg"><i class="fas fa-arrow-circle-left"></i></button>
                  </div>
                   <div class="col-sm-8">
                  </div>
                   <div class="col-sm-2">
                    <button type="button" id="boton-siguiente" class="btn btn-dark btn-block btn-lg"><i class="fas fa-arrow-circle-right"></i></button>
                  </div>
                </div>
              </div>



                </div>

                <div id="alta-4" style="display:none;">
                  <div class="contenido" style="overflow-y: scroll; overflow-x: hidden; max-height:430px">
                    <h2 id="tAlta45">Socio Avalador</h2>
                    <div id="zona-busqueda">
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="tel-socio" id="tAlta46">DNI del Avalador</label>
                          <input type="text" class="form-control form-control-lg" id="dni-a" placeholder="Introduce el DNI del socio avalador">
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="tel-socio" id="tAlta47">Nº de Socio</label>
                          <input type="number" class="form-control form-control-lg" id="n-a" placeholder="Introduce el número de socio del avalador">
                        </div>
                      </div>
                    </div>
                    <button type="button" class="btn btn-lg btn-info btn-block" id="consultar-a">Consultar</button>
                  </div>
                    <div class="text-center" id="carga-a" style="display:none;">
                      <img src="img/cargando.svg">
                    </div>
                    <div id="datos-avalador" class="container" style="display:none">

                    </div>

                    <div id="error-datos" class="text-center mt-5" style="display:none;">
                        <h3 id="tAlta48">Los datos no coinciden con ningún socio</h3>
                    </div>

                    <div class="text-center mt-5" style="display:none;" id="zona-firma">
                      <hr />
                      <h2 id="tAlta49">Tu firma</h2>
                    <div class="wrapper">
                      <canvas id="signature-pad" class="signature-pad" width=800 height=400></canvas>
                    </div>
                    <button class="btn btn-danger" id="clear">Borrar</button>

                    </div>
                    </div>

                    <div class="row mt-4">
                       <div class="col-sm-2">
                        <button type="button" id="boton-atras" class="btn btn-dark btn-block btn-lg"><i class="fas fa-arrow-circle-left"></i></button>
                      </div>
                       <div class="col-sm-4">
                      </div>
                       <div class="col-sm-6">
                        <button type="button" id="boton-finalizar" style="display:none;" class="btn btn-dark btn-block btn-lg">FINALIZAR</button>
                      </div>
                    </div>




                    </div>




            </form>
          </div>



        </div>

      </div>


     <div style="display:none" id="alta-5" class="animated jackInTheBox">



       <h1 class="text-white text-center mt-5" id="tAlta50">¡Enhorabuena!</h1>
       <h4 class="text-white text-center mt-5" id="tAlta51">Has sido registrado correctamente. Para terminar el proceso, consulta con </br>el personal
       de la asociación y se te proporcionará un número de socio para </br>que puedas empezar a disfrutar de nuestros servicios.</h4>
      <div class="text-center"><a href="index.php" class="btn btn-dark btn-lg mt-5" id="tAlta52">Inicio</a></div>
     </div>

</div>



<a tabIndex="-1" href="index.php"><div class="botonSalir animated fadeInUp">
  <i class="fas fa-home"></i></br><span class="inicio">Inicio</span>
</div></a>
   <?php
    include "php/footer.php";
   ?>



 <script src="js/signature_pad.umd.js"></script>
  <script src="js/altas.js"></script>
  <script src="js/traduccion.js"></script>
  </body>
</html>
