<?php

    require_once("php/cabecera.php");
    require_once("php/sesion.php");
    
    mostrarCabecera("index");
    comprobarRevision();

?>
  <body>

    <?php


    ?>
    <?php cargarListaArticulosCarrito();?>
    <div class="container">
      <?php   cargarHeader();  ?>


    <div class="row margen-a">
      <div class="col-sm-4 touchmenu uno animated zoomIn">
       <div id="btn-cannabis"><div class="btn-inicio bg-verde text-center"><div class="efectoZoom"><img src="img/maria.png"></br><span id="tHome1">CANNABIS</span></div></div></div>
      </div>

      <div class="col-sm-4 touchmenu dos animated zoomIn">
         <div id="btn-articulos"><div class="btn-inicio bg-naranja text-center"><div class="efectoZoom"><img src="img/articulos.png"></br><span id="tHome2">TIENDA</span></div></div></div>
      </div>
      <div class="col-sm-4 touchmenu tres animated zoomIn">
        <div id="btn-finalizar"><div class="btn-inicio bg-marron text-center"><div class="efectoZoom"><img src="img/coins.png"></br><span id="tHome3">PAGAR</span></div></div></div>

      </div>

  </div>
  <div class="recomendaciones">
  <h1 class="titulo-peque" id="tHome5">Recomendaciones y Novedades</h1>
  <ul class="list-group novedades animated fadeInUp">
    <?php
      cargarUltimasGeneticas();
    ?>

</ul>
</div>
</div>
</div>
  <div class="botonSalir animated fadeInUp">
  <i class="fas fa-power-off"></i></br><span class="text-salir">SALIR</span>
  </div>



   <?php
    include "php/footer.php";
   ?>

  <script src="js/sesion.js"></script>
  <script src="js/traduccion.js"></script>
  <script>

    $(document).ready(function(){
      $("#btn-cannabis").on("click", function(){
        $(this).addClass("animated zoomOut");
        $(".touchmenu.dos").fadeOut();
        $(".touchmenu.tres").fadeOut();
        esconderInterfaz();
        setTimeout(function(){
          window.location = "cannabis.php";
        },500);


      });
      $("#btn-articulos").on("click", function(){
        $(this).addClass("animated zoomOut");
        $(".touchmenu.uno").fadeOut();
        $(".touchmenu.tres").fadeOut();
        esconderInterfaz();
        setTimeout(function(){
          window.location = "articulos.php";
        },500);


      });

    });

    function esconderInterfaz(){
      $("#row-header").removeClass("animated fadeInDown").addClass("animated fadeOutUp");
      $(".novedades").removeClass("animated fadeInUp").fadeOut();
      $("h1.titulo-peque").fadeOut();
    }1

    $(".novedades>li").on("click", function(){
       var id = $(this).attr("data-id");
       location.href ="cannabis.php?"+id;
     
     

    });


  </script>
  </body>
</html>
