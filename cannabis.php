<?php
    require_once("php/cabecera.php");
    require_once("php/sesion.php");
    mostrarCabecera("index");
?>
  <body>
    <?php cargarListaArticulosCarrito();?>
    <div class="modal fade" id="modal-genetica" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" style="max-width:90%" role="document">
        <div class="modal-content">
          <div class="modal-header bg-dark text-white">
            <h5 class="modal-title" id="exampleModalLabel">Datos de la genética</h5>
            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
              <i class="fas fa-times"></i>
            </button>
          </div>
          <div class="modal-body" id="modal-datosGenetica">

          </div>
     
        </div>
      </div>
    </div>
    <div class="container mb-5">
       <?php   cargarHeader();  ?>

      <div class="row zona-seleccion">
        <div class="col-sm-6 touchmenu can ">  <div class="btn-sel bg-marihuana text-center"><div class="efectoZoom"><span>MARIHUANA</span></div></div></div>
        <div class="col-sm-6 touchmenu hac ">  <div class="btn-sel bg-hachis text-center"><div class="efectoZoom"><span>HACHIS</span></div></div></div>
      </div>
      <div class="zona-marihuana">
      
      <div class="row">
        <div class="col-sm-6">
          <h1 class="tituloCategoria" id="t-marihuana">MARIHUANA</h1>
        </div>
        <div class="col-sm-6">
          <h1 class="tituloCategoria hac2 menu-vacio" id="t-hachis">HACHIS</h1>
        </div>
      <div class="col-sm-3 mt-4">
      <ul class="nav nav-pills flex-column nav-justified" id="pills-tab" role="tablist">
        <li class="nav-item">
          <a class="nav-link active" id="btn-zona-hibridas" data-toggle="pill" href="#zona-hibridas" role="tab" aria-controls="pills-hibridas" aria-selected="true">HIBRIDAS</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="btn-zona-indicas" data-toggle="pill" href="#zona-indicas" role="tab" aria-controls="pills-indicas" aria-selected="false">INDICAS</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="btn-zona-sativas" data-toggle="pill" href="#zona-sativas" role="tab" aria-controls="pills-sativas" aria-selected="false">SATIVAS</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="btn-zona-extraccion" data-toggle="pill" href="#zona-extraccion" role="tab" aria-controls="pills-extraccion" aria-selected="false">EXTRACCIÓN</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="btn-zona-otros" data-toggle="pill" href="#zona-otros" role="tab" aria-controls="pills-otros" aria-selected="false">OTROS</a>
        </li>
      </ul>
    </div>
    <div class="col-sm-9 mt-4">
        <div class="tab-content" id="pills-tabContent">
          <div class="tab-panel fade show active" id="zona-hibridas" role="tabpanel" aria-labelledby="pills-hibridas-tab">

          </div>
          <div class="tab-panel fade" id="zona-indicas" role="tabpanel" aria-labelledby="pills-indicas-tab">

          </div>
          <div class="tab-panel fade" id="zona-sativas" role="tabpanel" aria-labelledby="pills-sativas-tab">

          </div>
           <div class="tab-panel fade" id="zona-extraccion" role="tabpanel" aria-labelledby="pills-extraccion-tab">

          </div>
           <div class="tab-panel fade" id="zona-otros" role="tabpanel" aria-labelledby="pills-otros-tab">

          </div>
           
        </div>
      </div>
    </div>
    </div>
    <div class="zona-hachis">
            <div class="row">
              <div class="col-sm-6">
                <h1 class="tituloCategoria can2 menu-vacio" id="t-marihuana">MARIHUANA</h1>
              </div>
              <div class="col-sm-6">
                <h1 class="tituloCategoria" id="t-hachis">HACHIS</h1>
              </div>
            </div>

      <ul class="nav nav-pills nav-justified" id="pills-tab" role="tablist">
        <li class="nav-item">
          <a class="nav-link active" id="btn-zona-indicas-marron" data-toggle="pill" href="#zona-indicas-marron" role="tab" aria-controls="zona-indicas-marron" aria-selected="true">INDICAS</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="btn-zona-sativas-marron" data-toggle="pill" href="#zona-sativas-marron" role="tab" aria-controls="pills-sativas-marron" aria-selected="false">SATIVAS</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="btn-zona-hibridas-marron" data-toggle="pill" href="#zona-hibridas-marron" role="tab" aria-controls="pills-hibridas-marron" aria-selected="false">HIBRIDAS</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="btn-zona-extraccion-marron" data-toggle="pill" href="#zona-extraccion-marron" role="tab" aria-controls="pills-extraccion-marron" aria-selected="false">EXTRACCIÓN</a>
        </li>
      </ul>

      <div class="tab-content mt-4" id="pills-tabContent">
        <div class="tab-panel fade show active" id="zona-indicas-marron" role="tabpanel" aria-labelledby="pills-indicas-marron">

        </div>
        <div class="tab-panel fade" id="zona-sativas-marron" role="tabpanel" aria-labelledby="pills-sativas-marron">

        </div>
        <div class="tab-panel fade" id="zona-hibridas-marron" role="tabpanel" aria-labelledby="pills-hibridas-tab">

        </div>
         <div class="tab-panel fade" id="zona-extraccion-marron" role="tabpanel" aria-labelledby="pills-extraccion-marron">

        </div>
      </div>
    </div>



</div>
</div>
<div class="botonPagar animated fadeInUp text-center">
 <i class="fas fa-comment-dollar"></i></br><span class="text-pagar">PAGAR</span>
</div>
<div class="botonVolver animated fadeInUp text-center">
  <i class="fas fa-undo"></i></br><span class="text-atras">ATRÁS</span>
</div>

   <?php
    include "php/footer.php";
   ?>

   <script src="js/geneticas.js"></script>
    <script src="js/sesion.js"></script>

  </body>
</html>
